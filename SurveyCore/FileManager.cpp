#include "stdafx.h"

#include <ios>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include "SurveyCore.h"
#include "Cryptography.h"
#include "FileManager.h"
#include "Document.h"


namespace survey {

	const char* ProjectInfo::s_ProjectsExtension = ".surv";

	std::unique_ptr<FileManager>  survey::FileManager::s_FileManager;


	bool FileManager::SaveDocument(const DocumentInfo& saveDocStruct)
	{
		try
		{
			auto itrtProject = m_Projects.find(saveDocStruct.m_DocumentProprties.m_Guid);
			if (itrtProject != m_Projects.end())
				return itrtProject->second->SetUpdate(saveDocStruct);

			m_Projects.emplace(saveDocStruct.m_DocumentProprties.m_Guid, std::unique_ptr<ProjectInfo>(new ProjectInfo(this, saveDocStruct)));
			return true;
		}
		catch (const boost::filesystem::filesystem_error& ex)
		{
			// to do log error
		}
		catch (const std::exception& ex)
		{
			// to do log error
		}
		catch (...)
		{
			// to do log error
		}

		return false;
	}

	// ===================================  BackupInfo  ===============================================================================================

	BackupInfo::BackupInfo(ProjectInfo* pProjectInfo)
		: m_pProjectInfo(pProjectInfo)
	{
		assert(m_pProjectInfo);
	}


	bool BackupInfo::createBackupFile()
	{
		const boost::filesystem::path& backupDir = GetBackupDirectory();
		if (!boost::filesystem::exists(backupDir))
		if (!boost::filesystem::create_directory(backupDir))
			return false;

		boost::filesystem::path backupFile = backupDir / (m_pProjectInfo->GetID() + std::string(".txt"));

		if (!boost::filesystem::exists(backupFile))
		{
			m_BackupStream.open(backupFile, boost::filesystem::fstream::out | boost::filesystem::fstream::binary);
			m_BackupStream.close();
		}

		m_BackupStream.open(backupFile, boost::filesystem::fstream::in | boost::filesystem::fstream::out | boost::filesystem::fstream::binary | boost::filesystem::fstream::ate);
		return m_BackupStream.is_open();
	}


	bool BackupInfo::setProjectData(const BackupProjectData& backupProjectData)
	{
		if (!createBackupFile())
			return false;

		// write to backup
		return write(backupProjectData);
	}


	bool BackupInfo::write(const BackupProjectData& docInfo)
	{
		// write files to backup dir
		const boost::filesystem::path& backupDir = GetBackupDirectory();

		// define hash
		const std::string& backupID = Cryptography::UUID_Generate();

		// define text backup
		std::unique_ptr<TiXmlElement> tiBackupPtr(new TiXmlElement("backup"));

		// hash
		TiXmlElement* pHashElement = new TiXmlElement("hash");
		TiXmlText* pTextElem = new TiXmlText(backupID);
		pHashElement->LinkEndChild(pTextElem);
		tiBackupPtr->LinkEndChild(pHashElement);

		// write xml document
		TiXmlNode* pDocElem = const_cast<TiXmlDocument&>(docInfo.m_projectDoc).FirstChild("document");
		assert(pDocElem);

		tiBackupPtr->LinkEndChild(pDocElem->Clone());

		// write sections
		for (const auto& sectionInfo : docInfo.m_Sections)
		{
			const boost::filesystem::path& sectionPath = backupDir / (sectionInfo.second.m_Hash + ".xml");
			if (boost::filesystem::is_regular_file(sectionPath))
				continue;

			if (!TiXmlHelper::SaveTiXml(sectionPath, sectionInfo.second.m_Xml))
			{
				// to do log error
				return false;
			}
		}

		// write previews
		for (const auto& previewPair : docInfo.m_Previews)
		{
			const boost::filesystem::path& previewPath = backupDir / (previewPair.second.m_Hash + ".png");
			if (boost::filesystem::is_regular_file(previewPath))
				continue;

			FileStream flStream(previewPath);
			if (!flStream.SetFileData(previewPair.second.m_Data))
			{
				// to do log error
				return false;
			}
		}

		// write backup to stream
		TiXmlPrinter printer;
		tiBackupPtr->Accept(&printer);
		const std::string& textBackup = printer.CStr();

		m_BackupStream.seekg(0, m_BackupStream.end);
		m_BackupStream.write(textBackup.c_str(), textBackup.length());

		// save
		m_BackupStream.close();
		boost::filesystem::path backupFile = backupDir / (m_pProjectInfo->GetID() + std::string(".txt"));
		m_BackupStream.open(backupFile, boost::filesystem::fstream::in | boost::filesystem::fstream::out | boost::filesystem::fstream::binary | boost::filesystem::fstream::ate);
		return m_BackupStream.is_open();
	}


	boost::filesystem::path BackupInfo::GetBackupDirectory() const
	{
		return m_pProjectInfo->GetProjectDirectory() / s_BackupDirectory;
	}


	bool BackupInfo::Update(const BackupProjectData& backupProjectData)
	{
		return write(backupProjectData);
	}


	bool BackupInfo::GetNextBackup(BackupProjectData& backupProjectData)
	{
		return getNext(backupProjectData);
	}


	bool BackupInfo::GetPreviosBackup(BackupProjectData& backupProjectData)
	{
		return getPrevios(backupProjectData);
	}


	bool BackupInfo::GetAllBackups(std::list<std::pair<std::string, BackupProjectData>>& docInfos)
	{
		const size_t currentPos = m_BackupStream.tellg();

		BackupProjectData backup;
		while (getNext(backup))
		{
			TiXmlElement* pXmlElement = backup.m_projectDoc.FirstChildElement("hash");
			const char* pText = NULL;

			if (!pXmlElement || !(pText = pXmlElement->GetText()))
			{
				// to do log error
				m_BackupStream.seekg(currentPos);
				return false;
			}

			docInfos.push_back(std::pair<std::string, BackupProjectData>(pText, backup));
			backup.Clear();
		}

		m_BackupStream.seekg(currentPos);
		return true;
	}


	bool BackupInfo::GetBackup(const std::string& backupHash, BackupProjectData& docBackup)
	{
		const size_t currentPos = m_BackupStream.tellg();

		TiXmlElement description("");
		while (getNextBackupDescription(description))
		{
			TiXmlElement* pXmlElement = description.FirstChildElement("hash");
			const char* pText = NULL;

			if (!pXmlElement || !(pText = pXmlElement->GetText()))
			{
				// to do log error
				continue;
			}

			if (pText == backupHash)
			{
				m_BackupStream.seekg(currentPos);
				return buildBackupProjectData(&description, docBackup);
			}

		}

		m_BackupStream.seekg(currentPos);
		return false;
	}


	bool BackupInfo::BackupProjectDataToDocumentInfo(const BackupProjectData& backupProjectData, DocumentInfo& documentInfo)
	{
		documentInfo.m_Sections = std::move(backupProjectData.m_Sections);
		HashesPreviewMapToPreviewMap(backupProjectData.m_Previews, documentInfo.m_Previews);

		// parse doc xml
		TiXmlDocument& projectDoc = const_cast<TiXmlDocument&>(backupProjectData.m_projectDoc);

		// guid
		TiXmlElement* pDocElement = projectDoc.FirstChildElement("document");
		if (!pDocElement)
		{
			// to do log error
			return false;
		}

		const char* pGuid = pDocElement->Attribute("guid");
		if (!pGuid)
		{
			// to do log error
			return false;
		}

		documentInfo.m_DocumentProprties.m_Guid = pGuid;

		// name
		TiXmlElement* pElement = pDocElement->FirstChildElement("name");
		if (!pElement)
		{
			// to do log error
			return false;
		}

		const char* pName = pElement->GetText();
		if (!pName)
		{
			// to do log error
			return false;
		}

		documentInfo.m_DocumentProprties.m_Name = pName;

		// created
		pElement = pDocElement->FirstChildElement("created");
		if (!pElement)
		{
			// to do log error
			return false;
		}

		const char* pCreated = pElement->GetText();
		if (!pCreated)
		{
			// to do log error
			return false;
		}

		documentInfo.m_DocumentProprties.m_CreatedDate = pCreated;

		// modify
		pElement = pDocElement->FirstChildElement("modify");
		if (!pElement)
		{
			// to do log error
			return false;
		}

		const char* pModify = pElement->GetText();
		if (!pModify)
		{
			// to do log error
			return false;
		}

		documentInfo.m_DocumentProprties.m_ModifyDate = pModify;
		return true;
	}


	class LexicalSearchFSM
	{
	public:

		LexicalSearchFSM(const std::string& lex)
			: m_Containt(lex)
			, m_CurrentPos(0)
		{
			assert(lex.length());
		}

		bool SetStep(const char ch)
		{
			bool wasEquils = false;
			return SetStep(ch, wasEquils);
		}

		bool SetStep(const char ch, bool& wasEquils)
		{
			if (m_Containt[m_CurrentPos] != ch)
			{
				m_CurrentPos = 0;
				wasEquils = false;
				return false;
			}

			wasEquils = true;

			if (++m_CurrentPos < m_Containt.length())
				return false;

			m_CurrentPos = 0;
			return true;
		}

	private:

		size_t m_CurrentPos;
		std::string m_Containt;
	};


	bool findPositionOfReverseStream(const std::string& searchLex, boost::filesystem::fstream& stream)
	{
		LexicalSearchFSM lexBackupSearch(searchLex);

		size_t currentPos = stream.tellg();
		char c = 0;

		while (currentPos--)
		{
			stream.seekg(currentPos);
			stream.read(&c, sizeof(c));

			if (lexBackupSearch.SetStep(c))
			{
				stream.seekg(currentPos);
				return true;
			}
		}

		return false;
	}


	bool findPositionOfStream(const std::string& searchLex, boost::filesystem::fstream& stream)
	{
		LexicalSearchFSM lexBackupSearch(searchLex);

		char c = 0;
		while (stream.read(&c, sizeof(c)))
		{
			if (lexBackupSearch.SetStep(c))
				return true;
		}

		return false;
	}


	bool BackupInfo::getPrevios(BackupProjectData& docInfo)
	{
		if (!moveToPrevios())
			return false;

		if (!moveToPrevios())
			return false;

		return getNext(docInfo);
	}


	bool BackupInfo::buildBackupProjectData(const TiXmlElement* pDescription, BackupProjectData& docInfo)
	{
		assert(pDescription);

		// backup
		TiXmlElement* pBackupElement = const_cast<TiXmlElement*>(pDescription);

		// document
		TiXmlElement* pDocumentElement = pBackupElement->FirstChildElement("document");
		if (!pDocumentElement)
		{
			// to do log error
			return false;
		}

		docInfo.m_projectDoc.LinkEndChild(new TiXmlDeclaration("1.0", "utf-8", ""));
		docInfo.m_projectDoc.LinkEndChild(pDocumentElement->Clone());

		const boost::filesystem::path& backupDir = GetBackupDirectory();

		// sections
		for (const std::string& sectionName : Document::GetNameSections())
		{
			// section element
			TiXmlElement* pSectionElement = pDocumentElement->FirstChildElement(sectionName);
			if (!pSectionElement)
			{
				// to do log error
				return false;
			}

			const char* hashStr = pSectionElement->GetText();
			if (!hashStr)
			{
				// to do log error
				return false;
			}

			const boost::filesystem::path& sectionPath = backupDir / (std::string(hashStr) + ".xml");

			if (!TiXmlHelper::LoadTiXml(sectionPath, docInfo.m_Sections[sectionName].m_Xml))
			{
				// to do log error
				return false;
			}
		}

		// previews
		TiXmlElement* pPreviewsSectionElement = pDocumentElement->FirstChildElement(m_pProjectInfo->s_NamePreviewsSection);
		if (!pPreviewsSectionElement)
		{
			// to do log error
			return false;
		}

		TiXmlNode* pTiPreviewNode = NULL;
		while (pTiPreviewNode = pPreviewsSectionElement->IterateChildren(pTiPreviewNode))
		{
			TiXmlElement* pTiPreviewElement = pTiPreviewNode->ToElement();
			if (!pTiPreviewElement)
			{
				// to do log error
				return false;
			}

			const ID previewID = boost::lexical_cast<int>(pTiPreviewElement->ValueStr());

			std::string hashAttribute;
			if (pTiPreviewElement->QueryStringAttribute("hash", &hashAttribute) != TIXML_SUCCESS)
			{
				// to do log error
				return false;
			}

			// read file
			docInfo.m_Previews[previewID].m_Hash = hashAttribute;
			auto& fileData = docInfo.m_Previews[previewID].m_Data;

			const boost::filesystem::path& previewPath = backupDir / (hashAttribute + ".png");
			FileStream flStream(previewPath);

			if (!flStream.GetFileData(fileData))
			{
				// to do log error
				return false;
			}
		}

		return true;
	}


	bool BackupInfo::getNext(BackupProjectData& docInfo)
	{
		TiXmlElement description("");
		if (!getNextBackupDescription(description))
			return false;

		return buildBackupProjectData(&description, docInfo);
	}


	bool BackupInfo::moveToNext()
	{
		if (!findPositionOfStream(s_EndOfBackup, m_BackupStream))
			return false;

		return true;
	}


	bool BackupInfo::moveToPrevios()
	{
		static const std::string reverseBeginOfBackup(s_BeginOfBackup.rbegin(), s_BeginOfBackup.rend());
		return findPositionOfReverseStream(reverseBeginOfBackup, m_BackupStream);
	}


	bool BackupInfo::getNextBackupDescription(TiXmlElement& description)
	{
		const size_t begPos = m_BackupStream.tellg();
		if (!moveToNext())
			return false;

		const size_t endPos = m_BackupStream.tellg();
		const size_t sizeArr = endPos - begPos;
		if (!sizeArr)
			return false;

		// write buffer
		std::vector<char> buff(sizeArr, 0);

		m_BackupStream.seekg(begPos);
		m_BackupStream.read((char*)buff.data(), sizeArr);

		// xml create
		description.Parse((char*)buff.data(), NULL, TIXML_ENCODING_UTF8);
		return true;
	}


	bool BackupInfo::GetAllBackupDescriptions(std::list<BackupProjectProperties>& descriptions)
	{
		const size_t currentPos = m_BackupStream.tellg();

		m_BackupStream.seekg(0);

		TiXmlElement elem("");
		while (getNextBackupDescription(elem))
		{
			// hash write
			TiXmlElement* pElem = elem.FirstChildElement("hash");
			if (!pElem)
			{
				// to do log error
				return false;
			}

			BackupProjectProperties props;
			props.m_Hash = pElem->GetText();

			// date write
			if (!(pElem = elem.FirstChildElement("document")))
			{
				// to do log error
				return false;
			}

			if (!(pElem = pElem->FirstChildElement("modify")))
			{
				// to do log error
				return false;
			}

			props.m_CreatedTime = TiXmlHelper::GetTime(pElem);
			descriptions.push_back(props);
		}

		m_BackupStream.seekg(currentPos);
		return true;
	}


	bool BackupInfo::getCurrent(BackupProjectData& docInfo)
	{
		if (!moveToPrevios())
			return false;

		return getNext(docInfo);
	}


	// ===================================  ProjectInfo  ===============================================================================================


	ProjectInfo::ProjectInfo(FileManager* pFileManager, const DocumentInfo& blocksInfo)
		: m_Project(NULL)
		, m_pFileManager(pFileManager)
		, m_ProjectBackup(this)
	{
		// file project
		const boost::filesystem::path projectFileName = blocksInfo.m_DocumentProprties.m_Guid + ProjectInfo::s_ProjectsExtension;
		const boost::filesystem::path projectFile = m_pFileManager->GetWorkDirectory() / projectFileName;

		m_ProjectDoc.Parse(ProjectInfo::s_DefProjectDoc);
		m_Project = m_ProjectDoc.FirstChildElement("document");
		assert(m_Project);

		// guid
		m_Project->SetAttribute("guid", blocksInfo.m_DocumentProprties.m_Guid);

		// name
		TiXmlNode* pTiNode = m_Project->IterateChildren("name", NULL);
		assert(pTiNode);
		pTiNode->LinkEndChild(new TiXmlText(blocksInfo.m_DocumentProprties.m_Name));

		// created
		pTiNode = m_Project->IterateChildren("created", NULL);
		assert(pTiNode);
		pTiNode->LinkEndChild(new TiXmlText(blocksInfo.m_DocumentProprties.m_CreatedDate));

		// modify
		pTiNode = m_Project->IterateChildren("modify", NULL);
		assert(pTiNode);
		pTiNode->LinkEndChild(new TiXmlText(blocksInfo.m_DocumentProprties.m_ModifyDate));

		// directory project
		const boost::filesystem::path projectDirectory = m_pFileManager->GetWorkDirectory() / boost::filesystem::path(blocksInfo.m_DocumentProprties.m_Guid);
		boost::filesystem::create_directories(projectDirectory);

		// sections
		for (auto itrtSection : blocksInfo.m_Sections)
		{
			pTiNode = m_Project->IterateChildren(itrtSection.first, NULL);
			assert(pTiNode);

			TiXmlText* pText = new TiXmlText(itrtSection.second.m_Hash);
			pText->SetCDATA(true);
			pTiNode->LinkEndChild(pText);

			// save sections to files
			const boost::filesystem::path sectionPath = projectDirectory / boost::filesystem::path(itrtSection.first + ProjectInfo::s_ProjectsExtension);
			TiXmlHelper::SaveTiXml(sectionPath, itrtSection.second.m_Xml);
		}

		// save previews
		const std::unordered_map<ID, std::string>& previewHashes = savePreviews(blocksInfo.m_Previews);

		// save doc
		if (!TiXmlHelper::SaveTiXml(projectFile, m_ProjectDoc))
		{
			// to do log error
			throw std::exception("Xml error");
		}

		// save backup
		BackupProjectData backupPrjData;
		backupPrjData.m_projectDoc = m_ProjectDoc;
		backupPrjData.m_Sections = blocksInfo.m_Sections;
		PreviewMapToHashesPreviewMap(blocksInfo.m_Previews, previewHashes, backupPrjData.m_Previews);

		if (!m_ProjectBackup.setProjectData(backupPrjData))
		{
			// to do log error
			throw std::exception("Fail of create of backup file");
		}
	}


	bool ProjectInfo::updateProperties(const DocumentInfo& updateInfo)
	{
		bool isUpdate = false;

		TiXmlElement* pElem = NULL;
		TiXmlNode* pTiNode = NULL;

		// name
		pTiNode = m_Project->IterateChildren("name", NULL);
		if (!pTiNode || !(pElem = pTiNode->ToElement()) || !pElem->FirstChild())
		{
			// to do log error
			throw std::exception("Invalid XML!");
		}

		TiXmlText* pText = pElem->FirstChild()->ToText();
		if (!pText)
		{
			// to do log error
			throw std::exception("Invalid XML!");
		}

		if (!pText->Value() || updateInfo.m_DocumentProprties.m_Name != pText->Value())
		{
			pText->SetValue(updateInfo.m_DocumentProprties.m_Name);
			isUpdate = true;
		}

		// set time modify
		if (!(pTiNode = m_Project->IterateChildren("modify", NULL)) || !pTiNode->FirstChild())
		{
			// to do log error
			throw std::exception("Invalid XML!");
		}

		pText = pTiNode->FirstChild()->ToText();
		if (!pText)
		{
			// to do log error
			throw std::exception("Invalid XML!");
		}

		if (!pText->Value() || updateInfo.m_DocumentProprties.m_ModifyDate != pText->Value())
		{
			pText->SetValue(updateInfo.m_DocumentProprties.m_ModifyDate);
			isUpdate = true;
		}

		return isUpdate;
	}


	bool ProjectInfo::SetUpdate(const DocumentInfo& updateInfo)
	{
		try
		{
			assert(m_Project);

			// doc properties update
			bool propsIsUpdate = updateProperties(updateInfo);

			// sinc sections
			TiXmlElement* pElem = NULL;
			TiXmlNode* pTiNode = NULL;
			std::unordered_set<std::string> sincSections;

			for (auto itrtSection : updateInfo.m_Sections)
			{
				if (!(pTiNode = m_Project->IterateChildren(itrtSection.first, NULL)) || !(pElem = pTiNode->ToElement()))
				{
					// to do log error
					throw std::exception("Invalid XML!");
				}

				if (itrtSection.second.m_Hash != pElem->GetText())
					sincSections.emplace(itrtSection.first);
			}

			if (!sincSections.size() && !propsIsUpdate)
				return true;

			// update sections
			const boost::filesystem::path& projectDirectory = GetProjectDirectory();
			boost::filesystem::create_directories(projectDirectory);

			for (const std::string& sectionUpdate : sincSections)
			{
				const DocumentSectionInfo& docInfo = updateInfo.m_Sections.at(sectionUpdate);

				// set new hash
				if (!(pTiNode = m_Project->IterateChildren(sectionUpdate, NULL)) || !pTiNode->FirstChild())
				{
					// to do log error
					throw std::exception("Invalid XML!");
				}

				TiXmlText* pText = pTiNode->FirstChild()->ToText();
				if (!pText)
				{
					// to do log error
					throw std::exception("Invalid XML!");
				}

				pText->SetCDATA(true);
				pText->SetValue(docInfo.m_Hash);

				// save sections to files
				const boost::filesystem::path sectionPath = projectDirectory / boost::filesystem::path(sectionUpdate + ProjectInfo::s_ProjectsExtension);
				TiXmlHelper::SaveTiXml(sectionPath, docInfo.m_Xml);
			}

			// previews
			const std::unordered_map<ID, std::string>& previewsHashs = savePreviews(updateInfo.m_Previews);

			// rewrite project file
			const boost::filesystem::path& projectFilePath = GetProjectPath();

			if (!TiXmlHelper::SaveTiXml(projectFilePath, m_ProjectDoc))
			{
				// to do log error
				throw std::exception("Xml error");
			}

			// backup 
			BackupProjectData backupProjectData;
			backupProjectData.m_projectDoc = m_ProjectDoc;
			backupProjectData.m_Sections = updateInfo.m_Sections;
			PreviewMapToHashesPreviewMap(updateInfo.m_Previews, previewsHashs, backupProjectData.m_Previews);

			m_ProjectBackup.Update(backupProjectData);
			return true;
		}
		catch (const boost::filesystem::filesystem_error& ex)
		{
			// to do log error
		}
		catch (const std::exception& ex)
		{
			// to do log error
		}
		catch (...)
		{
			// to do log error
		}

		return false;

	}


	bool ProjectInfo::GetDocInfo(DocumentInfo& updateInfo) const
	{
		try
		{
			// doc properties
			updateInfo.m_DocumentProprties.m_Guid = GetID();
			updateInfo.m_DocumentProprties.m_Name = GetName();
			updateInfo.m_DocumentProprties.m_CreatedDate = boost::lexical_cast<std::string>(GetCreatedDate());
			updateInfo.m_DocumentProprties.m_ModifyDate = boost::lexical_cast<std::string>(GetModifyDate());

			// xmls
			const boost::filesystem::path projectDirectory = GetProjectDirectory();

			for (const std::string& sectName : Document::GetNameSections())
			{
				const boost::filesystem::path sectionPath = projectDirectory / boost::filesystem::path(sectName + ProjectInfo::s_ProjectsExtension);
				if (!TiXmlHelper::LoadTiXml(sectionPath, updateInfo.m_Sections[sectName].m_Xml))
				{
					// to do log error
					throw std::exception("Load ti xml error");
				}
			}

			// previews
			const boost::filesystem::path& previewDirectory = GetPreviewDirectory();

			TiXmlNode* pTiNode = NULL;
			TiXmlElement* pTiElement = NULL;

			pTiNode = m_Project->FirstChild(s_NamePreviewsSection);
			if (!pTiNode || !(pTiElement = pTiNode->ToElement()))
			{
				// to do log error
				throw std::exception("Invalid XML!");
			}

			pTiNode = NULL;
			while (pTiNode = pTiElement->IterateChildren(pTiNode))
			{
				const std::string& namePreview = pTiNode->ValueStr();
				const boost::filesystem::path& previewPath = previewDirectory / boost::filesystem::path(namePreview + ".png");
				std::vector<unsigned char>& dataArr = updateInfo.m_Previews[boost::lexical_cast<int>(namePreview)];

				FileStream flStream(previewPath);
				if (!flStream.GetFileData(dataArr))
				{
					// to do log error
					throw std::exception("Invalid file!");
				}
			}

			return true;
		}
		catch (const boost::filesystem::filesystem_error& ex)
		{
			// to do log error
		}
		catch (const std::exception& ex)
		{
			// to do log error
		}
		catch (...)
		{
			// to do log error
		}

		return false;
	}


	std::unordered_map<ID, std::string> ProjectInfo::savePreviews(const PreviewMap& previews)
	{
		assert(m_Project);
		std::unordered_map<ID, std::string> hashes;

		// directory previews
		const boost::filesystem::path& previewDirectory = GetPreviewDirectory();

		if (boost::filesystem::exists(previewDirectory))
			boost::filesystem::remove_all(previewDirectory);

		boost::filesystem::create_directories(previewDirectory);

		// erase XML
		m_Project->RemoveChild(m_Project->FirstChild(s_NamePreviewsSection));
		TiXmlElement* pPreviewSection = new TiXmlElement(s_NamePreviewsSection);

		for (const auto& itrtPreview : previews)
		{
			const boost::filesystem::path previewPath = previewDirectory / boost::filesystem::path(std::to_string(itrtPreview.first) + ".png");

			// write file
			FileStream flStream(previewPath);
			if (!flStream.SetFileData(itrtPreview.second))
			{
				// to do log error
				throw std::exception("Invalid file!");
			}

			// save small preview
			const boost::filesystem::path smallPath = previewDirectory / boost::filesystem::path(std::to_string(itrtPreview.first) + "_small.png");
			if (!ImageHelper::ResizeImage(previewPath, smallPath, survey::ImageType::IT_PNG, s_SmallPreviewHeight, s_SmallPreviewWidth))
			{
				// to do log error
				throw std::exception("Section files error");
			}

			// create hash by image
			const std::string& previewHash = Cryptography::Bin2hex(Cryptography::CalculateMd5(itrtPreview.second));
			hashes.emplace(itrtPreview.first, previewHash);

			// write XML
			TiXmlElement* pPreview = new TiXmlElement(std::to_string(itrtPreview.first));
			pPreview->SetAttribute("hash", previewHash);

			pPreviewSection->LinkEndChild(pPreview);
		}

		m_Project->LinkEndChild(pPreviewSection);
		return hashes;
	}


	boost::filesystem::path  ProjectInfo::GetProjectDirectory() const
	{
		return m_pFileManager->GetWorkDirectory() / boost::filesystem::path(GetID());
	}


	boost::filesystem::path ProjectInfo::GetPreviewDirectory() const
	{
		return  GetProjectDirectory() / boost::filesystem::path(s_NamePreviewsSection);
	}


	boost::filesystem::path ProjectInfo::GetProjectPath() const
	{
		const boost::filesystem::path projectFileName = std::string(GetID()) + ProjectInfo::s_ProjectsExtension;
		return m_pFileManager->GetWorkDirectory() / projectFileName;
	}


	bool ProjectInfo::GetPreview(ID idObject, std::vector<unsigned char>& previewData) const
	{
		const boost::filesystem::path& previewDirectory = GetPreviewDirectory();
		const boost::filesystem::path previewPath = previewDirectory / boost::filesystem::path(std::to_string(idObject) + ".png");

		FileStream flStream(previewPath);
		return flStream.GetFileData(previewData);
	}


	bool ProjectInfo::GetSmallPreview(ID idObject, std::vector<unsigned char>& previewData) const
	{
		const boost::filesystem::path& previewDirectory = GetPreviewDirectory();
		const boost::filesystem::path& smallPath = previewDirectory / boost::filesystem::path(std::to_string(idObject) + "_small.png");

		FileStream flStream(smallPath);
		return flStream.GetFileData(previewData);
	}

	// =============================================  File Manager ====================================================================

	bool FileManager::GetDocument(const std::string& id, DocumentInfo& saveDocStruct)
	{
		auto itrtProject = m_Projects.find(id);
		if (itrtProject == m_Projects.end())
			return false;

		const ProjectInfo& prjInfo = *itrtProject->second.get();
		return prjInfo.GetDocInfo(saveDocStruct);
	}


	// ================================================================================================================================
}