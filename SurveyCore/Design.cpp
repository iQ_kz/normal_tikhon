#include "stdafx.h"
#include "MathSv.h"
#include "Design.h"
#include "Document.h"


using namespace survey::math;


namespace survey {

	DocumentObject* DesignElement::GetOwner() const
	{
		return dynamic_cast<DocumentObject*>(m_pDocument->DocObjectByID(m_ID));
	}


	DesignElement* DesignElement::GetParentDesignElement() const
	{
		DocumentObject* pDocObj = GetOwner();
		assert(pDocObj);

		while (pDocObj = dynamic_cast<DocumentObject*>(pDocObj->Parent()))
		{
			if (DesignElement* pDesignElement = pDocObj->GetDesignElement())
				return pDesignElement;
		}

		return NULL;
	}


	CoordDat DesignElement::GetAbsoluteCoordinate() const
	{
		assert(m_pCoordinate);

		return CoordDat(m_WCS, m_pCoordinate->GetCoord().m_Width, m_pCoordinate->GetCoord().m_Height);
	}


	math::Matrix2 DesignElement::GetWCS() const
	{
		return m_WCS;
	}


	void DesignElement::updateCoordinate()
	{
		const math::Matrix2& lcs = m_pCoordinate->GetCoord().ToMatrix();

		DesignElement* pParentDesignElement = GetParentDesignElement();
		if (!pParentDesignElement)
		{
			m_WCS = lcs;
		}
		else
		{
			m_WCS = lcs * pParentDesignElement->m_WCS;
		}
	}


	void DesignElement::updateDownTreeCoordinate()
	{
		updateCoordinate();

		const std::list<DocumentObject*>& designObjects = GetOwner()->GetDesignObjects();
		for (DocumentObject* pDocObj : designObjects)
		{
			DesignElement* pDesignElement = pDocObj->GetDesignElement();
			assert(pDesignElement);

			pDesignElement->updateCoordinate();
		}
	}


	void DesignElement::SetCoordinate(const CoordDat& crd)
	{
		assert(m_pCoordinate);
		m_pCoordinate->SetCoord(crd);

		updateDownTreeCoordinate();

		EventArg arg("Coordinate");
		InvokeChange(this, &arg);
	}


	void DesignElement::SetAbsoluteCoordinate(const CoordDat& crd)
	{
		m_WCS = crd.ToMatrix();

		DesignElement* pParentDesignElement = GetParentDesignElement();
		if (!pParentDesignElement)
		{
			m_pCoordinate->SetCoord(crd);
		}
		else
		{
			const math::Matrix2& lcs = m_WCS * InverseMatrix(pParentDesignElement->m_WCS);
			m_pCoordinate->SetCoord(CoordDat(lcs, m_pCoordinate->GetCoord().m_Width, m_pCoordinate->GetCoord().m_Height));
		}

		updateDownTreeCoordinate();

		EventArg arg("Coordinate");
		InvokeChange(this, &arg);
	}


	void DesignElement::UpdateCoordinate()
	{
		updateDownTreeCoordinate();
	}

}