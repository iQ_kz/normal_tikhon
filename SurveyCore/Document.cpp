#include "stdafx.h"
#include "Cryptography.h"
#include "Document.h"


namespace survey {


	bool Section::SaveToDocument(DocumentSectionInfo& toStruct) const
	{
		toStruct.m_Xml.LinkEndChild(new TiXmlDeclaration("1.0", "utf-8", ""));

		TiXmlElement* pElem = SaveTreeToXmlElement();
		if (!pElem)
		{
			// to do log error
			return false;
		}

		toStruct.m_Xml.LinkEndChild(pElem);
		toStruct.m_Hash = Cryptography::Bin2hex(Cryptography::CalculateMd5(pElem));
		return true;
	}


	Document::Document(const std::string& name)
		: m_Name(name)
		, m_pProject(NULL)
		, m_pLogic(NULL)
		, m_pDesign(NULL)
		, m_pToDo(NULL)
		, m_pRoLs(NULL)
		, m_pBlockShemeDesign(NULL)
		, m_pTexts(NULL)
		, m_pImages(NULL)
	{
		m_ID = Cryptography::UUID_Generate();

		m_pDesign = new Design(this);
		m_pBlockShemeDesign = new BlockShemeDesign(this);
		m_pToDo = new ToDo(this);
		m_pRoLs = new RoLs(this);
		m_pTexts = new Texts(this);
		m_pLogic = new Logic(this);
		m_pProject = new Project(this);
		m_pImages = new Images(this);
	}

	Document::Document(const DocumentInfo& docInfo)
		: m_Name("")
		, m_pProject(NULL)
		, m_pLogic(NULL)
		, m_pDesign(NULL)
		, m_pToDo(NULL)
		, m_pRoLs(NULL)
		, m_pBlockShemeDesign(NULL)
		, m_pTexts(NULL)
		, m_pImages(NULL)
	{
		if (!load(docInfo))
		{
			// to do log error
			throw std::exception("Invalid doc info");
		}
	}


	void Document::clear()
	{
		delete m_pProject;
		delete m_pTexts;
		delete m_pLogic;
		delete m_pRoLs;
		delete m_pToDo;
		delete m_pBlockShemeDesign;
		delete m_pDesign;
		delete m_pImages;

		m_pProject = NULL;
		m_pTexts = NULL;
		m_pLogic = NULL;
		m_pRoLs = NULL;
		m_pToDo = NULL;
		m_pBlockShemeDesign = NULL;
		m_pDesign = NULL;
		m_pImages = NULL;
	}


	bool Document::set(const DocumentInfo& docInfo)
	{
		m_ID_GenObjects.Reset();
		m_DocObjects.clear();

		m_ID = docInfo.m_DocumentProprties.m_Guid;
		m_Name = docInfo.m_DocumentProprties.m_Name;

		const TiXmlElement* pFirstChild = docInfo.m_Sections.at(Images::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pImages->UpdateTreeFromXmlElement(pFirstChild);

		pFirstChild = docInfo.m_Sections.at(Design::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pDesign->UpdateTreeFromXmlElement(pFirstChild);

		pFirstChild = docInfo.m_Sections.at(BlockShemeDesign::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pBlockShemeDesign->UpdateTreeFromXmlElement(pFirstChild);

		pFirstChild = docInfo.m_Sections.at(ToDo::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pToDo->UpdateTreeFromXmlElement(pFirstChild);

		pFirstChild = docInfo.m_Sections.at(RoLs::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pRoLs->UpdateTreeFromXmlElement(pFirstChild);

		pFirstChild = docInfo.m_Sections.at(Texts::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pTexts->UpdateTreeFromXmlElement(pFirstChild);

		pFirstChild = docInfo.m_Sections.at(Logic::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pLogic->UpdateTreeFromXmlElement(pFirstChild);

		pFirstChild = docInfo.m_Sections.at(Project::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pProject->UpdateTreeFromXmlElement(pFirstChild);


		// notify
		EventArg arg("Reset");
		InvokeChange(this, &arg);

		return true;
	}


	bool Document::load(const DocumentInfo& docInfo)
	{
		m_ID = docInfo.m_DocumentProprties.m_Guid;
		m_Name = docInfo.m_DocumentProprties.m_Name;

		const TiXmlElement* pFirstChild = docInfo.m_Sections.at(Images::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pImages = new Images(pFirstChild, this);

		pFirstChild = docInfo.m_Sections.at(Design::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pDesign = new Design(pFirstChild, this);

		pFirstChild = docInfo.m_Sections.at(BlockShemeDesign::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pBlockShemeDesign = new BlockShemeDesign(pFirstChild, this);

		pFirstChild = docInfo.m_Sections.at(ToDo::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pToDo = new ToDo(pFirstChild, this);

		pFirstChild = docInfo.m_Sections.at(RoLs::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pRoLs = new RoLs(pFirstChild, this);

		pFirstChild = docInfo.m_Sections.at(Texts::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pTexts = new Texts(pFirstChild, this);

		pFirstChild = docInfo.m_Sections.at(Logic::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pLogic = new Logic(pFirstChild, this);

		pFirstChild = docInfo.m_Sections.at(Project::Type().m_Name).m_Xml.FirstChildElement();
		if (!pFirstChild)
		{
			// to do log error
			return false;
		}

		m_pProject = new Project(pFirstChild, this);
		return true;
	}


	bool Document::Save(const PreviewMap& previews) const
	{
		try
		{
			DocumentInfo  docInfo;

			// save document properties 
			docInfo.m_DocumentProprties.m_Name = m_Name;
			docInfo.m_DocumentProprties.m_Guid = m_ID;

			docInfo.m_DocumentProprties.m_CreatedDate = docInfo.m_DocumentProprties.m_ModifyDate = TiXmlHelper::GetNowTime();

			// save sections 
			const std::list<Section*>& sections = GetSections();
			for (Section* pSection : sections)
			{
				DocumentSectionInfo docElInfo;
				if (!pSection->SaveToDocument(docElInfo))
				{
					// to do log error
					return false;
				}

				docInfo.m_Sections.emplace(pSection->GetType().m_Name, docElInfo);
			}

			// previews
			docInfo.m_Previews = std::move(previews);
			if (!FileManager::GetInstance()->SaveDocument(docInfo))
			{
				// to do log error
				return false;
			}

			return true;
		}
		catch (...)
		{
			// to do log error
		}

		return false;
	}

	
	bool Document::ToNextRevision()
	{
		auto& projectInfos = FileManager::GetInstance()->GetProjectInfos();
		auto itrt = projectInfos.find(m_ID);
		if (itrt == projectInfos.end())
			return false;

		BackupProjectData backupData;
		if (!itrt->second.get()->GetBackupInfo().GetNextBackup(backupData))
			return false;

		DocumentInfo docInfo;
		if (!BackupInfo::BackupProjectDataToDocumentInfo(backupData, docInfo))
		{
			// to do log error
			return false;
		}

		if (!set(docInfo))
		{
			// to do log error
			throw std::exception("Invalid XML");
		}

		return true;
	}
	
	
	bool Document::ToPreviosRevision()
	{
		auto& projectInfos = FileManager::GetInstance()->GetProjectInfos();
		auto itrt = projectInfos.find(m_ID);
		if (itrt == projectInfos.end())
			return false;

		BackupProjectData backupData;
		if (!itrt->second.get()->GetBackupInfo().GetPreviosBackup(backupData))
			return false;

		DocumentInfo docInfo;
		if (!BackupInfo::BackupProjectDataToDocumentInfo(backupData, docInfo))
		{
			// to do log error
			return false;
		}

		if (!set(docInfo))
		{
			// to do log error
			throw std::exception("Invalid XML");
		}

		return true;
	}


	std::list<BackupProjectProperties> Document::GetAllBackupDescriptions() const
	{
		std::list<BackupProjectProperties> resProps;

		auto& projectInfos = FileManager::GetInstance()->GetProjectInfos();
		auto itrt = projectInfos.find(m_ID);
		if (itrt == projectInfos.end())
			return resProps;
		
		itrt->second->GetBackupInfo().GetAllBackupDescriptions(resProps);
		return resProps;
	}
	
	
	bool Document::SetBackup(const std::string& id)
	{
		auto& projectInfos = FileManager::GetInstance()->GetProjectInfos();
		auto itrt = projectInfos.find(m_ID);
		if (itrt == projectInfos.end())
			return false;

		BackupProjectData backupData;
		if (!itrt->second.get()->GetBackupInfo().GetBackup(id, backupData))
			return false;

		DocumentInfo docInfo;
		if (!BackupInfo::BackupProjectDataToDocumentInfo(backupData, docInfo))
		{
			// to do log error
			return false;
		}

		if (!set(docInfo))
		{
			// to do log error
			throw std::exception("Invalid XML");
		}

		return true;
	}

}
