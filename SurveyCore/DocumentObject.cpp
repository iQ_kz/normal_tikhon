#include "stdafx.h"
#include "DocumentObject.h"
#include "Document.h"
#include "Project.h"


namespace survey {

	IDocumentObject::IDocumentObject(Document* pDoc, const std::string& name)
		: IXMLObject(pDoc)
		, m_ID(0)
		, m_Name(name)
	{
		assert(pDoc);		
		m_ID = m_pDocument->m_ID_GenObjects.GetID();
		m_pDocument->m_DocObjects[m_ID] = this;
	}


	IDocumentObject::IDocumentObject(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
		: IXMLObject(childs, pDoc)
	{
		assert(pTiElement && pDoc);

		if (!pTiElement->Attribute("ID", (int*)(&m_ID)))
		{
			// to do error log
			throw std::exception("Invalid XML");
		}

		const char* pName = pTiElement->Attribute("Name");
		if (pName)
			m_Name = pName;
	}


	void IDocumentObject::UpdateFromXmlElement(const TiXmlElement* pTiElement)
	{
		IXMLObject::UpdateFromXmlElement(pTiElement);

		assert(pTiElement);

		int id = 0;
		if (!pTiElement->Attribute("ID", &id) || id != m_ID)
		{
			// to do error log
			throw std::exception("Invalid XML");
		}

		const char* pName = pTiElement->Attribute("Name");
		if (pName)
			m_Name = pName;
	}


	TiXmlElement* IDocumentObject::SaveToXmlElement() const
	{
		TiXmlElement* pElement = IXMLObject::SaveToXmlElement();
		assert(pElement);

		pElement->SetAttribute("ID", m_ID);
		pElement->SetAttribute("Name", m_Name);

		return pElement;
	}


	void IDocumentObject::KillHimself()
	{
		IXMLObject::KillHimself();
	}

	// =========================================  DocumentObject ===============================================================

	DocumentObject::DocumentObject(Document* pDoc, const std::string& name)
		: IDocumentObject(pDoc, name)
		, m_pDesignElement(NULL)
		, m_pBSDesignElement(NULL)
	{ }


	DocumentObject::DocumentObject(Document* pDoc, const ColorDat& bsColor, const CoordDat& bsCoord, const std::string& name)
		: IDocumentObject(pDoc, name)
		, m_pDesignElement(NULL)
		, m_pBSDesignElement(NULL)
	{
		assert(pDoc);

		m_pBSDesignElement = pDoc->GetBlockShemeDesign()->AddDesignElement(m_ID, bsCoord, bsColor);
	}


	DocumentObject::DocumentObject(Document* pDoc, const ColorDat& bsColor, const CoordDat& bsCoord, const ColorDat& color, const CoordDat& coord, const std::string& name)
		: IDocumentObject(pDoc, name)
		, m_pDesignElement(NULL)
		, m_pBSDesignElement(NULL)
	{
		assert(pDoc);

		m_pBSDesignElement = pDoc->GetBlockShemeDesign()->AddDesignElement(m_ID, bsCoord, bsColor);
		m_pDesignElement = pDoc->GetDesign()->AddDesignElement(m_ID, coord, color);
	}


	DocumentObject::DocumentObject(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
		: IDocumentObject(pTiElement, childs, pDoc)
		, m_pDesignElement(NULL)
		, m_pBSDesignElement(NULL)
	{
		loadElement(pTiElement);
		afterTreeUpdate();

		m_pDesignElement = pDoc->GetDesign()->GetDesignElementByID(m_ID);
		m_pBSDesignElement = pDoc->GetBlockShemeDesign()->GetDesignElementByID(m_ID);
	}


	DocumentObject::~DocumentObject()
	{
		m_pDesignElement = NULL; m_pBSDesignElement = NULL; m_RangedNotifycators.clear();
	}


	DesignElement* DocumentObject::GetDesignElement() const
	{
		return m_pDesignElement;
	}


	BSDesignElement* DocumentObject::GetBSDesignElement() const
	{
		return m_pBSDesignElement;
	}


	void DocumentObject::KillHimself()
	{
		// erase sources
		releaseSources();

		// erase this
		IDocumentObject::KillHimself();
	}


	void DocumentObject::UpdateFromXmlElement(const TiXmlElement* pTiElement)
	{
		IDocumentObject::UpdateFromXmlElement(pTiElement);
		loadElement(pTiElement);
	}


	TiXmlElement* DocumentObject::SaveToXmlElement() const
	{
		TiXmlElement* pXmlelement = IDocumentObject::SaveToXmlElement();
		return pXmlelement;
	}


	void DocumentObject::UpdateTreeFromXmlElement(const TiXmlElement* pTiElement)
	{
		IDocumentObject::UpdateTreeFromXmlElement(pTiElement);
		afterTreeUpdate();
	}


	void DocumentObject::loadElement(const TiXmlElement* pTiElement)
	{
		// ...
	}


	void DocumentObject::afterTreeUpdate()
	{
		// ...
	}


	void DocumentObject::RemoveChild(IXMLObject* pXmlObject)
	{
		IDocumentObject::RemoveChild(pXmlObject);

		DocumentObject* pRemoveObject = dynamic_cast<DocumentObject*>(pXmlObject);
		if (pRemoveObject)
		{
			pRemoveObject->removeRangedNotifycators(m_RangedNotifycators);

			// update coordinates
			pRemoveObject->updateDesignElement();
		}

		// notify
		CollectionEventArg evArg("childs", CollectionEventType::CET_Erase, pRemoveObject);
		InvokeChange(this, &evArg);

		for (IRangedNotifycatorSupport* pNotifycator : m_RangedNotifycators)
			pNotifycator->notifyRemoveChild(pXmlObject);
	}


	void DocumentObject::AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject)
	{
		IDocumentObject::AddChild(pXmlObject, pAfterXmlObject);

		DocumentObject* pAddObject = dynamic_cast<DocumentObject*>(pXmlObject);
		if (pAddObject)
		{
			pAddObject->setRangedNotifycators(m_RangedNotifycators);

			// update coordinates
			pAddObject->updateDesignElement();
		}

		// notify
		CollectionEventArg evArg("childs", CollectionEventType::CET_PushBack, pAddObject);
		InvokeChange(this, &evArg);

		for (IRangedNotifycatorSupport* pNotifycator : m_RangedNotifycators)
			pNotifycator->notifyAddChild(pXmlObject);
	}


	void DocumentObject::setRangedNotifycators(const std::unordered_set<IRangedNotifycatorSupport*>& parentNotifycators)
	{
		std::queue<DocumentObject*>  docObjects;
		docObjects.push(this);
		
		while (!docObjects.empty())
		{
			DocumentObject* pObject = docObjects.front();
			docObjects.pop();

			for (IRangedNotifycatorSupport* pNotify : parentNotifycators)
				if (pObject->m_RangedNotifycators.find(pNotify) == pObject->m_RangedNotifycators.end())
					pObject->m_RangedNotifycators.emplace(pNotify);

			IXMLObject* pXMLObject = pObject->m_pChildFirst;
			while (pXMLObject)
			{
				DocumentObject* pDocObject = dynamic_cast<DocumentObject*>(pXMLObject);
				if (pDocObject)
					docObjects.push(pDocObject);

				pXMLObject = pXMLObject->NextBrother();
			}
		}

	}


	void DocumentObject::removeRangedNotifycators(const std::unordered_set<IRangedNotifycatorSupport*>& parentNotifycators)
	{
		std::queue<DocumentObject*>  docObjects;
		docObjects.push(this);


		while (!docObjects.empty())
		{
			DocumentObject* pObject = docObjects.front();
			docObjects.pop();

			for (IRangedNotifycatorSupport* pNotify : parentNotifycators)
				if (pObject->m_RangedNotifycators.find(pNotify) != pObject->m_RangedNotifycators.end())
					pObject->m_RangedNotifycators.erase(pNotify);

			IXMLObject* pXMLObject = pObject->m_pChildFirst;
			while (pXMLObject)
			{
				DocumentObject* pDocObject = dynamic_cast<DocumentObject*>(pXMLObject);
				if (pDocObject)
					docObjects.push(pDocObject);

				pXMLObject = pXMLObject->NextBrother();
			}
		}

	}


	std::list<DocumentObject*>  DocumentObject::GetDesignObjects() const
	{
		const std::list<IXMLObject*>& childs = ChildsTree();

		std::list<DocumentObject*> resLst;

		for (IXMLObject* pObject : childs)
			if (DocumentObject* pDocObject = dynamic_cast<DocumentObject*>(pObject))
				if (pDocObject->GetDesignElement())
					resLst.push_back(pDocObject);

		return resLst;
	}


	std::list<DesignElement*>  DocumentObject::GetTopDesignObjects() const
	{
		std::list<DesignElement*> res;

		std::queue<DocumentObject*> docObjs;
		docObjs.push(const_cast<DocumentObject*>(this));

		while (!docObjs.empty())
		{
			DocumentObject* pDocObj = docObjs.front();
			docObjs.pop();

			if (DesignElement* pDesignElem = pDocObj->GetDesignElement())
			{
				res.push_back(pDesignElem);
				continue;
			}

			const std::list<DocumentObject*>& childs = pDocObj->Childs<DocumentObject>();
			for (DocumentObject* pChild : childs)
				docObjs.push(pChild);
		}

		return res;
	}


	std::list<DocumentObject*>  DocumentObject::GetBlockShemeDesignObjects() const
	{
		const std::list<IXMLObject*>& childs = ChildsTree();

		std::list<DocumentObject*> resLst;

		for (IXMLObject* pObject : childs)
			if (DocumentObject* pDocObject = dynamic_cast<DocumentObject*>(pObject))
				if (pDocObject->GetBSDesignElement())
					resLst.push_back(pDocObject);

		return resLst;
	}


	void DocumentObject::EjectSourcesElements(std::unordered_map<std::string, std::list<IXMLObject*>>& sourcesElements)
	{
		sourcesElements[Design::Type().m_Name].push_back(m_pDesignElement);
		sourcesElements[BlockShemeDesign::Type().m_Name].push_back(m_pBSDesignElement);

		m_pDesignElement = NULL;
		m_pBSDesignElement = NULL;
	}

	// erase sources
	void DocumentObject::releaseSources()
	{
		const std::list<IXMLObject*>& childs = ChildsTree();

		std::list<ID> ids;
		ids.push_back(m_ID);

		std::unordered_map<std::string, std::list<IXMLObject*>> sourcesElenents;
		EjectSourcesElements(sourcesElenents);		

		for (IXMLObject* pObject : childs)
		{
			DocumentObject* pDocObj = dynamic_cast<DocumentObject*>(pObject);
			if (!pDocObj)
				continue;

			ids.push_back(pDocObj->m_ID);
			pDocObj->EjectSourcesElements(sourcesElenents);
		}

		std::list<Text*> txts;
		std::list<DesignElement*> designs;
		std::list<BSDesignElement*> bsDesigns;

		for (auto pairSourceElement : sourcesElenents)
		{
			if (pairSourceElement.first == Design::Type().m_Name)
			{
				for (IXMLObject* pIXMLObject : pairSourceElement.second)
					if (DesignElement* pDesignElem = dynamic_cast<DesignElement*>(pIXMLObject))
						designs.push_back(pDesignElem);
			}
			else if (pairSourceElement.first == BlockShemeDesign::Type().m_Name)
			{
				for (IXMLObject* pIXMLObject : pairSourceElement.second)
					if (BSDesignElement* pBsDesignElem = dynamic_cast<BSDesignElement*>(pIXMLObject))
						bsDesigns.push_back(pBsDesignElem);
			}
			else if (pairSourceElement.first == Texts::Type().m_Name)
			{
				for (IXMLObject* pIXMLObject : pairSourceElement.second)
					if (Text* pText = dynamic_cast<Text*>(pIXMLObject))
						txts.push_back(pText);
			}
		}

		m_pDocument->GetDesign()->RemoveDesignRange(designs);
		m_pDocument->GetBlockShemeDesign()->RemoveDesignRange(bsDesigns);
		m_pDocument->GetTexts()->RemoveTextRange(txts);
		m_pDocument->removeIDS(ids);
	}


	void DocumentObject::updateDesignElement()
	{
		const std::list<DesignElement*>& topDesignObjects = GetTopDesignObjects();
		for (DesignElement* pDesignObject : topDesignObjects)
			pDesignObject->UpdateCoordinate();
	}

}