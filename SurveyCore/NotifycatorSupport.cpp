#include "stdafx.h"
#include "NotifycatorSupport.h"
#include "SurveyObject.h"


namespace survey {

	void IRangedNotifycatorSupport::notifyAddChild(IXMLObject* pXmlObject)
	{
		std::list<IXMLObject*> childs = pXmlObject->ChildsTree();
		childs.push_front(pXmlObject);

		CollectionEventArg evArg("elements", CollectionEventType::CET_InsertRange, childs);
		InvokeChange(dynamic_cast<ISurveyObject*>(this), &evArg);
	}

	void IRangedNotifycatorSupport::notifyRemoveChild(IXMLObject* pXmlObject)
	{
		std::list<IXMLObject*> childs = pXmlObject->ChildsTree();
		childs.push_front(pXmlObject);

		CollectionEventArg evArg("elements", CollectionEventType::CET_EraseRange, childs);
		InvokeChange(dynamic_cast<ISurveyObject*>(this), &evArg);
	}


}