#include "Stdafx.h"
#include "SurveyObject.h"


namespace survey {


	void IXMLObject::RemoveChild(IXMLObject* pXmlObject)
	{
		IXMLObject* pChild = m_pChildFirst;

		while (pChild)
		{
			if (pChild != pXmlObject)
			{
				pChild = pChild->m_pNextBrother;
				continue;
			}

			if (m_pChildFirst == pChild)
				m_pChildFirst = pChild->m_pNextBrother;

			if (m_pChildLast == pChild)
				m_pChildLast = pChild->m_pPreviosBrother;

			pChild->m_pParent = NULL;

			if (pChild->m_pPreviosBrother)
				pChild->m_pPreviosBrother->m_pNextBrother = pChild->m_pNextBrother;

			if (pChild->m_pNextBrother)
				pChild->m_pNextBrother->m_pPreviosBrother = pChild->m_pPreviosBrother;

			pChild->m_pPreviosBrother = pChild->m_pNextBrother = NULL;

			return;
		}

		// to do log error
	}


	void IXMLObject::addChild(IXMLObject* pXmlObject)
	{
		if (!pXmlObject)
		{
			// to do log error
			return;
		}

		pXmlObject->m_pParent = this;

		if (m_pChildLast)
		{
			m_pChildLast->m_pNextBrother = pXmlObject;
			pXmlObject->m_pPreviosBrother = m_pChildLast;
			m_pChildLast = pXmlObject;
		}
		else
		{
			m_pChildFirst = m_pChildLast = pXmlObject;
		}
	}


	void IXMLObject::AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject)
	{
		if (!pAfterXmlObject)
		{
			addChild(pXmlObject);
			return;
		}

		IXMLObject* pChild = m_pChildFirst;

		while (pChild)
		{
			if (pChild != pAfterXmlObject)
			{
				pChild = pChild->m_pNextBrother;
				continue;
			}

			if (pChild->m_pNextBrother)
			{
				pChild->m_pNextBrother->m_pPreviosBrother = pXmlObject;
				pXmlObject->m_pNextBrother = pChild->m_pNextBrother;
			}

			pChild->m_pNextBrother = pXmlObject;
			pXmlObject->m_pPreviosBrother = pChild;
			pXmlObject->m_pParent = this;

			return;
		}

		// to do log error
	}


	void IXMLObject::releaseChildTree()
	{
		std::queue<IXMLObject*>  xmlObjects;

		IXMLObject* pXmlObject = m_pChildFirst;
		while (pXmlObject)
		{
			xmlObjects.push(pXmlObject);
			pXmlObject = pXmlObject->m_pNextBrother;
		}

		while (!xmlObjects.empty())
		{
			IXMLObject* pParentXmlObject = xmlObjects.front();
			xmlObjects.pop();

			pXmlObject = pParentXmlObject->m_pChildFirst;
			while (pXmlObject)
			{
				xmlObjects.push(pXmlObject);
				pXmlObject = pXmlObject->m_pNextBrother;
			}

			delete pParentXmlObject;  // delete object
		}

		m_pChildFirst = m_pChildLast = NULL;
	}


	void IXMLObject::UpdateFromXmlElement(const TiXmlElement* pTiElement)
	{
		updateFromXmlElement(pTiElement);

		if (pTiElement->Value() != GetType().m_Name)
		{
			// to do error log
			throw std::exception("Invalid XML");
		}
	}


	void IXMLObject::updateFromXmlElement(const TiXmlElement* pTiElement)
	{
		if (!pTiElement)
		{
			// to do error log
			throw std::exception("Invalid XML");
		}
	}


	TiXmlElement* IXMLObject::SaveToXmlElement() const
	{
		TiXmlElement* pElement = new TiXmlElement(GetType().m_Name);
		return pElement;
	}


	void IXMLObject::buildChildTree(const TiXmlElement* pTiElement)
	{
		// build stack for create from lower to upper element
		std::queue<const TiXmlElement*> tiElements;

		const TiXmlNode* pChNode = NULL;
		while (pChNode = pTiElement->IterateChildren(pChNode))
		{
			const TiXmlElement* pChElement = pChNode->ToElement();
			if (pChElement)
				tiElements.push(pChElement);
		}

		std::stack<const TiXmlElement*> tiElementsForCreate;

		while (!tiElements.empty())
		{
			const TiXmlElement* pParent = tiElements.front();
			tiElements.pop();

			tiElementsForCreate.push(pParent);

			const TiXmlNode* pChNode = NULL;
			while (pChNode = pParent->IterateChildren(pChNode))
			{
				const TiXmlElement* pChElement = pChNode->ToElement();
				if (pChElement)
					tiElements.push(pChElement);
			}
		}

		// create elements from stack
		std::unordered_map<const TiXmlElement*, std::list<IXMLObject*>> associations;

		while (!tiElementsForCreate.empty())
		{
			const TiXmlElement* pElement = tiElementsForCreate.top();
			tiElementsForCreate.pop();

			std::list<IXMLObject*> childs;

			auto itrtAssociations = associations.find(pElement);
			if (itrtAssociations != associations.end())
				childs = itrtAssociations->second;

			IXMLObject* pXmlObject = IXMLObject::CreateXMLObject(pElement, childs, m_pDocument);
			if (!pXmlObject)
				continue;

			const TiXmlNode* pTiParentNode = pElement->Parent();
			if (!pTiParentNode)
				continue;

			const TiXmlElement* pTiParent = pTiParentNode->ToElement();
			if(!pTiParent)
				continue;

			associations[pTiParent].push_front(pXmlObject);
		}

		// set childs
		auto itrtAssociations = associations.find(pTiElement);
		if (itrtAssociations != associations.end())
			setChilds(itrtAssociations->second);
	}


	void IXMLObject::setChilds(const std::list<IXMLObject*>& childs)
	{
		auto itrtChilds = childs.begin();
		if (itrtChilds == childs.end())
			return;

		IXMLObject* pCurrentChild = m_pChildFirst = *itrtChilds;

		while (++itrtChilds != childs.end())
		{
			IXMLObject* pNextChild = *itrtChilds;

			pCurrentChild->m_pNextBrother = pNextChild;
			pNextChild->m_pPreviosBrother = pCurrentChild;
			pCurrentChild = pNextChild;
		}

		m_pChildLast = pCurrentChild;

		// set parent
		for (IXMLObject* pXmlObj : childs)
			pXmlObj->m_pParent = this;
	}


	void IXMLObject::UpdateTreeFromXmlElement(const TiXmlElement* pTiElement)
	{
		// update only current element
		assert(pTiElement);
		UpdateFromXmlElement(pTiElement);

		// release old tree !!!
		releaseChildTree();

		// build new tree
		buildChildTree(pTiElement);
	}


	TiXmlElement* IXMLObject::SaveTreeToXmlElement() const
	{
		TiXmlElement* pRootElement = SaveToXmlElement();

		if (!pRootElement)
		{
			// to do log error
			return NULL;
		}

		// tree structure
		struct DocObjToParentXmlElement
		{
			IXMLObject*       m_pXmlObject;
			TiXmlElement*     m_pParentXmlElement;
		};

		std::queue<DocObjToParentXmlElement>  treeElements;

		// begining values
		IXMLObject* pXmlObject = m_pChildFirst;
		while (pXmlObject)
		{
			treeElements.push(DocObjToParentXmlElement{ pXmlObject, pRootElement });
			pXmlObject = pXmlObject->NextBrother();
		}

		// creating of the xml
		while (!treeElements.empty())
		{
			DocObjToParentXmlElement treeStruct = treeElements.front();
			treeElements.pop();

			TiXmlElement* pCurrentElement = treeStruct.m_pXmlObject->SaveToXmlElement();
			if (!pCurrentElement)
			{
				// to do log error
				continue;
			}

			treeStruct.m_pParentXmlElement->LinkEndChild(pCurrentElement);

			pXmlObject = treeStruct.m_pXmlObject->ChildFirst();
			while (pXmlObject)
			{
				treeElements.push(DocObjToParentXmlElement{ pXmlObject, pCurrentElement });
				pXmlObject = pXmlObject->NextBrother();
			}
		}

		return pRootElement;
	}


	void IXMLObject::KillHimself()
	{
		if (m_pParent)
			m_pParent->RemoveChild(this);

		releaseChildTree();
		delete this;
	}


	IXMLObject* IXMLObject::CreateXMLObject(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		if (!pXmlElement || !pDocument)
		{
			// to do log error
			return NULL;
		}

		try 
		{
			auto itrtTypes = SurveyType::s_Types.find(pXmlElement->Value());
			if (itrtTypes != SurveyType::s_Types.end())
				if (itrtTypes->second.m_fCreator)
					return itrtTypes->second.m_fCreator(pXmlElement, childs, pDocument);
		}
		catch (const boost::exception& ex)
		{
			// to do log error
		}
		catch (const std::exception& ex)
		{
			// to do log error
		}
		catch (...)
		{
			// to do log error
		}

		return NULL;
	}


	std::list<IXMLObject*> IXMLObject::Childs() const
	{
		std::list<IXMLObject*> outObjects;

		IXMLObject* pObject = ChildFirst();
		while (pObject)
		{
			outObjects.push_back(pObject);
			pObject = pObject->NextBrother();
		}

		return outObjects;
	}


	std::list<IXMLObject*> IXMLObject::ChildsTree() const
	{
		std::list<IXMLObject*> outObjects;

		std::queue<IXMLObject*> xmlObjects;
		
		IXMLObject* pObject = ChildFirst();
		while (pObject)
		{
			xmlObjects.push(pObject);
			pObject = pObject->NextBrother();
		}

		while (!xmlObjects.empty())
		{
			IXMLObject* parentObj = xmlObjects.front();
			xmlObjects.pop();

			outObjects.push_back(parentObj);

			IXMLObject* pObject = parentObj->ChildFirst();
			while (pObject)
			{
				xmlObjects.push(pObject);
				pObject = pObject->NextBrother();
			}
		}

		return outObjects;
	}

}