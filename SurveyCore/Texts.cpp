#include "stdafx.h"
#include "NotifycatorSupport.h"
#include "Texts.h"

namespace survey {

	// ==================================== Text =======================================================================================

	Text::Text(ID id, const std::string& text, Document* pDoc)
		: IXMLObject(pDoc)
		, m_ID(id)
		, m_Text(text)
	{ }


	Text::Text(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
		: IXMLObject(childs, pDoc)
		, m_ID(0)
	{
		loadElement(pTiElement);
	}


	void Text::SetText(const std::string& text)
	{
		m_Text = text;

		// notify
		EventArg evArg("Text");
		InvokeChange(this, &evArg);
	}

	// xml support
	void Text::UpdateFromXmlElement(const TiXmlElement* pTiElement)
	{
		IXMLObject::UpdateFromXmlElement(pTiElement);
		assert(pTiElement);

		m_Text.clear();
		loadElement(pTiElement);

		// notify
		EventArg evArg("reload");
		InvokeChange(this, &evArg);
	}


	TiXmlElement* Text::SaveToXmlElement() const
	{
		TiXmlElement* pXmlelement = IXMLObject::SaveToXmlElement();
		assert(pXmlelement);

		pXmlelement->SetAttribute("id", m_ID);

		TiXmlText* text = new TiXmlText(m_Text.c_str());
		text->SetCDATA(true);
		pXmlelement->LinkEndChild(text);	

		return pXmlelement;
	}


	void Text::loadElement(const TiXmlElement* pTiElement)
	{
		assert(pTiElement);

		const char* pID_Ch = pTiElement->Attribute("id");
		if (!pID_Ch)
		{
			// to do log error
			throw std::exception("Invalid XML");
		}

		m_ID = boost::lexical_cast<ID>(pID_Ch);

		const char* pText = pTiElement->GetText();
		if (pText)
			m_Text = pText;
	}


	// ================================================ Texts ==========================================================================================

	Texts::Texts(Document* pDoc)
		: Section(pDoc)
	{ }


	Texts::Texts(const TiXmlElement* pTiElement, Document* pDoc)
		: Section(pTiElement, pDoc)
	{
		updateFromXmlElement(pTiElement);
		afterTreeUpdate();
	}


	Text* Texts::AddText(ID id, const std::string& txt)
	{
		Text* pText = new Text(id, txt, m_pDocument);
		AddChild(pText);
		m_Texts.emplace(id, pText);

		// notify
		CollectionEventArg evArg("texts", CollectionEventType::CET_PushBack, pText);
		InvokeChange(this, &evArg);

		return pText;
	}


	void Texts::RemoveText(Text* pText)
	{
		if (!pText)
			return;

		m_Texts.erase(pText->m_ID);
		pText->KillHimself();

		// notify
		CollectionEventArg evArg("texts", CollectionEventType::CET_Erase, pText);
		InvokeChange(this, &evArg);	
	}


	void Texts::RemoveTextRange(const std::list<Text*>& txts)
	{
		std::list<IXMLObject*> xmlObjs;

		for (Text* pTxt : txts)
		{
			assert(pTxt);

			xmlObjs.push_back(pTxt);
			m_Texts.erase(pTxt->m_ID);
			pTxt->KillHimself();
		}

		// notify
		CollectionEventArg evArg("texts", CollectionEventType::CET_EraseRange, xmlObjs);
		InvokeChange(this, &evArg);
	}


	std::list<Text*> Texts::GetTexts() const
	{
		std::list<Text*> outList;

		IXMLObject* pObject = m_pChildFirst;
		while (pObject)
		{
			Text* pText = SurveyType::Cast<Text>(pObject);
			if (pText)
				outList.push_back(pText);

			pObject = pObject->NextBrother();
		}

		return outList;
	}


	Text* Texts::GetTextByID(ID id) const
	{
		auto itrtTxt = m_Texts.find(id);
		if (itrtTxt != m_Texts.end())
			return itrtTxt->second;

		return NULL;
	}


	void Texts::UpdateFromXmlElement(const TiXmlElement* pTiElement)
	{
		Section::UpdateFromXmlElement(pTiElement);
		updateFromXmlElement(pTiElement);
	}


	TiXmlElement* Texts::SaveToXmlElement() const
	{
		TiXmlElement* pXmlelement = Section::SaveToXmlElement();
		// ...
		return pXmlelement;
	}


	void Texts::UpdateTreeFromXmlElement(const TiXmlElement* pTiElement)
	{
		IXMLObject::UpdateTreeFromXmlElement(pTiElement);
		m_Texts.clear();
		afterTreeUpdate();

		// notify
		CollectionEventArg evArg("texts", CollectionEventType::CET_Reset);
		InvokeChange(this, &evArg);
	}


	void Texts::updateFromXmlElement(const TiXmlElement* pTiElement)
	{
		// ...
	}


	void Texts::afterTreeUpdate()
	{
		IXMLObject* pObject = m_pChildFirst;
		while (pObject)
		{
			Text* pText = SurveyType::Cast<Text>(pObject);
			if (pText)
				m_Texts.emplace(pText->m_ID, pText);

			pObject = pObject->NextBrother();
		}
	}

}