#pragma once
#include"../include/transport/transport_types.h" 
#include "../include/logger.h"
#include"../include/common_types.h"

namespace survey
{
	namespace transport
	{


		
		//////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////


		class CientProxy
		{
		public:
			CientProxy(ConnectionPtr SPtr) :m_SessionPtr(SPtr){}
			CientProxy(){}

			CientProxy& operator =(const CientProxy& obj);

			void OnLogin(const Result& res, ID RequestID);
			void OnNewProject(const Result& res, const ProjectId& Id, ID RequestID);
			void OnUploadResource(const Result& res, ID ProjId, const std::string& fileHash, const std::string& Name, ID RequestID);
			void OnDownloadResource(const Result& res, const ResourceData& data, ByteBufferPtr body, ID RequestID);
			void OnGetProjectList(const Result& res, const ProjectInfoV& pr, ID RequestID);
			void OnGetUserList(const Result& res, const UserInfoV& ui, ID RequestID);


		private:
			ConnectionPtr m_SessionPtr;
			src::severity_logger< SeverityLevel > slg;
		};

	}//!namespace transport

}//!namespace survey
