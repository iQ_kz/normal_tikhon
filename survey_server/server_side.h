#pragma once



#include "../Include/transport_client_interface.h"

#include <iostream>

#include <vector>
#include <boost/shared_ptr.hpp>
#include <deque>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/thread.hpp>


#include "../include/logger.h"

#include <boost/make_shared.hpp>
#include <boost/function.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "../Include/ID_Genearator.h"
#include "../Include/transport_client_interface.h"
#include "../Include/transport/transport_types.h"
#include "../Include/transport/survey_serialization.h"
#include "../Include/transport/transport_core.h"

#include "DAL.h"
#include "server_settings.h"
#include "session_manager.h"
#include "server_handler.h"




namespace survey
{
	namespace transport
	{
		/*# ������ ����������, ����� ������� �� ��������� �������.
		*/

		//////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////

		/*#�������� ��������� ������� ������������� ��������� ����������
		*/
		class ServerCryptContext : public CryprContextManager
		{
		public:
			ServerCryptContext() {}

			void Init();
		private:
			std::string get_password() const
			{
				return "";
			}
			src::severity_logger< SeverityLevel > slg;
		};
	
		/*#��������������� �������� �������, �������� ���������� ����������� (ServerHandler)
		*/
		template <typename HandlerType = ServerHandler>
		class ServerStub : public StubBase<HandlerType>
		{
		public:
			typedef HandlerType HandlerType;
			typedef StubBase<HandlerType> Base;

			ServerStub()  {}

			void OnRead(ByteBufferPtr BuffPtr);

			HandlerType& Handler(){ return Base::m_Handler; }
		private:
			
		};

		/*#����������, ����� ��������������� �����.*/
		template <typename HandlerType /*= ServerHandler*/>
		void ServerStub<HandlerType>::OnRead(ByteBufferPtr BuffPtr)
		{
			BOOST_LOG_NAMED_SCOPE("ServerStub: OnRead");
			try
			{
				ID RequestID = 0;
				ITransportClient::MethodId MID;
				Read(*BuffPtr, MID);
				switch (MID)
				{
				case survey::transport::ITransportClient::ePing:
					break;
				case survey::transport::ITransportClient::ePong:
					break;
				case survey::transport::ITransportClient::eLogin:
				{
					BOOST_LOG_SEV(slg, critical) << "connection " << Base::GetID() << "; message :  Login ";

					std::string LoginStr;
					std::string Password;
					Read(*BuffPtr, LoginStr);
					Read(*BuffPtr, Password);
					Read(*BuffPtr, RequestID);
					Base::m_Handler.Login(LoginStr.c_str(), Password.c_str(), RequestID);
				}
				break;

				case survey::transport::ITransportClient::eCreateProject:
				{
					BOOST_LOG_SEV(slg, critical) << "connection " << Base::GetID() << "; message :  CreateProject ";
					std::string ProjectName;
					Read(*BuffPtr, ProjectName);
					Read(*BuffPtr, RequestID);

					Base::m_Handler.CreateProject(ProjectName, RequestID);
				}
					break;
				case survey::transport::ITransportClient::eUploadFile:
				{
					
					ResourceData data;
					ByteBufferPtr Buff = ByteBuffer::Create();
					
					Read(*BuffPtr,  data);
					Read(*BuffPtr, *Buff);
					Read(*BuffPtr, RequestID);
					Base::m_Handler.UploadResource(data, *Buff, RequestID);
				}
					break;
				case survey::transport::ITransportClient::eCommit:
					break;
				case survey::transport::ITransportClient::eGetRevision:
					break;
				case survey::transport::ITransportClient::eSendFileList:
					break;
				case survey::transport::ITransportClient::eGetFile:
				{
						ResourceData data;
						Read(*BuffPtr, data);
						Read(*BuffPtr, RequestID);
						Base::m_Handler.DownloadResource(data, RequestID);
				}
					break;
				case survey::transport::ITransportClient::eGetVersionList:
					break;
				case survey::transport::ITransportClient::eGetProjectList:
				{
					Read(*BuffPtr, RequestID);
					Base::m_Handler.GetProjectList(RequestID);
				}
				break;

				case survey::transport::ITransportClient::eGetUserList:
				{
					Read(*BuffPtr, RequestID);
					Base::m_Handler.GetUserList(RequestID);
				}
				break;

				default:
					BOOST_LOG_SEV(slg, error) << "connection " << Base::GetID() << ": wrong message";
					Base::Disconnect();
					break;
				}
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << "connection " << Base::GetID() << " - exception: " << e.what();
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << "connection " << Base::GetID() << " - unexpected exception";
			}
		}

		typedef boost::shared_ptr <ServerStub<> > ServerProxyStubPtr;
		/*��������� ��������� ���������� ����������*/
		class ConnectionManager : public  IO_Handler,  public IConnectManager
		{
		public:

			typedef boost::unique_lock<boost::mutex> Lock;

			ConnectionManager(IO_Svc& IO) :IO_Handler(IO){}//, m_CryptContext(){}
			typedef std::vector <ConnectionPtr> ConnectionContainer;

			void OnConnect(ConnectionPtr SPtr);

			ConnectionPtr GetSession(ID Id)
			{
				Lock Locker(m_mutex);
				return m_SessionContainer.at(Id);
			}

			virtual void OnDisconnect(ConnectionPtr SPtr)	 override;

			virtual void OnError(const std::string msg, const boost::system::error_code& Error, ID Id)override;

			virtual ConnectionPtr CreateSession()override;

			void SetWorkManager(WorkManagerPtr WorkManagerObj)
			{
				m_WorkManagerPtr = WorkManagerObj;
			}

			void SetServiceAccess(CommonServiceAccessPtr SA)
			{
				m_CommonServiceAccessPtr = SA;
			}

			void Init(const TransportSettings& settings);

			TransportSettings GetTransportSerrings()
			{
				Lock Locker(m_mutex);
				return m_Settings;
			}

			void OnConnectWilligness(ConnectionPtr SPtr);

			EConnectionType GetConnectionType()
			{
				return m_Settings.GetConnectionType();
			}

		private:
			ID_Genearator m_ID_Generator;
			ConnectionContainer m_SessionContainer;
			WorkManagerPtr m_WorkManagerPtr;
			CommonServiceAccessPtr m_CommonServiceAccessPtr;
			ServerCryptContext m_CryptContext;
			TransportSettings m_Settings;
			src::severity_logger< SeverityLevel > slg;

			boost::mutex m_mutex;
		};

		//////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////
	
		/*#���������� � ��������� �������� ���������� �������.
		*/
		class ServerCore
		{
		public:
			typedef boost::shared_ptr <ConnectionManager> ConnectionManagerPtr;
			typedef boost::shared_ptr <TCP_Acceptor>   TCP_AcceptorPtr;

			struct Stat
			{
				Stat() :ConnectionCount(0), KBytesSent(0), KBytesRecieved(0){}

				size_t ConnectionCount;
				double KBytesSent;
				double KBytesRecieved;
			};

			~ServerCore(){ Stop(); }

			void Init(const ServerSettings& SettingsObj, const std::string& rootDir );
			TransportSettings GetTransportSettings()
			{
				return m_ConnectionManager.m_WorkerPtr->GetTransportSerrings();
			}

			void Stop();


			Stat GetStat()
			{
				return m_Stat;
			}

			EConnectionType GetConnectionType()
			{
				return m_ConnectionManager.m_WorkerPtr->GetConnectionType();
			}

		private:
			ThreadManagerPtr m_ThreadManagerPtr;
			IOWorker<TCP_Acceptor>    m_TCP_Acceptor;
			IOWorker<ConnectionManager>  m_ConnectionManager;
			IOWorker <WorkManager>	m_WorkManager;

			SessionManager m_SessionManager;
			CommonServiceAccessPtr m_CommonServiceAccessPtr;

			server::ProjectManagerPtr m_ProjectManagerPtr;
			
			Stat m_Stat;
			src::severity_logger< SeverityLevel > slg;
		};
	}//!namespace transport

}//!namespace survey
