#include "stdafx.h"
#include "client_proxy.h"
#include "../include/transport/survey_serialization.h"
#include "../include/transport/Connection.h"

namespace survey
{
	namespace transport
	{

		CientProxy& CientProxy::operator=(const CientProxy& obj)
		{
			if (this != &obj)
			{
				m_SessionPtr = obj.m_SessionPtr;
			}
			return *this;
		}

		void CientProxy::OnLogin(const Result& res, ID RequestID)
		{
			ITransportClientRep::MethodId MID = ITransportClientRep::eOnLogin;
			ByteBufferPtr Buff = ByteBuffer::Create();
			Write(*Buff, MID);
			Write(*Buff, res);

			//////////////////////////////////////////////////////////////////////////
			Write(*Buff, RequestID);
			m_SessionPtr->Send(Buff);
		}

		void CientProxy::OnNewProject(const Result& res, const ProjectId& Id, ID RequestID)
		{
			ITransportClientRep::MethodId MID = ITransportClientRep::eOnCreateProject;
			ByteBufferPtr Buff = ByteBuffer::Create();
			Write(*Buff, MID);
			Write(*Buff, res);
			Write(*Buff, Id);
			//////////////////////////////////////////////////////////////////////////
			Write(*Buff, RequestID);
			m_SessionPtr->Send(Buff);
		}

		void CientProxy::OnUploadResource(const Result& res, ID ProjId, const std::string& fileHash, const std::string& Name, ID RequestID)
		{
			BOOST_LOG_SEV(slg, normal) << "OnUploadResource fileHash :   " << fileHash;
			ITransportClientRep::MethodId MID = ITransportClientRep::eOnUploadFile;
			ByteBufferPtr Buff = ByteBuffer::Create();
			Write(*Buff, MID);
			Write(*Buff, res);

			Write(*Buff, ProjId);
			Write(*Buff, fileHash);
			Write(*Buff, Name);
			//////////////////////////////////////////////////////////////////////////
			Write(*Buff, RequestID);
			m_SessionPtr->Send(Buff);
		}

		void CientProxy::OnDownloadResource(const Result& res, const ResourceData& data, ByteBufferPtr body, ID RequestID)
		{
			BOOST_LOG_SEV(slg, normal) << "OnDownloadResource fileHash :   " << data.Resource.Hash;
			ITransportClientRep::MethodId MID = ITransportClientRep::eOnGetFile;
			ByteBufferPtr Buff = ByteBuffer::Create();
			Write(*Buff, MID);
			Write(*Buff, res);

			Write(*Buff, data);
			Write(*Buff, *body);
			//////////////////////////////////////////////////////////////////////////
			Write(*Buff, RequestID);
			m_SessionPtr->Send(Buff);

		}

		void CientProxy::OnGetProjectList(const Result& res, const ProjectInfoV& pr, ID RequestID)
		{
			BOOST_LOG_SEV(slg, normal) << "OnGetProjectList project count =    " << pr.size();
			ITransportClientRep::MethodId MID = ITransportClientRep::eOnGetProjectLList;
			ByteBufferPtr Buff = ByteBuffer::Create();
			Write(*Buff, MID);

			Write(*Buff, pr);

			Write(*Buff, RequestID);
			m_SessionPtr->Send(Buff);


		}

		void CientProxy::OnGetUserList(const Result& res, const UserInfoV& ui, ID RequestID)
		{
			ITransportClientRep::MethodId MID = ITransportClientRep::eOnGetUserList;
			ByteBufferPtr Buff = ByteBuffer::Create();
			Write(*Buff, MID);

			Write(*Buff, ui);

			Write(*Buff, RequestID);
			m_SessionPtr->Send(Buff);


		}

	}//!namespace transport

}//!namespace survey
