#include "stdafx.h"

#include <boost/timer.hpp>
#include <boost/chrono.hpp>
#include <boost/asio.hpp>


#include "server_side.h"
#include "project_manager.h"


//#include <boost/asio/strand.hpp>


#include "../../Include/transport/survey_serialization.h"

namespace survey
{
	namespace transport
	{


		void ConnectionManager::OnConnect(ConnectionPtr NewSession)
		{
			{
				Lock Locker(m_mutex);
				ID NewId = m_ID_Generator.GetID();
				//SessionPtr NewSession = Session::Create(IO_Handler::m_IO, NewId);
				NewSession->SetID(NewId);
				if (m_SessionContainer.size() <= NewId)
				{
					m_SessionContainer.resize(NewId + 1);
				}

				m_SessionContainer[NewId] = NewSession;
				ServerStub<>* NewStub = new ServerStub<>;	   //TODO additional init for ServerStub 

				NewStub->Init(NewSession);


				NewSession->SetEventHandler(this);

				NewSession->SetWorkManager(m_WorkManagerPtr);

				NewStub->Handler().SetServiceAccess(m_CommonServiceAccessPtr);

				//#��� �������, ����� ��� ����������
				NewSession->SetWillignessHandler(boost::bind(&ConnectionManager::OnConnectWilligness, this, _1));
			}


			NewSession->ServerStart();
		}

		void ConnectionManager::OnError(const std::string msg, const boost::system::error_code& Error, ID Id)
		{
			BOOST_LOG_NAMED_SCOPE("ConnectionManager: OnError");
			
			std::cout << msg;
			if (Error)
			{
				std::cout << Error.message();
				
				ConnectionPtr SessPtr = GetSession(Id);
				if (SessPtr)
				{
					BOOST_LOG_SEV(slg, error) << " connection: " << SessPtr->GetID() << " - " << Error.message();
					SessPtr->Disconnect();
				}
				else
				{
					BOOST_LOG_SEV(slg, error) << "unknown connection - " << Error.message();
				}
			}
			std::cout << std::endl;
		}

		void ConnectionManager::OnDisconnect(ConnectionPtr SPtr)
		{
			Lock Locker(m_mutex);
			if (SPtr)
			{
				ID Id = SPtr->GetID();
				m_SessionContainer[Id].reset();
				m_ID_Generator.RemoveID(Id);
			}
		}

		survey::transport::ConnectionPtr ConnectionManager::CreateSession()
		{
			ConnectionPtr ret;
			if (m_Settings.GetConnectionType() == eNative)
			{
				ret = Connection::Create(IO_Handler::m_IO);
			}
			else
			{
				ret = Connection::CreateCrypt(IO_Handler::m_IO, m_CryptContext.Context());
			}
			return ret;
		}

		void ConnectionManager::Init(const TransportSettings& settings)
		{
			BOOST_LOG_NAMED_SCOPE("SessionManager: Init");
			try
			{

				m_Settings = settings;
				//# �������� ������� ���������, ���� ������� ������������� ssl � ������� � ������ ��� ����������.

				if (m_Settings.GetConnectionType() == eSSL)
				{
					m_CryptContext.Init();
				}
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
				throw;
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
				throw;
			}
		}

		void ConnectionManager::OnConnectWilligness(ConnectionPtr SPtr)
		{
			SPtr ->DoRead();
		}


		void ServerCore::Init(const ServerSettings& SettingsObj, const std::string& rootDir)
		{
			BOOST_LOG_NAMED_SCOPE("ServerCore: Init");
			try
			{
				m_ThreadManagerPtr = boost::make_shared<ThreadManager>();

				m_ConnectionManager.Init(m_ThreadManagerPtr, 1);

				m_ConnectionManager.m_WorkerPtr->Init(SettingsObj.m_TransportSettings);

				m_WorkManager.Init(m_ThreadManagerPtr, 2);

				m_TCP_Acceptor.Init(m_ThreadManagerPtr, 1);

				m_TCP_Acceptor.m_WorkerPtr->SetHandler(m_ConnectionManager.m_WorkerPtr.get());


				m_ConnectionManager.m_WorkerPtr->SetWorkManager(m_WorkManager.m_WorkerPtr);

				m_CommonServiceAccessPtr = boost::make_shared<CommonServiceAccess>();

	
				SessionManager* sessionManager = new SessionManager;
				m_CommonServiceAccessPtr->SetSessionManager(SessionManagerPtr(sessionManager));


				DALImpl* DAL = new DALImpl;
				IDALPtr DALPtr(DAL);


				DAL->Init(SettingsObj.m_DBSettings);

				bool ret = DAL->Connect();

				if (ret)
				{
					m_CommonServiceAccessPtr->SetDAL(DALPtr);

					m_ProjectManagerPtr = boost::make_shared<server::ProjectManager>();

					fs::path root = fs::path(rootDir)/SettingsObj.m_FileStorage;
					m_ProjectManagerPtr->Init(DALPtr, root.string());

					m_CommonServiceAccessPtr->SetPM(m_ProjectManagerPtr);

					m_ConnectionManager.m_WorkerPtr->SetServiceAccess(m_CommonServiceAccessPtr);
					///

					m_TCP_Acceptor.m_WorkerPtr->Start(atoi(SettingsObj.m_TransportSettings.GetPort().c_str()));
				}
				else
				{
					BOOST_LOG_SEV(slg, critical) << "DAL connect failed " ;
				}
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
				throw;
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
				throw;
			}
		}

		void ServerCore::Stop()
		{
			if (m_TCP_Acceptor.m_WorkerPtr)
			{
				m_TCP_Acceptor.m_WorkerPtr->Stop();
			}
		}


		void ServerCryptContext::Init()
		{
			BOOST_LOG_NAMED_SCOPE("ServerCryptContext: Init");
			try
			{
				CryprContextManager::m_Context->set_options(
					boost::asio::ssl::context::default_workarounds
					| boost::asio::ssl::context::no_sslv2
					| boost::asio::ssl::context::single_dh_use);


				CryprContextManager::m_Context->set_password_callback(boost::bind(&ServerCryptContext::get_password, this));
				CryprContextManager::m_Context->use_certificate_chain_file("user.crt");
				CryprContextManager::m_Context->use_private_key_file("user.key", boost::asio::ssl::context::pem);
				CryprContextManager::m_Context->use_tmp_dh_file("dh2048.pem");
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
				throw;
			}
		}

	}//!namespace transport

}//!namespace survey
