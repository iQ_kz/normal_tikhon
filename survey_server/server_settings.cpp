#include "stdafx.h"
#include "server_settings.h"

#pragma warning(push)
#pragma warning(disable : 4512)
#include <boost/program_options.hpp>
#pragma warning(pop)
#include <fstream>
#include <boost/filesystem.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;


bool survey::DBSettings::Load(std::string FileName)
{
	bool ret = false;
	po::options_description desc("Allowed options");
	std::string ConnnectionTypestr;

	desc.add_options()
		("database.host", po::value<std::string>(&m_Host)->default_value("localhost"), "DB server host")
		("database.port", po::value<int>(&m_Port)->default_value(3306), "Port")
		("database.login", po::value<std::string>(&m_Login)->default_value("root"), "login")
		("database.password", po::value<std::string>(&m_Password)->default_value("1"), "password")
		("database.name", po::value<std::string>(&m_DBName)->default_value("test"), "DB name")
		;

	std::ifstream ifs(FileName.c_str());

	if (ifs)
	{
		po::variables_map vm;
		po::store(po::parse_config_file(ifs, desc, true), vm);
		po::notify(vm);
		ret = true;
	}

	return ret;
}

bool survey::transport::ServerSettings::Load(std::string FileName)
{
	bool ret = m_TransportSettings.Load(FileName);
	ret &= m_DBSettings.Load(FileName);


	po::options_description desc("Allowed options");
	std::string ConnnectionTypestr;

	desc.add_options()
		("server.storage", po::value<std::string>(&m_FileStorage)->default_value("test"), "file_storage")

	
		;

	std::ifstream ifs(FileName.c_str());

	if (ifs)
	{
		po::variables_map vm;
		po::store(po::parse_config_file(ifs, desc, true), vm);
		po::notify(vm);
		ret &= true;
	}


	return ret;
}
