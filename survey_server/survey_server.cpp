// survey_server.cpp: ���������� ����� ����� ��� ����������� ����������.
//
//#define 
#include "stdafx.h"

#include <fstream>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/make_shared_object.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include "../include/common_types.h"
#include "../include/enum_convert.h"
#include <boost/algorithm/string/trim.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>

namespace fs = boost::filesystem;

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;

#include "../include/logger.h"
#include "server_side.h"

using namespace  survey;
using namespace  survey::transport;



class CommandProcessor
{
public:

	enum ECommands{ eSetLogLevel, eGeLogLevel, eStat, eGetTransportSettings, eShutdown, eShowCommand, eUnknownCommand };

	typedef std::unordered_map <std::string, ECommands> Strin2Command;

	CommandProcessor(){ InitCommands(); }
	void SetCore(ServerCore* Core)
	{
		m_Core = Core;
	}

	void InitCommands()
	{
		m_Strin2Command["sll"] = eSetLogLevel;
		m_Strin2Command["gll"] = eGeLogLevel;
		m_Strin2Command["s"] = eStat;
		m_Strin2Command["gts"] = eGetTransportSettings;
		m_Strin2Command["sd"] = eShutdown;
		m_Strin2Command["sc"] = eShowCommand;
		m_Strin2Command["h"] = eShowCommand;
		m_Strin2Command["help"] = eShowCommand;
	}


	void SetLogLevel()
	{
		while (true)
		{
			std::cout << "Current Log level: " << Logger::LogLevel2String(Logger::GetLogLevel()) << std::endl;
			std::cout << "Select Log level: " << std::endl;

			std::cout << "1: normal" << std::endl;
			std::cout << "2: notification" << std::endl;
			std::cout << "3: warning" << std::endl;
			std::cout << "4: error" << std::endl;
			std::cout << "5: critical" << std::endl;
			std::cout << "6: skip" << std::endl;
			std::string c;
			std::cin >> c;
			if (c == "6")
			{
				return;
			}

			int v = atoi(c.c_str()) - 1;
			std::cout << v << "...   ";
			--v;
			if (v >= 0 && v<UnknownSeverity)
			{
				SeverityLevel  l = (SeverityLevel)v;
				Logger::SetFilter(l);
				std::cout << "Current Log level: " << Logger::LogLevel2String(Logger::GetLogLevel()) << std::endl;
			}
			else
			{
				std::cout  << "incorrect input" << std::endl;;
			}
		}
	}



	void  ShowLogLevel()
	{
		std::cout << "Current Log level: " << Logger::LogLevel2String(Logger::GetLogLevel()) << std::endl;
	}

	void ShowStat()
	{
		ServerCore::Stat stat = m_Core->GetStat();
		std::cout << "Connection count:" << stat.ConnectionCount << std::endl;
	}


	void ShowConnectionType(EConnectionType ConnType)
	{
		std::cout << "Connection Type:   " ;		
		switch (ConnType)
		{
		case survey::eNative:
			std::cout << "Native, without encryption";
			break;
		case survey::eSSL:
			std::cout << "SSL boost realization";
			break;
		case survey::UnknownConnType:
			std::cout << "wrong connection type";
			break;
		default:
			break;
		}

		std::cout << std::endl;
	}

	void ShowTransportSettings()
	{
		const TransportSettings& trs = m_Core->GetTransportSettings();
		std::cout << "Transport settings: " << std::endl;

		std::cout << "port: " << trs.GetPort() << std::endl;

		EConnectionType ConnType = trs.GetConnectionType();
		ShowConnectionType(ConnType);
	}

	void Shutdown()
	{
		m_Core->Stop();
	}

	void ShowCommands()
	{
		std::cout
			<< "sll - Set Log Level" << std::endl
			<< "gll - Get Log Level" << std::endl
			<< "s   - Stat" << std::endl
			<< "gts - Get Transport Settings" << std::endl
			<< "sd  - Shutdown" << std::endl
			<< "sc or 'h' or 'help'  - show commands" << std::endl;
	}

	ECommands GetCommand(std::string& CommandStr)		  
	{
		BOOST_LOG_NAMED_SCOPE("command processor: GetCommand");
		ECommands ret = eUnknownCommand;
		std::cin >> CommandStr;
		

		BOOST_LOG_SEV(slg, critical) << "command: " << CommandStr;

		boost::trim_copy(CommandStr);
		auto it = m_Strin2Command.find(CommandStr);
		if (it != m_Strin2Command.end())
		{
			ret = it->second;
		}
		else
		{
			std::cout << "unknown command" << std::endl;
		}
			return ret;
	}

	void Run(std::string RootDir)
	{
		std::cout << "Command line processor" << std::endl;
		std::string CommandStr;
		ShowCommands();
		while (true)
		{
			
			std::cout << "type command:"  ;

			ECommands CommandCode = GetCommand(CommandStr);
			switch (CommandCode)
			{
			case eSetLogLevel:
				SetLogLevel();
				break;
			case eGeLogLevel:
				ShowLogLevel();
				break;
			case eStat:
				ShowStat();
				break;
			case eGetTransportSettings:
				ShowTransportSettings();
				break;
			case eShutdown:
				Shutdown();
				return;
				break;
			case eShowCommand:
				ShowCommands();
				break;
			case eUnknownCommand:
				std::cout << "Unknown command" << std::endl;
				ShowCommands();
				break;
			default:
				std::cout << "Unknown command" << std::endl;
				ShowCommands();

				break;
			}
		}
	}

private:
	Strin2Command m_Strin2Command;
	ServerCore* m_Core;
	src::severity_logger< SeverityLevel > slg;
};


int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");


	fs::path p = fs::complete(fs::path(argv[0]));
	p = p.branch_path();

	std::string RootDir = p.string();

	std::cout << "RootDir = " << RootDir << std::endl;


	Logger::Init("Transport");

	src::severity_logger< SeverityLevel > slg;
	BOOST_LOG_SEV(slg, critical) << "Start";


	ServerCore TrCore;
	ServerSettings s;
	
	fs::path ini_path = p/ "survey_server.ini";

	std::cout << "RootDir: " << RootDir << std::endl;

	std::cout << "ini file: " << ini_path.string() << std::endl;

	BOOST_LOG_SEV(slg, critical) << "ini file:" << ini_path.string();

	bool res = s.Load(ini_path.string());

	if (res)
	{
		std::cout << "load settings: ok"<< std::endl;
		BOOST_LOG_SEV(slg, critical) << "load settings: ok";
	}
	else
	{
		std::cout << "load settings: failed" << std::endl;
		BOOST_LOG_SEV(slg, critical) << "load settings: failed";
	}
	


	TrCore.Init(s, RootDir);
	

	BOOST_LOG_SEV(slg, critical) << "Core started";

	CommandProcessor CProc;
	CProc.SetCore(&TrCore);

	CProc.Run(RootDir);

	BOOST_LOG_SEV(slg, critical) << "-------------------------------------------";
	BOOST_LOG_SEV(slg, critical) << "end";
	std::cin.get();
																																										    
	
	return 0;
}

