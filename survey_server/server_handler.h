#pragma once

#include <string>
#include "../include/logger.h"
#include "client_proxy.h"
#include "common_service_access.h"


namespace survey
{
	namespace transport
	{

	

		class ServerHandler //: public IClient2Serv
		{
		public:
			ServerHandler() {}

			typedef boost::unique_lock<boost::mutex> Lock;

			void Login(const std::string& login, const std::string&  password, ID RequestID);

			void CreateProject(const std::string& Name, ID RequestID);


			void UploadResource(const ResourceData& data, const ByteBuffer& buff, ID RequestID);

			void DownloadResource(const ResourceData& data, ID RequestID);

			void Init(ConnectionPtr SPtr);

			void SetServiceAccess(CommonServiceAccessPtr SA);
			void Logout();

			virtual void GetProjectList(ID RequestID);

			virtual void GetUserList(ID RequestID);
		private:

			ConnectionPtr m_ConnectionPtr;
			CientProxy m_proxy;
			CommonServiceAccessPtr m_CommonServiceAccessPtr;
			src::severity_logger< SeverityLevel > slg;
			SessionPtr m_Session;
			boost::mutex m_mutex;
		};


	}//!namespace transport

}//!namespace survey
