#pragma once

#include <string>
#include <unordered_map>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include "../include/transport/transport_types.h"
#include "../include/logger.h"


namespace survey
{
	namespace transport
	{


		class SessionManager
		{
		public:
			
			typedef boost::unique_lock<boost::mutex>  Lock;
			typedef std::unordered_map<SessionId, SessionPtr> SessionContainer;

			SessionPtr AddSession(const SessionId& Id, ConnectionPtr Connn)
			{
				BOOST_LOG_NAMED_SCOPE("SessionManager: AddSession");
				Lock Locker(m_Mutex);
				SessionPtr ret;
				auto it = m_SessionContainer.find(Id);
				if (it == m_SessionContainer.end())
				{
					ret = boost::make_shared< Session>(Id, Connn);
					BOOST_LOG_SEV(slg, error) << " session: " << Id;
					m_SessionContainer[Id] = ret;
				}
				else
				{
					BOOST_LOG_SEV(slg, error) << " session: " << Id << "already exists";
				}
				return ret;
			}

			void RemoveSession(const SessionId& Id)
			{
				BOOST_LOG_NAMED_SCOPE("SessionManager: RemoveSession");
				Lock Locker(m_Mutex);
				m_SessionContainer.erase(Id);
				BOOST_LOG_SEV(slg, error) << " session: " << Id ;
			}

			SessionPtr GetSession(const SessionId& Id)
			{
				Lock Locker(m_Mutex);
				SessionPtr ret;

				auto it = m_SessionContainer.find(Id);
				if (it != m_SessionContainer.end())
				{
					ret = it->second;
				}

				return ret;
			}

		private:
			SessionContainer m_SessionContainer;
			boost::mutex m_Mutex;
			src::severity_logger< SeverityLevel > slg;
		};

		typedef boost::shared_ptr <SessionManager> SessionManagerPtr;

	}//!namespace transport

}//!namespace survey
