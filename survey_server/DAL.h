#pragma once
#include "../../include/common_types.h"
#include "../../include/mysql_connector.h"
#include <boost/shared_ptr.hpp>
#include  <sstream>
#include "server_settings.h"
#include "../include/transport/transport_types.h"
#include <iostream>
#include "../include/logger.h"

using namespace survey::transport;

namespace survey
{

	/*class ResourceData;
	typedef unsigned int UserState;
	typedef unsigned int Role;
	struct UserInfo
	{
		ID Id;
		std::string Name;
		std::string Login;
		std::string Password;
		UserState State;
	};

	typedef std::vector <UserInfo> UserInfoV;

	struct ProjectDBInfo
	{
		ID Id;
		std::string Name;
		ID  Autor;
		time_t CreateTime;
		ProjectDBInfo() :Id(0), Autor(0){}

		ProjectDBInfo(const ProjectDBInfo& obj)
		{
			operator = (obj);
		}

		ProjectDBInfo& operator =(const ProjectDBInfo& obj)
		{
			Id = obj.Id;
			Name = obj.Name;
			Autor = obj.Autor;
			CreateTime = obj.CreateTime;
			return *this;
		}

	};
	typedef std::vector <ProjectDBInfo> ProjectDBInfoV;*/

	struct IDAL
	{
		enum  ResType
		{
			eSuccess = 0, eFailed = 1, eEmpty = 2,
		};

		virtual ~IDAL(){}
		virtual ResType  GetUser(UserInfo& Info) = 0;
		virtual ResType  CreateProject(const std::string& Name, const ID& OwnerId, ProjectId& Id) = 0;
		virtual ResType RegisterResouce(const ID& OwnerId, const transport::ResourceData& data) = 0;
		virtual ResType GetResources(ResourceInfoV& resources, ProjectId&  Id, ID  UploaderId) = 0;
		virtual ResType GetProjects(ProjectInfoV& projects) = 0;
		virtual ResType GetUsers(UserInfoV& users) = 0;

	};
	typedef boost::shared_ptr <IDAL>		IDALPtr;

	class DALImpl :public  IDAL
	{
	public:
		typedef MySqlConnector::ResultPtr ResultPtr;
		typedef boost::unique_lock<boost::mutex> Lock;

		void Init(const DBSettings& Settings)
		{
			m_Settings = Settings;
		}

		bool  Connect()
		{
			return m_Conn.Connect(m_Settings.Host(), m_Settings.Port(), m_Settings.Login(), m_Settings.Password(), m_Settings.DBName());
		}


		virtual ResType  GetUser(UserInfo& Info)
		{
			BOOST_LOG_NAMED_SCOPE("DALImpl: GetUser");

			Lock lock(m_mutex);
			ResType ret = eFailed;
			try
			{
				BOOST_LOG_SEV(slg, critical) << " begin ";

				std::stringstream ss;
				ss << "SELECT * FROM userlist where  login = '" << Info.Login << "'";
				std::string q = ss.str();
				ResultPtr res = m_Conn.Query(q);

				MYSQL_ROW row;
				if (res->GetRow(row))
				{
					Info.Id = atoi(row[0]);
					Info.Name = row[1];
					Info.Login = row[2];
					Info.Password = row[3];
					Info.State = atoi(row[4]);
				}
				ret = MakeRes(res);

			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
			}
			return ret;
			
		}

		virtual ResType  CreateProject(const std::string& Name, const ID& OwnerId, ProjectId& Id)
		{
			BOOST_LOG_NAMED_SCOPE("DALImpl: CreateProject");
			Lock lock(m_mutex);
			ResType ret = eFailed;
			try
			{
				BOOST_LOG_SEV(slg, critical) << " begin ";

				std::stringstream ss;
				ss << "INSERT  INTO projectlist  (name,autor, create_time) values('" << Name << "', '" << OwnerId << "', now()) ";
				std::string q = ss.str();
				ResultPtr res = m_Conn.Query(q); 
				my_ulonglong lastId = m_Conn.insert_id();

				Id = static_cast<ProjectId>(lastId);

				ret  = MakeRes(res);
				if (eSuccess == ret)
				{
					Role RoleValue = 123;//TODO stub
					std::stringstream ss;
					ss << "INSERT INTO roles(userid, projectid, role) values( " << OwnerId << ", " << Id << ", " << RoleValue<<" )";
					std::string q = ss.str();
					ResultPtr res = m_Conn.Query(q);
					ret = MakeRes(res);
				}
				else 
				{
					int code = m_Conn.ErrorCode();
					std::cout << "DB err =  " << code << std::endl;
					if (eEmpty == ret)
					{
						ret = eSuccess;
					}
				}
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();

			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";

			}
			return ret;
		}


		virtual ResType RegisterResouce(const ID& OwnerId, const transport::ResourceData& data)
		{
			Lock lock(m_mutex);
			BOOST_LOG_NAMED_SCOPE("DALImpl: RegisterResouce");
			ResType ret = eFailed;
			std::stringstream ss;
			ss << "INSERT  INTO resources  (hash,name, uploader_id, project_id, upload_time) values('" << data.Resource.Hash << "',   '" << data.Resource.Name << "',   '" << OwnerId << "',  '" << data.Id << "' , now() )";

			std::string q = ss.str();

			try
			{
				BOOST_LOG_SEV(slg, critical) << " begin ";

				ResultPtr res;
			
				res = m_Conn.Query(q);

				ret = MakeRes(res);
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what() << "; " << q;
				
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception" << "; " << q;
				
			}
	
			return ret;
		}


		virtual ResType GetResources(ResourceInfoV& resources, ProjectId&  Id, ID  UploaderId)
		{
			Lock lock(m_mutex);
			BOOST_LOG_NAMED_SCOPE("DALImpl: GetResources");
			ResType ret = eFailed;
			
			try
			{

				BOOST_LOG_SEV(slg, critical) << " begin ";

				std::stringstream ss;
				ss << "SELECT * FROM resources where resources.project_id = '" << Id << "'";
				std::string q = ss.str();
				ResultPtr res = m_Conn.Query(q);

				MYSQL_ROW row;

				size_t rowCount = res->GetRowCount();//TODO

				resources.resize(rowCount);

				for (size_t i = 0; i < rowCount&& res->GetRow(row); ++i)
				{
					ResourceInfo& Current = resources[i];
					Current.Hash = row[1];
					Current.Name = row[2];
				}

				ret =  MakeRes(res);
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
			}
			return ret;
		}

		virtual ResType GetProjects(ProjectInfoV& projects)
		{
			Lock lock(m_mutex);
			BOOST_LOG_NAMED_SCOPE("DALImpl: GetProjects");
			BOOST_LOG_SEV(slg, critical) << " begin ";
			ResType ret = eFailed;
			try 
			{

				std::stringstream ss;
				ss << "SELECT projectlist.id,  userlist.name as 'autor', projectlist.name, create_time from projectlist left join userlist on userlist.id = projectlist.autor" ;
				std::string q = ss.str();
				ResultPtr res = m_Conn.Query(q);

				MYSQL_ROW row;

				size_t rowCount = res->GetRowCount();//TODO

				projects.resize(rowCount);

				for (size_t i = 0; i < rowCount&& res->GetRow(row); ++i)
				{
					ProjectInfo& Current = projects[i];
					Current.Id = atoi(row[0]);
					Current.Name = row[1];
					Current.Autor = atoi(row[2]);
					Current.CreateTime = atoi(row[3]);
				}

				ret =  MakeRes(res);

			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
			}
			return ret;
		}


		virtual ResType GetUsers(UserInfoV& users)
		{
			Lock lock(m_mutex);
			BOOST_LOG_NAMED_SCOPE("DALImpl: GetUsers");

			BOOST_LOG_SEV(slg, critical) << " begin " ;

			ResType ret = eFailed;
			try
			{
				std::stringstream ss;
				ss << "SELECT id, name,  autor, UNIX_TIMESTAMP(create_time) FROM projectlist;";
				std::string q = ss.str();
				ResultPtr res = m_Conn.Query(q);

				MYSQL_ROW row;

				size_t rowCount = res->GetRowCount();//TODO

				users.resize(rowCount);

				for (size_t i = 0; i < rowCount&& res->GetRow(row); ++i)
				{
					UserInfo& Current = users[i];
					Current.Id = atoi(row[0]);
					Current.Name = row[1];
					Current.Login = row[2];
					Current.Password = row[3];
					Current.State = atoi(row[4]);
				}
				ret = MakeRes(res);

			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
			}			std::stringstream ss;
			return ret;
		}


	private:

		ResType MakeRes(ResultPtr res)
		{
			ResType ret = eFailed;
			if (m_Conn.Success())
			{
				assert(res != NULL);

				if (res->GetRowCount() == 0)
				{
					ret = eEmpty;
				}
				else
				{
					ret = eSuccess;
				}
			}
			return ret;
		}

		DBSettings m_Settings;
		MySqlConnector m_Conn;
		src::severity_logger< SeverityLevel > slg;
		boost::mutex m_mutex;
	};


}//!namespace survey