#include "stdafx.h"
#include "server_handler.h"
#include "server_side.h"


namespace survey
{
	namespace transport
	{

		void ServerHandler::Login(const std::string& login, const std::string& password, ID RequestID)
		{
			Lock lock(m_mutex);
			Result res;
			IDALPtr DAL = m_CommonServiceAccessPtr->GetDAL();
			if (DAL)
			{

				UserInfo User;
				User.Login = login;
				IDAL::ResType r = DAL->GetUser(User);
				switch (r)
				{
				case survey::IDAL::eSuccess:
				{
					if (password == User.Password)
					{
						res.RetCode = Success;
					
						SessionManagerPtr SM = m_CommonServiceAccessPtr->GetSessionManager();
						if (SM)
						{
							BOOST_LOG_SEV(slg, normal) << " Login:  User.Id = " << User.Id;
							m_Session = SM->AddSession(User.Id, m_ConnectionPtr);
						}
					}
					else
					{
						res.RetCode = WrongPassword;
					}
				}
				break;
				case survey::IDAL::eFailed:
					res.RetCode = UnknownError;
					break;
				case survey::IDAL::eEmpty:
					res.RetCode = UnknownLogin;
					break;
				default:
					break;
				}
			}
			

			m_proxy.OnLogin(res, RequestID);
		}

		void ServerHandler::Init(ConnectionPtr SPtr)
		{
			Lock lock(m_mutex);
			m_ConnectionPtr = SPtr;
			m_proxy = CientProxy(m_ConnectionPtr);
		}

		void ServerHandler::CreateProject(const std::string& Name,  ID RequestID)
		{
			Lock lock(m_mutex);
			ProjectId PrId = 0;
		
			server::ProjectManagerPtr PMPtr = m_CommonServiceAccessPtr->GetPM();
			Result res = PMPtr->CreateProject(Name, PrId, m_Session->Id);
		
			m_proxy.OnNewProject(res, PrId, RequestID);
		}

		void ServerHandler::SetServiceAccess(CommonServiceAccessPtr SA)
		{
			m_CommonServiceAccessPtr = SA;
		}

		void ServerHandler::Logout()
		{
			Lock lock(m_mutex);
			BOOST_LOG_NAMED_SCOPE("ServerHandler::Logout");
			if (m_ConnectionPtr)
			{
				SessionManagerPtr SM = m_CommonServiceAccessPtr->GetSessionManager();
				if (SM)
				{
					if (m_Session)
					{
						BOOST_LOG_SEV(slg, normal) << " RemoveSession" << m_Session->Id;
						SM->RemoveSession(m_Session->Id);
						m_Session.reset();
					}
				}
			}
			else
			{
				BOOST_LOG_SEV(slg, critical) << " !m_ConnectionPtr" ;
			}
		}

		void ServerHandler::UploadResource(const ResourceData& data, const ByteBuffer& buff, ID RequestID)
		{
			server::ProjectManagerPtr PMPtr = m_CommonServiceAccessPtr->GetPM();
			Result res = PMPtr->UploadResource(data, buff, m_Session->Id);
			m_proxy.OnUploadResource(res, data.Id,data.Resource.Hash, data.Resource.Name, RequestID);
		}

		void ServerHandler::DownloadResource(const ResourceData& data, ID RequestID)
		{
			Lock lock(m_mutex);
			server::ProjectManagerPtr PMPtr = m_CommonServiceAccessPtr->GetPM();
			ByteBufferPtr Buff = ByteBuffer::Create();
			Result res = PMPtr->DownloadResource(data.Resource, *Buff);
			m_proxy.OnDownloadResource(res, data, Buff, RequestID);
		}

		void ServerHandler::GetProjectList(ID RequestID)
		{
			Lock lock(m_mutex);
			Result res;
			ProjectInfoV retData;
			IDALPtr DAL = m_CommonServiceAccessPtr->GetDAL();
			if (DAL)
			{
				IDAL::ResType r = DAL->GetProjects(retData);
				if (r == IDAL::eFailed)
				{
					res.RetCode = DBError;
				}
			}
			else
			{
				res.RetCode = CommonServerError;

			}

			m_proxy.OnGetProjectList(res, retData, RequestID);
		}

		void ServerHandler::GetUserList(ID RequestID)
		{
			Lock lock(m_mutex);
			Result res;
			UserInfoV retData;
			IDALPtr DAL = m_CommonServiceAccessPtr->GetDAL();
			if (DAL)
			{
				IDAL::ResType r = DAL->GetUsers(retData);
				
				if (r == IDAL::eFailed)
				{
					res.RetCode = DBError;
				}
			}
			else
			{
				res.RetCode = CommonServerError;
			}
			m_proxy.OnGetUserList(res, retData, RequestID);
		}

	}//!namespace transport

}//!namespace survey
