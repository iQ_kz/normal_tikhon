﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;


namespace SurveyTest
{
	/// <summary>
	/// Interaction logic for QuestionControl.xaml
	/// </summary>
	public partial class QuestionControl : UserControl, IDisposable
	{
		public QuestionControl(Canvas canvas, Survey.QuestionWrpp qst, HeaderControl hdrCtrl)
		{
			InitializeComponent();

			if (canvas == null || qst == null)
				throw new ArgumentException("Invalid arguments");

			m_QuestionData = new QuestionData(qst);
			m_HdrCtrl = hdrCtrl;

			m_Canvas = canvas;
			m_Canvas.Children.Add(this);

			OnHeaderDataChanged(null, null);
			m_QuestionData.m_Question.DesignElement.PropertyChanged += OnHeaderDataChanged;
		}

		public void Dispose()
		{
			m_QuestionData.m_Question.DesignElement.PropertyChanged -= OnHeaderDataChanged;

			m_HdrCtrl = null;
			m_Canvas = null;
			m_QuestionData.Dispose();
			m_QuestionData = null;

			GC.SuppressFinalize(this);
		}

		~QuestionControl()
		{
			Dispose();
		}

		public void OnHeaderDataChanged(object sender, PropertyChangedEventArgs e)
		{
			// reset data context
			DataContext = m_QuestionData;
			this.RenderTransform = new RotateTransform(m_QuestionData.AngleRotation);

			// reset canvas position
			Canvas.SetLeft(this, m_QuestionData.DesignElementLeft);
			Canvas.SetTop(this, m_QuestionData.DesignElementTop);

			m_HdrCtrl.OnHeaderDataChanged(null, null);
		}

		// fields
		public QuestionData  m_QuestionData = null;
		Canvas m_Canvas = null;
		HeaderControl m_HdrCtrl = null;

		bool m_IsDragging = false;

		// start rotation
		private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
		{
			m_IsDragging = true;
		}

		// end rotation
		private void Rectangle_MouseUp(object sender, MouseButtonEventArgs e)
		{
			m_IsDragging = false;
		}

		// mouse move rotation
		private void Rectangle_MouseMove(object sender, MouseEventArgs e)
		{
			if (!m_IsDragging)
				return;
			
			Point currentPosition = e.GetPosition(this.Parent as UIElement);
			Point centrPoint = m_QuestionData.CentrPoint();

			double angle = Math.Atan2(currentPosition.X - centrPoint.X, centrPoint.Y - currentPosition.Y);
			if (double.IsNaN(angle))
				return;

			m_QuestionData.AngleRotation = Survey.Helper.Converter.RadToGradus(angle);
		}
	}

	public class QuestionData : INotifyPropertyChanged, IDisposable
	{
		public QuestionData(Survey.QuestionWrpp quest)
		{
			if (quest == null)
				throw new NullReferenceException("hdr");

			m_Question = quest;
		}

		public void Dispose()
		{
			m_Question = null;
		}

		~QuestionData()
		{
			m_Question = null;
		}

		// Notify prop changed
		public event PropertyChangedEventHandler PropertyChanged;

		void NotifyPropertyChanged(String arg)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(arg));
		}

		// fields
		public Survey.QuestionWrpp m_Question = null;

		public Point CentrPoint()
		{
			Survey.CoordDatWrpp absoluteCoord = m_Question.DesignElement.AbsoluteCoordinate;
			return new Point(absoluteCoord.m_Left + absoluteCoord.m_Width / 2.0, absoluteCoord.m_Top + absoluteCoord.m_Height / 2.0);
		}

		public double DesignElementLeft
		{
			get { return m_Question.DesignElement.AbsoluteCoordinate.m_Left; }
			set
			{
				Survey.CoordDatWrpp absoluteCoord = m_Question.DesignElement.AbsoluteCoordinate;
				
				m_Question.DesignElement.AbsoluteCoordinate
					= new Survey.CoordDatWrpp(
						absoluteCoord.m_Top
						, value, absoluteCoord.m_Width
						, absoluteCoord.m_Height
						, absoluteCoord.m_Angle
						);
			}
		}

		public double DesignElementTop
		{
			get { return m_Question.DesignElement.AbsoluteCoordinate.m_Top; }
			set
			{
				Survey.CoordDatWrpp absoluteCoord = m_Question.DesignElement.AbsoluteCoordinate;

				m_Question.DesignElement.AbsoluteCoordinate
					= new Survey.CoordDatWrpp(
						value, absoluteCoord.m_Left
						, absoluteCoord.m_Width
						, absoluteCoord.m_Height
						, absoluteCoord.m_Angle
						);
			}

		}

		public double DesignElementWidth
		{
			get { return m_Question.DesignElement.AbsoluteCoordinate.m_Width; }
			set
			{
				Survey.CoordDatWrpp absoluteCoord = m_Question.DesignElement.AbsoluteCoordinate;

				m_Question.DesignElement.AbsoluteCoordinate 
					= new Survey.CoordDatWrpp(
						absoluteCoord.m_Top
						, absoluteCoord.m_Left
						, value, absoluteCoord.m_Height
						, absoluteCoord.m_Angle
						);
			}
		}

		public double DesignElementHeight
		{
			get { return m_Question.DesignElement.AbsoluteCoordinate.m_Height; }
			set
			{
				Survey.CoordDatWrpp absoluteCoord = m_Question.DesignElement.AbsoluteCoordinate;

				m_Question.DesignElement.AbsoluteCoordinate
					= new Survey.CoordDatWrpp(
						absoluteCoord.m_Top
						, absoluteCoord.m_Left
						, absoluteCoord.m_Width, value
						, absoluteCoord.m_Angle
						);
			}
		}

		public double AngleRotation
		{
			get { return Survey.Helper.Converter.RadToGradus(m_Question.DesignElement.AbsoluteCoordinate.m_Angle); }
			set
			{
				Survey.CoordDatWrpp absoluteCoord = m_Question.DesignElement.AbsoluteCoordinate;

				double validGradus = value;

				m_Question.DesignElement.AbsoluteCoordinate
					= new Survey.CoordDatWrpp(
						absoluteCoord.m_Top
						, absoluteCoord.m_Left
						, absoluteCoord.m_Width
						, absoluteCoord.m_Height
						, Survey.Helper.Converter.GradusToRad(validGradus)
						);
			}
		}

	}
}
