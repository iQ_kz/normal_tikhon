﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace MDI.Extensions
{   
   public static class WindowBehaviorExtension
   {

       private static void SaveParameters(this MDIWindow window)
       {          
           window.PreviousTop = window.Top;           
           window.PreviousLeft = window.Left;           
           window.PreviousWidth = window.Width;          
           window.PreviousHeight = window.Height;
       }

       private static void RestoreParameters(this MDIWindow window)
       {
           window.Top = window.PreviousTop;
           window.Left = window.PreviousLeft;
           window.Width = window.PreviousWidth;
           window.Height = window.PreviousHeight;
       }

      public static void Maximize(this MDIWindow window)
      {
         
            if (window.Container != null)
            { 
                window.SaveParameters();
                    
                window.Top = 0.0;
                window.Left = 0.0;

                Canvas.SetTop(window, window.Top);                
                Canvas.SetLeft(window, window.Left);                
           
                window.Width = window.Container.ActualWidth;
                window.Height = window.Container.ActualHeight;

                AnimateResize(window, window.Width, window.Height, true);
            }               
         
      }

      public static void Normalize(this MDIWindow window)
      {

         window.RestoreParameters();

         Canvas.SetTop(window, window.Top);
         Canvas.SetLeft(window, window.Left);
         
         AnimateResize(window, window.Width, window.Height, false);
         
      }

      public static void Minimize(this MDIWindow window)
      {         

         var index = window.Container.MinimizedWindowsCount;

         if (window.Container != null)
         window.SaveParameters();

         window.Top = window.Container.ActualHeight - 32;
         window.Left = index * 205;
         window.Width = 200;
         window.Height = 32;

         Canvas.SetTop(window, window.Top);
         Canvas.SetLeft(window, window.Left);
        
         AnimateResize(window, window.Width, window.Height, true);

         window.Tumblr.Source = window.CreateSnapshot();
              
      }

      private static void AnimateResize(MDIWindow window, double newWidth, double newHeight, bool lockWindow)
      {
         window.LayoutTransform = new ScaleTransform();
       
         var widthAnimation = new DoubleAnimation(window.ActualWidth, newWidth, new Duration(TimeSpan.FromMilliseconds(10)));         
         var heightAnimation = new DoubleAnimation(window.ActualHeight, newHeight, new Duration(TimeSpan.FromMilliseconds(10)));

         if (lockWindow == false)
         {
            widthAnimation.Completed += (s, e) => window.BeginAnimation(FrameworkElement.WidthProperty, null);
            heightAnimation.Completed += (s, e) => window.BeginAnimation(FrameworkElement.HeightProperty, null);
         }

         window.BeginAnimation(FrameworkElement.WidthProperty, widthAnimation, HandoffBehavior.Compose);
         window.BeginAnimation(FrameworkElement.HeightProperty, heightAnimation, HandoffBehavior.Compose);
      }

      public static void RemoveWindowLock(this MDIWindow window)
      {
         window.BeginAnimation(FrameworkElement.WidthProperty, null);
         window.BeginAnimation(FrameworkElement.HeightProperty, null);
      }     

   }
}