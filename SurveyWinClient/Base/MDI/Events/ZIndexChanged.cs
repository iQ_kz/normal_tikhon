﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MDI.Events
{
    public sealed class ZIndexChangedEventArgs : RoutedEventArgs
    {
        public int OldValue { get; private set; }
        public int NewValue { get; private set; }

        public ZIndexChangedEventArgs(RoutedEvent routedEvent, int oldValue, int newValue)
            : base(routedEvent)
        {
            this.NewValue = newValue;
            this.OldValue = oldValue;
        }
    }

    public delegate void ZIndexChangedRoutedEventHandler(object sender, ZIndexChangedEventArgs e);
}