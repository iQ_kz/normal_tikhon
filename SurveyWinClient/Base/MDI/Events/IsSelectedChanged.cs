﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MDI.Events
{
    public sealed class IsSelectedChangedEventArgs : RoutedEventArgs
    {
        public bool OldValue { get; private set; }
        public bool NewValue { get; private set; }

        public IsSelectedChangedEventArgs(RoutedEvent routedEvent, bool oldValue, bool newValue)
            : base(routedEvent)
        {
            this.NewValue = newValue;
            this.OldValue = oldValue;
        }
    }

    public delegate void IsSelectedChangedRoutedEventHandler(object sender, IsSelectedChangedEventArgs e);
}