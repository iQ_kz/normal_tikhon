﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using MDI;

namespace Base.Interfaces
{
    public interface IModeWindow : IMdiWindow
    {



        bool IsInvalid { get; set; }
        //bool CanClose { get; set; }

        //bool IsClosed { get; set; }

        //bool IsFocused { get; set; }      
        
    }
}
