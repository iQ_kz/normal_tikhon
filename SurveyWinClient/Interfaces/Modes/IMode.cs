﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Base.Interfaces
{
    public interface IMode
    {
        bool IsEnabled { get; set; }
        IModeMenu Menu { get; set; }
        IModeWindow Window { get; set; }
    }
}
