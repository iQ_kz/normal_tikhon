﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base.Interfaces
{
    interface IProjectWorkSpace
    {
        string Name { get; set; }
    }
}
