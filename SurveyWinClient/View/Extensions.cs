﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SurveyWinClient.View
{
    public static class RemoveChildHelper
    {
        public static void RemoveChild(this DependencyObject parent, UIElement child)
        {
            var panel = parent as Panel;
            if (panel != null)
            {
                panel.Children.Remove(child);
                return;
            }

            var decorator = parent as Decorator;
            if (decorator != null)
            {
                if (decorator.Child == child)
                {
                    decorator.Child = null;
                }
                return;
            }

            var contentPresenter = parent as ContentPresenter;
            if (contentPresenter != null)
            {
                if (contentPresenter.Content == child)
                {
                    contentPresenter.Content = null;
                }
                return;
            }

            var contentControl = parent as ContentControl;
            if (contentControl != null)
            {
                if (contentControl.Content == child)
                {
                    contentControl.Content = null;
                }
                return;
            }

            // maybe more
        }
    }
    public static class MathExtensions
    {
        public static Point RotatePointAroundPoint(Point toRotate, Point axis, double angle)
        {
            double radius = Math.Sqrt(Math.Pow(toRotate.X - axis.X, 2) + Math.Pow(toRotate.Y - axis.Y, 2));
            return new Point(axis.X + radius * Math.Cos(angle), axis.Y + Math.Sin(angle) * radius);
        }
    }
    public static class VisualTreeExtensions
    {
        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }
    }

    public static class ObservableCollectionExtensions
    {
        public static void Swap<T>(
           this ObservableCollection<T> collection,
           int index1, int index2)
        {
            if (index1 < 0 || index2 < 0)
                return;
            var indexes = new List<int> { index1, index2 };
            if (indexes[0] == indexes[1]) return;
            indexes.Sort();
            var values = new List<T> { collection[indexes[0]], collection[indexes[1]] };
            collection.RemoveAt(indexes[1]);
            collection.RemoveAt(indexes[0]);
            collection.Insert(indexes[0], values[1]);
            collection.Insert(indexes[1], values[0]);
        }
        public static void ChangePos<T>(
           this ObservableCollection<T> collection,
           int From, int To)
        {
            if (From == To) return;
            T element = collection[From];
            collection.RemoveAt(From);
            collection.Insert(To, element);
        }
    }
    public class TabSizeConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            TabControl tabControl = values[0] as TabControl;
            double width = ((double)values[1]-50) / tabControl.Items.Count;
            if (width > 200)
                width = 200;
            return (width <= 1) ? 0 : (width - 1);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
    public class TabMarginConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            TabControl tabControl = values[0] as TabControl;
            double width = (((double)values[1]) / tabControl.Items.Count)/100;
            Thickness temp = new Thickness(width, 0, 0, 0);
            return temp;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
