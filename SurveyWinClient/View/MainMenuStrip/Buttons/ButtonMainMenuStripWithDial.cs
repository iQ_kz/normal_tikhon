﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Survey;
using SurveyWinClient.ViewModel;

namespace SurveyWinClient.View
{
    class ButtonMainMenuStripWithDial : Button
    {   
        public static readonly DependencyProperty TextButton2Property =
        DependencyProperty.Register("TextButton2", typeof(string), typeof(ButtonMainMenuStripWithDial), new UIPropertyMetadata(null));
        public string TextButton2
        {
            get { return (string)GetValue(TextButton2Property); }
            set { SetValue(TextButton2Property, value); }
        }

        public ButtonMainMenuStripWithDial() : base() { }
        public static readonly DependencyProperty IconButton2Property =
        DependencyProperty.Register("IconButton2", typeof(ImageSource), typeof(ButtonMainMenuStripWithDial), new UIPropertyMetadata(null));
        public ImageSource IconButton2
        {
            get { return base.GetValue(IconButton2Property) as ImageSource; }
            set { SetValue(IconButton2Property, value); }
        }

        public static readonly DependencyProperty IconBorder2Property =
        DependencyProperty.Register("IconBorder2", typeof(ImageSource), typeof(ButtonMainMenuStripWithDial), new UIPropertyMetadata(null));
        public ImageSource IconBorder2
        {
            get { return base.GetValue(IconBorder2Property) as ImageSource; }
            set { SetValue(IconBorder2Property, value); }
        }

        public static readonly DependencyProperty DownMenu2Property =
        DependencyProperty.Register("DownMenu2", typeof(ImageSource), typeof(ButtonMainMenuStripWithDial), new UIPropertyMetadata(null));
        public ImageSource DownMenu2
        {
            get { return base.GetValue(DownMenu2Property) as ImageSource; }
            set { SetValue(DownMenu2Property, value); }
        }
    }
}
