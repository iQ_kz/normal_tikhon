﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Survey;
using SurveyWinClient.ViewModel;

namespace SurveyWinClient.View
{
    class ButtonMainMenuStrip : Button
    {
        public static readonly DependencyProperty TextButtonProperty =
        DependencyProperty.Register("TextButton", typeof(string), typeof(ButtonMainMenuStrip), new UIPropertyMetadata(null));
        public string TextButton
        {
            get { return (string)GetValue(TextButtonProperty); }
            set { SetValue(TextButtonProperty, value); }
        }

        public ButtonMainMenuStrip() : base() { }
        public static readonly DependencyProperty IconButtonProperty =
        DependencyProperty.Register("IconButton", typeof(ImageSource), typeof(ButtonMainMenuStrip), new UIPropertyMetadata(null));
        public ImageSource IconButton
        {
            get { return base.GetValue(IconButtonProperty) as ImageSource; }
            set { SetValue(IconButtonProperty, value); }
        }

        public static readonly DependencyProperty IconBorderProperty =
        DependencyProperty.Register("IconBorder", typeof(ImageSource), typeof(ButtonMainMenuStrip), new UIPropertyMetadata(null));
        public ImageSource IconBorder
        {
            get { return base.GetValue(IconBorderProperty) as ImageSource; }
            set { SetValue(IconBorderProperty, value); }
        }

        public static readonly DependencyProperty DownMenuProperty =
        DependencyProperty.Register("DownMenu", typeof(ImageSource), typeof(ButtonMainMenuStrip), new UIPropertyMetadata(null));
        public ImageSource DownMenu
        {
            get { return base.GetValue(DownMenuProperty) as ImageSource; }
            set { SetValue(DownMenuProperty, value); }
        }
    }
}
