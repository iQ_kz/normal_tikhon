﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Survey;
using System.IO;
using Survey.View;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for VisualEditorBorder.xaml
    /// </summary>
    public partial class VisualEditorBorder : UserControl
    {
        public CoordDatWrpp coords;
        private VisualEditorFromModel editorParent;
        private Point dragStart2;
        private Point dragStart3;
        private int pickPoint;

        public static readonly DependencyProperty IsScalableProperty =
        DependencyProperty.Register("IsScalable", typeof(bool), typeof(VisualEditorBorder), new UIPropertyMetadata(null));
        public bool IsScalable
        {
            get { return (bool)GetValue(IsScalableProperty); }
            set { SetValue(IsScalableProperty, value); }
        }

        public static readonly DependencyProperty IsMovableProperty =
        DependencyProperty.Register("IsMovable", typeof(bool), typeof(VisualEditorBorder), new UIPropertyMetadata(null));
        public bool IsMovable
        {
            get { return (bool)GetValue(IsMovableProperty); }
            set { SetValue(IsMovableProperty, value); }
        }

        public static readonly DependencyProperty IsRotatableProperty =
        DependencyProperty.Register("IsRotatable", typeof(bool), typeof(VisualEditorBorder), new UIPropertyMetadata(null));
        public bool IsRotatable
        {
            get { return (bool)GetValue(IsRotatableProperty); }
            set { SetValue(IsRotatableProperty, value); }
        }

        public static readonly DependencyProperty IsBordersShowingProperty =
        DependencyProperty.Register("IsBordersShowing", typeof(bool), typeof(VisualEditorBorder), new UIPropertyMetadata(null));
        public bool IsBordersShowing
        {
            get { return (bool)GetValue(IsBordersShowingProperty); }
            set { SetValue(IsBordersShowingProperty, value); }
        }

        public static readonly DependencyProperty IsButtonsShowingProperty =
        DependencyProperty.Register("IsButtonsShowing", typeof(bool), typeof(VisualEditorBorder), new UIPropertyMetadata(null));
        public bool IsButtonsShowing
        {
            get { return (bool)GetValue(IsButtonsShowingProperty); }
            set { SetValue(IsButtonsShowingProperty, value); }
        }
        public static readonly DependencyProperty IsParentShowingProperty =
        DependencyProperty.Register("IsParentShowing", typeof(bool), typeof(VisualEditorBorder), new UIPropertyMetadata(null));
        public bool IsParentShowing
        {
            get { return (bool)GetValue(IsParentShowingProperty); }
            set { SetValue(IsParentShowingProperty, value); }
        }


        public static readonly DependencyProperty IsActiveInEditorProperty =
        DependencyProperty.Register("IsActiveInEditor", typeof(bool), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public bool IsActiveInEditor
        {
            get { return (bool)GetValue(IsActiveInEditorProperty); }
            set { SetValue(IsActiveInEditorProperty, value); }
        }

        public static readonly DependencyProperty IsInnerAddButtonShowingProperty =
        DependencyProperty.Register("IsInnerAddButtonShowing", typeof(bool), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public bool IsInnerAddButtonShowing
        {
            get { return (bool)GetValue(IsInnerAddButtonShowingProperty); }
            set { SetValue(IsInnerAddButtonShowingProperty, value); }
        }
        public static readonly DependencyProperty IsOuterAddButtonShowingProperty =
         DependencyProperty.Register("IsOuterAddButtonShowing", typeof(bool), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public bool IsOuterAddButtonShowing
        {
            get { return (bool)GetValue(IsOuterAddButtonShowingProperty); }
            set { SetValue(IsOuterAddButtonShowingProperty, value); }
        }





        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == IsBordersShowingProperty)
            {
                if (IsBordersShowing)
                {
                    mainBorder.BorderBrush = dashedBrush;
                }
                else
                {
                    mainBorder.BorderBrush = null;
                }
            }

            if (e.Property == IsButtonsShowingProperty || e.Property == IsOuterAddButtonShowingProperty)
            {
                if (IsOuterAddButtonShowing && IsButtonsShowing)

                    addButtonOuter.Visibility = Visibility.Visible;
                else
                    addButtonOuter.Visibility = Visibility.Collapsed;
            }
            if (e.Property == IsButtonsShowingProperty || e.Property == IsInnerAddButtonShowingProperty)
            {
                if (IsInnerAddButtonShowing && IsButtonsShowing)

                    addButtonInner.Visibility = Visibility.Visible;
                else
                    addButtonInner.Visibility = Visibility.Collapsed;
            }
            if (e.Property == IsMovableProperty)
            {
                mainBorder_Move.IsHitTestVisible = IsMovable;

            }
            if (e.Property == IsRotatableProperty)
            {
               
                if (IsRotatable)
                    RotationRect.Visibility = Visibility.Visible;
                else
                    RotationRect.Visibility = Visibility.Collapsed;

            }
            if (e.Property == IsScalableProperty)
            {
                ScalerSW.IsHitTestVisible = IsScalable;
                ScalerNW.IsHitTestVisible = IsScalable;
                ScalerSE.IsHitTestVisible = IsScalable;
                ScalerNE.IsHitTestVisible = IsScalable;
                ScalerS.IsHitTestVisible = IsScalable;
                ScalerN.IsHitTestVisible = IsScalable;
                ScalerE.IsHitTestVisible = IsScalable;
                ScalerW.IsHitTestVisible = IsScalable;
            }
            if (e.Property == IsActiveInEditorProperty)
            {
                if (IsActiveInEditor)
                    mainBorder.BorderBrush = Brushes.Black;
                else
                    mainBorder.BorderBrush = dashedBrush;
            }
            base.OnPropertyChanged(e);
        }


       /* public void EndChanges()
        {
            _wrpp.DesignElement.AbsoluteCoordinate = new CoordDatWrpp(_wrpp.DesignElement.AbsoluteCoordinate.m_Top, _wrpp.DesignElement.AbsoluteCoordinate.m_Left, _wrpp.DesignElement.AbsoluteCoordinate.m_Width, coords.m_Height, _wrpp.DesignElement.AbsoluteCoordinate.m_Angle);
        }*/
        //public void SetLeftPix(double _left) {
        //   //Canvas.SetLeft(this, _left);
        //   //coords.m_Left = (_left - getParentLeft()) / getParentWidth();
        //    _wrpp.DesignElement.AbsoluteCoordinate = new CoordDatWrpp(_wrpp.DesignElement.AbsoluteCoordinate.m_Top, (_left / editorParent.mainCanvas.Width), _wrpp.DesignElement.AbsoluteCoordinate.m_Width, coords.m_Height, _wrpp.DesignElement.AbsoluteCoordinate.m_Angle);
        //   //EndChanges();
        //}
        //public void SetTopPix(double _top)
        //{
        //    //Canvas.SetTop(this, _top);
        //    /*if(isTrueHeightParent)
        //        coords.m_Top = ((_top - getParentTop()) / getParentHeight());
        //    else
        //        coords.m_Top = ((_top - getParentTop()) / getParentWidth());*/
        //    _wrpp.DesignElement.AbsoluteCoordinate = new CoordDatWrpp((_top / editorParent.mainCanvas.Width), _wrpp.DesignElement.AbsoluteCoordinate.m_Left, _wrpp.DesignElement.AbsoluteCoordinate.m_Width, coords.m_Height, _wrpp.DesignElement.AbsoluteCoordinate.m_Angle);
        //    //EndChanges();
        //}

        public void DoRotate(MouseEventArgs e)
        {

            double xDiff;

            double yDiff;




            xDiff = e.GetPosition(editorParent.mainCanvas).X - (DesignElementWidthScaled / 2) - (DesignElementLeftScaled);

            yDiff = e.GetPosition(editorParent.mainCanvas).Y - (DesignElementHeightScaled / 2) - (DesignElementTopScaled);



            double temp = Survey.Helper.Converter.GradusToRad((Math.Atan2(yDiff, xDiff) * 180.0 / Math.PI) + 90);
            _wrpp.DesignElement.AbsoluteCoordinate = new CoordDatWrpp(_wrpp.DesignElement.AbsoluteCoordinate.m_Top, _wrpp.DesignElement.AbsoluteCoordinate.m_Left, _wrpp.DesignElement.AbsoluteCoordinate.m_Width, _wrpp.DesignElement.AbsoluteCoordinate.m_Height, temp);

            // this.RenderTransform = new RotateTransform(Survey.Helper.Converter.RadToGradus(_wrpp.DesignElement.AbsoluteCoordinate.m_Angle), mainBorder.Width / 2, mainBorder.Height / 2);

        }
        /*public void SetWidthPix(double _width)
        {
            
            _wrpp.DesignElement.Coordinate = new CoordDatWrpp(_wrpp.DesignElement.Coordinate.m_Top,
                _wrpp.DesignElement.Coordinate.m_Left, ((_width / getParentWidth())),
                _wrpp.DesignElement.Coordinate.m_Height, _wrpp.DesignElement.Coordinate.m_Angle);
          
            //EndChanges();
        }*/
        /* public void SetHeightPix(double _height)
         {
             _wrpp.DesignElement.Coordinate = new CoordDatWrpp(_wrpp.DesignElement.Coordinate.m_Top,
                 _wrpp.DesignElement.Coordinate.m_Left, _wrpp.DesignElement.Coordinate.m_Width,
                 (_height / getParentHeight()), _wrpp.DesignElement.Coordinate.m_Angle);
         }*/

        public Point LowestPoint
        {
            get
                {
                    Point lowest = LeftTopScaled;
                    if (RightTopScaled.Y > lowest.Y)
                        lowest = RightTopScaled;
                    if (RightBottomScaled.Y > lowest.Y)
                        lowest = RightBottomScaled;
                    if (LeftBottomScaled.Y > lowest.Y)
                        lowest = LeftBottomScaled;
                    return lowest;
                }
            }

        public void ActualizeToCoords()
        {
            Point temp = LowestPoint;
            if (editorParent.LowestPoint.Y < temp.Y/editorParent.Scale)
                editorParent.LowestPoint = new Point( temp.X/editorParent.Scale,temp.Y/editorParent.Scale );
            Canvas.SetLeft(this, DesignElementLeftScaled);
            mainBorder.Width = DesignElementWidthScaled;
            Canvas.SetTop(this, DesignElementTopScaled);
            mainBorder.Height = DesignElementHeightScaled;
            this.RenderTransform = new RotateTransform(Survey.Helper.Converter.RadToGradus(_wrpp.DesignElement.AbsoluteCoordinate.m_Angle), mainBorder.Width / 2, mainBorder.Height / 2);

            if (richtextBox.Visibility == Visibility.Visible)
            {
                richtextBox.LayoutTransform = new ScaleTransform(editorParent.VEEffectiveWidth / 1000, editorParent.VEEffectiveWidth / 1000);
            }
        }

        /*double getParentHeight() {
            if (parent is VisualEditorBorder)
                return ((VisualEditorBorder)parent).mainBorder.Height;
            else
                return (editorParent).mainCanvas.Width;
        }*/
        /*double getParentWidth()
        {
            if (parent is VisualEditorBorder)
                return ((VisualEditorBorder)parent).mainBorder.Width;
            else
                return (editorParent).mainCanvas.Width;
        }*/
        /*double getParentLeft()
        {
            return Canvas.GetLeft(parent);
        }*/
        /*(double getParentTop()
        {
            return Canvas.GetTop(parent);
        }*/

        DocumentObject _wrpp;

        private bool isButtonPresent = true;
        public void EstablishBindings()
        {
            var binding = new Binding("IsScalingOperation");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(IsScalableProperty, binding);
            binding = new Binding("IsMovingOperation");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(IsMovableProperty, binding);
            binding = new Binding("IsRotatingOperation");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(IsRotatableProperty, binding);
            binding = new Binding("IsBordersShowing");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(IsBordersShowingProperty, binding);
            binding = new Binding("IsButtonsShowing");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(IsButtonsShowingProperty, binding);
        }
        VisualBrush dashedBrush;
        Rectangle borderRect;

        public void CreateBorderVisual(Color inColor)
        {
            _wrpp.BSDesignElement.BSColor = new ColorDatWrpp(inColor.R, inColor.G, inColor.B, inColor.A);

            borderRect = new Rectangle();

            DoubleCollection tempCollection = new DoubleCollection();
            tempCollection.Add(2);
            tempCollection.Add(4);
            borderRect.StrokeDashArray = tempCollection;
            borderRect.Stroke = new SolidColorBrush(inColor);
            borderRect.StrokeThickness = 1;
            dashedBrush.Visual = borderRect;
            mainBorder.BorderBrush = dashedBrush;
            mainBorder.BorderThickness = new Thickness(1);
            var myBinding = new Binding("Height");
            myBinding.Source = mainBorder;
            borderRect.SetBinding(Rectangle.HeightProperty, myBinding);
            myBinding = new Binding("Width");
            myBinding.Source = mainBorder;
            borderRect.SetBinding(Rectangle.WidthProperty, myBinding);
        }

        public VisualEditorBorder( ref DocumentObject wrpp, VisualEditorFromModel _editorParent)
        {

            InitializeComponent();
            EstablishBindings();
            _wrpp = wrpp;
            
            editorParent = _editorParent;
            coords = wrpp.DesignElement.Coordinate;
            IsActiveInEditor = false;
 
            IsOuterAddButtonShowing = false;
            if (wrpp.Childs.Count == 0)
                IsInnerAddButtonShowing = true;
            else
                IsInnerAddButtonShowing = false;
            richtextBox.Background = Brushes.Transparent;

            if (_wrpp is HeaderWrpp)
            {

                //addButton.Visibility = Visibility.Collapsed;
                //IsOuterAddButtonShowing = false;
                IsInnerAddButtonShowing = false;
                string imgid = (wrpp.DesignElement as HeaderDesignElement).ImageElementDesign.IdImage;
                if (imgid != "")
                {
                    mainImage.Source = ProjectsTabControlView.GetActiveProject().Images.ImageByID(imgid).TryGetImageDataAsBitmap();
                    mainImage.Visibility = Visibility.Visible;
                }
                else
                {
                    richtextBox.Visibility = Visibility.Visible;

                    MemoryStream stream = new MemoryStream(ASCIIEncoding.Default.GetBytes((wrpp as HeaderWrpp).Text.Text));
                    richtextBox.Selection.Load(stream, DataFormats.Rtf);
                }
            }

            /*if(_wrpp is HeaderWrpp) {
                addButton.Visibility = Visibility.Collapsed;
            }*/
            if (_wrpp is BlockWrpp)
            {
                innerAddButtonText.Text = "    Add QA";
                outerAddButtonText.Text = "    Add Block";
            }
            if (_wrpp is QaWrpp)
            {
                innerAddButtonText.Text = "    Add Question";
                outerAddButtonText.Text = "    Add QA";
            }
            if (_wrpp is QuestionWrpp)
            {
                innerAddButtonText.Text = "    Add Header";
                outerAddButtonText.Text = "    Add Question";
            }

            zoom = 1;

            mainBorder_Move.IsHitTestVisible = IsMovable;
            ScalerSW.IsHitTestVisible = IsScalable;
            ScalerNW.IsHitTestVisible = IsScalable;
            ScalerSE.IsHitTestVisible = IsScalable;
            ScalerNE.IsHitTestVisible = IsScalable;
            dashedBrush = new VisualBrush();
            Color col2 = new Color();
            col2.R = wrpp.BSDesignElement.BSColor.m_R;
            col2.G = wrpp.BSDesignElement.BSColor.m_G;
            col2.B = wrpp.BSDesignElement.BSColor.m_B;
            col2.A = wrpp.BSDesignElement.BSColor.m_A;
            
            CreateBorderVisual(col2);

            Color col = new Color();
            col.R = wrpp.DesignElement.Color.m_R;
            col.G = wrpp.DesignElement.Color.m_G;
            col.B = wrpp.DesignElement.Color.m_B;
            col.A = wrpp.DesignElement.Color.m_A;

            mainBorder.Background = new SolidColorBrush(col);

            if (IsBordersShowing)
            {
                mainBorder.BorderBrush = dashedBrush;
            }
            else
            {
                mainBorder.BorderBrush = null;
            }
            if (IsRotatable)
                RotationRect.Visibility = Visibility.Visible;
            else
                RotationRect.Visibility = Visibility.Collapsed;

            /*if (IsBordersShowing)
            {
                mainBorder.BorderThickness = new System.Windows.Thickness(1);
                richtextBox.BorderThickness = new Thickness(1);
            }
            else
            {
                mainBorder.BorderThickness = new System.Windows.Thickness(0);
                richtextBox.BorderThickness = new Thickness(0);
            }*/
            /* if (IsButtonsShowing&&isButtonPresent)
                 addButton.Visibility = Visibility.Visible;
             else
                 addButton.Visibility = Visibility.Collapsed;*/
            
            /*if (type == 1)
            {
                mainImage.Visibility = Visibility.Visible;
            }
            if (type == 2)
            {
                mainText.Visibility = Visibility.Visible;
            }*/
            ActualizeToCoords();
        }

        double relateX;
        double relateY;
        double max;
        double zoom;

        public void DoDrag(MouseEventArgs e, double Zoom)
        {
            double temp1;
            double temp2;
            zoom = Zoom;
            if (IsMovable)
                if (pickPoint == 0)
                {
                    Point point = e.GetPosition(editorParent.mainCanvas);
                    DesignElementLeftScaled = ((point.X - (dragStart2.X)));
                    DesignElementTopScaled = ((point.Y - (dragStart2.Y)));
                }
            if (IsScalable)
            {

                if (pickPoint == 1)
                {
                    /*temp1 = e.GetPosition(parent).X + dragStart2.X;
                    temp2 = e.GetPosition(parent).Y - dragStart2.Y;
                    temp3 = Canvas.GetTop(this);
                    temp4 = Canvas.GetLeft(this) + this.mainBorder.Width;
                    if (temp2 <= 0 || this.mainBorder.Height + temp3 - temp2 < 30 || this.mainBorder.Width + temp1 - temp4 < 30)
                        return;
                    if (temp2 > 0)
                    {
                        Canvas.SetTop(this, temp2);
                        if (isTrueHeightParent)
                            VisualEditorView.SetVETop(this, Canvas.GetTop(this) / parent.Height);
                        else
                            VisualEditorView.SetVETop(this, Canvas.GetTop(this) / parent.Width);
                    }
                    this.mainBorder.Height += temp3 - temp2;
                    this.mainBorder.Width += temp1 - temp4;
                    VisualEditorView.SetVEWidth(this, this.mainBorder.Width / parent.Width);
                    if (isTrueHeightParent)
                        VisualEditorView.SetVEHeight(this, this.mainBorder.Height / parent.Height);
                    else
                        VisualEditorView.SetVEHeight(this, this.mainBorder.Height / parent.Width);*/
                    //double centerX = /*(e.GetPosition(editorParent).X - Canvas.GetLeft(this))*/ mainBorder.Width / 2 + Canvas.GetLeft(this);
                    //double centerY = mainBorder.Height / 2 + Canvas.GetTop(this);
                    relateX = DesignElementLeft;
                    relateY = DesignElementTop + DesignElementHeight;
                    max = Math.Max(Math.Abs(e.GetPosition(editorParent.mainCanvas).Y - relateY), Math.Abs(e.GetPosition(editorParent.mainCanvas).X - relateX));
                    /*temp1 = e.GetPosition(editorParent).X + dragStart2.X;
                    temp2 = e.GetPosition(editorParent).Y - dragStart2.Y;
                    temp3 = Canvas.GetTop(this);
                    temp4 = Canvas.GetLeft(this) + this.mainBorder.Width;*/
                    //if (temp2 <= 0 || this.mainBorder.Height + temp3 - temp2 < 30 || this.mainBorder.Width + temp1 - temp4 < 30)
                    //    return;

                    if (max < 10)
                        return;

                    double prevloc = DesignElementTop + DesignElementHeight;

                    DesignElementWidth = max * DesignElementWidth / DesignElementHeight;
                    DesignElementHeight = max;
                    DesignElementTop = prevloc - DesignElementHeight;
                }
                if (pickPoint == 8)
                {
                    temp2 = DesignElementTopScaled + DesignElementHeightScaled;
                    temp1 = e.GetPosition(editorParent.mainCanvas).Y;
                    DesignElementTopScaled = temp1;
                    max = temp2 - temp1;
                    if (max < 10)
                        return;
                    DesignElementHeightScaled = max;
                }
                if (pickPoint == 6)
                {
                    temp2 = DesignElementLeftScaled + DesignElementWidthScaled;
                    temp1 = e.GetPosition(editorParent.mainCanvas).X;
                    DesignElementLeftScaled = temp1;
                    max = temp2 - temp1;
                    if (max < 10)
                        return;
                    DesignElementWidthScaled = max;
                }
                if (pickPoint == 4)
                {
                    max = e.GetPosition(editorParent.mainCanvas).Y - DesignElementTopScaled;
                    if (max < 10)
                        return;
                    DesignElementHeightScaled = max;
                }
                if (pickPoint == 2)
                {
                    max = e.GetPosition(editorParent.mainCanvas).X - DesignElementLeftScaled;
                    if (max < 10)
                        return;
                    DesignElementWidthScaled = max;
                }
                if (pickPoint == 3)
                {
                    relateX = DesignElementLeft;
                    relateY = DesignElementTop;
                    max = Math.Max(Math.Abs(e.GetPosition(editorParent.mainCanvas).Y - relateY), Math.Abs(e.GetPosition(editorParent.mainCanvas).X - relateX));
                    if (max < 10)
                        return;

                    DesignElementWidth = max * DesignElementWidth / DesignElementHeight;
                    DesignElementHeight = max;
                }
                if (pickPoint == 5)
                {
                    relateX = DesignElementLeft + DesignElementWidth;
                    relateY = DesignElementTop;
                    max = Math.Max(Math.Abs(e.GetPosition(editorParent.mainCanvas).Y - relateY), Math.Abs(e.GetPosition(editorParent.mainCanvas).X - relateX));
                    if (max < 10)
                        return;
                    double prevloc = DesignElementLeft + DesignElementWidth;
                    DesignElementWidth = max * DesignElementWidth / DesignElementHeight;
                    DesignElementHeight = max;
                    DesignElementLeft = prevloc - DesignElementWidth;
                }
                if (pickPoint == 7)
                {
                    relateX = DesignElementLeft + DesignElementWidth;
                    relateY = DesignElementTop + DesignElementHeight;
                    max = Math.Max(Math.Abs(e.GetPosition(editorParent.mainCanvas).Y - relateY), Math.Abs(e.GetPosition(editorParent.mainCanvas).X - relateX));
                    if (max < 10)
                        return;
                    double prevlocX = DesignElementLeft;
                    double prevlocY = DesignElementTop;
                    DesignElementWidth = max * DesignElementWidth / DesignElementHeight;
                    DesignElementHeight = max;
                    DesignElementLeft = relateX - DesignElementWidth;
                    DesignElementTop = relateY - DesignElementHeight;
                }
            }
        }

        private void mainBorder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            /*SetHeightPix(70);
            SetWidthPix(90);
            SetLeftPix(20);*/
        }

        private void InitiateDragging(object sender, MouseButtonEventArgs e)
        {
            //dragStart = e.GetPosition(parent);
            dragStart3 = e.GetPosition(this);
            dragStart2 = e.GetPosition((FrameworkElement)sender);
            editorParent.InitiateDragging(this);

        }
        private void InitiateRotating(object sender, MouseButtonEventArgs e)
        {
            //dragStart = e.GetPosition(parent);
            dragStart3 = e.GetPosition(this);
            dragStart2 = e.GetPosition((FrameworkElement)sender);
            editorParent.InitiateRotating(this);

        }

        private void rotateBlock_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            InitiateRotating(this, e);
        }
        private void LeftUp_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            InitiateDragging(this, e);
            pickPoint = 7;
        }
        private void RightUp_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            InitiateDragging(this, e);
            pickPoint = 1;
        }

        private void LeftDown_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            InitiateDragging(this, e);
            pickPoint = 5;
        }
        private void RightDown_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            InitiateDragging(this, e);
            pickPoint = 3;
        }
        private void Right_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            InitiateDragging(this, e);
            pickPoint = 2;
        }
        private void Down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            InitiateDragging(this, e);
            pickPoint = 4;
        }
        private void Left_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            InitiateDragging(this, e);
            pickPoint = 6;
        }
        private void Up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            InitiateDragging(this, e);
            pickPoint = 8;
        }

        private void Center_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            editorParent.ActiveElement = this;
            IsActiveInEditor = true;
            InitiateDragging(this, e);
            pickPoint = 0;
        }

        private void mainBorder_Drop(object sender, DragEventArgs e)
        {
            Survey.ImageWrpp bmp2 = (e.Data.GetData("Survey.ImageWrpp") as Survey.ImageWrpp);
            if (bmp2 == null)
                return;
            string bmpid = bmp2.ID;

            //string[] text = e.Data.GetFormats();
            Point droppoint = e.GetPosition(editorParent);
            editorParent.AddImageBlock(e, _wrpp.Id, bmpid, mainBorder.Width, mainBorder.Height);

        }


        public void SetFontSize(double value)
        {
            // Make sure we have a richtextbox.
            RichTextBox target = richtextBox;
            if (target == null)
                return;

            // Make sure we have a selection. Should have one even if there is no text selected.
            if (target.Selection != null)
            {
                // Check whether there is text selected or just sitting at cursor
                if (target.Selection.IsEmpty)
                {
                    // Check to see if we are at the start of the textbox and nothing has been added yet
                    if (target.Selection.Start.Paragraph == null)
                    {
                        // Add a new paragraph object to the richtextbox with the fontsize
                        Paragraph p = new Paragraph();
                        p.FontSize = value;
                        target.Document.Blocks.Add(p);
                    }
                    else
                    {
                        // Get current position of cursor
                        TextPointer curCaret = target.CaretPosition;
                        // Get the current block object that the cursor is in
                        System.Windows.Documents.Block curBlock = target.Document.Blocks.Where
                            (x => x.ContentStart.CompareTo(curCaret) == -1 && x.ContentEnd.CompareTo(curCaret) == 1).FirstOrDefault();
                        if (curBlock != null)
                        {
                            Paragraph curParagraph = curBlock as Paragraph;
                            // Create a new run object with the fontsize, and add it to the current block
                            Run newRun = new Run();
                            newRun.FontSize = value;
                            curParagraph.Inlines.Add(newRun);
                            // Reset the cursor into the new block. 
                            // If we don't do this, the font size will default again when you start typing.
                            target.CaretPosition = newRun.ElementStart;
                        }
                    }
                }
                else // There is selected text, so change the fontsize of the selection
                {
                    TextRange selectionTextRange = new TextRange(target.Selection.Start, target.Selection.End);
                    selectionTextRange.ApplyPropertyValue(TextElement.FontSizeProperty, value);
                }
            }
            // Reset the focus onto the richtextbox after selecting the font in a toolbar etc
            target.Focus();
            TextRange tr = new TextRange(richtextBox.Document.ContentStart,
                                   richtextBox.Document.ContentEnd);
            if (_wrpp is HeaderWrpp)
                SaveTextRangeToTextWrpp(tr, (_wrpp as HeaderWrpp).Text);
            ProjectsTabControlView.GetActiveProjectViewModel().InvalidateTypewriter();
        }

        public void SetTextColor(Color value)
        {
            // Make sure we have a richtextbox.
            RichTextBox target = richtextBox;
            if (target == null)
                return;

            // Make sure we have a selection. Should have one even if there is no text selected.
            if (target.Selection != null)
            {
                // Check whether there is text selected or just sitting at cursor
                if (target.Selection.IsEmpty)
                {
                    // Check to see if we are at the start of the textbox and nothing has been added yet
                    if (target.Selection.Start.Paragraph == null)
                    {
                        // Add a new paragraph object to the richtextbox with the fontsize
                        Paragraph p = new Paragraph();
                        p.Foreground = new SolidColorBrush(value);
                        target.Document.Blocks.Add(p);
                    }
                    else
                    {
                        // Get current position of cursor
                        TextPointer curCaret = target.CaretPosition;
                        // Get the current block object that the cursor is in
                        System.Windows.Documents.Block curBlock = target.Document.Blocks.Where
                            (x => x.ContentStart.CompareTo(curCaret) == -1 && x.ContentEnd.CompareTo(curCaret) == 1).FirstOrDefault();
                        if (curBlock != null)
                        {
                            Paragraph curParagraph = curBlock as Paragraph;
                            // Create a new run object with the fontsize, and add it to the current block
                            Run newRun = new Run();
                            newRun.Foreground = new SolidColorBrush(value);
                            curParagraph.Inlines.Add(newRun);
                            // Reset the cursor into the new block. 
                            // If we don't do this, the font size will default again when you start typing.
                            target.CaretPosition = newRun.ElementStart;
                        }
                    }
                }
                else // There is selected text, so change the fontsize of the selection
                {
                    TextRange selectionTextRange = new TextRange(target.Selection.Start, target.Selection.End);
                    selectionTextRange.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(value));
                }
            }
            // Reset the focus onto the richtextbox after selecting the font in a toolbar etc
            target.Focus();
            TextRange tr = new TextRange(richtextBox.Document.ContentStart,
                                     richtextBox.Document.ContentEnd);
            if (_wrpp is HeaderWrpp)
                SaveTextRangeToTextWrpp(tr, (_wrpp as HeaderWrpp).Text);
            ProjectsTabControlView.GetActiveProjectViewModel().InvalidateTypewriter();
        }



        public void SetFillColor(Color inColor)
        {
            mainBorder.Background = new SolidColorBrush(inColor);
            _wrpp.DesignElement.Color = new ColorDatWrpp(inColor.R, inColor.G, inColor.B, inColor.A);

        }
        public void SetFontStyle(FontWeight weight, FontStyle style, TextDecorationCollection dec, FontVariants var)
        {
            // Make sure we have a richtextbox.
            RichTextBox target = richtextBox;
            if (target == null)
                return;

            // Make sure we have a selection. Should have one even if there is no text selected.
            if (target.Selection != null)
            {
                // Check whether there is text selected or just sitting at cursor
                if (target.Selection.IsEmpty)
                {
                    // Check to see if we are at the start of the textbox and nothing has been added yet
                    if (target.Selection.Start.Paragraph == null)
                    {
                        // Add a new paragraph object to the richtextbox with the fontsize
                        Paragraph p = new Paragraph();
                        p.FontWeight = weight;
                        p.FontStyle = style;
                        p.TextDecorations = dec;
                        p.Typography.Variants = var;


                        //FontVariants.Superscript
                        // p.Typography.Variants;
                        target.Document.Blocks.Add(p);
                    }
                    else
                    {
                        // Get current position of cursor
                        TextPointer curCaret = target.CaretPosition;
                        // Get the current block object that the cursor is in
                        System.Windows.Documents.Block curBlock = target.Document.Blocks.Where
                            (x => x.ContentStart.CompareTo(curCaret) == -1 && x.ContentEnd.CompareTo(curCaret) == 1).FirstOrDefault();
                        if (curBlock != null)
                        {
                            Paragraph curParagraph = curBlock as Paragraph;
                            // Create a new run object with the fontsize, and add it to the current block
                            Run newRun = new Run();
                            newRun.FontStyle = style;
                            newRun.FontWeight = weight;
                            newRun.TextDecorations = dec;
                            newRun.Typography.Variants = var;
                            curParagraph.Inlines.Add(newRun);
                            // Reset the cursor into the new block. 
                            // If we don't do this, the font size will default again when you start typing.
                            target.CaretPosition = newRun.ElementStart;
                        }
                    }
                }
                else // There is selected text, so change the fontsize of the selection
                {
                    TextRange selectionTextRange = new TextRange(target.Selection.Start, target.Selection.End);
                    selectionTextRange.ApplyPropertyValue(TextElement.FontStyleProperty, style);
                    selectionTextRange.ApplyPropertyValue(TextElement.FontWeightProperty, weight);
                    selectionTextRange.ApplyPropertyValue(TextBlock.TextDecorationsProperty, dec);
                    /* var currentAlignment = selectionTextRange.GetPropertyValue(Inline.BaselineAlignmentProperty); 
                     BaselineAlignment newAlignment = ((BaselineAlignment)var == BaselineAlignment.Superscript) ? BaselineAlignment.Baseline : BaselineAlignment.Superscript;
                     newAlignment = ((BaselineAlignment)var == BaselineAlignment.Subscript) ? BaselineAlignment.Baseline : BaselineAlignment.Subscript;

                     selectionTextRange.ApplyPropertyValue(Inline.BaselineAlignmentProperty, newAlignment);*/


                    //selectionTextRange.ApplyPropertyValue(RichTextBox.inlVariantsProperty, var);
                }
            }
            // Reset the focus onto the richtextbox after selecting the font in a toolbar etc
            target.Focus();
            TextRange tr = new TextRange(richtextBox.Document.ContentStart,
                                     richtextBox.Document.ContentEnd);
            if (_wrpp is HeaderWrpp)
                SaveTextRangeToTextWrpp(tr, (_wrpp as HeaderWrpp).Text);
            ProjectsTabControlView.GetActiveProjectViewModel().InvalidateTypewriter();
        }



        public static void SaveTextRangeToTextWrpp(TextRange tr, TextWrpp textwrpp)
        {

            MemoryStream ms = new MemoryStream();
            tr.Save(ms, DataFormats.Rtf);
            string xamlText = ASCIIEncoding.Default.GetString(ms.ToArray());
            //if (_wrpp is HeaderWrpp)
            textwrpp.Text = xamlText;
        }

        private void richtextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
            TextRange tr = new TextRange(richtextBox.Document.ContentStart,
                               richtextBox.Document.ContentEnd);
            if (_wrpp is HeaderWrpp)
                SaveTextRangeToTextWrpp(tr, (_wrpp as HeaderWrpp).Text);
            ProjectsTabControlView.GetActiveProjectViewModel().InvalidateTypewriter();
        }

        private void TextBlock_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            editorParent.ActiveElement = this;
            IsActiveInEditor = true;
        }

        private void AddButtonInner_Click(object sender, RoutedEventArgs e)
        {
            if (_wrpp is BlockWrpp)
                _wrpp.AddChild(new QaWrpp(ProjectsTabControlView.GetActiveProject(), new ColorDatWrpp(0, 0, 0, 255), new CoordDatWrpp(0, 0, 0, 0, 0), new ColorDatWrpp(0, 0, 0, 0), new CoordDatWrpp(20, 20, 500, 100, 0), "QA " + (_wrpp.Childs.Count + 1)));
            if (_wrpp is QaWrpp)
                _wrpp.AddChild(new QuestionWrpp(ProjectsTabControlView.GetActiveProject(), new ColorDatWrpp(0, 0, 0, 255), new CoordDatWrpp(0, 0, 0, 0, 0), new ColorDatWrpp(0, 0, 0, 0), new CoordDatWrpp(20, 20, 500, 100, 0), "Question" + (_wrpp.Childs.Count + 1)));
            if (_wrpp is QuestionWrpp)
                _wrpp.AddChild(new HeaderWrpp(ProjectsTabControlView.GetActiveProject(), new ColorDatWrpp(0, 0, 0, 255), new CoordDatWrpp(0, 0, 0, 0, 0), new ColorDatWrpp(0, 0, 0, 0), new CoordDatWrpp(20, 20, 500, 100, 0),
                    @"{\rtf1\ansi{\fonttbl{\f0\fcharset0;}}}"
                    , new ImageProperties(), new BorderDat(), "Header" + (_wrpp.Childs.Count + 1)));
            IsInnerAddButtonShowing = false;
            ProjectsTabControlView.GetActiveProjectViewModel().InvalidateEditors();
        }
        private void AddButtonOuter_Click(object sender, RoutedEventArgs e)
        {
            if (_wrpp.Parent is PageWrpp)
                _wrpp.Parent.AddChild(new BlockWrpp(ProjectsTabControlView.GetActiveProject(), new ColorDatWrpp(0, 0, 0, 255), new CoordDatWrpp(0, 0, 0, 0, 0), new ColorDatWrpp(0, 0, 0, 0),
                    new CoordDatWrpp(_wrpp.DesignElement.Coordinate.m_Top + _wrpp.DesignElement.Coordinate.m_Height + 20, _wrpp.DesignElement.Coordinate.m_Left, _wrpp.DesignElement.Coordinate.m_Width, _wrpp.DesignElement.Coordinate.m_Height, _wrpp.DesignElement.Coordinate.m_Angle), "Block " + (_wrpp.Parent.Childs().Count + 1)));

            if (_wrpp.Parent is BlockWrpp)
                _wrpp.Parent.AddChild(new QaWrpp(ProjectsTabControlView.GetActiveProject(), new ColorDatWrpp(0, 0, 0, 255), new CoordDatWrpp(0, 0, 0, 0, 0), new ColorDatWrpp(0, 0, 0, 0),
                    new CoordDatWrpp(_wrpp.DesignElement.Coordinate.m_Top + _wrpp.DesignElement.Coordinate.m_Height + 20, _wrpp.DesignElement.Coordinate.m_Left, _wrpp.DesignElement.Coordinate.m_Width, _wrpp.DesignElement.Coordinate.m_Height, _wrpp.DesignElement.Coordinate.m_Angle), "QA " + (_wrpp.Parent.Childs().Count + 1)));
            if (_wrpp.Parent is QaWrpp)
                _wrpp.Parent.AddChild(new QuestionWrpp(ProjectsTabControlView.GetActiveProject(), new ColorDatWrpp(0, 0, 0, 255),
                    new CoordDatWrpp(0, 0, 0, 0, 0), new ColorDatWrpp(0, 0, 0, 0), new CoordDatWrpp(_wrpp.DesignElement.Coordinate.m_Top + _wrpp.DesignElement.Coordinate.m_Height + 20, _wrpp.DesignElement.Coordinate.m_Left, _wrpp.DesignElement.Coordinate.m_Width, _wrpp.DesignElement.Coordinate.m_Height, _wrpp.DesignElement.Coordinate.m_Angle), "Question" + (_wrpp.Parent.Childs().Count + 1)));
            if (_wrpp.Parent is QuestionWrpp)
                _wrpp.Parent.AddChild(new HeaderWrpp(ProjectsTabControlView.GetActiveProject(), new ColorDatWrpp(0, 0, 0, 255),
                    new CoordDatWrpp(0, 0, 0, 0, 0), new ColorDatWrpp(0, 0, 0, 0), new CoordDatWrpp(_wrpp.DesignElement.Coordinate.m_Top + _wrpp.DesignElement.Coordinate.m_Height + 20, _wrpp.DesignElement.Coordinate.m_Left, _wrpp.DesignElement.Coordinate.m_Width, _wrpp.DesignElement.Coordinate.m_Height, _wrpp.DesignElement.Coordinate.m_Angle),
                    @"{\rtf1\ansi{\fonttbl{\f0\fcharset0;}}}"
                    , new ImageProperties(), new BorderDat(), "Header" + (_wrpp.Parent.Childs().Count + 1)));
            IsOuterAddButtonShowing = false;
            ProjectsTabControlView.GetActiveProjectViewModel().InvalidateEditors();
        }


        public Point GetCenterScaled()
        {
            
            return new Point(DesignElementLeftScaled + DesignElementWidthScaled / 2.0, DesignElementTopScaled + DesignElementHeightScaled / 2.0);
        }

        public double AngleRotation
        {
            get { return Survey.Helper.Converter.RadToGradus(_wrpp.DesignElement.AbsoluteCoordinate.m_Angle); }
            set
            {
                Survey.CoordDatWrpp absoluteCoord = _wrpp.DesignElement.AbsoluteCoordinate;

                double validGradus = value;

                _wrpp.DesignElement.AbsoluteCoordinate
                    = new Survey.CoordDatWrpp(
                        absoluteCoord.m_Top
                        , absoluteCoord.m_Left
                        , absoluteCoord.m_Width
                        , absoluteCoord.m_Height
                        , Survey.Helper.Converter.GradusToRad(validGradus)
                        );
            }
        }

        public double DesignElementLeftScaled
        {
            get { return DesignElementLeft * editorParent.Scale; }
            set { DesignElementLeft = value / editorParent.Scale; }
        }
        public double DesignElementTopScaled
        {
            get { return DesignElementTop * editorParent.Scale; }
            set { DesignElementTop = value / editorParent.Scale; }
        }
        public double DesignElementWidthScaled
        {
            get { return DesignElementWidth * editorParent.Scale; }
            set { DesignElementWidth = value / editorParent.Scale; }
        }
        public double DesignElementHeightScaled
        {
            get { return DesignElementHeight * editorParent.Scale; }
            set { DesignElementHeight = value / editorParent.Scale; }
        }

        public double DesignElementLeft
        {
            get { return _wrpp.DesignElement.AbsoluteCoordinate.m_Left; }
            set
            {
                Survey.CoordDatWrpp absoluteCoord = _wrpp.DesignElement.AbsoluteCoordinate;

                _wrpp.DesignElement.AbsoluteCoordinate
                    = new Survey.CoordDatWrpp(
                        absoluteCoord.m_Top
                        , value, absoluteCoord.m_Width
                        , absoluteCoord.m_Height
                        , absoluteCoord.m_Angle
                        );
            }
        }

        public double DesignElementTop
        {
            get { return _wrpp.DesignElement.AbsoluteCoordinate.m_Top; }
            set
            {
                Survey.CoordDatWrpp absoluteCoord = _wrpp.DesignElement.AbsoluteCoordinate;

                _wrpp.DesignElement.AbsoluteCoordinate
                    = new Survey.CoordDatWrpp(
                        value, absoluteCoord.m_Left
                        , absoluteCoord.m_Width
                        , absoluteCoord.m_Height
                        , absoluteCoord.m_Angle
                        );
            }

        }

        public double DesignElementWidth
        {
            get { return _wrpp.DesignElement.Coordinate.m_Width; }
            set
            {
                Survey.CoordDatWrpp absoluteCoord = _wrpp.DesignElement.Coordinate;

                _wrpp.DesignElement.Coordinate
                    = new Survey.CoordDatWrpp(
                        absoluteCoord.m_Top
                        , absoluteCoord.m_Left
                        , value, absoluteCoord.m_Height
                        , absoluteCoord.m_Angle
                        );
            }
        }

        public double DesignElementHeight
        {
            get { return _wrpp.DesignElement.Coordinate.m_Height; }
            set
            {
                Survey.CoordDatWrpp absoluteCoord = _wrpp.DesignElement.Coordinate;

                _wrpp.DesignElement.Coordinate
                    = new Survey.CoordDatWrpp(
                        absoluteCoord.m_Top
                        , absoluteCoord.m_Left
                        , absoluteCoord.m_Width, value
                        , absoluteCoord.m_Angle
                        );
            }
        }
        public Point LeftTopScaled
        {
            get { return MathExtensions.RotatePointAroundPoint(new Point(DesignElementLeftScaled, DesignElementTopScaled), GetCenterScaled(), AngleRotation+315); }
        }
        public Point RightTopScaled
        {
            get { return MathExtensions.RotatePointAroundPoint(new Point(DesignElementLeftScaled + DesignElementWidthScaled, DesignElementTopScaled), GetCenterScaled(), AngleRotation+45); }
        }
        public Point LeftBottomScaled
        {
            get { return MathExtensions.RotatePointAroundPoint(new Point(DesignElementLeftScaled, DesignElementTopScaled + DesignElementHeightScaled), GetCenterScaled(), AngleRotation+135); }
        }
        public Point RightBottomScaled
        {
            get { return MathExtensions.RotatePointAroundPoint(new Point(DesignElementLeftScaled + DesignElementWidthScaled, DesignElementTopScaled + DesignElementHeightScaled), GetCenterScaled(), AngleRotation+225); }
        }

    }
}
