﻿using Survey;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for TypeWriterWindows.xaml
    /// </summary>
    public partial class TypeWriterWindowView : UserControl
    {
        public ObservableCollection<HeaderWrpp> Collection { get; set; }
        private TextBox TextBoxInitialization()
        {
            TextBox textbox = new TextBox();
            textbox.Height = 50;
            DockPanel.SetDock(textbox, Dock.Top);
            return textbox;
        }
        public static readonly DependencyProperty IsInvalidProperty =
        DependencyProperty.Register("IsInvalid", typeof(bool), typeof(TypeWriterWindowView), new UIPropertyMetadata(null));
        public bool IsInvalid
        {
            get { return (bool)GetValue(IsInvalidProperty); }
            set { SetValue(IsInvalidProperty, value); }
        }
        public void EstablishBindings()
        {
            var binding = new Binding("IsInvalid");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(IsInvalidProperty, binding);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == IsInvalidProperty)
            {
                if (IsInvalid)
                {
                    SetElements();
                    IsInvalid = false;
                }
            }
            base.OnPropertyChanged(e);
        }
        public TypeWriterWindowView()
        {
            InitializeComponent();
            EstablishBindings();
            SetElements();
        }
        public void SetElements()
        {
            TextsWrpp wrpp = ProjectsTabControlView.GetActiveProject().Texts;
            mainListView.Items.Clear();
            for (int i = 0; i < wrpp.Texts.Count; i++)
            {
                //if (wrpp.Texts[i].Parent is HeaderWrpp) { 
                FlowDocument fldoc = new FlowDocument();
                TextRange tr = new TextRange(
                 fldoc.ContentStart, fldoc.ContentEnd);
                MemoryStream inputStream = new MemoryStream(Encoding.ASCII.GetBytes(wrpp.Texts[i].Text));
                tr.Load(inputStream, DataFormats.Rtf);
                System.Windows.Documents.Block currentBlock = fldoc.Blocks.FirstBlock;


                List<TextPointer> pointers = new List<TextPointer>();
                if (currentBlock == null)
                {
                    fldoc.Blocks.Add(new System.Windows.Documents.Paragraph());
                    currentBlock = fldoc.Blocks.FirstBlock;
                }
                while (currentBlock != null)
                {
                    System.Windows.Documents.Inline currentInline = (currentBlock as System.Windows.Documents.Paragraph).Inlines.FirstInline;
                    if (currentInline == null)
                    {
                        (currentBlock as System.Windows.Documents.Paragraph).Inlines.Add(new Span());
                        currentInline = (currentBlock as System.Windows.Documents.Paragraph).Inlines.FirstInline;
                    }
                    while (currentInline != null)
                    {
                        System.Windows.Documents.Run currentRun = (currentInline as System.Windows.Documents.Span).Inlines.FirstInline as System.Windows.Documents.Run;
                        if (currentRun == null)
                        {
                            (currentInline as System.Windows.Documents.Span).Inlines.Add(new Run());
                            currentRun = (currentInline as System.Windows.Documents.Span).Inlines.FirstInline as System.Windows.Documents.Run;
                        }
                        while (currentRun != null)
                        {
                            //VisualEditorBorder.SaveTextRangeToTextWrpp(tr, wrpp.Texts[i]);
                            mainListView.Items.Add(new TypeWriterField(tr, currentRun.ContentStart, wrpp.Texts[i]));
                            currentRun = currentRun.NextInline as System.Windows.Documents.Run;
                        };
                        currentInline = currentInline.NextInline;
                    };
                    currentBlock = currentBlock.NextBlock;
                };
                inputStream.Close();
                VisualEditorBorder.SaveTextRangeToTextWrpp(tr, wrpp.Texts[i]);
                //}
            }
        }
    }
}
