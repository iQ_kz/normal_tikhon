﻿using Survey;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for TextEditorWindow.xaml
    /// </summary>
    public partial class LogicWindowView : UserControl
    {
        public ObservableCollection<DocumentObject> Collection { get; set; }
        public LogicWindowView()
        {
            InitializeComponent();
            Collection = ProjectsTabControlView.GetActiveProject().Project.Chapters[0].Childs;
        }
    }
}
