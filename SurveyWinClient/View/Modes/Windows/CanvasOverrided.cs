﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using SurveyWinClient.ViewModel;

using Survey;
using System.IO;

namespace SurveyWinClient.View
{
    static class Wrapper
    {

        public static void Wrap(System.Windows.Threading.Dispatcher _disp, out DocumentWrapper _doc, out ProjectWrpp _project)
        {

            _doc = Survey.DocumentManagerWrapper.GetInstance().Documents[0];

            _project = _doc.Project;

            TextsWrpp texts = _doc.Texts;
            DesignWrpp design = _doc.Design;
            BlockShemeDesignWrpp bsDesign = _doc.BlockShemeDesign;

            ChapterWrpp chapterOne = new ChapterWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), "Chapter 1");
            _project.AddChild(chapterOne);

            PageWrpp pageOne = new PageWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1.2, chapterOne.Id, 0), "Page 1");
            chapterOne.AddChild(pageOne);

            {
                BlockWrpp blockOne = new BlockWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 0.2, pageOne.Id, 0), "Block 01");
                pageOne.AddChild(blockOne);

                BlockWrpp blockTwo = new BlockWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0.25, 0, 0.7, 0.35, pageOne.Id, 0), "Block 02");
                pageOne.AddChild(blockTwo);

                {
                    QaWrpp qA = new QaWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, blockTwo.Id, 0), "Q/A 1");
                    blockTwo.AddChild(qA);

                    {
                        QuestionWrpp quest = new QuestionWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, qA.Id, 0), "Q: тестовая");
                        qA.AddChild(quest);

                        {
							ImageProperties imgProps = new ImageProperties();
							BorderDat borderDat = new BorderDat();

                            HeaderWrpp headerOne = new HeaderWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 0.5, 0.45, quest.Id, 0)
                                , @"{\rtf1\ansi{\fonttbl{\f0\fcharset0;}}\f0\fs20\u1090?\u1077?\u1089?\u1090?\u1086?\u1074?\u1072?\u1103? \u1089?\u1090?\u1088?\u1086?\u1082?\u1072?  \u8470?1}"
								, imgProps, borderDat
                                , "тестовая");

                            quest.AddChild(headerOne);

                            HeaderWrpp headerTwo = new HeaderWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0.1, 0.25, 0.5, 0.3, quest.Id, 0)
                                , @"{\rtf1\ansi{\fonttbl{\f0\fcharset0;}}{\colortbl;\red255;\red0;}\f0\fs28\u1090?\u1077?\u1089?\u1090?\u1086?\u1074?\u1072?\u1103? \cf1\u1089?\u1090?\u1088?\u1086?\u1082?\u1072?\cf2  \u8470?2}"
								, imgProps, borderDat
                                , "тестовая 2");

                            quest.AddChild(headerTwo);
                        }

                    }

                }

            }


// 			Survey.DocumentWrapper newDoc = Survey.DocumentManagerWrapper.GetInstance().CreateDocumentByID("47acdc49-5acb-496a-a2ef-44309e7b6650");
// 			if (newDoc == null)
// 				return;
// 
			Survey.ObservableRangeCollection<ChapterWrpp> chapters = _doc.Project.Chapters;

			foreach(ChapterWrpp chapter in chapters)
			{
				Survey.ObservableRangeCollection<PageWrpp> pages = chapter.Pages;

				foreach(PageWrpp page in pages)
				{
					Survey.ObservableRangeCollection<XMLObjectWrpp> childs = page.ChildsTree;

					foreach (XMLObjectWrpp xmlObject in childs)
					{
						DocumentObject docObj = xmlObject as DocumentObject;
						if(docObj != null)
							MessageBox.Show(docObj.Name + " " + docObj.Id);
					}

				}

			}

// 			foreach(ProjectInfoWrapper projectInfo in Survey.FileManagerWrapper.GetInstance().ProjectInfos)
// 				Survey.DocumentManagerWrapper.GetInstance().CreateDocumentByID(projectInfo.ID);
			

		/*	FileStream fileStream = new FileStream("600488.bmp", FileMode.Open, FileAccess.Read);

			var img = new System.Windows.Media.Imaging.BitmapImage();
			img.BeginInit();
			img.StreamSource = fileStream;
			img.EndInit();

			Dictionary<string, BitmapImage> pageFiles = new Dictionary<string, BitmapImage>() { {"page 1", img} };

			_doc.Save(new Dictionary<string, Dictionary<string, BitmapImage>>() { { "project", pageFiles } });

			pageOne.AddChild( new BlockWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0.25, 0, 0.7, 0.35, pageOne.Id, 0), "Block XXX"));

			System.Threading.Thread.Sleep(2000);

			_doc.Save(new Dictionary<string, Dictionary<string, BitmapImage>>() { { "project", pageFiles } });

// 			_doc.ToPreviosRevision();
// 			_doc.ToNextRevision();*/
// 
// 			List<BackupProjectProperties> backupProjectProperties = _doc.GetAllBackupDescriptions();
// 			_doc.SetBackup(backupProjectProperties[0].m_Hash);

        }

    }


    class IElement
    {
        public string Text { get; set; }

        public double Top { get; set; }
        public double Left { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }

        public IElement(string _text, double _top, double _left, double _width, double _height)
        {
            Text = _text;
            
            Top = _top;
            Left = _left;
            Width = _width;
            Height = _height;
        }
    };

    /*public class CanvasOverridedAttachedProperties
    {
        public static readonly System.Windows.DependencyProperty COWidthProperty;
        public static void SetCOWidth(System.Windows.UIElement element, double value)
        {
            element.SetValue(COWidthProperty, value);
        }
        public static double GetCOWidth(System.Windows.UIElement element)
        {
            return (int)element.GetValue(COWidthProperty);
        }

        public static readonly System.Windows.DependencyProperty COHeightProperty;
        public static void SetCOHeight(System.Windows.UIElement element, double value)
        {
            element.SetValue(COHeightProperty, value);
        }
        public static double GetCOWidth(System.Windows.UIElement element)
        {
            return (int)element.GetValue(COHeightProperty);
        }
        static CanvasOverridedAttachedProperties()
        {
            COHeightProperty = System.Windows.DependencyProperty.RegisterAttached(
                "COHeight", typeof(int), typeof(CanvasOverrided));
            COWidthProperty = System.Windows.DependencyProperty.RegisterAttached(
                "COHeight", typeof(int), typeof(CanvasOverrided));
        }
    };
    */
    public static class RemoveChildHelper
    {
        public static void RemoveChild(this DependencyObject parent, UIElement child)
        {
            var panel = parent as Panel;
            if (panel != null)
            {
                panel.Children.Remove(child);
                return;
            }

            var decorator = parent as Decorator;
            if (decorator != null)
            {
                if (decorator.Child == child)
                {
                    decorator.Child = null;
                }
                return;
            }

            var contentPresenter = parent as ContentPresenter;
            if (contentPresenter != null)
            {
                if (contentPresenter.Content == child)
                {
                    contentPresenter.Content = null;
                }
                return;
            }

            var contentControl = parent as ContentControl;
            if (contentControl != null)
            {
                if (contentControl.Content == child)
                {
                    contentControl.Content = null;
                }
                return;
            }

            // maybe more
        }
    }
    class CanvasOverrided : Canvas
    {

        public static readonly System.Windows.DependencyProperty COWidthProperty;
        private Size startingSize;
        private double desiredHeight;
        private bool loaded;
        private Viewbox viewbox;
        private MDI.MDIWindow MDIWindowParent;
        private VisualWindowView VWVParent;
        private Grid GridParent;
        private Viewbox ViewboxParent;
        private ScrollViewer ScrollViewerParent;
        private List<FrameworkElement> scalables = new List<FrameworkElement>();
        public static void SetCOWidth(System.Windows.UIElement element, double value)
        {
            element.SetValue(COWidthProperty, value);
        }
        public static double GetCOWidth(System.Windows.UIElement element)
        {
            return (double)element.GetValue(COWidthProperty);
        }

        public static readonly System.Windows.DependencyProperty COHeightProperty;
        public static void SetCOHeight(System.Windows.UIElement element, double value)
        {
            element.SetValue(COHeightProperty, value);
        }
        public static double GetCOHeight(System.Windows.UIElement element)
        {
            return (double)element.GetValue(COHeightProperty);
        }
        public static readonly System.Windows.DependencyProperty COTopProperty;
        public static void SetCOTop(System.Windows.UIElement element, double value)
        {
            element.SetValue(COTopProperty, value);
        }
        public static double GetCOTop(System.Windows.UIElement element)
        {
            return (double)element.GetValue(COTopProperty);
        }
        public static readonly System.Windows.DependencyProperty COLeftProperty;
        public static void SetCOLeft(System.Windows.UIElement element, double value)
        {
            element.SetValue(COLeftProperty, value);
        }
        public static double GetCOLeft(System.Windows.UIElement element)
        {
            return (double)element.GetValue(COLeftProperty);
        }
        ObservableCollection<IElement> collection { get; set; }

        static CanvasOverrided()
        {
            COHeightProperty = System.Windows.DependencyProperty.RegisterAttached(
               "COHeight", typeof(double), typeof(CanvasOverrided));
            COWidthProperty = System.Windows.DependencyProperty.RegisterAttached(
               "COWidth", typeof(double), typeof(CanvasOverrided));
            COTopProperty = System.Windows.DependencyProperty.RegisterAttached(
              "COTop", typeof(double), typeof(CanvasOverrided));
            COLeftProperty = System.Windows.DependencyProperty.RegisterAttached(
              "COLeft", typeof(double), typeof(CanvasOverrided));
        }

        public void SetScrollOffsets(double _vertOffset, double _horOffset)
        {
           /* ScrollViewerParent.VerticalOffset = _vertOffset;
            Sc*/
        }

        public CanvasOverrided()
        {
            viewbox = new Viewbox();
            loaded = false;
            desiredHeight = 0;
            Scale = 1;
            this.Zoom = 1;
            this.Loaded += CanvasOverrided_Loaded;
            this.MouseRightButtonDown += CanvasOverrided_MouseRightButtonDown;
            //this.Background = Brushes.White;
            
            

            var binding = new Binding("Zoom");
            binding.Mode = BindingMode.TwoWay; // make TwoWay if needed. Not for this example
            SetBinding(MyZoomProperty, binding);

            // some data context. No matter
            //DataContext = this;

            /*this.RenderTransform = new ScaleTransform(1.2);
            Binding myBinding = new Binding("Width");
            myBinding.Source = Survey.WinClient;
            rect.SetBinding(Rectangle.WidthProperty, myBinding);*/
            //base.MouseRightButtonDown += CanvasOverrided_MouseRightButtonDown;
        }

        void CanvasOverrided_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            System.Windows.MessageBox.Show(COScrollVertical.ToString());
            COScrollVertical = 100;
        }

        public RenderTargetBitmap GetCurrentPreview()
        {
            Viewbox parent = this.Parent as Viewbox;
            this.Parent.RemoveChild(this);

            double bmpHeight = 20;
            double bmpWidth = 20;
            viewbox.Height = bmpHeight;
            viewbox.Width = bmpWidth;
            viewbox.Child = this; //control to render

            viewbox.Measure(new System.Windows.Size(bmpWidth, bmpHeight));
            viewbox.Arrange(new Rect(0, 0, bmpWidth, bmpHeight));

            viewbox.UpdateLayout();

            RenderTargetBitmap render = new RenderTargetBitmap((int)bmpWidth, (int)bmpHeight, 96, 96, PixelFormats.Pbgra32);

            render.Render(viewbox);
            //var stream2 = new FileStream("C:/Games/test2.jpg", FileMode.Create);

            //BitmapEncoder encoder = new JpegBitmapEncoder();

            //encoder.Frames.Add(BitmapFrame.Create(render));

            //encoder.Save(stream2);

            //stream2.Close();
            viewbox.RemoveChild(this);
            this.Height = startingSize.Height;
            this.Width = startingSize.Width;
            parent.Child=this;
            return render;
            /*this.Measure(new Size(500,500));
            this.Arrange(new Rect(new Size(500, 500)));

            var bitmap = new RenderTargetBitmap(
                (int)new Size(500, 500).Width, (int)new Size(500, 500).Height, 96, 96, PixelFormats.Default);

            bitmap.Render(this);

            */
        }

        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }

        void CanvasOverrided_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (loaded) {
                //this.Clip = new RectangleGeometry(new Rect(0, 0, GridParent.ColumnDefinitions[(Grid.GetColumn(this))].ActualWidth / Zoom, GridParent.ActualHeight / Zoom));
                
            for (int i = 0; i < base.Children.Count; i++)
            {
                ((FrameworkElement)(base.Children[i])).Width = CanvasOverrided.GetCOWidth(base.Children[i]) * COEffectiveWidth;
                ((FrameworkElement)(base.Children[i])).Height = CanvasOverrided.GetCOHeight(base.Children[i]) * COEffectiveWidth;
                Canvas.SetTop(base.Children[i], CanvasOverrided.GetCOTop(base.Children[i]) * COEffectiveWidth);
                Canvas.SetLeft(base.Children[i], CanvasOverrided.GetCOLeft(base.Children[i]) * COEffectiveWidth);
            }
            
            for (int i = 0; i < scalables.Count; i++)
            {
                Border papa = FindParent<Border>(scalables[i]);
                scalables[i].Width = papa.Width * CanvasOverrided.GetCOWidth(scalables[i]);
                scalables[i].Height = papa.Height * CanvasOverrided.GetCOHeight(scalables[i]);
                Canvas.SetTop(scalables[i], CanvasOverrided.GetCOTop(scalables[i]) * papa.Height);
                Canvas.SetLeft(scalables[i], CanvasOverrided.GetCOLeft(scalables[i]) * papa.Width);
            }

            //double widthchange = e.NewSize.Width / (e.PreviousSize.Width+0.001);
            //Scale = widthchange == 0 ? 1 : widthchange;
            if (startingSize == null || startingSize.Width == 0)
                return;
            Scale = COEffectiveWidth / startingSize.Width;
            ViewboxParent.Width = startingSize.Width*Scale*Zoom;
            ViewboxParent.Height = startingSize.Height*Scale*Zoom;
            ViewboxParent.RenderTransform = new ScaleTransform((double)1 / Scale, (double)1 / Scale);
            /*ScaleTransform st = new ScaleTransform();
            
            if (startingSize==null||startingSize.Width == 0)
                return;
            double widthchange = e.NewSize.Width / startingSize.Width;
            st.ScaleX = widthchange;
            st.ScaleY = widthchange;

            for (int i = 0; i < scalables.Count; i++)
            {
                scalables[i].RenderTransform = st;
                //scalables[i].Width *= widthchange;
                //scalables[i].Width *= widthchange;
            }*/

            //this.RenderTransform = st;
            }
        }

        private void Drop2(object sender, DragEventArgs e)
        {
            
            Canvas canv = sender as Canvas;
            //Border border = canv.Parent as Border;
            Image img = new Image();
            string[] text = e.Data.GetFormats();
           /* img.Source = (e.Data.GetData("SurveyWinClient.View.DragNDropBlockInfo") as DragNDropBlockInfo).MainImage;
            Point droppoint = e.GetPosition(sender as IInputElement);
            double _COTop = droppoint.Y / canv.ActualHeight;
            double _COLeft = droppoint.X / canv.ActualWidth;
            double _COWidth = img.Source.Width / canv.ActualWidth;
            double _COHeight = img.Source.Height / canv.ActualHeight;
            CanvasOverrided.SetCOHeight(img, _COHeight);
            CanvasOverrided.SetCOWidth(img, _COWidth);
            CanvasOverrided.SetCOTop(img, _COTop);
            CanvasOverrided.SetCOLeft(img, _COLeft);
            scalables.Add(img);
            Canvas.SetTop(img, droppoint.Y);
            Canvas.SetLeft(img, droppoint.X);
            canv.Children.Add(img);*/
        }

        public double Zoom
        {
            get {
                double toret = (double)GetValue(MyZoomProperty);
                if (toret < 10)
                    return 0.1;
                else
                    return toret/100;
            }
            set { SetValue(MyZoomProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyString. This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyZoomProperty =
          DependencyProperty.Register("COZoom", typeof(double), typeof(CanvasOverrided), new UIPropertyMetadata(null));

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (loaded&&e.Property == MyZoomProperty)
            {

                //this.RenderTransform = new ScaleTransform(Zoom,Zoom);
                ViewboxParent.Height = startingSize.Height * Zoom * Scale;
                ViewboxParent.Width = startingSize.Width * Zoom * Scale;
                //this.Clip = new RectangleGeometry(new Rect(0, 0, GridParent.ColumnDefinitions[(Grid.GetColumn(this))].ActualWidth / Zoom, GridParent.ActualHeight / Zoom));
                

                //Zoom = MyString; // if MyString changed i set Title to visualize
            }
            base.OnPropertyChanged(e);
        }
       
        public double Scale {
            get { return (double)GetValue(ScaleProperty); }
            set { SetValue(ScaleProperty, value); } 
        }

        public double COScrollVertical
        {
            get { return this.ScrollViewerParent.VerticalOffset; }
            set { this.ScrollViewerParent.ScrollToVerticalOffset(value); }
        }

        private double COEffectiveWidth
        {
            get { return ScrollViewerParent.ActualWidth - SystemParameters.VerticalScrollBarWidth; }
        }

        public static readonly DependencyProperty ScaleProperty =
            DependencyProperty.Register("Scale", typeof(double), typeof(CanvasOverrided), new FrameworkPropertyMetadata(1.0));

        void CanvasOverrided_Loaded(object sender, RoutedEventArgs e)
        {
            //this.Background = Brushes.Black;
            ScrollViewerParent = FindParent<ScrollViewer>(this);
            ScrollViewerParent.SizeChanged += CanvasOverrided_SizeChanged;   
            GridParent = FindParent<Grid>(this);
            ViewboxParent = FindParent<Viewbox>(this);
            MDIWindowParent = FindParent<MDI.MDIWindow>(this);
            VWVParent = FindParent<VisualWindowView>(this);
            Binding myBinding;
            /*myBinding = new Binding("Height");
            myBinding.Source = ViewboxParent;
            this.SetBinding(Canvas.HeightProperty, myBinding);
            myBinding = new Binding("Width");
            myBinding.Source = ViewboxParent;
            this.SetBinding(Canvas.WidthProperty, myBinding);*/
           
            if (loaded)
                return;
            loaded = true;
            startingSize = new Size(COEffectiveWidth, ScrollViewerParent.ActualWidth * 2);
            Rectangle allrect = new Rectangle();
            allrect.Fill = Brushes.White;
            allrect.Height = startingSize.Height;
            allrect.Width = startingSize.Width;
            CanvasOverrided.SetCOHeight(allrect, startingSize.Height / startingSize.Width);
            CanvasOverrided.SetCOWidth(allrect, 1);
            CanvasOverrided.SetCOTop(allrect, 0);
            CanvasOverrided.SetCOLeft(allrect, 0);


            this.Children.Add(allrect);

            ViewboxParent.Width = startingSize.Width;
            ViewboxParent.Height = startingSize.Height;
            DocumentWrapper doc;
            ProjectWrpp project;

            Wrapper.Wrap(this.Dispatcher, out doc, out project);

            /*collection = new ObservableCollection<IElement>();

            

            var blocks = project.TreeObjects.Where(x => x is BlockWrpp);
            var headers = project.TreeObjects.Where(x => x is HeaderWrpp);
            
            var blockToFind = (XMLObjectWrpp)headers.First().Parent;

            while (!(blockToFind is BlockWrpp))
                     blockToFind = blockToFind.Parent;

            //ActualHeight.if (blockToFind.Childs().Contains(headers.First())) { MessageBox.Show("Пропускаю стакан и спать!"); }
            
                
            //collection.Add(new IElement("22",0,0,0,0));
            foreach (DocumentObject dObj in blocks)
            {
                CoordDatWrpp coords = ((DocumentObject)dObj).DesignElement.Coordinate;
                //collection.Add(new IElement(dObj.Text.Text, coords.m_Top, coords.m_Left, coords.m_Width, coords.m_Height));
                //collection.Add(new IElement(dObj.Text.Text, coords.m_Top, coords.m_Left, coords.m_Width, coords.m_Height));
            }

            foreach (DocumentObject dObj in headers)
            {
                CoordDatWrpp coords = ((DocumentObject)dObj).DesignElement.Coordinate;
                collection.Add(new IElement(dObj.Text.Text, coords.m_Top, coords.m_Left, coords.m_Width, coords.m_Height));
            }
            */
            //this.Clip = new RectangleGeometry(new Rect(0, 0, GridParent.ColumnDefinitions[(Grid.GetColumn(this))].ActualWidth, GridParent.ActualHeight));
            this.Height = startingSize.Height;
            this.Width = startingSize.Width;
            

            for (int i = 0; i < project.TreeObjects.Count; i++)
            {
                BlockWrpp currentBlock;
                if (project.TreeObjects[i] is BlockWrpp)
                {
                    
                    currentBlock = project.TreeObjects[i] as BlockWrpp;
                    Border blockBorder = new Border();

                    CoordDatWrpp blockCoords = ((DocumentObject)currentBlock).DesignElement.Coordinate;



                    CanvasOverrided.SetCOHeight(blockBorder,blockCoords.m_Height);
                    CanvasOverrided.SetCOWidth(blockBorder, blockCoords.m_Width);
                    CanvasOverrided.SetCOTop(blockBorder, blockCoords.m_Top);
                    CanvasOverrided.SetCOLeft(blockBorder, blockCoords.m_Left);

                    blockBorder.Height = blockCoords.m_Height * startingSize.Width;
                    blockBorder.Width = blockCoords.m_Width * startingSize.Width;

                    VisualBrush dashedBrush = new VisualBrush();
                    Rectangle rect = new Rectangle();
                    DoubleCollection tempCollection = new DoubleCollection();
                    tempCollection.Add(2);
                    tempCollection.Add(4);
                    rect.StrokeDashArray = tempCollection;
                    rect.Stroke = new SolidColorBrush(Color.FromRgb(30, 30, 30));
                    rect.StrokeThickness = 1;
                    

                    myBinding = new Binding("Height");
                    myBinding.Source = blockBorder;
                    rect.SetBinding(Rectangle.HeightProperty, myBinding);

                    myBinding = new Binding("Width");
                    myBinding.Source = blockBorder;
                    rect.SetBinding(Rectangle.WidthProperty, myBinding);

                    dashedBrush.Visual = rect;
                    blockBorder.BorderBrush = dashedBrush;
                    blockBorder.BorderThickness = new System.Windows.Thickness(1);

                    Canvas blockCanvas = new Canvas();

                    /*myBinding = new Binding("Width");
                    myBinding.Source = blockBorder;
                    blockCanvas.SetBinding(Rectangle.WidthProperty, myBinding);

                    myBinding = new Binding("Height");
                    myBinding.Source = blockBorder;
                    blockCanvas.SetBinding(Rectangle.HeightProperty, myBinding);
                    */
                    blockCanvas.AllowDrop = true;
                    blockCanvas.Background = Brushes.White;
                    blockCanvas.Drop += Drop2;
                    blockBorder.Child = blockCanvas;
                    //blockCanvas.Clip = new RectangleGeometry ( new Rect(-1,-1,blockBorder.Width-1,blockBorder.Height -1));

                    var list = currentBlock.Childs().Where(x => x is HeaderWrpp);
					foreach (HeaderWrpp currentHeader in list)
                    {
                        

                        Border headerBorder = new Border();
                        scalables.Add(headerBorder);

                        CoordDatWrpp headerCoords = ((DocumentObject)currentHeader).DesignElement.Coordinate;

                        CanvasOverrided.SetCOHeight(headerBorder, headerCoords.m_Height);
                        CanvasOverrided.SetCOWidth(headerBorder, headerCoords.m_Width);
                        CanvasOverrided.SetCOTop(headerBorder, headerCoords.m_Top);
                        CanvasOverrided.SetCOLeft(headerBorder, headerCoords.m_Left);

                        headerBorder.Height = headerCoords.m_Height * blockBorder.Height;
                        headerBorder.Width = headerCoords.m_Width * blockBorder.Width;

                        VisualBrush dashedBrush2 = new VisualBrush();
                        Rectangle rect2 = new Rectangle();
                        DoubleCollection tempCollection2 = new DoubleCollection();
                        tempCollection2.Add(1);
                        tempCollection2.Add(4);
                        rect2.StrokeDashArray = tempCollection2;
                        rect2.Stroke = new SolidColorBrush(Color.FromRgb(70, 70, 70));
                        rect2.StrokeThickness = 1;


                        myBinding = new Binding("Height");
                        myBinding.Source = headerBorder;
                        rect2.SetBinding(Rectangle.HeightProperty, myBinding);

                        myBinding = new Binding("Width");
                        myBinding.Source = headerBorder;
                        rect2.SetBinding(Rectangle.WidthProperty, myBinding);
                        
                        dashedBrush2.Visual = rect2;
                        headerBorder.BorderBrush = dashedBrush2;
                        headerBorder.BorderThickness = new System.Windows.Thickness(1);

                        ScaleTransform scaleTransform = new ScaleTransform();

                        //scaleTransform.ScaleX = 0.5;
                        //scaleTransform.ScaleY = 0.5;

                        Binding scaleBinding = new Binding("Scale");
                        scaleBinding.Source = this;

                        BindingOperations.SetBinding(scaleTransform, ScaleTransform.ScaleXProperty, scaleBinding);
                        BindingOperations.SetBinding(scaleTransform, ScaleTransform.ScaleYProperty, scaleBinding);
                       

                        RichTextBox textbox = new RichTextBox();
                        
                        textbox.LayoutTransform = scaleTransform;                                               

                        textbox.Background = Brushes.Transparent;
                        textbox.BorderThickness = new Thickness(0);
                        MemoryStream stream = new MemoryStream(ASCIIEncoding.Default.GetBytes(currentHeader.Text.Text));
                        textbox.Selection.Load(stream, DataFormats.Rtf);

                        headerBorder.Child = textbox;
                        blockCanvas.Children.Add(headerBorder);
                        Canvas.SetTop(headerBorder, headerCoords.m_Top * blockBorder.Height - 1);
                        Canvas.SetLeft(headerBorder, headerCoords.m_Left * blockBorder.Width - 1);
                    }
                    base.Children.Add(blockBorder);
                    Canvas.SetZIndex(blockBorder, i);
                    Canvas.SetTop(blockBorder, blockCoords.m_Top * startingSize.Width);
                    Canvas.SetLeft(blockBorder, blockCoords.m_Left * startingSize.Width);
                    double temp = blockCoords.m_Top * startingSize.Width + blockCoords.m_Height * startingSize.Width;
                    if (temp > desiredHeight)
                        desiredHeight = temp;
                }
            }

            
            /*for (int i = 0; i < collection.Count; i++)
            {
                BlockWrpp father;
                while (!(blockToFind is BlockWrpp))
                    blockToFind = blockToFind.Parent;

                Border border = new Border();
                border.Height = collection[i].Height * this.ActualWidth;
                border.Width = collection[i].Width * this.ActualWidth;
                VisualBrush dashedBrush = new VisualBrush();
                Rectangle rect = new Rectangle();
                DoubleCollection tempCollection = new DoubleCollection();
                tempCollection.Add(1);
                tempCollection.Add(4);
                rect.StrokeDashArray = tempCollection;
                rect.Stroke = new SolidColorBrush(Color.FromRgb(70, 70, 70));
                rect.StrokeThickness = 1;
                dashedBrush.Visual = rect;
                border.BorderBrush = dashedBrush;
                border.BorderThickness = new System.Windows.Thickness(5);


                Binding myBinding = new Binding("Height");
                myBinding.Source = border;
                rect.SetBinding(Rectangle.HeightProperty, myBinding);

                myBinding = new Binding("Width");
                myBinding.Source = border;
                rect.SetBinding(Rectangle.WidthProperty, myBinding);

                RichTextBox textbox = new RichTextBox();
                textbox.BorderThickness = new Thickness(0);
                MemoryStream stream = new MemoryStream(ASCIIEncoding.Default.GetBytes(collection[i].Text));
                textbox.Selection.Load(stream, DataFormats.Rtf);

                border.Child = textbox;
                base.Children.Add(border);
                Canvas.SetZIndex(textbox, i);
                Canvas.SetZIndex(border, i);
                Canvas.SetTop(border, collection[i].Top);
                Canvas.SetLeft(border, collection[i].Left);
                
            }*/
        }

    }
}