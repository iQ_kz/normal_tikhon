﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Survey;
using SurveyWinClient;
using SurveyWinClient.ViewModel;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for OpenLocalSurveyWindow.xaml
    /// </summary>
    /// 
    public class OpenLocalSurveyWindowListViewInfo
    {
        public string Name { get; set; }
        public System.DateTime Created { get; set; }
        public BitmapImage Status { get; set; }
        public int Edited { get; set; }
        public string Id { get; set; }
        public OpenLocalSurveyWindowListViewInfo(string name, System.DateTime datePrj, int edited, bool status, string _Id)
        {
            try
            {
                Id = _Id;
                Name = name;
                Created = datePrj;
                if (status)
                {
                    var uriSource = new Uri(@"/Resources/Windows/OpenLocalSurveyWindow/Icons/GreenCircleIcon.png", UriKind.Relative);
                    Status = new BitmapImage(uriSource);
                }
                else
                {
                    var uriSource = new Uri(@"/Resources/Windows/OpenLocalSurveyWindow/Icons/RedCircleIcon.png", UriKind.Relative);
                    Status = new BitmapImage(uriSource);
                }
                Edited = edited;
            }
            catch { }
        }
    };

    public partial class OpenLocalSurveyWindowView : Window
    {
        ObservableCollection<OpenLocalSurveyWindowListViewInfo> listViewCollection = new ObservableCollection<OpenLocalSurveyWindowListViewInfo>();

        public OpenLocalSurveyWindowView()
        {
            InitializeComponent();
            DataContext = MainViewModel.GetMainViewModel;
            try
            {
                Survey.FileManagerWrapper.GetInstance();

                foreach (ProjectInfoWrapper prj in Survey.FileManagerWrapper.GetInstance().ProjectInfos)
                {
                    listViewCollection.Add(new OpenLocalSurveyWindowListViewInfo(prj.Name, prj.CreateDate, prj.Edited, prj.Status,prj.ID));
                }
            }
            catch (Exception ex)
            { MessageBox.Show("open error"); }
            OpenLocalSurveyListView.ItemsSource = listViewCollection;

            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(OpenLocalSurveyListView.ItemsSource);
            view.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            view.SortDescriptions.Add(new SortDescription("Created", ListSortDirection.Ascending));
            view.SortDescriptions.Add(new SortDescription("Last Opened", ListSortDirection.Ascending));
            view.Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(searchFilter.Text))
                return true;
            else
                return ((item as OpenLocalSurveyWindowListViewInfo).Name.IndexOf(searchFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }

        private void searchFilter_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(OpenLocalSurveyListView.ItemsSource).Refresh();
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            OpenLocalSurveyWindowListViewInfo temp = OpenLocalSurveyListView.SelectedItem as OpenLocalSurveyWindowListViewInfo;
            if (temp != null)
            {
                MainViewModel mainvm = MainViewModel.GetMainViewModel;
                //Survey.DocumentManagerWrapper temp2 = Survey.DocumentManagerWrapper.GetInstance();
                mainvm.LoadProject(temp.Id);
            }
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OpenLocalSurveyListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //int ToIndex = OpenLocalSurveyListView.Items.IndexOf(((FrameworkElement)sender).DataContext);
            Load_Click(sender, e);
        }
    }


}
