﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for CloseLocalSurveyWindow.xaml
    /// </summary>
    public partial class CloseLocalSurveyWindowView : Window
    {
        public CloseLocalSurveyWindowView()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
       
    }
}
