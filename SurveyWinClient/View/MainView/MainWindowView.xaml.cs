﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CustomWindow;

using Survey;
using SurveyWinClient;


namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView : CustomWindow.CustomWindow
    {
        public MainWindowView()
        {
            InitializeComponent();

            Survey.FileManagerWrapper.Instance(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\work_dir", "temp_dir");
			Survey.DocumentManagerWrapper.Instance(this.Dispatcher);
            
            /*DocumentManagerWrapper _doc2 = Survey.DocumentManagerWrapper.GetInstance();

			DocumentWrapper _doc = Survey.DocumentManagerWrapper.GetInstance().CreateDocument("Document one11");
            ProjectWrpp _project = _doc.Project;
            ChapterWrpp chapterOne = new ChapterWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), "Chapter 11");
            _project.AddChild(chapterOne);
            _doc2.m_Documents[1].Save(new Dictionary<string, Dictionary<string, BitmapImage>>());
            _doc.Save(new Dictionary<string, Dictionary<string, BitmapImage>>() { });
            _doc2.CloseDocument(_doc);

            */
            //DocumentManagerWrapper _doc2 = Survey.DocumentManagerWrapper.GetInstance();
            /*
			
			
            DocumentManagerWrapper _doc2 = Survey.DocumentManagerWrapper.GetInstance();

			DocumentWrapper _doc = Survey.DocumentManagerWrapper.GetInstance().CreateDocument("Document one");
            

			ProjectWrpp _project = _doc.Project;

			TextsWrpp texts = _doc.Texts;
			DesignWrpp design = _doc.Design;
			BlockShemeDesignWrpp bsDesign = _doc.BlockShemeDesign;

			ChapterWrpp chapterOne = new ChapterWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), "Chapter 1");
			_project.AddChild(chapterOne);


			PageWrpp pageOne = new PageWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1.2, chapterOne.Id, 0), "Page 1");

			DesignElementWrpp des = pageOne.DesignElement;
			chapterOne.AddChild(pageOne);

			{
				BlockWrpp blockOne = new BlockWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 0.2, pageOne.Id, 0), "Block 01");
				pageOne.AddChild(blockOne);

				BlockWrpp blockTwo = new BlockWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0.25, 0, 0.7, 0.35, pageOne.Id, 0), "Block 02");
				pageOne.AddChild(blockTwo);

				{
					QaWrpp qA = new QaWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, blockTwo.Id, 0), "Q/A 1");
					blockTwo.AddChild(qA);

					{
						QuestionWrpp quest = new QuestionWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, qA.Id, 0), "Q: тестовая");
						qA.AddChild(quest);

						{
							ImageProperties imgProps = new ImageProperties();
							BorderDat borderDat = new BorderDat();

							HeaderWrpp headerOne = new HeaderWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 0.5, 0.45, quest.Id, 0)
								, @"{\rtf1\ansi{\fonttbl{\f0\fcharset0;}}\f0\fs20\u1090?\u1077?\u1089?\u1090?\u1086?\u1074?\u1072?\u1103? \u1089?\u1090?\u1088?\u1086?\u1082?\u1072?  \u8470?1}"
								, imgProps, borderDat
								, "тестовая");

							quest.AddChild(headerOne);

							HeaderWrpp headerTwo = new HeaderWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0.1, 0.25, 0.5, 0.3, quest.Id, 0)
								, @"{\rtf1\ansi{\fonttbl{\f0\fcharset0;}}{\colortbl;\red255;\red0;}\f0\fs28\u1090?\u1077?\u1089?\u1090?\u1086?\u1074?\u1072?\u1103? \cf1\u1089?\u1090?\u1088?\u1086?\u1082?\u1072?\cf2  \u8470?2}"
								, imgProps, borderDat
								, "тестовая 2");

							quest.AddChild(headerTwo);
						}

					}

				}

			}
            _doc2.m_Documents[1].Save(new Dictionary<string, Dictionary<string, BitmapImage>>());
            _doc.Save(new Dictionary<string, Dictionary<string, BitmapImage>>() { });
            _doc2.CloseDocument(_doc);*/


			// 			Survey.DocumentWrapper newDoc = Survey.DocumentManagerWrapper.GetInstance().CreateDocumentByID("47acdc49-5acb-496a-a2ef-44309e7b6650");
			// 			if (newDoc == null)
			// 				return;
			// 
			//Survey.ObservableRangeCollection<ChapterWrpp> chapters = _doc.Project.Chapters;

            //foreach (ChapterWrpp chapter in chapters)
            //{
            //    Survey.ObservableRangeCollection<PageWrpp> pages = chapter.Pages;

            //    foreach (PageWrpp page in pages)
            //    {
            //        Survey.ObservableRangeCollection<XMLObjectWrpp> childs = page.ChildsTree;

            //        foreach (XMLObjectWrpp xmlObject in childs)
            //        {
            //            if(xmlObject is HeaderWrpp)
            //            {
            //                HeaderWrpp hdrWrpp = (HeaderWrpp)xmlObject;
            //                HeaderDesignElement headerDesEl = hdrWrpp.DesignElement as HeaderDesignElement;

            //                ImageElement imgEl = headerDesEl.ImageElementDesign;
            //                BorderElement bordEl = headerDesEl.BorderElementDesign;

            //                bordEl.ShadowSize = 43;
            //            }

            //            DocumentObject docObj = xmlObject as DocumentObject;
            //            /*if (docObj != null)
            //                MessageBox.Show(docObj.Name + " " + docObj.Id);*/
            //        }

            //    }

            //}
            /*
			System.IO.FileStream fileStream = new System.IO.FileStream("600488.bmp", System.IO.FileMode.Open, System.IO.FileAccess.Read);

			var img = new System.Windows.Media.Imaging.BitmapImage();
			img.BeginInit();
			img.StreamSource = fileStream;
			img.EndInit();

			Dictionary<string, BitmapImage> pageFiles = new Dictionary<string, BitmapImage>() { {"page 1", img} };

			_doc.Save(new Dictionary<string, Dictionary<string, BitmapImage>>() { { "project", pageFiles } });

			pageOne.AddChild( new BlockWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0.25, 0, 0.7, 0.35, pageOne.Id, 0), "Block XXX"));

			System.Threading.Thread.Sleep(2000);

			Survey.ImageWrpp imgWrpp = _doc.Images.AddImage("600488.bmp");
			String id = "";
			if (imgWrpp != null)
			{
				id = imgWrpp.ID;
				BitmapImage bmp = imgWrpp.ImageData;
				double fdf = 645;
			}

			_doc.Save(new Dictionary<string, Dictionary<string, BitmapImage>>() { { "project", pageFiles } });

			_doc.ToPreviosRevision();
			_doc.ToNextRevision();

			string idDoc = _doc.ID;

			Survey.DocumentManagerWrapper.GetInstance().CloseDocument(_doc);

			_doc = Survey.DocumentManagerWrapper.GetInstance().LoadDocumentByID(idDoc);


			List<ProjectInfoWrapper> projectInfos = Survey.FileManagerWrapper.GetInstance().ProjectInfos;

            */

// 
// 			List<BackupProjectProperties> backupProjectProperties = _doc.GetAllBackupDescriptions();
// 			_doc.SetBackup(backupProjectProperties[0].m_Hash);

//			double fdf2 = 434;


            //var lines = project.TreeObjects.Cast<DocumentObject>().Select(x => x.Name + " " + x.Id);

            //var msg = string.Join(Environment.NewLine, lines);
                                         

            //var page1 = pages[0];

            //var blocks = page1.Childs().Where(x => x is BlockWrpp);

            //var rects = blocks.Select(x => x.Childs()).Cast<DocumentObject>();

           // var collection = rects.Select(x => x.DesignElement.Coordinate);
           // rects.OrderBy(x => x.ZIndex)
            //var lines2 = design.Designs.Select(x => string.Join(";", x.Coordinate.m_Top, x.Coordinate.m_Left, x.Coordinate.m_Width, x.Coordinate.m_Height, x.Color.m_R));
            
            //var msg2 = string.Join(Environment.NewLine, lines);
            
            //MessageBox.Show(msg2);  
            

// 			try
// 			{
// 				List<ChapterWrpp> chapters = project.Childs<ChapterWrpp>();
// 				List<PageWrpp> pages = chapters[0].Childs<PageWrpp>();
// 				List<BlockWrpp> blocks = pages[0].Childs<BlockWrpp>();
// 
// 				DesignElementWrpp designBlock = blocks[0].DesignElement;
// 
// 				CoordDatWrpp coordBlock = designBlock.Coordinate;
// 				MessageBox.Show(coordBlock.m_Top.ToString());
// 			}
// 			catch(Exception ex)
// 			{
// 
// 			}

        }

		private void CustomWindow_Closed(object sender, EventArgs e)
		{
			Survey.FileManagerWrapper.Clear();
			Survey.DocumentManagerWrapper.Clear();
		}

    }
}
