﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Survey;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for ProjectTabItem.xaml
    /// </summary>
    public partial class ProjectTabItemView : UserControl
    {
        public static readonly DependencyProperty ProjectIDProperty =
        DependencyProperty.Register("ProjectID", typeof(int), typeof(ProjectTabItemView), new UIPropertyMetadata(null));
        public int ProjectID
        {
            get { return (int)GetValue(ProjectIDProperty); }
            set { SetValue(ProjectIDProperty, value); }
        }

        public ProjectTabItemView()
        {
            var binding = new Binding("ProjectID");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(ProjectIDProperty, binding);
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            CloseLocalSurveyWindowView modal = new CloseLocalSurveyWindowView();
            modal.DataContext = this.DataContext;
            modal.ShowDialog();
            
        }
    }
}
