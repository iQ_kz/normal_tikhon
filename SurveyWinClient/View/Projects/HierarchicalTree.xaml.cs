﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SurveyWinClient.View;
using Survey;

namespace SurveyWinClient.View
{
    public partial class HierarchicalTree : UserControl
    {
        public ObservableCollection<ChapterWrpp> pages { get; set; }

        DocumentWrapper _doc;
        public int SelectedIndex;
        public HierarchicalTree()
        {
            _doc = ProjectsTabControlView.GetActiveProject();
            pages=_doc.Project.Chapters;
            //DocumentObject asdasd;
            //asdasd.Childs
            InitializeComponent();

            //SelectedIndex = 1;
            /*DocumentObject*pages.Add(new Page { Title1 = "Page 1" });
            pages.Add(new Page { Title1 = "Page 2" });

            pages[0].blocks.Add(new Block { Title2 = "Block 1" });
            pages[0].blocks.Add(new Block { Title2 = "Block 2" });

            pages[1].blocks.Add(new Block { Title2 = "Block 1" });
            pages[1].blocks.Add(new Block { Title2 = "Block 2" });

            pages[0].blocks[0].questions.Add(new Question { Title3 = "Question 1" });
            
            */
            //this.DataContext = pages;
        }

        //FOR DRAG&DROP
        Point _lastMouseDown;
        TreeViewItem draggedItem, _target;

        private void TreeView_MouseDown (object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                _lastMouseDown = e.GetPosition(TreeBlock);
            }
        }


        private void TreeView_DragOver(object sender, DragEventArgs e)
        {
            try
            {
                Point currentPosition = e.GetPosition(TreeBlock);
                if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
                    (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
                {
                    // Verify that this is a valid drop and then store the drop target
                    TreeViewItem item = GetNearestContainer(e.OriginalSource as UIElement);
                    if (CheckDropTarget(draggedItem, item))
                    {
                        e.Effects = DragDropEffects.Move;
                    }
                    else
                    {
                        e.Effects = DragDropEffects.None;
                    }
                }
                e.Handled = true;
            }
            catch (Exception)
            {
            }
        }
        private void TreeView_Drop(object sender, DragEventArgs e)
        {
            try
            {
                e.Effects = DragDropEffects.None;
                e.Handled = true;
                // Verify that this is a valid drop and then store the drop target
                TreeViewItem TargetItem = GetNearestContainer(e.OriginalSource as UIElement);
                if (TargetItem != null && draggedItem != null)
                {
                    _target = TargetItem;
                    e.Effects = DragDropEffects.Move;
                }
            }
            catch (Exception)
            {
            }
        }

        private bool CheckDropTarget(TreeViewItem _sourceItem, TreeViewItem _targetItem)
        {
            //Check whether the target item is meeting your condition
            bool _isEqual = false;
            if (!_sourceItem.Header.ToString().Equals(_targetItem.Header.ToString()))
            {
                _isEqual = true;
            }
            return _isEqual;
        }

        private void CopyItem(TreeViewItem _sourceItem, TreeViewItem _targetItem)
        {
            //Asking user wether he want to drop the dragged TreeViewItem here or not
            if (MessageBox.Show("Would you like to drop " + _sourceItem.Header.ToString() + " into " + _targetItem.Header.ToString() + "", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                try
                {
                    //adding dragged TreeViewItem in target TreeViewItem
                    addChild(_sourceItem, _targetItem);

                    //finding Parent TreeViewItem of dragged TreeViewItem 
                    TreeViewItem ParentItem = FindVisualParent<TreeViewItem>(_sourceItem);
                    // if parent is null then remove from TreeView else remove from Parent TreeViewItem
                    if (ParentItem == null)
                    {
                        TreeBlock.Items.Remove(_sourceItem);
                    }
                    else
                    {
                        ParentItem.Items.Remove(_sourceItem);
                    }
                }
                catch
                {
                }
            }
        }

        public void addChild(TreeViewItem _sourceItem, TreeViewItem _targetItem)
        {
            // add item in target TreeViewItem 
            TreeViewItem item1 = new TreeViewItem();
            item1.Header = _sourceItem.Header;
            _targetItem.Items.Add(item1);
            foreach (TreeViewItem item in _sourceItem.Items)
            {
                addChild(item, item1);
            }
        }
        
        static TObject FindVisualParent<TObject>(UIElement child) where TObject : UIElement
        {
            if (child == null)
            {
                return null;
            }

            UIElement parent = VisualTreeHelper.GetParent(child) as UIElement;

            while (parent != null)
            {
                TObject found = parent as TObject;
                if (found != null)
                {
                    return found;
                }
                else
                {
                    parent = VisualTreeHelper.GetParent(parent) as UIElement;
                }
            }
            return null;
        }

        private TreeViewItem GetNearestContainer(UIElement element)
        {
            // Walk up the element tree to the nearest tree view item.
            TreeViewItem container = element as TreeViewItem;
            while ((container == null) && (element != null))
            {
                element = VisualTreeHelper.GetParent(element) as UIElement;
                container = element as TreeViewItem;
            }
            return container;
        }

    
        private void TreeView_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    Point currentPosition = e.GetPosition(TreeBlock);
                    if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
                        (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
                    {
                        draggedItem = (TreeViewItem)TreeBlock.SelectedItem;
                        if (draggedItem != null)
                        {
                            DragDropEffects finalDropEffect = DragDrop.DoDragDrop(TreeBlock, TreeBlock.SelectedValue,
                                DragDropEffects.Move);
                            //Checking target is not null and item is dragging(moving)
                            if ((finalDropEffect == DragDropEffects.Move) && (_target != null))
                            {
                                // A Move drop was accepted
                                if (!draggedItem.Header.ToString().Equals(_target.Header.ToString()))
                                {
                                    CopyItem(draggedItem, _target);
                                    _target = null;
                                    draggedItem = null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }



        /*private void AddTreeItem_Click(object sender, RoutedEventArgs e)
        {
            Survey.DocumentManagerWrapper.Instance(this.Dispatcher);
            DocumentWrapper _doc = Survey.DocumentManagerWrapper.GetInstance().Documents[ProjectsTabControlView.GlobalTabControlView.MainTabControl.SelectedIndex + 1];
            //PageWrpp pageOne = new PageWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1.2, _doc.Project.Chapters[0].Id, 0), "Page " + (_doc.Project.Chapters[0].Childs.Count + 1));
            //_doc.Project.Chapters[0].AddChild(pageOne);
            SelectedIndex = -1;
            SelectedIndex = _doc.Project.Chapters[0].Pages.Count - 1;
        }

        private void Tree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            int index = 0;
            if (TreeBlock.Items.Count >= 0)
            {
                var tree = sender as TreeView;
                if (TreeBlock.SelectedValue != null)
                {
                    index++;
                    TreeViewItem item = tree.SelectedItem as TreeViewItem;
                    //TreeViewItem item = (TreeViewItem)tree.ItemContainerGenerator.ContainerFromItem(newW);
                    ItemsControl parent = ItemsControl.ItemsControlFromItemContainer(item);
                    while (parent != null && parent.GetType() == typeof(TreeViewItem))
                    {
                        index++;
                        parent = ItemsControl.ItemsControlFromItemContainer(parent);
                    }
                    SelectedIndex = index;
                    
                    item.IsSelected = true;
                }
            }
        }     */
    }   
     
    public class Block
    {
        public string Title2 { get; set; }
        public ObservableCollection<Question> questions { get; set; }

        public Block()
        {
            this.Title2 = Title2;
            questions = new ObservableCollection<Question>();
        }
    }

    public class Question
    {
        public string Title3 { get; set; }

        public Question()
        {
            this.Title3 = Title3;
        }
    }
}
