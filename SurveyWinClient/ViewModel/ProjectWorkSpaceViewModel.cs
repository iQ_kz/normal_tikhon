﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using Survey;

using MVVM;

using Base.Interfaces;
using System.Windows.Media.Imaging;

namespace SurveyWinClient.ViewModel
{
    public class ProjectWorkSpaceViewModel : ViewModelBase, IProjectWorkSpace
    {
        private string guid;
        private DocumentManagerWrapper documentManager;
        public DocumentWrapper Document {get;set;}

        public void InvalidateEditors()
        {
            for (int i = 0; i < Modes.Count; i++)
            {
                Modes[i].Window.IsInvalid = true;
            }
        }

        public void InvalidateTypewriter()
        {
            for (int i = 0; i < Modes.Count; i++)
            {
                if (Modes[i].Window is TypeWriterWindowViewModel)
                {
                    Modes[i].Window.IsInvalid = true;
                    break;
                }
            }
        }

        private void InitializeInternals()
        {
            DeleteProject = new RelayCommand(deleteProject, p => { return true; });
            SaveAndCloseProject = new RelayCommand(saveAndCloseProject, p => { return true; });
            EditorsMenuVisibility = true;
           // ToggleEditorsMenuVisibility = new RelayCommand(toggleEditorsMenuVisibility, p => true);
        }

        public ProjectWorkSpaceViewModel(string guid)
        {
            InitializeInternals();
            documentManager = Survey.DocumentManagerWrapper.GetInstance();
            Document = documentManager.LoadDocumentByID(guid);
            Name = Document.Name;
            guid = Document.ID;

            IMode[] _modes = ProjectLoadSettings();

            Modes = new ObservableRangeCollection<IMode>(_modes.Where(x => x.IsEnabled));
            Windows = new ObservableRangeCollection<IModeWindow>(Modes.Where(x => x.Window.IsDisplayed).Select(x => x.Window)); ;


        }
        private IMode[] ProjectLoadSettings()
        {

            IMode[] _imode = new IMode[]{new VisualViewModel(true, new VisualMenuViewModel(), new VisualWindowViewModel() { Left = 0, Top = 0, Height = 300, Width = 300, ZIndex = 7, IsDisplayed = true }),
                                    new TreeViewViewModel(true, new TreeViewMenuViewModel(), new TreeViewWindowViewModel() { Left = 10, Top = 10, Height = 300, Width = 300, ZIndex = 8, IsDisplayed = true }),
                                    new FlowChartViewModel(true, new FlowChartMenuViewModel(), new FlowChartWindowViewModel() { Left = 20, Top = 20, Height = 300, Width = 300, ZIndex = 6, IsSelected = true, IsDisplayed = true }),
                                    new TypeWriterViewModel(true, new TypeWriterMenuViewModel(), new TypeWriterWindowViewModel() { Left = 30, Top = 30, Height = 300, Width = 300, ZIndex = 5, IsDisplayed = true }),
                                    new LogicViewModel(true, new LogicMenuViewModel(), new LogicWindowViewModel() { Left = 40, Top = 40, Height = 300, Width = 300, ZIndex = 4, IsDisplayed = true }),
                                    new StatementOfWorkViewModel(false, new StatementOfWorkMenuViewModel(), new StatementOfWorkWindowViewModel() { Left = 50, Top = 50, Height = 300, Width = 300, ZIndex = 3 }),
                                    new ToDoViewModel(false, new ToDoMenuViewModel(), new ToDoWindowViewModel() { Left = 60, Top = 60, Height = 300, Width = 300, ZIndex = 2 }),
                                    new RulesViewModel(false, new RulesMenuViewModel(), new RulesWindowViewModel() { Left = 70, Top = 70, Height = 300, Width = 300, ZIndex = 1 }),
                                    new CompileViewModel(false, new CompileMenuViewModel(), new CompileWindowViewModel() { Left = 80, Top = 80, Height = 300, Width = 300, ZIndex = 0 })};

            return _imode;
        }
        public ProjectWorkSpaceViewModel(string _name, params IMode[] _modes)
        {
            InitializeInternals();
            Name = _name;
            
            documentManager = Survey.DocumentManagerWrapper.GetInstance();
            Document = documentManager.CreateDocument(name);
            Document.Project.AddChild(new ChapterWrpp(Document, new ColorDatWrpp(0, 0, 0, 0), new CoordDatWrpp(0, 0, 0, 0, 0), "Chapter 1"));
            guid = Document.ID;
            
			
            Modes = new ObservableRangeCollection<IMode>(_modes.Where(x => x.IsEnabled));
            Windows = new ObservableRangeCollection<IModeWindow>(Modes.Where(x => x.Window.IsDisplayed).Select(x => x.Window));
            
			/*DocumentWrapper _doc = Survey.DocumentManagerWrapper.GetInstance().CreateDocument(name);
            ChapterWrpp chapterOne = new ChapterWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), "Chapter 1");
            _doc.Project.AddChild(chapterOne);
            
            ProjectWrpp _project = _doc.Project;

            PageWrpp pageOne = new PageWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1.2, chapterOne.Id, 0), "Page 1");
            chapterOne.AddChild(pageOne);

            {
                BlockWrpp blockOne = new BlockWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 0.2, pageOne.Id, 0), "Block 01");
                pageOne.AddChild(blockOne);

                BlockWrpp blockTwo = new BlockWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0.25, 0, 0.7, 0.35, pageOne.Id, 0), "Block 02");
                pageOne.AddChild(blockTwo);

                {
                    QaWrpp qA = new QaWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, blockTwo.Id, 0), "Q/A 1");
                    blockTwo.AddChild(qA);

                    {
                        QuestionWrpp quest = new QuestionWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, qA.Id, 0), "Q: тестовая");
                        qA.AddChild(quest);

                        {
                            ImageProperties imgProps = new ImageProperties();
                            BorderDat borderDat = new BorderDat();

                            HeaderWrpp headerOne = new HeaderWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 0.5, 0.45, quest.Id, 0)
                                , @"{\rtf1\ansi{\fonttbl{\f0\fcharset0;}}\f0\fs20\u1090?\u1077?\u1089?\u1090?\u1086?\u1074?\u1072?\u1103? \u1089?\u1090?\u1088?\u1086?\u1082?\u1072?  \u8470?1}"
                                , imgProps, borderDat
                                , "тестовая");

                            quest.AddChild(headerOne);

                            HeaderWrpp headerTwo = new HeaderWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0.1, 0.25, 0.5, 0.3, quest.Id, 0)
                                , @"{\rtf1\ansi{\fonttbl{\f0\fcharset0;}}{\colortbl;\red255;\red0;}\f0\fs28\u1090?\u1077?\u1089?\u1090?\u1086?\u1074?\u1072?\u1103? \cf1\u1089?\u1090?\u1088?\u1086?\u1082?\u1072?\cf2  \u8470?2}"
                                , imgProps, borderDat
                                , "тестовая 2");

                            quest.AddChild(headerTwo);
                        }

                    }

                }

            }*/
            

           
            
            //editors visible - from rules, settings - from file(?)
            //windows visible + settings - from file(?)

           // Modes.AddRange(_modes.Where(x => x.IsEnabled));
            //ActiveMode.Window.IsSelected = false;
            

            //Windows.AddRange(_modes.Where(x => x.IsEnabled).Select(x => x.Window));

            //ActiveMode = Modes[2];   

            //if (Name == "Survey 1") ActiveMode = Modes[3];
             
                                  
        }

        MainViewModel parent;
        public MainViewModel Parent
        {
            get { return parent; }
            set
            {
                parent = value;
                OnPropertyChanged("Parent");
            }
        }

        string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        
        bool switchByWindow = false;

        #region tab

        /*int projectID;
        public int ProjectID
        {
            get { return projectID; }
            set
            {
                projectID = value;
                OnPropertyChanged("ProjectID");
            }
        }*/

        //int activeModeIndex;
        //public int ActiveModeIndex
        //{
        //    get { return activeModeIndex; }
        //    set
        //    {               
        //        activeModeIndex = value;                
        //        OnPropertyChanged("ActiveModeIndex");                
        //    }
        //}
        
      
        private void ProjectSaveSettings()
        {
            
        }        

        private int MaxZIndex
        {
            get
            {
                int mzi = -1;

                foreach (IModeWindow wnd in Windows)
                     if (wnd.ZIndex > mzi && wnd.ZIndex != 100)
                         mzi = wnd.ZIndex;

                return mzi;
            }
        }

        IMode activeMode;
        public IMode ActiveMode
        {
            get { return activeMode; }
            set
            {
               
               activeMode = value;
               
                   if (!switchByWindow && activeMode != null) //источник - не окно, выводим его наверх
                   {

                       if (!Windows.Contains(activeMode.Window))
                       {                            
                           activeMode.Window.IsDisplayed = true;
                           activeMode.Window.ZIndex = MaxZIndex + 1;

                           if (Windows.Count == 1) Windows[0].IsClosable = true;
                           Windows.Add(activeMode.Window);
                           

                           activeMode.Window.IsSelected = true;                           
                       }
                       else
                       {
                           activeMode.Window.IsSelected = true;                           
                       }
                       //datatrig что посл нельзя закрывать
                   }    
                                    
               OnPropertyChanged("ActiveMode");               

            }
        }

        public ObservableRangeCollection<IMode> Modes { get; private set; }

        #endregion

        #region win

        IModeWindow activeModeWindow;
        public IModeWindow ActiveModeWindow
        {
            get { return activeModeWindow; }
            set
            {               
     
                activeModeWindow = value;                                    
                    
                        if (activeModeWindow != null)
                        {
                            switchByWindow = true;

                                IMode mode = Modes.FirstOrDefault(x => x.Window == activeModeWindow);
                                ActiveMode = mode;

                            switchByWindow = false;
                        }
                        else { ActiveMode = null; }                       
                   

                OnPropertyChanged("ActiveModeWindow");
            }
        }

        private bool editorsMenuVisibility;
        public bool EditorsMenuVisibility
        {
            get { return editorsMenuVisibility; }
            set
            {
                editorsMenuVisibility = value;
                OnPropertyChanged("EditorsMenuVisibility");
            }
        }

        //int activeModeWindowIndex;
        //public int ActiveModeWindowIndex
        //{
        //    get { return activeModeWindowIndex; }
        //    set
        //    {
        //        activeModeWindowIndex = value;
        //        OnPropertyChanged("ActiveModeWindowIndex");                
        //    }
        //}               

        /* private Visibility editorsMenuVisibility;
         public Visibility EditorsMenuVisibility
         {
             get { return editorsMenuVisibility; }
             set
             {
                 editorsMenuVisibility = value;
                 OnPropertyChanged("EditorsMenuVisibility");
             }
         }

         public RelayCommand ToggleEditorsMenuVisibility 
         {
             get;
             private set;
         }

         public void toggleEditorsMenuVisibility(object _param)
         {
             if (EditorsMenuVisibility == Visibility.Visible)
             {
                 EditorsMenuVisibility = Visibility.Collapsed;
             }
             else
                 EditorsMenuVisibility = Visibility.Visible;
         }*/

        public RelayCommand DeleteProject
        {
            get;
            private set;
        }

        public RelayCommand SaveAndCloseProject
        {
            get;
            private set;
        }
        

        void deleteProject(object _param)
        {
            documentManager.CloseDocument(Document); //закрывается проект александра
            Parent.DelProject(this);
        }
        public void SaveProject()        
        {
            //documentManager.m_Documents[temp].Save(new Dictionary<string, Dictionary<string, BitmapImage>>());
            Document.Save(new Dictionary<uint, BitmapImage>() { });
            ProjectSaveSettings();
        }

        public void saveAndCloseProject(object _param)
        {
            SaveProject();
            deleteProject(true);
        }

        public ObservableRangeCollection<IModeWindow> Windows { get; private set; }

        #endregion

    }
}
