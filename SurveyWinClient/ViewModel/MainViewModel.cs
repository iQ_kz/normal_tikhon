﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

using MVVM;
using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        public ObservableRangeCollection<ProjectWorkSpaceViewModel> Projects { get; set; }
        public static MainViewModel GetMainViewModel { get; set; }
        int activeProjectIndex;
        public int ActiveProjectIndex
        {
            get { return activeProjectIndex; }
            set
            {
                activeProjectIndex = value;
                OnPropertyChanged("ActiveProjectIndex");
            }
        }

        ProjectWorkSpaceViewModel activeProject;
        public ProjectWorkSpaceViewModel ActiveProject
        {
            get { return activeProject; }
            set
            {
                activeProject = value;
                OnPropertyChanged("ActiveProject");
            }
        }


        public void DelProject(ProjectWorkSpaceViewModel proj)
        {
            Projects.Remove(proj);
            /*for (int i = 0; i < Projects.Count; i++)
            {
                Projects[i].ProjectID = i;
            }*/
        }

        public void LoadProject(string guid)
        {
            for (int i = 0; i < Projects.Count; i++)
            {
                if (Projects[i].Document.ID == guid)
                {
                    ActiveProjectIndex = i;
                    return;
                }
            }
            Projects.Add(new ProjectWorkSpaceViewModel(guid));
            Projects[Projects.Count - 1].Parent = this;
        }

       /* public RelayCommand AddProject
        {
            get;
            private set;
        }*/

        public void AddProject2(string name)
        {

            var modes = new IMode[]{new VisualViewModel(true, new VisualMenuViewModel(), new VisualWindowViewModel() { Left = 0, Top = 0, Height = 300, Width = 300, ZIndex = 7, IsClosable = true, IsDisplayed = true }),
                                    new TreeViewViewModel(true, new TreeViewMenuViewModel(), new TreeViewWindowViewModel() { Left = 10, Top = 10, Height = 300, Width = 300, ZIndex = 8, IsClosable = true, IsDisplayed = true }),
                                    new FlowChartViewModel(false, new FlowChartMenuViewModel(), new FlowChartWindowViewModel() { Left = 20, Top = 20, Height = 300, Width = 300, ZIndex = 6, IsClosable = true, IsDisplayed = true}),
                                    new TypeWriterViewModel(true, new TypeWriterMenuViewModel(), new TypeWriterWindowViewModel() { Left = 30, Top = 30, Height = 300, Width = 300, ZIndex = 5, IsClosable = true, IsDisplayed = true, IsSelected = true  }),
                                    new LogicViewModel(false, new LogicMenuViewModel(), new LogicWindowViewModel() { Left = 40, Top = 40, Height = 300, Width = 300, ZIndex = 4, IsClosable = true, IsDisplayed = true }),
                                    new StatementOfWorkViewModel(false, new StatementOfWorkMenuViewModel(), new StatementOfWorkWindowViewModel() { Left = 50, Top = 50, Height = 300, Width = 300, ZIndex = 3, IsClosable = true }),
                                    new ToDoViewModel(false, new ToDoMenuViewModel(), new ToDoWindowViewModel() { Left = 60, Top = 60, Height = 300, Width = 300, ZIndex = 2, IsClosable = true }),
                                    new RulesViewModel(false, new RulesMenuViewModel(), new RulesWindowViewModel() { Left = 70, Top = 70, Height = 300, Width = 300, ZIndex = 1, IsClosable = true, }),
                                    new CompileViewModel(true, new CompileMenuViewModel(), new CompileWindowViewModel() { Left = 80, Top = 80, Height = 300, Width = 300, ZIndex = 0, IsClosable = true })};
 

            Projects.Add(
                new ProjectWorkSpaceViewModel(name, modes));
            //Projects[Projects.Count - 1].ProjectID = Projects.Count;

            Projects[Projects.Count - 1].Parent = this;
        }

        

        /*public void addProject(object _param, string guid)
        {

            Projects.Add(
                new ProjectWorkSpaceViewModel(guid, "Project " + Projects.Count,
                    new VisualViewModel(true, new VisualMenuViewModel(), new VisualWindowViewModel() { Left = 0, Top = 0, Height = 300, Width = 300, ZIndex = 7 }),
                                    new TreeViewViewModel(true, new TreeViewMenuViewModel(), new TreeViewWindowViewModel() { Left = 10, Top = 10, Height = 300, Width = 300, ZIndex = 8 }),
                                    new FlowChartViewModel(true, new FlowChartMenuViewModel(), new FlowChartWindowViewModel() { Left = 20, Top = 20, Height = 300, Width = 300, ZIndex = 6, IsSelected = true }),
                                    new TypeWriterViewModel(true, new TypeWriterMenuViewModel(), new TypeWriterWindowViewModel() { Left = 30, Top = 30, Height = 300, Width = 300, ZIndex = 5 }),
                                    new LogicViewModel(true, new LogicMenuViewModel(), new LogicWindowViewModel() { Left = 40, Top = 40, Height = 300, Width = 300, ZIndex = 4 }),
                                    new StatementOfWorkViewModel(false, new StatementOfWorkMenuViewModel(), new StatementOfWorkWindowViewModel() { Left = 50, Top = 50, Height = 300, Width = 300, ZIndex = 3 }),
                                    new ToDoViewModel(false, new ToDoMenuViewModel(), new ToDoWindowViewModel() { Left = 60, Top = 60, Height = 300, Width = 300, ZIndex = 2 }),
                                    new RulesViewModel(false, new RulesMenuViewModel(), new RulesWindowViewModel() { Left = 70, Top = 70, Height = 300, Width = 300, ZIndex = 1 }),
                                    new CompileViewModel(false, new CompileMenuViewModel(), new CompileWindowViewModel() { Left = 80, Top = 80, Height = 300, Width = 300, ZIndex = 0 }))
                );
            Projects[Projects.Count - 1].ProjectID = Projects.Count;
            Projects[Projects.Count - 1].Parent = this;
        }
        */

        private bool projectsLineVisibility;
        public bool ProjectsLineVisibility
        {
            get { return projectsLineVisibility; }
            set
            {
                projectsLineVisibility = value;
                OnPropertyChanged("ProjectsLineVisibility");
            }
        }

       /* private Visibility projectsLineVisibility;
        public Visibility ProjectsLineVisibility
        {
            get { return projectsLineVisibility; }
            set
            {
                projectsLineVisibility = value;
                OnPropertyChanged("ProjectsLineVisibility");
            }
        }

        public RelayCommand ToggleProjectsLineVisibility
        {
            get;
            private set;
        }

        public void toggleProjectsLineVisibility(object _param)
        {
            if(ProjectsLineVisibility == Visibility.Visible) {
                ProjectsLineVisibility = Visibility.Collapsed;
            } else
                ProjectsLineVisibility = Visibility.Visible;
        }
        */

        #region Constructor

        public MainViewModel(IEnumerable<ProjectWorkSpaceViewModel> _projects)
        {
            GetMainViewModel = this;
            //AddProject = new RelayCommand(addProject, p => true);
            ProjectsLineVisibility = true;
           // ProjectsLineVisibility = new RelayCommand(ProjectsLineVisibility, p => true);
            Projects = new ObservableRangeCollection<ProjectWorkSpaceViewModel>(_projects);
            for (int i = 0; i < Projects.Count; i++)
            {
                //Projects[i].ProjectID = i;
                Projects[i].Parent = this;
            }           
        }

        #endregion
    }
}
