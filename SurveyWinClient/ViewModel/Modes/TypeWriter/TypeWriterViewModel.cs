﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class TypeWriterViewModel : ViewModelBase, IMode
    {
        public TypeWriterViewModel(bool _enabled, TypeWriterMenuViewModel _menu, TypeWriterWindowViewModel _window)
        {
            IsEnabled = _enabled;
            Menu = _menu;
            Window = _window;
        }

        bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        TypeWriterMenuViewModel menu;
        public IModeMenu Menu
        {
            get { return menu; }
            set
            {
                menu = (TypeWriterMenuViewModel)value;
                OnPropertyChanged("Menu");
            }
        }

        TypeWriterWindowViewModel window;
        public IModeWindow Window
        {
            get { return window; }
            set
            {
                window = (TypeWriterWindowViewModel)value;
                OnPropertyChanged("Window");
            }
        }
    }
}
