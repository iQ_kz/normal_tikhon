﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class CompileViewModel : ViewModelBase, IMode
    {
        public CompileViewModel(bool _enabled, CompileMenuViewModel _menu, CompileWindowViewModel _window)
        {
            IsEnabled = _enabled;
            Menu = _menu;
            Window = _window;
        }

        bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        CompileMenuViewModel menu;
        public IModeMenu Menu
        {
            get { return menu; }
            set
            {
                menu = (CompileMenuViewModel)value;
                OnPropertyChanged("Menu");
            }
        }

        CompileWindowViewModel window;
        public IModeWindow Window
        {
            get { return window; }
            set
            {
                window = (CompileWindowViewModel)value;
                OnPropertyChanged("Window");
            }
        }
    }
}
