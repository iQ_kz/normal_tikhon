﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class CompileWindowViewModel : ViewModelBase, IModeWindow
    {
        bool isInvalid;
        public bool IsInvalid
        {
            get { return isInvalid; }
            set { isInvalid = value; OnPropertyChanged("IsInvalid"); }
        }
        string title;
        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged("Title"); }
        }
        bool isDisplayed;
        public bool IsDisplayed
        {
            get { return isDisplayed; }
            set { isDisplayed = value; OnPropertyChanged("IsDisplayed"); }
        }
        bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set { isSelected = value; OnPropertyChanged("IsSelected"); }
        }

        bool isClosable;
        public bool IsClosable
        {
            get { return isClosable; }
            set { isClosable = value; OnPropertyChanged("IsClosable"); }
        }
        
        WindowState windowState;
        public WindowState WindowState
        {
            get { return windowState; }
            set { windowState = value; OnPropertyChanged("WindowState"); }
        }

        public RelayCommand NormalizeWindow
        {
            get;
            private set;
        }
        
        void normalizeWindow(object _param)
        {
            WindowState = WindowState.Normal;
        }

        public RelayCommand CloseWindow
        {
            get;
            private set;
        }

        void closeWindow(object _param)
        {
            IsDisplayed = false;
        }

        public CompileWindowViewModel()
        {
            IsInvalid = false;
            Title = "Compile";
            NormalizeWindow = new RelayCommand(normalizeWindow, p => WindowState == WindowState.Maximized);
            CloseWindow = new RelayCommand(closeWindow, p => IsDisplayed && IsClosable);       
        }

        int zIndex;
        public int ZIndex
        {
            get { return zIndex; }
            set { zIndex = value; OnPropertyChanged("ZIndex"); }
        }

        #region PositionSize
        double top;
        public double Top
        {
            get { return top; }
            set { top = value; OnPropertyChanged("Top"); }
        }

        double left;
        public double Left
        {
            get { return left; }
            set { left = value; OnPropertyChanged("Left"); }
        }

        double width;
        public double Width
        {
            get { return width; }
            set { width = value; OnPropertyChanged("Width"); }
        }

        double height;
        public double Height
        {
            get { return height; }
            set { height = value; OnPropertyChanged("Height"); }
        }

        double previousTop;
        public double PreviousTop
        {
            get { return previousTop; }
            set { previousTop = value; OnPropertyChanged("PreviousTop"); }
        }

        double previousLeft;
        public double PreviousLeft
        {
            get { return previousLeft; }
            set { previousLeft = value; OnPropertyChanged("PreviousLeft"); }
        }

        double previousWidth;
        public double PreviousWidth
        {
            get { return previousWidth; }
            set { previousWidth = value; OnPropertyChanged("PreviousWidth"); }
        }

        double previousHeight;
        public double PreviousHeight
        {
            get { return previousHeight; }
            set { previousHeight = value; OnPropertyChanged("PreviousHeight"); }
        }

        #endregion
    }
}
