﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class ToDoViewModel : ViewModelBase, IMode
    {
        public ToDoViewModel(bool _enabled, ToDoMenuViewModel _menu, ToDoWindowViewModel _window)
        {
            IsEnabled = _enabled;
            Menu = _menu;
            Window = _window;
        }

        bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        ToDoMenuViewModel menu;
        public IModeMenu Menu
        {
            get { return menu; }
            set
            {
                menu = (ToDoMenuViewModel)value;
                OnPropertyChanged("Menu");
            }
        }

        ToDoWindowViewModel window;
        public IModeWindow Window
        {
            get { return window; }
            set
            {
                window = (ToDoWindowViewModel)value;
                OnPropertyChanged("Window");
            }
        }
    }
}
