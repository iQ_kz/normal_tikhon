﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class VisualViewModel : ViewModelBase, IMode
    {
        public VisualViewModel(bool _enabled, VisualMenuViewModel _menu, VisualWindowViewModel _window)
        {
            IsEnabled = _enabled;
            Menu = _menu;
            Window = _window;
        }

        bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        VisualMenuViewModel menu;
        public IModeMenu Menu
        {
            get { return menu; }
            set
            {
                menu = (VisualMenuViewModel)value;
                OnPropertyChanged("Menu");
            }
        }

        VisualWindowViewModel window;
        public IModeWindow Window
        {
            get { return window; }
            set
            {
                window = (VisualWindowViewModel)value;
                OnPropertyChanged("Window");
            }
        }
    }
}
