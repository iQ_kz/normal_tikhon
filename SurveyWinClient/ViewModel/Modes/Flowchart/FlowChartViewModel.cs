﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class FlowChartViewModel : ViewModelBase, IMode
    {
        public FlowChartViewModel(bool _enabled, FlowChartMenuViewModel _menu, FlowChartWindowViewModel _window)
        {
            IsEnabled = _enabled;
            Menu = _menu;
            Window = _window;
        }

        bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        FlowChartMenuViewModel menu;
        public IModeMenu Menu
        {
            get { return menu; }
            set
            {
                menu = (FlowChartMenuViewModel)value;
                OnPropertyChanged("Menu");
            }
        }

        FlowChartWindowViewModel window;
        public IModeWindow Window
        {
            get { return window; }
            set
            {
                window = (FlowChartWindowViewModel)value;
                OnPropertyChanged("Window");
            }
        }
    }
}
