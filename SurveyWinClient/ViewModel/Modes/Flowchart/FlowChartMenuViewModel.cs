﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class FlowChartMenuViewModel : ViewModelBase, IModeMenu
    {
        string title = "Flowchart";
        public string Title
        {
            get { return title; }
            set { title = value; }
        }  
    }
}
