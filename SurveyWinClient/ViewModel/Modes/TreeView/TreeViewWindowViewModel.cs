﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class TreeViewWindowViewModel : ViewModelBase, IModeWindow
    {
        string title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        WindowState windowState;
        public WindowState WindowState
        {
            get { return windowState; }
            set { windowState = value; OnPropertyChanged("WindowState"); }
        }
        bool isDisplayed;
        public bool IsDisplayed
        {
            get { return isDisplayed; }
            set { isDisplayed = value; OnPropertyChanged("IsDisplayed"); }
        }
        bool isInvalid;
        public bool IsInvalid
        {
            get { return isInvalid; }
            set { isInvalid = value; OnPropertyChanged("IsInvalid"); }
        }
        bool isClosable;
        public bool IsClosable
        {
            get { return isClosable; }
            set { isClosable = value; OnPropertyChanged("IsClosable"); }
        }

        bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set { isSelected = value; OnPropertyChanged("IsSelected"); }
        }
        
        public RelayCommand NormalizeWindow
        {
            get;
            private set;
        }
        
        void normalizeWindow(object _param)
        {
            WindowState = WindowState.Normal;
        }
        public RelayCommand CloseWindow
        {
            get;
            private set;
        }

        void closeWindow(object _param)
        {
            IsDisplayed = false;
        }
        public TreeViewWindowViewModel()
        {
            IsInvalid = false;
            Title = "Treeview";
            Zoom = 100;
            ZoomPlus = new RelayCommand(zoomPlus, p => true);
            ZoomMinus = new RelayCommand(zoomMinus, p => true);
            UnZoom = new RelayCommand(unZoom, p => true);
            NormalizeWindow = new RelayCommand(normalizeWindow, p => WindowState == WindowState.Maximized);
            CloseWindow = new RelayCommand(closeWindow, p => IsDisplayed && IsClosable);
        }

        int zIndex;
        public int ZIndex
        {
            get { return zIndex; }
            set { zIndex = value; OnPropertyChanged("ZIndex"); }
        }

        #region Zoom

        double zoom;
        public double Zoom
        {
            get { return zoom; }
            set
            {
                if (value < 10)
                {
                    zoom = 10;
                }
                else
                {
                    zoom = value;
                }
                OnPropertyChanged("Zoom");
            }
        }

        public RelayCommand ZoomPlus
        {
            get;
            private set;
        }

        void zoomPlus(object _param)
        {
            Zoom += 10;
        }

        public RelayCommand ZoomMinus
        {
            get;
            private set;
        }

        void zoomMinus(object _param)
        {
            Zoom -= 10;
        }
        public RelayCommand UnZoom
        {
            get;
            private set;
        }

        void unZoom(object _param)
        {
            Zoom = 100;
        }

        #endregion zoom

        #region PositionSize
        double top;
        public double Top
        {
            get { return top; }
            set { top = value; OnPropertyChanged("Top"); }
        }

        double left;
        public double Left
        {
            get { return left; }
            set { left = value; OnPropertyChanged("Left"); }
        }

        double width;
        public double Width
        {
            get { return width; }
            set { width = value; OnPropertyChanged("Width"); }
        }

        double height;
        public double Height
        {
            get { return height; }
            set { height = value; OnPropertyChanged("Height"); }
        }

        double previousTop;
        public double PreviousTop
        {
            get { return previousTop; }
            set { previousTop = value; OnPropertyChanged("PreviousTop"); }
        }

        double previousLeft;
        public double PreviousLeft
        {
            get { return previousLeft; }
            set { previousLeft = value; OnPropertyChanged("PreviousLeft"); }
        }

        double previousWidth;
        public double PreviousWidth
        {
            get { return previousWidth; }
            set { previousWidth = value; OnPropertyChanged("PreviousWidth"); }
        }

        double previousHeight;
        public double PreviousHeight
        {
            get { return previousHeight; }
            set { previousHeight = value; OnPropertyChanged("PreviousHeight"); }
        }

        #endregion
    }    
}
