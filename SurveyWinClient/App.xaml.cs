﻿using System.Windows;
using System.Collections.Generic;
using System.Windows.Media;

using SurveyWinClient.ViewModel;
using SurveyWinClient.View;

namespace SurveyWinClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {


        
        public void OnStartup(object sender, StartupEventArgs e)
        {
            List<ProjectWorkSpaceViewModel> projects;
            projects = new List<ProjectWorkSpaceViewModel>();
           /* projects.Add(new ProjectWorkSpaceViewModel("Survey 1",
                                    new VisualViewModel(true, new VisualMenuViewModel(), new VisualWindowViewModel(){ Left = 0, Top = 0, Width = 200, ZIndex = 7}),
                                    new TreeViewViewModel(true, new TreeViewMenuViewModel(), new TreeViewWindowViewModel(){ Left = 10, Top = 10, ZIndex = 8}),
                                    new FlowChartViewModel(true, new FlowChartMenuViewModel(), new FlowChartWindowViewModel(){ Left = 20, Top = 20, ZIndex = 6, IsSelected = true}),
                                    new TypeWriterViewModel(true, new TypeWriterMenuViewModel(), new TypeWriterWindowViewModel(){ Left = 30, Top = 30, ZIndex = 5 }),
                                    new LogicViewModel(true, new LogicMenuViewModel(), new LogicWindowViewModel(){ Left = 40, Top = 40, ZIndex = 4 }),
                                    new StatementOfWorkViewModel(false, new StatementOfWorkMenuViewModel(), new StatementOfWorkWindowViewModel(){ Left = 50, Top = 50, ZIndex = 3 }),
                                    new ToDoViewModel(false, new ToDoMenuViewModel(), new ToDoWindowViewModel(){ Left = 60, Top = 60, ZIndex = 2 }),
                                    new RulesViewModel(false, new RulesMenuViewModel(), new RulesWindowViewModel(){ Left = 70, Top = 70, ZIndex = 1 }),
                                    new CompileViewModel(false, new CompileMenuViewModel(), new CompileWindowViewModel(){ Left = 80, Top = 80, ZIndex = 0 })));
                */
                /* new ProjectWorkSpaceViewModel(this.Dispatcher,"Survey 2",
                                    new VisualViewModel(true, new VisualMenuViewModel(), new VisualWindowViewModel(){ Left = 0, Top = 0, ZIndex = 1, IsSelected = true }),
                                    new TreeViewViewModel(true, new TreeViewMenuViewModel(), new TreeViewWindowViewModel(){ Left = 10, Top = 10, ZIndex = 2 }),
                                    new FlowChartViewModel(true, new FlowChartMenuViewModel(), new FlowChartWindowViewModel(){ Left = 20, Top = 20, ZIndex = 3}),
                                    new TypeWriterViewModel(true, new TypeWriterMenuViewModel(), new TypeWriterWindowViewModel(){ Left = 30, Top = 30, ZIndex = 4 }),
                                    new LogicViewModel(false, new LogicMenuViewModel(), new LogicWindowViewModel(){ Left = 40, Top = 40, ZIndex = 5 }),
                                    new StatementOfWorkViewModel(false, new StatementOfWorkMenuViewModel(), new StatementOfWorkWindowViewModel(){ Left = 50, Top = 50, ZIndex = 6 }),
                                    new ToDoViewModel(false, new ToDoMenuViewModel(), new ToDoWindowViewModel(){ Left = 60, Top = 60, ZIndex = 7 }),
                                    new RulesViewModel(false, new RulesMenuViewModel(), new RulesWindowViewModel(){ Left = 70, Top = 70, ZIndex = 8 }),
                                    new CompileViewModel(false, new CompileMenuViewModel(), new CompileWindowViewModel(){ Left = 80, Top = 80, ZIndex = 9 })),
                                    
                new ProjectWorkSpaceViewModel(this.Dispatcher,"Survey 3",
                                    new VisualViewModel(true, new VisualMenuViewModel(), new VisualWindowViewModel(){ Left = 0, Top = 0, ZIndex = 1 }),
                                    new TreeViewViewModel(true, new TreeViewMenuViewModel(), new TreeViewWindowViewModel(){ Left = 10, Top = 10, ZIndex = 2, IsSelected = true }),
                                    new FlowChartViewModel(true, new FlowChartMenuViewModel(), new FlowChartWindowViewModel(){ Left = 20, Top = 30, ZIndex = 3 }),
                                    new TypeWriterViewModel(false, new TypeWriterMenuViewModel(), new TypeWriterWindowViewModel(){ Left = 30, Top = 30, ZIndex = 8 }),
                                    new LogicViewModel(false, new LogicMenuViewModel(), new LogicWindowViewModel(){ Left = 40, Top = 40, ZIndex = 4 }),
                                    new StatementOfWorkViewModel(false, new StatementOfWorkMenuViewModel(), new StatementOfWorkWindowViewModel(){ Left = 50, Top = 50, ZIndex = 5 }),
                                    new ToDoViewModel(false, new ToDoMenuViewModel(), new ToDoWindowViewModel(){ Left = 60, Top = 60, ZIndex = 6 }),
                                    new RulesViewModel(false, new RulesMenuViewModel(), new RulesWindowViewModel(){ Left = 70, Top = 70, ZIndex = 7 }),
                                    new CompileViewModel(false, new CompileMenuViewModel(), new CompileWindowViewModel(){ Left = 80, Top = 80, ZIndex = 0 })),
                                   
            }; */           

           
            MainWindowView view = new MainWindowView(); // создали View

            view.MaxHeight = SystemParameters.WorkArea.Size.Height;
            view.MaxWidth = SystemParameters.WorkArea.Size.Width;                       

            //load settings of main wnd
            //view.WindowStartupLocation = 
            //view.Width =
            //view.Height = 
            //view.WindowState = 

            MainViewModel viewModel = new ViewModel.MainViewModel(projects); // Создали ViewModel
            view.DataContext = viewModel; // положили ViewModel во View в качестве DataContext
            view.Show();
        }
    }
}
