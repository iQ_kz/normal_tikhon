#pragma once



using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections::ObjectModel;
using namespace System::Windows::Threading;
using namespace System::Collections::Generic;



namespace Survey {

	generic <class T>
	public ref class ObservableRangeCollection
		: ObservableCollection<T>
	{
	public:

		/// <summary> 
		/// Initializes a new instance of the System.Collections.ObjectModel.ObservableCollection(Of T) class. 
		/// </summary> 
		ObservableRangeCollection<T>()
			: ObservableCollection<T>() 
		{ }

		/// <summary> 
		/// Initializes a new instance of the System.Collections.ObjectModel.ObservableCollection(Of T) class that contains elements copied from the specified collection. 
		/// </summary> 
		/// <param name="collection">collection: The collection from which the elements are copied.</param> 
		/// <exception cref="System.ArgumentNullException">The collection parameter cannot be null.</exception> 
		ObservableRangeCollection<T>(IEnumerable<T>^ collection)
			: ObservableCollection<T>(collection) 
		{ }

		/// <summary> 
		/// Adds the elements of the specified collection to the end of the ObservableCollection(Of T). 
		/// </summary> 
		void AddRange(IEnumerable<T>^ collection)
		{
			if (!collection) 
				throw gcnew ArgumentNullException("collection");

			for each(T enmrt in collection)
				Items->Add(enmrt);

			OnCollectionChanged(gcnew System::Collections::Specialized::NotifyCollectionChangedEventArgs(System::Collections::Specialized::NotifyCollectionChangedAction::Reset));
		}

		/// <summary> 
		/// Removes the first occurence of each item in the specified collection from ObservableCollection(Of T). 
		/// </summary> 
		void RemoveRange(IEnumerable<T>^ collection)
		{
			if (!collection)
				throw gcnew ArgumentNullException("collection");
			
			for each(T enmrt in collection)
				Items->Remove(enmrt);

			OnCollectionChanged(gcnew System::Collections::Specialized::NotifyCollectionChangedEventArgs(System::Collections::Specialized::NotifyCollectionChangedAction::Reset));
		}

		/// <summary> 
		/// Clears the current collection and replaces it with the specified collection. 
		/// </summary> 
		void ReplaceRange(IEnumerable<T>^ collection)
		{
			if (!collection) 
				throw gcnew ArgumentNullException("collection");

			Items->Clear();

			for each(T enmrt in collection)
				Items->Add(enmrt);
			
			OnCollectionChanged(gcnew System::Collections::Specialized::NotifyCollectionChangedEventArgs(System::Collections::Specialized::NotifyCollectionChangedAction::Reset));
		}
	};


}

