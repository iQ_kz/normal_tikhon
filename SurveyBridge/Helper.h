#pragma once
#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h>
#include <gcroot.h>

#include <string>
#include "MathSv.h"

using namespace System;
using namespace System::Text;


namespace Survey {

	namespace Helper {

		public ref class Converter
		{
		internal:


			static String^ ConvertFromUtf8(const std::string& uStr)
			{
				return gcnew String(uStr.c_str(), 0, uStr.length(), Encoding::UTF8);
			}

			static std::string ConvertToUtf8(String^ wStr)
			{
				if (!wStr)
					return "";

				array<Byte>^ bytes = Encoding::UTF8->GetBytes(wStr + "\0");
				pin_ptr<Byte> pinnedBytes = &bytes[0];

				return std::string((char*)pinnedBytes);
			}

		public:

			static double GradusToRad(double gradus)
			{
				return (PI / 180.0) * gradus;
			}

			static double RadToGradus(double radians)
			{
				return (180.0 / PI) * radians;
			}

		};

	}
}