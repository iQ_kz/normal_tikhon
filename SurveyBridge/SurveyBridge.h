// SurveyBridge.h
#pragma once
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>

#include "Helper.h"
#include "ObservableRangeCollection.h"

#include "SurveyCore.h"
#include "FileManager.h"
#include "DocumentManager.h"


using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections::ObjectModel;
using namespace System::Windows::Threading;
using namespace System::Collections::Generic;
using namespace System::Windows::Media::Imaging;


namespace Survey {

	ref class NotifycatorContainer;
	ref class DocumentObject;
	ref class DocumentWrapper;
	ref class DocumentManagerWrapper;
	ref class ImageWrpp;
}


namespace survey {

	namespace marshalling {

		struct WrapperNotifycator : public INotifycator
		{
			WrapperNotifycator(Survey::NotifycatorContainer^ pWrapper)
			{
				assert(pWrapper);
				m_pWrapper = pWrapper;
			}

			virtual ~WrapperNotifycator() { m_pWrapper = nullptr; }

			gcroot<Survey::NotifycatorContainer^>  m_pWrapper;

			virtual void OnChanged(ISurveyObject* sender, EventArg* pArgs, bool async) override;
		};

		ref class MarshallingHelper
		{
		internal:

			static std::string ToNativeString(String^ str)
			{
				assert(str);
				return msclr::interop::marshal_as<std::string>(str);
			}
		};

	}
}



namespace Survey {


	public ref class XMLObjectWrpp abstract : IDisposable
	{
	internal:

		XMLObjectWrpp(survey::IXMLObject* pObject, DocumentWrapper^ docWrpp, bool autoKill);

		survey::IXMLObject*  m_pObject;
		DocumentWrapper^  m_DocWrpp = nullptr;
		bool m_AutoKill = true;

		virtual void dispose();

		virtual void onAfterRevival(Dictionary<IntPtr, XMLObjectWrpp^>^ nativeToManaged) { }

	public:

		~XMLObjectWrpp()
		{
			dispose();
			GC::SuppressFinalize(this);
		}

		!XMLObjectWrpp()
		{
			dispose();
		}

		property XMLObjectWrpp^ Parent
		{
			XMLObjectWrpp^ get();
		}

		property XMLObjectWrpp^ ChildFirst
		{
			XMLObjectWrpp^ get();
		}

		property XMLObjectWrpp^ ChildLast
		{
			XMLObjectWrpp^ get();
		}

		property XMLObjectWrpp^ PreviosBrother
		{
			XMLObjectWrpp^ get();
		}

		property XMLObjectWrpp^ NextBrother
		{
			XMLObjectWrpp^ get();
		}

		List<XMLObjectWrpp^>^ Childs();

		void RemoveChild(XMLObjectWrpp^ xmlObject);

		void AddChild(XMLObjectWrpp^ xmlObject);

		void AddChild(XMLObjectWrpp^ xmlObject, XMLObjectWrpp^ afterXmlObject);
	};


	public ref class NotifycatorContainer abstract : XMLObjectWrpp, INotifyPropertyChanged
	{
	internal:

		NotifycatorContainer(survey::IXMLObject* pObject, DocumentWrapper^ docWrpp, bool autoKill)
			: XMLObjectWrpp(pObject, docWrpp, autoKill)
			, m_pNotifycator(NULL)
		{
			assert(pObject && docWrpp);

			survey::INotifycatorSupport* pNonifySupportObject = dynamic_cast<survey::INotifycatorSupport*>(m_pObject);
			assert(pNonifySupportObject);

			m_pNotifycator = new survey::marshalling::WrapperNotifycator(this);
			pNonifySupportObject->AddNotificator(m_pNotifycator);
		}

		virtual void dispose() override
		{
			delete m_pNotifycator;
			m_pNotifycator = NULL;

			XMLObjectWrpp::dispose();
		}

	public:

		virtual event PropertyChangedEventHandler^ PropertyChanged;

	internal:

		delegate void NotifyPropertyChangedDelegate(IntPtr senderPtr, IntPtr argPtr);

		virtual void NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr)
		{
			PropertyChanged(this, gcnew PropertyChangedEventArgs(Survey::Helper::Converter::ConvertFromUtf8(((survey::EventArg*)argPtr.ToPointer())->m_EventName)));
		}

		void InvokeNotifyPropertyChanged(survey::ISurveyObject* sender, survey::EventArg* args, bool async);

		// fields
		survey::INotifycator*  m_pNotifycator;
	};



	public ref class TextWrpp : NotifycatorContainer
	{
	internal:

		TextWrpp(survey::Text* pTxt, DocumentWrapper^ docWrpp)
			: NotifycatorContainer(pTxt, docWrpp, false)
		{
			assert(pTxt && docWrpp);
		}

	public:

		property String^ Text
		{
			String^ get()
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				return Helper::Converter::ConvertFromUtf8(survey::SurveyType::Cast<survey::Text>(m_pObject)->GetText());
			}
			void set(String^ txt)
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				survey::SurveyType::Cast<survey::Text>(m_pObject)->SetText(Helper::Converter::ConvertToUtf8(txt));
			}
		}

		property survey::ID Id
		{
			survey::ID get()
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				return survey::SurveyType::Cast<survey::Text>(m_pObject)->GetID();
			}
		}
	};


	public ref class CoordDatWrpp
	{
	public:

		CoordDatWrpp(double top, double left, double width, double height, double angle)
			: m_Top(top)
			, m_Left(left)
			, m_Width(width)
			, m_Height(height)
			, m_Angle(angle)
		{ }

		double  m_Top, m_Left;
		double  m_Width, m_Height;
		double  m_Angle;

	internal:

		CoordDatWrpp(const survey::CoordDat& coordDat)
			: m_Top(coordDat.m_Top)
			, m_Left(coordDat.m_Left)
			, m_Width(coordDat.m_Width)
			, m_Height(coordDat.m_Height)
			, m_Angle(coordDat.m_RotateAngle)
		{ }

		operator survey::CoordDat()
		{
			return ToCoordDat();
		}

		survey::CoordDat ToCoordDat()
		{
			return survey::CoordDat(m_Top, m_Left, m_Width, m_Height, m_Angle);
		}
	};


	public ref class ColorDatWrpp
	{
	public:

		ColorDatWrpp(unsigned char R, unsigned char G, unsigned char B, unsigned char A)
			: m_R(R)
			, m_G(G)
			, m_B(B)
			, m_A(A)
		{ }

		unsigned char m_R, m_G, m_B, m_A;

	internal:

		ColorDatWrpp(const survey::ColorDat& colorDat)
			: m_R(colorDat.m_R)
			, m_G(colorDat.m_G)
			, m_B(colorDat.m_B)
			, m_A(colorDat.m_A)
		{ }

		operator survey::ColorDat()
		{
			return ToColorDat();
		}

		survey::ColorDat ToColorDat()
		{
			return survey::ColorDat(m_R, m_G, m_B, m_A);
		}
	};



	public ref class DesignElementWrpp : NotifycatorContainer
	{
	internal:

		DesignElementWrpp(survey::DesignElement* pDesign, DocumentWrapper^ docWrpp)
			: NotifycatorContainer(pDesign, docWrpp, false)
		{
			assert(pDesign && docWrpp);
		}

	public:

		property DocumentObject^ DesignObject
		{
			DocumentObject^ get();
		}

		void SetTop() 
		{
			dynamic_cast<survey::DesignElement*>(m_pObject)->SetTop();
		}

		void SetLow()
		{
			dynamic_cast<survey::DesignElement*>(m_pObject)->SetLow();
		}

		void SetAllTop() 
		{
			dynamic_cast<survey::DesignElement*>(m_pObject)->SetAllTop();
		}

		void SetAllLow() 
		{
			dynamic_cast<survey::DesignElement*>(m_pObject)->SetAllLow();
		}

		void Up() 
		{
			dynamic_cast<survey::DesignElement*>(m_pObject)->Up();
		}

		void Down() 
		{
			dynamic_cast<survey::DesignElement*>(m_pObject)->Down();
		}

		property CoordDatWrpp^ Coordinate
		{
			CoordDatWrpp^ get()
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				return gcnew CoordDatWrpp(dynamic_cast<survey::DesignElement*>(m_pObject)->GetCoordinate());
			}
			void set(CoordDatWrpp^ coord)
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				dynamic_cast<survey::DesignElement*>(m_pObject)->SetCoordinate(coord->ToCoordDat());
			}
		}

		property CoordDatWrpp^ AbsoluteCoordinate
		{
			CoordDatWrpp^ get()
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				return gcnew CoordDatWrpp(dynamic_cast<survey::DesignElement*>(m_pObject)->GetAbsoluteCoordinate());
			}
			void set(CoordDatWrpp^ coord)
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				dynamic_cast<survey::DesignElement*>(m_pObject)->SetAbsoluteCoordinate(coord->ToCoordDat());
			}
		}

		property ColorDatWrpp^ Color
		{
			ColorDatWrpp^ get()
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				return gcnew ColorDatWrpp(dynamic_cast<survey::DesignElement*>(m_pObject)->GetColor());
			}
			void set(ColorDatWrpp^ color)
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				dynamic_cast<survey::DesignElement*>(m_pObject)->SetColor(color->ToColorDat());
			}
		}
	};



	// ==================================================  HeaderDesignElement =============================================================================

	public value class Crop
	{
	internal:

		Crop(const survey::Crop& crop)
			: m_Left(crop.m_Left)
			, m_Top(crop.m_Top)
			, m_Right(crop.m_Right)
			, m_Bottom(crop.m_Bottom)
		{ }

		survey::Crop ToCrop()
		{
			return survey::Crop(m_Left, m_Top, m_Right, m_Bottom);
		}

	public:

		Crop(double left, double top, double right, double bottom)
			: m_Left(left)
			, m_Top(top)
			, m_Right(right)
			, m_Bottom(bottom)
		{ }

		double  m_Left, m_Top;
		double  m_Right, m_Bottom;
	};


	public ref class ImageElement : NotifycatorContainer
	{
	internal:

		ImageElement(survey::ImageElement* pImageElement, DocumentWrapper^ docWrpp)
			: NotifycatorContainer(pImageElement, docWrpp, false)
		{
			assert(pImageElement && docWrpp);
		}

	public:

		property String^ IdImage
		{
			String^ get()
			{
				assert(m_pObject);
				return gcnew String(dynamic_cast<survey::ImageElement*>(m_pObject)->GetIdImage().c_str());
			}

			void set(String^ str);
		}

		property ImageWrpp^ Image
		{
			ImageWrpp^ get();
			void set(ImageWrpp^ img);
		}

		property double RotateAngle
		{
			double get()
			{
				assert(m_pObject);
				return dynamic_cast<survey::ImageElement*>(m_pObject)->GetRotateAngle();
			}
			void set(double rotAngle)
			{
				assert(m_pObject);
				dynamic_cast<survey::ImageElement*>(m_pObject)->SetRotateAngle(rotAngle);
			}
		}

		property Crop CropImg
		{
			Crop get()
			{
				assert(m_pObject);
				return Crop(dynamic_cast<survey::ImageElement*>(m_pObject)->GetCrop());
			}
			void set(Crop crp)
			{
				assert(m_pObject);
				dynamic_cast<survey::ImageElement*>(m_pObject)->SetCrop(crp.ToCrop());
			}
		}
	};


	public ref class BorderSideElement : NotifycatorContainer
	{
	internal:

		BorderSideElement(survey::BorderSideElement* pBorderSideElement, DocumentWrapper^ docWrpp)
			: NotifycatorContainer(pBorderSideElement, docWrpp, false)
		{
			assert(pBorderSideElement && docWrpp);
		}

	public:

		property double Thickness
		{
			double get()
			{
				assert(m_pObject);
				return dynamic_cast<survey::BorderSideElement*>(m_pObject)->GetThickness();
			}
			void set(double vl)
			{
				assert(m_pObject);
				dynamic_cast<survey::BorderSideElement*>(m_pObject)->SetThickness(vl);
			}
		}

		property unsigned int LineType
		{
			unsigned int get()
			{
				assert(m_pObject);
				return dynamic_cast<survey::BorderSideElement*>(m_pObject)->GetLineType();
			}
			void set(unsigned int vl)
			{
				assert(m_pObject);
				dynamic_cast<survey::BorderSideElement*>(m_pObject)->SetLineType(vl);
			}
		}

		property ColorDatWrpp^ ColorDat
		{
			ColorDatWrpp^ get()
			{
				assert(m_pObject);
				return gcnew ColorDatWrpp(dynamic_cast<survey::BorderSideElement*>(m_pObject)->GetColorDat());
			}
			void set(ColorDatWrpp^ clrDat)
			{
				if (!clrDat)
					throw gcnew NullReferenceException("clrDat");

				dynamic_cast<survey::BorderSideElement*>(m_pObject)->SetColorDat(clrDat->ToColorDat());
			}
		}

	};


	public ref class BorderElement : NotifycatorContainer
	{
	internal:
		
		BorderElement(survey::BorderElement* pBordeElement, DocumentWrapper^ docWrpp)
			: NotifycatorContainer(pBordeElement, docWrpp, false)
		{
			assert(pBordeElement && docWrpp);

			m_pLeftBorderSide = gcnew BorderSideElement(pBordeElement->GetLeftBorderSide(), docWrpp);
			m_pRightBorderSide = gcnew BorderSideElement(pBordeElement->GetRightBorderSide(), docWrpp);
			m_pTopBorderSide = gcnew BorderSideElement(pBordeElement->GetTopBorderSide(), docWrpp);
			m_pBottomBorderSide = gcnew BorderSideElement(pBordeElement->GetBottomBorderSide(), docWrpp);
		}

		BorderSideElement^ m_pLeftBorderSide = nullptr;
		BorderSideElement^ m_pRightBorderSide = nullptr;
		BorderSideElement^ m_pTopBorderSide = nullptr;
		BorderSideElement^ m_pBottomBorderSide = nullptr;

		virtual void dispose() override
		{
			if (m_pLeftBorderSide)
			{
				delete m_pLeftBorderSide;
				m_pLeftBorderSide = nullptr;
			}

			if (m_pRightBorderSide)
			{
				delete m_pRightBorderSide;
				m_pRightBorderSide = nullptr;
			}

			if (m_pTopBorderSide)
			{
				delete m_pTopBorderSide;
				m_pTopBorderSide = nullptr;
			}

			if (m_pBottomBorderSide)
			{
				delete m_pBottomBorderSide;
				m_pBottomBorderSide = nullptr;
			}

			NotifycatorContainer::dispose();
		}

	public:

		property BorderSideElement^ LeftBorderSide
		{
			BorderSideElement^ get()
			{
				return m_pLeftBorderSide;
			}
		}

		property BorderSideElement^ RightBorderSide
		{
			BorderSideElement^ get()
			{
				return m_pRightBorderSide;
			}
		}

		property BorderSideElement^ TopBorderSide
		{
			BorderSideElement^ get()
			{
				return m_pTopBorderSide;
			}
		}

		property BorderSideElement^ BottomBorderSide
		{
			BorderSideElement^ get()
			{
				return m_pBottomBorderSide;
			}
		}

		property double ShadowSize
		{
			double get()
			{
				assert(m_pObject);
				return dynamic_cast<survey::BorderElement*>(m_pObject)->GetShadowSize();
			}
			void set(double shadowSize)
			{
				assert(m_pObject);
				dynamic_cast<survey::BorderElement*>(m_pObject)->SetShadowSize(shadowSize);
			}
		}

	};


	public ref class HeaderDesignElement : DesignElementWrpp
	{
	internal:

		HeaderDesignElement(survey::HeaderDesignElement* pDesign, DocumentWrapper^ docWrpp)
			: DesignElementWrpp(pDesign, docWrpp)
		{
			assert(pDesign && docWrpp);

			m_pBorderElement = gcnew BorderElement(pDesign->GetBorderElement(), docWrpp);
			m_pImageElement = gcnew ImageElement(pDesign->GetImageElement(), docWrpp);
		}

		BorderElement^  m_pBorderElement = nullptr;
		ImageElement^  m_pImageElement = nullptr;

		virtual void dispose() override
		{
			if (m_pBorderElement)
			{
				delete m_pBorderElement;
				m_pBorderElement = nullptr;
			}

			if (m_pImageElement)
			{
				delete m_pImageElement;
				m_pImageElement = nullptr;
			}

			NotifycatorContainer::dispose();
		}

	public:

		property BorderElement^ BorderElementDesign
		{
			BorderElement^ get()
			{
				return m_pBorderElement;
			}
		}

		property ImageElement^ ImageElementDesign
		{
			ImageElement^ get()
			{
				return m_pImageElement;
			}
		}

	};


	public ref class BSDesignElementWrpp : NotifycatorContainer
	{
	internal:

		BSDesignElementWrpp(survey::BSDesignElement* pDesign, DocumentWrapper^ docWrpp)
			: NotifycatorContainer(pDesign, docWrpp, false)
		{
			assert(pDesign && docWrpp);
			m_AutoKill = false;
		}

	public:

		property survey::ID DesignParentID
		{
			survey::ID get()
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				return survey::SurveyType::Cast<survey::BSDesignElement>(m_pObject)->GetBSDesignParentID();
			}
		}

		property CoordDatWrpp^ BSCoordinate
		{
			CoordDatWrpp^ get()
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				return gcnew CoordDatWrpp(survey::SurveyType::Cast<survey::BSDesignElement>(m_pObject)->GetBSCoordinate());
			}
			void set(CoordDatWrpp^ coord)
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				survey::SurveyType::Cast<survey::BSDesignElement>(m_pObject)->SetBSCoordinate(coord->ToCoordDat());
			}
		}

		property ColorDatWrpp^ BSColor
		{
			ColorDatWrpp^ get()
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				return gcnew ColorDatWrpp(survey::SurveyType::Cast<survey::BSDesignElement>(m_pObject)->GetBSColor());
			}
			void set(ColorDatWrpp^ color)
			{
				if (!m_pObject)
					throw gcnew System::Exception("Object was disposed");

				survey::SurveyType::Cast<survey::BSDesignElement>(m_pObject)->SetBSColor(color->ToColorDat());
			}
		}
	};



	public ref class DocumentObject : NotifycatorContainer
	{
	internal:

		DocumentObject(survey::DocumentObject* pObj, DocumentWrapper^ docWrpp, bool autoKill)
			: NotifycatorContainer(pObj, docWrpp, autoKill)
		{
			assert(pObj && docWrpp);
		}

		virtual void onAfterRevival(Dictionary<IntPtr, XMLObjectWrpp^>^ nativeToManaged) override;

		virtual void NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr) override;

		ObservableRangeCollection<DocumentObject^>^ m_Childs = gcnew ObservableRangeCollection<DocumentObject^>();

	public:

		property survey::ID Id
		{
			survey::ID get();
		}

		property String^ Name
		{
			String^ get();
			void set(String^ name);
		}

		property DesignElementWrpp^ DesignElement
		{
			DesignElementWrpp^ get();
		}

		property BSDesignElementWrpp^ BSDesignElement
		{
			BSDesignElementWrpp^ get();
		}

		property List<DocumentObject^>^ DesignObjects
		{
			List<DocumentObject^>^ get();
		}

		property List<DocumentObject^>^ BlockShemeDesignObjects
		{
			List<DocumentObject^>^ get();
		}

		property ObservableRangeCollection<DocumentObject^>^ Childs
		{
			ObservableRangeCollection<DocumentObject^>^ get()
			{
				return m_Childs;
			}
		}
	};


	// ===========================================   Header =======================================================================================

	public value class ColorDat
	{
	public:

		unsigned char m_R = 0;
		unsigned char m_G = 0;
		unsigned char m_B = 0;
		unsigned char m_A = 0;

	internal:

		survey::ColorDat ToNative()
		{
			return survey::ColorDat(m_R, m_G, m_B, m_A);
		}
	};

	public value class BorderSideDat
	{
	public:

		double        m_Thickness = 0;
		unsigned int  m_LineType = 0;
		ColorDat      m_Color;

	internal:

		survey::BorderSideDat ToNative()
		{
			survey::BorderSideDat res;
			res.m_Color = m_Color.ToNative();
			res.m_LineType = m_LineType;
			res.m_Thickness = m_Thickness;
			return res;
		}
	};


	public value class BorderDat
	{
	public:

		double         m_ShadowSize = 0.0;
		BorderSideDat  m_Left, m_Right, m_Top, m_Bottom;

	internal:

		survey::BorderDat ToNative()
		{
			survey::BorderDat res;
			res.m_ShadowSize = m_ShadowSize;
			res.m_Left = m_Left.ToNative();
			res.m_Right = m_Right.ToNative();
			res.m_Top = m_Top.ToNative();
			res.m_Bottom = m_Bottom.ToNative();

			return res;
		}
	};


	public value class ImageProperties
	{
	public:

		String^     m_ImageId;
		Crop        m_Crop;
		double      m_RotateAngle;

	internal:

		survey::ImageProperties ToNative()
		{
			survey::ImageProperties res;
			res.m_Crop = m_Crop.ToCrop();
			
			if (m_ImageId)
				res.m_ImageId = survey::marshalling::MarshallingHelper::ToNativeString(m_ImageId);
			
			res.m_RotateAngle = m_RotateAngle;
			return res;
		}
	};


	public ref class HeaderWrpp : DocumentObject
	{
	internal:

		HeaderWrpp(IntPtr pObj, DocumentWrapper^ docWrpp)
			: DocumentObject((survey::DocumentObject*)pObj.ToPointer(), docWrpp, false)
		{ }

	public:

		HeaderWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, ColorDatWrpp^ color,
			CoordDatWrpp^ coord, String^ text, ImageProperties imgProps, BorderDat borderDat, String^ name);

		property TextWrpp^ Text
		{
			TextWrpp^ get();
		}

	};


	public ref class QuestionWrpp : DocumentObject
	{
	internal:

		QuestionWrpp(IntPtr pObj, DocumentWrapper^ docWrpp)
			: DocumentObject((survey::DocumentObject*)pObj.ToPointer(), docWrpp, false)
		{ }

	public:

		QuestionWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, ColorDatWrpp^ color, CoordDatWrpp^ coord, String^ name);

	};


	public ref class QaWrpp : DocumentObject
	{
	internal:

		QaWrpp(IntPtr pObj, DocumentWrapper^ docWrpp)
			: DocumentObject((survey::DocumentObject*)pObj.ToPointer(), docWrpp, false)
		{ }

	public:

		QaWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, ColorDatWrpp^ color, CoordDatWrpp^ coord, String^ name);
	};


	public ref class BlockWrpp : DocumentObject
	{
	internal:

		BlockWrpp(IntPtr pObj, DocumentWrapper^ docWrpp)
			: DocumentObject((survey::DocumentObject*)pObj.ToPointer(), docWrpp, false)
		{ }

	public:

		BlockWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, ColorDatWrpp^ color, CoordDatWrpp^ coord, String^ name);
	};


	public ref class PageWrpp : DocumentObject
	{
	internal:

		PageWrpp(IntPtr pObj, DocumentWrapper^ docWrpp)
			: DocumentObject((survey::DocumentObject*)pObj.ToPointer(), docWrpp, false)
		{ }

		virtual void onAfterRevival(Dictionary<IntPtr, XMLObjectWrpp^>^ nativeToManaged) override;

		virtual void NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr) override;

		void onUpdateChilds(Dictionary<IntPtr, XMLObjectWrpp^>^ nativeToManaged);

		ObservableRangeCollection<XMLObjectWrpp^>^ m_DocObjects = gcnew ObservableRangeCollection<XMLObjectWrpp^>();

		ObservableRangeCollection<RenderTargetBitmap^>^ miniature = gcnew ObservableRangeCollection<RenderTargetBitmap^> ();
	public:

		property ObservableRangeCollection<RenderTargetBitmap^>^ Miniature
		{
			ObservableRangeCollection<RenderTargetBitmap^>^ get() { return miniature; }
		}

		PageWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, ColorDatWrpp^ color, CoordDatWrpp^ coord, String^ name);

		property ObservableRangeCollection<XMLObjectWrpp^>^ ChildsTree
		{
			ObservableRangeCollection<XMLObjectWrpp^>^ get() { return m_DocObjects; }
		}
	};


	public ref class ChapterWrpp : DocumentObject
	{
	internal:

		ChapterWrpp(IntPtr pObj, DocumentWrapper^ docWrpp)
			: DocumentObject((survey::DocumentObject*)pObj.ToPointer(), docWrpp, false)
		{ }

		virtual void NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr) override;

		ObservableRangeCollection<PageWrpp^>^ m_Pages = gcnew ObservableRangeCollection<PageWrpp^>();

		virtual void onAfterRevival(Dictionary<IntPtr, XMLObjectWrpp^>^ nativeToManaged) override;

	public:

		ChapterWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, String^ name);

		property ObservableRangeCollection<PageWrpp^>^ Pages
		{
			ObservableRangeCollection<PageWrpp^>^ get()
			{
				return m_Pages;
			}
		}
	};


	public ref class SectionWrpp : NotifycatorContainer
	{
	internal:

		SectionWrpp(survey::Section* pObj, DocumentWrapper^ docWrpp)
			: NotifycatorContainer(pObj, docWrpp, false)
		{
			assert(pObj && docWrpp);
			pObj->AddNotificator(m_pNotifycator);

			m_AutoKill = false;
		}

	};


	public ref class TextsWrpp : SectionWrpp
	{
	internal:

		TextsWrpp(survey::Texts* pObj, DocumentWrapper^ docWrpp);

		virtual void dispose() override
		{
			m_TextWrpps->Clear();

			for each(KeyValuePair<IntPtr, TextWrpp^>^ kvp in m_NativeToManaged)
				delete kvp->Value;

			m_NativeToManaged->Clear();
		}

		virtual void NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr) override
		{
			survey::EventArg* pEvArg = (survey::EventArg*)argPtr.ToPointer();
			if (!pEvArg)
				return;

			survey::CollectionEventArg* pCollEvArg = dynamic_cast<survey::CollectionEventArg*>(pEvArg);
			if (!pCollEvArg || pCollEvArg->m_EventName != "texts")
			{
				SectionWrpp::NotifyPropertyChanged(senderPtr, argPtr);
				return;
			}

			if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_PushBack)
			{
				survey::Text* pTxt = survey::SurveyType::Cast<survey::Text>(pCollEvArg->m_pObject);
				assert(pTxt);

				TextWrpp^ txtWrpp = gcnew TextWrpp(pTxt, m_DocWrpp);
				
				m_TextWrpps->Add(txtWrpp);
				m_NativeToManaged->Add(IntPtr(pTxt), txtWrpp);
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Erase)
			{
				survey::Text* pTxt = survey::SurveyType::Cast<survey::Text>(pCollEvArg->m_pObject);
				assert(pTxt);

				TextWrpp^ txtWrpp = m_NativeToManaged[IntPtr(pTxt)];
				m_NativeToManaged->Remove(IntPtr(pTxt));
				m_TextWrpps->Remove(txtWrpp);

				delete txtWrpp;
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_EraseRange)
			{
				List<TextWrpp^>^ txtWrppers = gcnew List<TextWrpp^>();

				for (survey::IXMLObject* pTxt : pCollEvArg->m_Objects)
				{
					assert(pTxt);

					txtWrppers->Add(m_NativeToManaged[IntPtr(pTxt)]);
					m_NativeToManaged->Remove(IntPtr(pTxt));
				}

				m_TextWrpps->RemoveRange(txtWrppers);

				for each(TextWrpp^ txtWrpp in txtWrppers)
					delete txtWrpp;
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Reset)
			{
				// delete old wrappers
				for each(KeyValuePair<IntPtr, TextWrpp^>^ kvp in m_NativeToManaged)
					delete kvp->Value;

				// create new wrappers
				replaceWrapps();
			}
		}

		Dictionary<IntPtr, TextWrpp^>^ m_NativeToManaged = gcnew Dictionary<IntPtr, TextWrpp^>();
		ObservableRangeCollection<TextWrpp^>^ m_TextWrpps = gcnew ObservableRangeCollection<TextWrpp^>();

	private:

		void replaceWrapps()
		{
			m_NativeToManaged->Clear();

			const std::list<survey::Text*>& txts = dynamic_cast<survey::Texts*>(m_pObject)->GetTexts();
			List<TextWrpp^>^ txtWrpps = gcnew List<TextWrpp^>();

			for (survey::Text* pTxt : txts)
			{
				assert(pTxt);

				TextWrpp^ txtWrpp = gcnew TextWrpp(pTxt, m_DocWrpp);
				txtWrpps->Add(txtWrpp);
				m_NativeToManaged->Add(IntPtr(pTxt), txtWrpp);
			}

			m_TextWrpps->ReplaceRange(txtWrpps);
		}

	public:

		property ObservableRangeCollection<TextWrpp^>^ Texts
		{
			ObservableRangeCollection<TextWrpp^>^ get();
		}
	};


	// ============================================  ImagesWrapper =============================================================================

	public ref class ImageWrpp : NotifycatorContainer
	{
	internal:

		ImageWrpp(survey::Image* pImage, DocumentWrapper^ docWrpp)
			: NotifycatorContainer(pImage, docWrpp, false)
		{
			assert(pImage && docWrpp);
		}

	public:

		property String^ ID
		{
			String^ get()
			{
				return gcnew String(survey::SurveyType::Cast<survey::Image>(m_pObject)->GetID().c_str());
			}
		}

		property String^ Name
		{
			String^ get()
			{
				return Helper::Converter::ConvertFromUtf8(survey::SurveyType::Cast<survey::Image>(m_pObject)->GetName());
			}
			void set(String^ name)
			{
				survey::SurveyType::Cast<survey::Image>(m_pObject)->SetName(Helper::Converter::ConvertToUtf8(name));
			}
		}

		property System::Drawing::Image^ ImageData
		{
			System::Drawing::Image^ get();
		}

		property String^ ImagePath
		{
			String^ get();
		}

		BitmapImage^ TryGetImageDataAsBitmap();

		property BitmapImage^ PreviewImageData
		{
			BitmapImage^ get();
		}

	};


	public ref class ImagesWrpp : SectionWrpp
	{
	internal:

		ImagesWrpp(survey::Images* pObj, DocumentWrapper^ docWrpp)
			: SectionWrpp(pObj, docWrpp)
		{
			assert(pObj && docWrpp);
			pObj->AddNotificator(m_pNotifycator);

			replaceWrapps();
		}

		virtual void dispose() override
		{
			SectionWrpp::dispose();

			m_ImagesWrpps->Clear();

			for each(KeyValuePair<IntPtr, ImageWrpp^>^ kvp in m_NativeToManaged)
				delete kvp->Value;

			m_NativeToManaged->Clear();
		}

		virtual void NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr) override
		{
			survey::EventArg* pEvArg = (survey::EventArg*)argPtr.ToPointer();
			if (!pEvArg)
				return;

			survey::CollectionEventArg* pCollEvArg = dynamic_cast<survey::CollectionEventArg*>(pEvArg);
			if (!pCollEvArg || pCollEvArg->m_EventName != "images")
			{
				SectionWrpp::NotifyPropertyChanged(senderPtr, argPtr);
				return;
			}

			if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_PushBack)
			{
				survey::Image* pImage = dynamic_cast<survey::Image*>(pCollEvArg->m_pObject);
				assert(pImage);

				ImageWrpp^ imgWrpp = gcnew ImageWrpp(pImage, m_DocWrpp);

				m_ImagesWrpps->Add(imgWrpp);
				m_NativeToManaged->Add(IntPtr(pImage), imgWrpp);
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Erase)
			{
				survey::Image* pImage = dynamic_cast<survey::Image*>(pCollEvArg->m_pObject);
				assert(pImage);

				ImageWrpp^ imgWrpp = m_NativeToManaged[IntPtr(pImage)];
				m_NativeToManaged->Remove(IntPtr(pImage));
				m_ImagesWrpps->Remove(imgWrpp);

				delete imgWrpp;
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_EraseRange)
			{
				List<ImageWrpp^>^ imgWrppers = gcnew List<ImageWrpp^>();

				for (survey::IXMLObject* pImage : pCollEvArg->m_Objects)
				{
					assert(pImage);

					imgWrppers->Add(m_NativeToManaged[IntPtr(pImage)]);
					m_NativeToManaged->Remove(IntPtr(pImage));
				}

				m_ImagesWrpps->RemoveRange(imgWrppers);

				for each(ImageWrpp^ imgWrpper in imgWrppers)
					delete imgWrpper;
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Reset)
			{
				// delete old wrappers
				for each(KeyValuePair<IntPtr, ImageWrpp^>^ kvp in m_NativeToManaged)
					delete kvp->Value;

				// create new wrappers
				replaceWrapps();
			}
		}

		Dictionary<IntPtr, ImageWrpp^>^  m_NativeToManaged = gcnew Dictionary<IntPtr, ImageWrpp^>();
		ObservableRangeCollection<ImageWrpp^>^  m_ImagesWrpps = gcnew ObservableRangeCollection<ImageWrpp^>();

	private:

		void replaceWrapps()
		{
			m_NativeToManaged->Clear();

			const std::unordered_map<std::string, survey::Image*>& images = dynamic_cast<survey::Images*>(m_pObject)->GetImages();

			List<ImageWrpp^>^ imageWrpps = gcnew List<ImageWrpp^>();

			for (auto imagePair : images)
			{
				ImageWrpp^ imageWrpp = gcnew ImageWrpp(imagePair.second, m_DocWrpp);
				imageWrpps->Add(imageWrpp);
				m_NativeToManaged->Add(IntPtr(imagePair.second), imageWrpp);
			}

			m_ImagesWrpps->ReplaceRange(imageWrpps);
		}

	public:

		ImageWrpp^ ImageByID(String^ id)
		{
			const auto& imgs = survey::SurveyType::Cast<survey::Images>(m_pObject)->GetImages();
			auto itrt = imgs.find(survey::marshalling::MarshallingHelper::ToNativeString(id));
			if (itrt == imgs.end())
				return nullptr;

			return m_NativeToManaged[IntPtr(itrt->second)];
		}

		ImageWrpp^ AddImage(String^ pathFile)
		{
			if (!pathFile)
				throw gcnew NullReferenceException("pathFile");

			boost::filesystem::path pathFl = msclr::interop::marshal_as<std::wstring>(pathFile);
			survey::Image* pAddImage = survey::SurveyType::Cast<survey::Images>(m_pObject)->AddImage(pathFl);

			if (!pAddImage)
				throw gcnew Exception("Invalide path");

			return m_NativeToManaged[IntPtr(pAddImage)];
		}

		void RemoveImage(ImageWrpp^ pImage)
		{
			if (!pImage)
				throw gcnew NullReferenceException("pImage");

			survey::SurveyType::Cast<survey::Images>(m_pObject)->RemoveImage(survey::SurveyType::Cast<survey::Image>(pImage->m_pObject));
		}

		void RemoveImages(List<ImageWrpp^>^ images)
		{
			if (!images)
				throw gcnew NullReferenceException("images");

			std::set<survey::Image*> ntImgs;
			for each(ImageWrpp^ image in images)
				ntImgs.emplace(survey::SurveyType::Cast<survey::Image>(image->m_pObject));

			survey::SurveyType::Cast<survey::Images>(m_pObject)->RemoveImages(ntImgs);
		}

		property ObservableRangeCollection<ImageWrpp^>^ Images
		{
			ObservableRangeCollection<ImageWrpp^>^ get()
			{
				return m_ImagesWrpps;
			}
		}

	};


	// ============================================  DesignWrpp ================================================================================

	public ref class DesignWrpp : SectionWrpp
	{
	internal:

		DesignWrpp(survey::Design* pObj, DocumentWrapper^ docWrpp);

		virtual void dispose() override
		{
			SectionWrpp::dispose();

			m_DesignElWrpps->Clear();

			for each(KeyValuePair<IntPtr, DesignElementWrpp^>^ kvp in m_NativeToManaged)
				delete kvp->Value;

			m_NativeToManaged->Clear();
		}

		virtual void NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr) override
		{
			survey::EventArg* pEvArg = (survey::EventArg*)argPtr.ToPointer();
			if (!pEvArg)
				return;

			survey::CollectionEventArg* pCollEvArg = dynamic_cast<survey::CollectionEventArg*>(pEvArg);
			if (!pCollEvArg || pCollEvArg->m_EventName != "designs")
			{
				SectionWrpp::NotifyPropertyChanged(senderPtr, argPtr);
				return;
			}

			if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_PushBack)
			{
				survey::DesignElement* pDe = dynamic_cast<survey::DesignElement*>(pCollEvArg->m_pObject);
				assert(pDe);

				DesignElementWrpp^ deWrpp = createDesignElementWrpp(pDe);

				m_DesignElWrpps->Add(deWrpp);
				m_NativeToManaged->Add(IntPtr(pDe), deWrpp);
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Erase)
			{
				survey::DesignElement* pDe = dynamic_cast<survey::DesignElement*>(pCollEvArg->m_pObject);
				assert(pDe);

				DesignElementWrpp^ deWrpp = m_NativeToManaged[IntPtr(pDe)];
				m_NativeToManaged->Remove(IntPtr(pDe));
				m_DesignElWrpps->Remove(deWrpp);

				delete deWrpp;
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_EraseRange)
			{
				List<DesignElementWrpp^>^ deWrppers = gcnew List<DesignElementWrpp^>();

				for (survey::IXMLObject* pDe : pCollEvArg->m_Objects)
				{
					assert(pDe);

					deWrppers->Add(m_NativeToManaged[IntPtr(pDe)]);
					m_NativeToManaged->Remove(IntPtr(pDe));
				}

				m_DesignElWrpps->RemoveRange(deWrppers);

				for each(DesignElementWrpp^ deWrpp in deWrppers)
					delete deWrpp;
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Reset)
			{
				// delete old wrappers
				for each(KeyValuePair<IntPtr, DesignElementWrpp^>^ kvp in m_NativeToManaged)
					delete kvp->Value;

				// create new wrappers
				replaceWrapps();
			}
		}

		Dictionary<IntPtr, DesignElementWrpp^>^  m_NativeToManaged = gcnew Dictionary<IntPtr, DesignElementWrpp^>();
		ObservableRangeCollection<DesignElementWrpp^>^  m_DesignElWrpps = gcnew ObservableRangeCollection<DesignElementWrpp^>();

	private:

		DesignElementWrpp^ createDesignElementWrpp(survey::DesignElement* pDesignElement)
		{
			assert(pDesignElement);

			if (pDesignElement->GetType() == survey::HeaderDesignElement::Type())
				return gcnew HeaderDesignElement(survey::SurveyType::Cast<survey::HeaderDesignElement>(pDesignElement), m_DocWrpp);

			return gcnew DesignElementWrpp(pDesignElement, m_DocWrpp);
		}

		void replaceWrapps()
		{
			m_NativeToManaged->Clear();

			const std::list<survey::DesignElement*>& designs = dynamic_cast<survey::Design*>(m_pObject)->GetDesignElements();
			List<DesignElementWrpp^>^ deWrpps = gcnew List<DesignElementWrpp^>();

			for (survey::DesignElement* pDE : designs)
			{
				assert(pDE);

				DesignElementWrpp^ deWrpp = createDesignElementWrpp(pDE);
				deWrpps->Add(deWrpp);
				m_NativeToManaged->Add(IntPtr(pDE), deWrpp);
			}

			m_DesignElWrpps->ReplaceRange(deWrpps);
		}

	public:

		property ObservableRangeCollection<DesignElementWrpp^>^ Designs
		{
			ObservableRangeCollection<DesignElementWrpp^>^ get();
		}

		property ObservableRangeCollection<DesignElementWrpp^>^ DesignsTree
		{
			ObservableRangeCollection<DesignElementWrpp^>^ get()
			{
				return nullptr; // to do
			}
		}

	};

	// ========================================== BlockShemeDesignWrpp ============================================================================

	public ref class BlockShemeDesignWrpp : SectionWrpp
	{
	internal:

		BlockShemeDesignWrpp(survey::BlockShemeDesign* pObj, DocumentWrapper^ docWrpp);

		virtual void dispose() override
		{
			SectionWrpp::dispose();

			m_DesignElWrpps->Clear();

			for each(KeyValuePair<IntPtr, BSDesignElementWrpp^>^ kvp in m_NativeToManaged)
				delete kvp->Value;

			m_NativeToManaged->Clear();
		}

		virtual void NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr) override
		{
			survey::EventArg* pEvArg = (survey::EventArg*)argPtr.ToPointer();
			if (!pEvArg)
				return;

			survey::CollectionEventArg* pCollEvArg = dynamic_cast<survey::CollectionEventArg*>(pEvArg);
			if (!pCollEvArg || pCollEvArg->m_EventName != "designs")
			{
				SectionWrpp::NotifyPropertyChanged(senderPtr, argPtr);
				return;
			}

			if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_PushBack)
			{
				survey::BSDesignElement* pDe = survey::SurveyType::Cast<survey::BSDesignElement>(pCollEvArg->m_pObject);
				assert(pDe);

				BSDesignElementWrpp^ deWrpp = gcnew BSDesignElementWrpp(pDe, m_DocWrpp);

				m_DesignElWrpps->Add(deWrpp);
				m_NativeToManaged->Add(IntPtr(pDe), deWrpp);
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Erase)
			{
				survey::BSDesignElement* pDe = survey::SurveyType::Cast<survey::BSDesignElement>(pCollEvArg->m_pObject);
				assert(pDe);

				BSDesignElementWrpp^ deWrpp = m_NativeToManaged[IntPtr(pDe)];
				m_NativeToManaged->Remove(IntPtr(pDe));
				m_DesignElWrpps->Remove(deWrpp);

				delete deWrpp;
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_EraseRange)
			{
				List<BSDesignElementWrpp^>^ deWrppers = gcnew List<BSDesignElementWrpp^>();

				for (survey::IXMLObject* pDe : pCollEvArg->m_Objects)
				{
					assert(pDe);

					deWrppers->Add(m_NativeToManaged[IntPtr(pDe)]);
					m_NativeToManaged->Remove(IntPtr(pDe));
				}

				m_DesignElWrpps->RemoveRange(deWrppers);

				for each(BSDesignElementWrpp^ deWrpp in deWrppers)
					delete deWrpp;
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Reset)
			{
				// delete old wrappers
				for each(KeyValuePair<IntPtr, BSDesignElementWrpp^>^ kvp in m_NativeToManaged)
					delete kvp->Value;

				// create new wrappers
				replaceWrapps();
			}
		}

		Dictionary<IntPtr, BSDesignElementWrpp^>^  m_NativeToManaged = gcnew Dictionary<IntPtr, BSDesignElementWrpp^>();
		ObservableRangeCollection<BSDesignElementWrpp^>^  m_DesignElWrpps = gcnew ObservableRangeCollection<BSDesignElementWrpp^>();

	private:

		void replaceWrapps()
		{
			m_NativeToManaged->Clear();

			const std::list<survey::BSDesignElement*>& designs = dynamic_cast<survey::BlockShemeDesign*>(m_pObject)->GetDesignElements();
			List<BSDesignElementWrpp^>^ deWrpps = gcnew List<BSDesignElementWrpp^>();

			for (survey::BSDesignElement* pDE : designs)
			{
				assert(pDE);

				BSDesignElementWrpp^ deWrpp = gcnew BSDesignElementWrpp(pDE, m_DocWrpp);
				deWrpps->Add(deWrpp);
				m_NativeToManaged->Add(IntPtr(pDE), deWrpp);
			}

			m_DesignElWrpps->ReplaceRange(deWrpps);
		}

	public:

		property ObservableRangeCollection<BSDesignElementWrpp^>^ Designs
		{
			ObservableRangeCollection<BSDesignElementWrpp^>^ get();
		}
	};

	// ========================================== ProjectWrpp ======================================================================================

	public ref class ProjectWrpp : SectionWrpp
	{
	internal:

		ProjectWrpp(survey::Project* pObj, DocumentWrapper^ docWrpp)
			: SectionWrpp(pObj, docWrpp)
		{
			m_AutoKill = false;

			assert(pObj && docWrpp);
			pObj->AddNotificator(m_pNotifycator);

			replaceWrapps();
		}

		virtual void dispose() override
		{
			SectionWrpp::dispose();

			m_DocObjects->Clear();
			disposeWrappers();
			m_NativeToManaged->Clear();
		}

		virtual void NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr) override
		{
			survey::EventArg* pEvArg = (survey::EventArg*)argPtr.ToPointer();
			if (!pEvArg)
				return;

			survey::CollectionEventArg* pCollEvArg = dynamic_cast<survey::CollectionEventArg*>(pEvArg);
			if (!pCollEvArg)
			{
				SectionWrpp::NotifyPropertyChanged(senderPtr, argPtr);
				return;
			}

			if (pCollEvArg->m_EventName == "elements")
			{
				if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_InsertRange)
				{
					onUpdateChilds();
				}
				else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_EraseRange)
				{
					onUpdateChilds();
				}
				else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Reset)
				{
					// delete old wrappers
					disposeWrappers();

					// create new wrappers
					replaceWrapps();

					// reset chapters
					resetChapters();
				}
			}
			else if (pCollEvArg->m_EventName == "chapter")
			{
				if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_PushBack)
				{
					const std::list<survey::IXMLObject*>& objs = pCollEvArg->m_Objects;
					assert(objs.size());

					ChapterWrpp^ newChapter = dynamic_cast<ChapterWrpp^>(m_NativeToManaged[IntPtr(*objs.begin())]);
					assert(newChapter);

					m_Chapters->Add(newChapter);
				}
				else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Erase)
				{
					const std::list<survey::IXMLObject*>& objs = pCollEvArg->m_Objects;
					assert(objs.size());

					ChapterWrpp^ removeChapter = dynamic_cast<ChapterWrpp^>(m_NativeToManaged[IntPtr(*objs.begin())]);
					assert(removeChapter);

					m_Chapters->Remove(removeChapter);
				}
			}

		}

		ObservableRangeCollection<ChapterWrpp^>^ m_Chapters = gcnew ObservableRangeCollection<ChapterWrpp^>();
		ObservableRangeCollection<XMLObjectWrpp^>^ m_DocObjects = gcnew ObservableRangeCollection<XMLObjectWrpp^>();
		Dictionary<IntPtr, XMLObjectWrpp^>^  m_NativeToManaged = gcnew Dictionary<IntPtr, XMLObjectWrpp^>();

		// statics
		static Dictionary<String^, System::Type^>^ GetNativeTypeToManagedType()
		{
			Dictionary<String^, System::Type^>^  nativeTypeToManagedType = gcnew Dictionary<String^, System::Type^>();

			nativeTypeToManagedType->Add("header", HeaderWrpp::typeid);
			nativeTypeToManagedType->Add("question", QuestionWrpp::typeid);
			nativeTypeToManagedType->Add("qa", QaWrpp::typeid);
			nativeTypeToManagedType->Add("block", BlockWrpp::typeid);
			nativeTypeToManagedType->Add("page", PageWrpp::typeid);
			nativeTypeToManagedType->Add("chapter", ChapterWrpp::typeid);

			return nativeTypeToManagedType;
		}

		static Dictionary<String^, System::Type^>^ s_NativeTypeToManagedType = GetNativeTypeToManagedType();

	private:

		void disposeWrappers()
		{
			for each(KeyValuePair<IntPtr, XMLObjectWrpp^>^ kvp in m_NativeToManaged)
			{
				kvp->Value->m_AutoKill = false;
				kvp->Value->m_pObject = NULL;

				delete kvp->Value;
			}
		}

		void onUpdateChilds()
		{
			const std::list<survey::IXMLObject*>& objs = m_pObject->ChildsTree();
			List<XMLObjectWrpp^>^ wrppers = gcnew List<XMLObjectWrpp^>();

			for (survey::IXMLObject* pObject : objs)
			{
				assert(pObject);
				wrppers->Add(m_NativeToManaged[IntPtr(pObject)]);
			}

			m_DocObjects->ReplaceRange(wrppers);
		}

		void replaceWrapps()
		{
			try
			{
				m_NativeToManaged->Clear();

				// create wrappers
				const std::list<survey::IXMLObject*>& objs = m_pObject->ChildsTree();
				List<XMLObjectWrpp^>^ wrppers = gcnew List<XMLObjectWrpp^>();

				for (survey::IXMLObject* pObject : objs)
				{
					assert(pObject);

					array<Object^>^ args = gcnew array<Object^>(2);
					args[0] = IntPtr(pObject);
					args[1] = m_DocWrpp;

					XMLObjectWrpp^ wrpp = dynamic_cast<XMLObjectWrpp^>(Activator::CreateInstance(s_NativeTypeToManagedType[gcnew String(pObject->GetType().m_Name.c_str())]
						, System::Reflection::BindingFlags::NonPublic | System::Reflection::BindingFlags::Instance
						, nullptr
						, args
						, nullptr));

					assert(wrpp);

					wrppers->Add(wrpp);
					m_NativeToManaged->Add(IntPtr(pObject), wrpp);
				}

				// update wrappers
				for each(XMLObjectWrpp^ wrapper in wrppers)
					wrapper->onAfterRevival(m_NativeToManaged);

				m_DocObjects->ReplaceRange(wrppers);

				// update chapters
				List<ChapterWrpp^>^ chapters = gcnew List<ChapterWrpp^>();
				for each(XMLObjectWrpp^ wrapper in wrppers)
				{
					ChapterWrpp^ chapter = dynamic_cast<ChapterWrpp^>(wrapper);
					if (chapter)
						chapters->Add(chapter);
				}

				m_Chapters->ReplaceRange(chapters);
				return;
			}
			catch (Exception^ ex)
			{
				// to do log error
			}
			catch (const std::exception& ex)
			{
				// to do log error
			}
			catch (...)
			{
				// to do log error
			}

			throw gcnew Exception("Internal exception, terminate application !!!");

		}

		void resetChapters()
		{
			m_Chapters->Clear();

			List<ChapterWrpp^>^ wrppers = gcnew List<ChapterWrpp^>();

			const std::list<survey::Chapter*>& chapters = m_pObject->Childs<survey::Chapter>();
			for (survey::Chapter* pChapter : chapters)
			{
				assert(pChapter);
				wrppers->Add(dynamic_cast<ChapterWrpp^>(m_NativeToManaged[IntPtr(pChapter)]));
			}

			m_Chapters->ReplaceRange(wrppers);
		}

	public:

		property ObservableRangeCollection<ChapterWrpp^>^ Chapters
		{
			ObservableRangeCollection<ChapterWrpp^>^ get()
			{
				return m_Chapters;
			}
		}

		property ObservableRangeCollection<XMLObjectWrpp^>^ TreeObjects
		{
			ObservableRangeCollection<XMLObjectWrpp^>^ get();
		}

		property ObservableRangeCollection<DesignElementWrpp^>^ Designs
		{
			ObservableRangeCollection<DesignElementWrpp^>^ get() { return nullptr; }
		}

		property ObservableRangeCollection<BSDesignElementWrpp^>^ BSDesigns
		{
			ObservableRangeCollection<BSDesignElementWrpp^>^ get() { return nullptr; }
		}
	};


	public ref class ProjectInfoWrapper
	{
	internal:

		ProjectInfoWrapper(survey::ProjectInfo* pProjInfo)
			: m_pProjInfo(pProjInfo)
		{
			assert(pProjInfo);
		}

		survey::ProjectInfo* m_pProjInfo = NULL;

	public:

		property String^ ID
		{
			String^ get()
			{
				assert(m_pProjInfo);
				return gcnew String(m_pProjInfo->GetID());
			}
		}

		property String^ Name
		{
			String^ get()
			{
				assert(m_pProjInfo);

				try
				{
					return Helper::Converter::ConvertFromUtf8(m_pProjInfo->GetName());
				}
				catch (...)
				{
				}

				return nullptr;
			}
		}

		property int Edited
		{
			int get()
			{
				assert(m_pProjInfo);

				try
				{
					return m_pProjInfo->Edited();
				}
				catch (...)
				{
				}

				return -1;
			}
		}

		property bool Status
		{
			bool get()
			{
				assert(m_pProjInfo);

				try
				{
					return m_pProjInfo->Status();
				}
				catch (...)
				{
				}

				return false;
			}
		}

		property System::DateTime  CreateDate
		{
			System::DateTime  get()
			{
				assert(m_pProjInfo);

				try
				{
					const boost::posix_time::ptime& posTime = m_pProjInfo->GetCreatedDate();
					boost::posix_time::ptime::date_type dt = posTime.date();
					return System::DateTime(dt.year(), dt.month(), dt.day(), posTime.time_of_day().hours(), posTime.time_of_day().minutes(), posTime.time_of_day().seconds());
				}
				catch (...)
				{ }

				return System::DateTime::Now;
			}
		}

		property System::DateTime  ModifyDate
		{
			System::DateTime  get()
			{
				assert(m_pProjInfo);

				try
				{
					const boost::posix_time::ptime& posTime = m_pProjInfo->GetModifyDate();

					return System::DateTime(posTime.date().year()
						, posTime.date().month()
						, posTime.date().day()
						, posTime.time_of_day().hours()
						, posTime.time_of_day().minutes()
						, posTime.time_of_day().seconds()
						);
				}
				catch (...)
				{
				}

				return System::DateTime::Now;
			}
		}

		BitmapImage^ GetPreview(survey::ID id);

		BitmapImage^ GetPreviewSmall(survey::ID id);

	};


	public ref class FileManagerWrapper
	{
	internal:

		FileManagerWrapper(String^ workDirectory, String^ tempDirectory)
		{
			try
			{
				// create doc manager
				survey::FileManager::Instance(msclr::interop::marshal_as<std::wstring>(workDirectory), msclr::interop::marshal_as<std::wstring>(tempDirectory));
			}
			catch (...)
			{
				throw gcnew System::ArgumentException("Invalid direcories!");
			}
		}

		// fields
		static FileManagerWrapper^ s_FileManagerWrapper = nullptr;

	public:

		static FileManagerWrapper^ Instance(String^ workDirectory, String^ tempDirectory)
		{
			if (s_FileManagerWrapper == nullptr)
				s_FileManagerWrapper = gcnew FileManagerWrapper(workDirectory, tempDirectory);

			return s_FileManagerWrapper;
		}

		static FileManagerWrapper^ GetInstance()
		{
			if (s_FileManagerWrapper == nullptr)
				throw gcnew NullReferenceException("FileManagerWrapper is null");

			return s_FileManagerWrapper;
		}

		static void Clear()
		{
			if (s_FileManagerWrapper != nullptr)
			{
				delete s_FileManagerWrapper;
				s_FileManagerWrapper = nullptr;
			}
		}

		property List<ProjectInfoWrapper^>^ ProjectInfos
		{
			List<ProjectInfoWrapper^>^ get()
			{
				List<ProjectInfoWrapper^>^ resLst = gcnew List<ProjectInfoWrapper^>();

				try
				{
					const auto& infos = survey::FileManager::GetInstance()->GetProjectInfos();

					for (const auto& pairInfo : infos)
						resLst->Add(gcnew ProjectInfoWrapper(pairInfo.second.get()));
				}
				catch (...)
				{ }

				return resLst;
			}
		}


	internal:

		~FileManagerWrapper()
		{
			dispose();
			GC::SuppressFinalize(this);
		}

		void dispose()
		{
			// reset notifyer
			if (!survey::FileManager::TryGetInstance())
				return;

			// delete doc manager
			survey::FileManager::Clear();
		}

	public:

		!FileManagerWrapper()
		{
			dispose();
		}

	};


	public ref class BackupProjectProperties
	{
	internal:

		BackupProjectProperties(const survey::BackupProjectProperties& backupProjectProperties)
		{
			m_Hash = gcnew String(backupProjectProperties.m_Hash.c_str());

			m_CreatedTime = System::DateTime(backupProjectProperties.m_CreatedTime.date().year()
				, backupProjectProperties.m_CreatedTime.date().month()
				, backupProjectProperties.m_CreatedTime.date().day()
				, backupProjectProperties.m_CreatedTime.time_of_day().hours()
				, backupProjectProperties.m_CreatedTime.time_of_day().minutes()
				, backupProjectProperties.m_CreatedTime.time_of_day().seconds()
			);
		}

	public:

		String^ m_Hash = "";
		System::DateTime m_CreatedTime;
	};


	public ref class DocumentWrapper : IDisposable
	{
	internal:

		DocumentWrapper(survey::Document* pDoc)
			: m_pDoc(pDoc)
		{
			assert(m_pDoc);

			m_ImagesWrpp = gcnew ImagesWrpp(pDoc->GetImages(), this);
			m_TextsWrpp = gcnew TextsWrpp(pDoc->GetTexts(), this);
			m_BlockShemeDesignWrpp = gcnew BlockShemeDesignWrpp(pDoc->GetBlockShemeDesign(), this);
			m_DesignWrpp = gcnew DesignWrpp(pDoc->GetDesign(), this);
			m_ProjectWrpp = gcnew ProjectWrpp(pDoc->GetProject(), this);
		}

		void dispose()
		{
			if (!m_pDoc)
				return;

			delete m_ProjectWrpp;
			delete m_DesignWrpp;
			delete m_BlockShemeDesignWrpp;
			delete m_TextsWrpp;
			delete m_ImagesWrpp;

			m_ProjectWrpp = nullptr;
			m_DesignWrpp = nullptr;
			m_BlockShemeDesignWrpp = nullptr;
			m_TextsWrpp = nullptr;
			m_ImagesWrpp = nullptr;

			m_pDoc = NULL;
		}

		~DocumentWrapper()
		{
			dispose();
			GC::SuppressFinalize(this);
		}

	public:

		!DocumentWrapper()
		{
			dispose();
		}

		property String^ ID
		{
			String^ get()
			{
				return gcnew String(m_pDoc->GetID().c_str());
			}
		}

		property ProjectWrpp^ Project
		{
			ProjectWrpp^ get()
			{
				return m_ProjectWrpp;
			}
		}
		
		property DesignWrpp^ Design
		{
			DesignWrpp^ get()
			{
				return m_DesignWrpp;
			}
		}

		property BlockShemeDesignWrpp^ BlockShemeDesign
		{
			BlockShemeDesignWrpp^ get()
			{
				return m_BlockShemeDesignWrpp;
			}
		}

		property TextsWrpp^ Texts
		{
			TextsWrpp^ get()
			{
				return m_TextsWrpp;
			}
		}

		property ImagesWrpp^ Images
		{
			ImagesWrpp^ get()
			{
				return m_ImagesWrpp;
			}
		}

		property String^ Name
		{
			String^ get() { return Helper::Converter::ConvertFromUtf8(m_pDoc->Name()); }
		}

		void Save(Dictionary<survey::ID, BitmapImage^>^ previews);

		bool ToNextRevision();
		bool ToPreviosRevision();

		List<BackupProjectProperties^>^ GetAllBackupDescriptions();
		bool SetBackup(String^ id);


	internal:

		ProjectWrpp^  m_ProjectWrpp = nullptr;
		DesignWrpp^   m_DesignWrpp = nullptr;
		BlockShemeDesignWrpp^  m_BlockShemeDesignWrpp = nullptr;
		TextsWrpp^ m_TextsWrpp = nullptr;
		ImagesWrpp^ m_ImagesWrpp = nullptr;

		survey::Document*  m_pDoc;
	};


	public ref class DocumentManagerWrapper : IDisposable
	{
	internal:

 		DocumentManagerWrapper(Dispatcher^ dispatcher)
 			: m_GUI_Dispatcher(dispatcher)
 		{
			if (!m_GUI_Dispatcher)
				throw gcnew NullReferenceException("dispatcher is null");

// 			// create doc manager
// 			survey::DocumentManager::Instance()->AddNotificator(m_pNotifycator);
 
			// load documents
			for (const auto& pairDoc : survey::DocumentManager::Instance()->Documents())
				m_Documents->Add(gcnew DocumentWrapper(pairDoc.second.get()));
 		}

		// fields
		static DocumentManagerWrapper^ s_DocumentManagerWrapper = nullptr;
		Dispatcher^  m_GUI_Dispatcher = nullptr;

	public:

		static DocumentManagerWrapper^ Instance(Dispatcher^ dispatcher)
		{
			if (s_DocumentManagerWrapper == nullptr)
				s_DocumentManagerWrapper = gcnew DocumentManagerWrapper(dispatcher);

			return s_DocumentManagerWrapper;
		}

		static DocumentManagerWrapper^ GetInstance()
		{
			if (s_DocumentManagerWrapper == nullptr)
				throw gcnew NullReferenceException("dispatcher is null");

			return s_DocumentManagerWrapper;
		}

		static void Clear()
		{
			if (s_DocumentManagerWrapper != nullptr)
			{
				delete s_DocumentManagerWrapper;
				s_DocumentManagerWrapper = nullptr;
			}
		}

		property ObservableCollection<DocumentWrapper^>^ Documents
		{
			ObservableCollection<DocumentWrapper^>^ get()
			{
				return m_Documents;
			}
		}

		DocumentWrapper^ CreateDocument(String^ name)
		{
			survey::Document* pDoc = NULL;

			try
			{
				pDoc = survey::DocumentManager::Instance()->CreateDocument(Helper::Converter::ConvertToUtf8(name));
			}
			catch (...)
			{ 
				// to do log 
			}

			if (!pDoc)
				return nullptr;

 			DocumentWrapper^ newDoc = gcnew DocumentWrapper(pDoc);
 			m_Documents->Add(newDoc);

			return newDoc;
		}

		DocumentWrapper^ LoadDocumentByID(String^ id)
		{
			survey::Document* pDoc = NULL;

			try
			{
				pDoc = survey::DocumentManager::Instance()->LoadDocumentByID(Helper::Converter::ConvertToUtf8(id));
			}
			catch (...)
			{
				// to do log 
			}

			if (!pDoc)
				return nullptr;

			DocumentWrapper^ newDoc = gcnew DocumentWrapper(pDoc);
			m_Documents->Add(newDoc);

			return newDoc;
		}

		void CloseDocument(DocumentWrapper^ docWrpp)
		{
			if (!docWrpp)
				throw gcnew NullReferenceException("docWrpp");

			m_Documents->Remove(docWrpp);
			survey::Document* pNativeDoc = docWrpp->m_pDoc;
			survey::DocumentManager::Instance()->CloseDocument(pNativeDoc);

			delete docWrpp;
		}

	internal:

		~DocumentManagerWrapper()
		{
			dispose();
			GC::SuppressFinalize(this);
		}

		void dispose()
		{
			// reset notifyer
			if (!survey::DocumentManager::TryGetInstance())
				return;

			// 			survey::DocumentManager::Instance()->ResetNotificators();
			// 			NotifycatorContainer::dispose();

			// dispose documents
			for each(DocumentWrapper^ doc in m_Documents)
				delete doc;

			m_Documents->Clear();

			// delete doc manager
			survey::DocumentManager::Clear();
		}

	public:

		!DocumentManagerWrapper()
		{
			dispose();
		}

		// fields
		ObservableCollection<DocumentWrapper^>^  m_Documents = gcnew ObservableCollection<DocumentWrapper^>();
	};

}
