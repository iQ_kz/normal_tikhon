// This is the main DLL file.

#include "stdafx.h"
#include "SurveyBridge.h"


using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections::ObjectModel;
using namespace System::Windows::Threading;
using namespace System::Collections::Generic;
using namespace System::Windows::Media::Imaging;
using namespace System::IO;


namespace survey {

	namespace marshalling {


		void WrapperNotifycator::OnChanged(ISurveyObject* sender, EventArg* pArgs, bool async = false)
		{
			if (!sender || !pArgs)
				return;

			m_pWrapper->InvokeNotifyPropertyChanged(sender, pArgs, async);
		}

	}
}


namespace Survey {


	XMLObjectWrpp::XMLObjectWrpp(survey::IXMLObject* pObject, DocumentWrapper^ docWrpp, bool autoKill)
		: m_pObject(pObject)
		, m_DocWrpp(docWrpp)
		, m_AutoKill(autoKill)
	{
		assert(m_pObject && docWrpp);

		if (m_AutoKill)
			docWrpp->m_ProjectWrpp->m_NativeToManaged->Add(IntPtr(pObject), this);
	}


	XMLObjectWrpp^ XMLObjectWrpp::Parent::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		survey::IXMLObject* pParet = m_pObject->Parent();
		if (!pParet)
			return nullptr;

		return m_DocWrpp->m_ProjectWrpp->m_NativeToManaged[IntPtr(pParet)];
	}


	XMLObjectWrpp^ XMLObjectWrpp::ChildFirst::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		survey::IXMLObject* pChildFirst = m_pObject->ChildFirst();
		if (!pChildFirst)
			return nullptr;

		return m_DocWrpp->m_ProjectWrpp->m_NativeToManaged[IntPtr(pChildFirst)];
	}


	XMLObjectWrpp^ XMLObjectWrpp::ChildLast::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		survey::IXMLObject* pChildLast = m_pObject->ChildLast();
		if (!pChildLast)
			return nullptr;

		return m_DocWrpp->m_ProjectWrpp->m_NativeToManaged[IntPtr(pChildLast)];
	}


	XMLObjectWrpp^ XMLObjectWrpp::PreviosBrother::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		survey::IXMLObject* pPreviosBrother = m_pObject->PreviosBrother();
		if (!pPreviosBrother)
			return nullptr;

		return m_DocWrpp->m_ProjectWrpp->m_NativeToManaged[IntPtr(pPreviosBrother)];
	}


	XMLObjectWrpp^ XMLObjectWrpp::NextBrother::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		survey::IXMLObject* pNextBrother = m_pObject->NextBrother();
		if (!pNextBrother)
			return nullptr;

		return m_DocWrpp->m_ProjectWrpp->m_NativeToManaged[IntPtr(pNextBrother)];
	}


	List<XMLObjectWrpp^>^ XMLObjectWrpp::Childs()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		const std::list<survey::IXMLObject*>& childs = m_pObject->ChildsTree();
		List<XMLObjectWrpp^>^ resLst = gcnew List<XMLObjectWrpp^>();

		for (survey::IXMLObject* pChild : childs)
			resLst->Add(m_DocWrpp->m_ProjectWrpp->m_NativeToManaged[IntPtr(pChild)]);

		return resLst;
	}


	void XMLObjectWrpp::RemoveChild(XMLObjectWrpp^ xmlObject)
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		m_pObject->RemoveChild(xmlObject->m_pObject);
	}


	void XMLObjectWrpp::AddChild(XMLObjectWrpp^ xmlObject)
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		m_pObject->AddChild(xmlObject->m_pObject);
	}


	void XMLObjectWrpp::AddChild(XMLObjectWrpp^ xmlObject, XMLObjectWrpp^ afterXmlObject)
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		m_pObject->AddChild(xmlObject->m_pObject, afterXmlObject->m_pObject);
	}


	void XMLObjectWrpp::dispose()
	{
		if (m_AutoKill)
		{
			const std::list<survey::IXMLObject*>& childs = m_pObject->ChildsTree();
			for (survey::IXMLObject* pObject : childs)
			{
				XMLObjectWrpp^ searchObj = nullptr;
				if (!m_DocWrpp->m_ProjectWrpp->m_NativeToManaged->TryGetValue(IntPtr(pObject), searchObj))
					continue;

				searchObj->m_AutoKill = false;
				searchObj->m_pObject = NULL;

				m_DocWrpp->m_ProjectWrpp->m_NativeToManaged->Remove(IntPtr(pObject));
				delete searchObj;
			}

			m_pObject->KillHimself();
			m_DocWrpp->m_ProjectWrpp->m_NativeToManaged->Remove(IntPtr(m_pObject));

			m_AutoKill = false;
		}

		m_pObject = NULL;
		m_DocWrpp = nullptr;
	}

	// =================================== DocumentObject ==================================================================== 

	survey::ID DocumentObject::Id::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		return dynamic_cast<survey::DocumentObject*>(m_pObject)->GetID();
	}


	String^ DocumentObject::Name::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		return Helper::Converter::ConvertFromUtf8(dynamic_cast<survey::DocumentObject*>(m_pObject)->GetName());
	}


	void DocumentObject::Name::set(String^ name)
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		dynamic_cast<survey::DocumentObject*>(m_pObject)->SetName(Helper::Converter::ConvertToUtf8(name));
	}


	DesignElementWrpp^ DocumentObject::DesignElement::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		survey::DesignElement* pDesignEl = dynamic_cast<survey::DocumentObject*>(m_pObject)->GetDesignElement();
		if (!pDesignEl)
			return nullptr;

		return dynamic_cast<DesignElementWrpp^>(m_DocWrpp->m_DesignWrpp->m_NativeToManaged[IntPtr(pDesignEl)]);
	}


	BSDesignElementWrpp^ DocumentObject::BSDesignElement::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		survey::BSDesignElement* pDesignEl = dynamic_cast<survey::DocumentObject*>(m_pObject)->GetBSDesignElement();
		if (!pDesignEl)
			return nullptr;

		return dynamic_cast<BSDesignElementWrpp^>(m_DocWrpp->m_BlockShemeDesignWrpp->m_NativeToManaged[IntPtr(pDesignEl)]);
	}


	List<DocumentObject^>^ DocumentObject::DesignObjects::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		const std::list<survey::DocumentObject*>& designObjs = dynamic_cast<survey::DocumentObject*>(m_pObject)->GetDesignObjects();
		List<DocumentObject^>^ resLst = gcnew List<DocumentObject^>();

		for (survey::DocumentObject* pChild : designObjs)
			resLst->Add(dynamic_cast<DocumentObject^>(m_DocWrpp->m_ProjectWrpp->m_NativeToManaged[IntPtr(pChild)]));

		return resLst;
	}


	List<DocumentObject^>^ DocumentObject::BlockShemeDesignObjects::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		const std::list<survey::DocumentObject*>& designObjs = dynamic_cast<survey::DocumentObject*>(m_pObject)->GetBlockShemeDesignObjects();
		List<DocumentObject^>^ resLst = gcnew List<DocumentObject^>();

		for (survey::DocumentObject* pChild : designObjs)
			resLst->Add(dynamic_cast<DocumentObject^>(m_DocWrpp->m_ProjectWrpp->m_NativeToManaged[IntPtr(pChild)]));

		return resLst;
	}


	void DocumentObject::onAfterRevival(Dictionary<IntPtr, XMLObjectWrpp^>^ nativeToManaged)
	{
		NotifycatorContainer::onAfterRevival(nativeToManaged);

		List<DocumentObject^>^ arrDocObjWrpps = gcnew List<DocumentObject^>();

		survey::DocumentObject* pDocObject = dynamic_cast<survey::DocumentObject*>(m_pObject->ChildFirst());
		while (pDocObject)
		{
			DocumentObject^ objWrpp = dynamic_cast<DocumentObject^>(nativeToManaged[IntPtr(pDocObject)]);
			if (!objWrpp)
			{
				// to do log error
				throw gcnew Exception("Undefined behavoir");
			}

			arrDocObjWrpps->Add(objWrpp);
			pDocObject = dynamic_cast<survey::DocumentObject*>(pDocObject->NextBrother());
		}

		m_Childs->ReplaceRange(arrDocObjWrpps);
	}


	void DocumentObject::NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr)
	{
		survey::EventArg* pEvArg = (survey::EventArg*)argPtr.ToPointer();
		if (!pEvArg)
			return;

		survey::CollectionEventArg* pCollEvArg = dynamic_cast<survey::CollectionEventArg*>(pEvArg);
		if (pCollEvArg && pCollEvArg->m_EventName == "childs")
		{
			if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_PushBack)
			{
				Dictionary<IntPtr, XMLObjectWrpp^>^ nativeToManaged = m_DocWrpp->m_ProjectWrpp->m_NativeToManaged;

				DocumentObject^ objWrpp = dynamic_cast<DocumentObject^>(nativeToManaged[IntPtr(pCollEvArg->m_pObject)]);
				if (!objWrpp)
				{
					// to do log error
					throw gcnew Exception("Undefined behavoir");
				}

				m_Childs->Add(objWrpp);
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Erase)
			{
				Dictionary<IntPtr, XMLObjectWrpp^>^ nativeToManaged = m_DocWrpp->m_ProjectWrpp->m_NativeToManaged;

				DocumentObject^ objWrpp = dynamic_cast<DocumentObject^>(nativeToManaged[IntPtr(pCollEvArg->m_pObject)]);
				if (!objWrpp)
				{
					// to do log error
					throw gcnew Exception("Undefined behavoir");
				}

				m_Childs->Remove(objWrpp);
			}
		}
		else
		{
			DocumentObject::NotifyPropertyChanged(senderPtr, argPtr);
		}

	}

	// ================================================= Header =====================================================================

	HeaderWrpp::HeaderWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, ColorDatWrpp^ color,
		CoordDatWrpp^ coord, String^ text, ImageProperties imgProps, BorderDat borderDat, String^ name)
		: DocumentObject(new survey::Header(docWrpp->m_pDoc, bsColor->ToColorDat(), bsCoord->ToCoordDat(), color->ToColorDat(), coord->ToCoordDat()
		, Helper::Converter::ConvertToUtf8(text), borderDat.ToNative(), imgProps.ToNative(), Helper::Converter::ConvertToUtf8(name)), docWrpp, true)
	{ }


	TextWrpp^ HeaderWrpp::Text::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		survey::Text* pTxt = dynamic_cast<survey::Header*>(m_pObject)->GetText();
		if (!pTxt)
			return nullptr;

		return dynamic_cast<TextWrpp^>(m_DocWrpp->m_TextsWrpp->m_NativeToManaged[IntPtr(pTxt)]);
	}


	QuestionWrpp::QuestionWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, ColorDatWrpp^ color, CoordDatWrpp^ coord, String^ name)
		: DocumentObject(new survey::Question(docWrpp->m_pDoc, bsColor->ToColorDat(), bsCoord->ToCoordDat(), color->ToColorDat(), coord->ToCoordDat()
		, Helper::Converter::ConvertToUtf8(name)), docWrpp, true)
	{ }


	QaWrpp::QaWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, ColorDatWrpp^ color, CoordDatWrpp^ coord, String^ name)
		: DocumentObject(new survey::Qa(docWrpp->m_pDoc, bsColor->ToColorDat(), bsCoord->ToCoordDat(), color->ToColorDat(), coord->ToCoordDat()
		, Helper::Converter::ConvertToUtf8(name)), docWrpp, true)
	{ }


	BlockWrpp::BlockWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, ColorDatWrpp^ color, CoordDatWrpp^ coord, String^ name)
		: DocumentObject(new survey::Block(docWrpp->m_pDoc, bsColor->ToColorDat(), bsCoord->ToCoordDat(), color->ToColorDat(), coord->ToCoordDat()
		, Helper::Converter::ConvertToUtf8(name)), docWrpp, true)
	{ }


	// ============================================  Page Wrapper ===============================================================================================

	PageWrpp::PageWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, ColorDatWrpp^ color, CoordDatWrpp^ coord, String^ name)
		: DocumentObject(new survey::Page(docWrpp->m_pDoc, bsColor->ToColorDat(), bsCoord->ToCoordDat(), color->ToColorDat(), coord->ToCoordDat()
		, Helper::Converter::ConvertToUtf8(name)), docWrpp, true)
	{ }


	void PageWrpp::NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr)
	{
		survey::EventArg* pEvArg = (survey::EventArg*)argPtr.ToPointer();
		if (!pEvArg)
			return;

		survey::CollectionEventArg* pCollEvArg = dynamic_cast<survey::CollectionEventArg*>(pEvArg);
		if (pCollEvArg && pCollEvArg->m_EventName == "elements")
		{
			if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_InsertRange)
			{
				onUpdateChilds(m_DocWrpp->m_ProjectWrpp->m_NativeToManaged);
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_EraseRange)
			{
				onUpdateChilds(m_DocWrpp->m_ProjectWrpp->m_NativeToManaged);
			}
		}
		else
		{
			DocumentObject::NotifyPropertyChanged(senderPtr, argPtr);
		}

	}

	void PageWrpp::onAfterRevival(Dictionary<IntPtr, XMLObjectWrpp^>^ nativeToManaged)
	{
		DocumentObject::onAfterRevival(nativeToManaged);
		onUpdateChilds(nativeToManaged);
	}

	void PageWrpp::onUpdateChilds(Dictionary<IntPtr, XMLObjectWrpp^>^ nativeToManaged)
	{
		const std::list<survey::IXMLObject*>& objs = m_pObject->ChildsTree();
		List<XMLObjectWrpp^>^ wrppers = gcnew List<XMLObjectWrpp^>();

		for (survey::IXMLObject* pObject : objs)
		{
			assert(pObject);
			wrppers->Add(nativeToManaged[IntPtr(pObject)]);
		}

		m_DocObjects->ReplaceRange(wrppers);
	}

	// ============================================ Chapter Wrapper ===============================================================================================

	ChapterWrpp::ChapterWrpp(DocumentWrapper^ docWrpp, ColorDatWrpp^ bsColor, CoordDatWrpp^ bsCoord, String^ name)
		: DocumentObject(new survey::Chapter(docWrpp->m_pDoc, bsColor->ToColorDat(), bsCoord->ToCoordDat()
		, Helper::Converter::ConvertToUtf8(name)), docWrpp, true)
	{ }


	void ChapterWrpp::NotifyPropertyChanged(IntPtr senderPtr, IntPtr argPtr)
	{
		survey::EventArg* pEvArg = (survey::EventArg*)argPtr.ToPointer();
		if (!pEvArg)
			return;

		survey::CollectionEventArg* pCollEvArg = dynamic_cast<survey::CollectionEventArg*>(pEvArg);
		if (pCollEvArg && pCollEvArg->m_EventName == "pages")
		{
			if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_PushBack)
			{
				const std::list<survey::IXMLObject*>& objs = pCollEvArg->m_Objects;
				assert(objs.size());

				PageWrpp^ newPage = dynamic_cast<PageWrpp^>(m_DocWrpp->m_ProjectWrpp->m_NativeToManaged[IntPtr(*objs.begin())]);
				assert(newPage);

				m_Pages->Add(newPage);
			}
			else if (pCollEvArg->m_EventType == survey::CollectionEventType::CET_Erase)
			{
				const std::list<survey::IXMLObject*>& objs = pCollEvArg->m_Objects;
				assert(objs.size());

				PageWrpp^ removePage = dynamic_cast<PageWrpp^>(m_DocWrpp->m_ProjectWrpp->m_NativeToManaged[IntPtr(*objs.begin())]);
				assert(removePage);

				m_Pages->Remove(removePage);
			}
		}
		else
		{
			DocumentObject::NotifyPropertyChanged(senderPtr, argPtr);
		}
	}

	void ChapterWrpp::onAfterRevival(Dictionary<IntPtr, XMLObjectWrpp^>^ nativeToManaged)
	{
		DocumentObject::onAfterRevival(nativeToManaged);

		const std::list<survey::IXMLObject*>& objs = m_pObject->ChildsTree();
		List<PageWrpp^>^ wrppers = gcnew List<PageWrpp^>();

		for (survey::IXMLObject* pObject : objs)
		{
			assert(pObject);
			PageWrpp^ pageWrpp = dynamic_cast<PageWrpp^>(nativeToManaged[IntPtr(pObject)]);
			if (pageWrpp)
				wrppers->Add(pageWrpp);
		}

		m_Pages->ReplaceRange(wrppers);
	}

	// ==================================================  NotifycatorContainer ============================================================== 

	void NotifycatorContainer::InvokeNotifyPropertyChanged(survey::ISurveyObject* sender, survey::EventArg* args, bool async)
	{
		NotifyPropertyChangedDelegate^ action = gcnew NotifyPropertyChangedDelegate(this, &NotifycatorContainer::NotifyPropertyChanged);

		if (async)
			DocumentManagerWrapper::GetInstance()->m_GUI_Dispatcher->BeginInvoke(action, IntPtr(sender), IntPtr(args));
		else
			action(IntPtr(sender), IntPtr(args));
	}


	// ======================================== TextsWrpp ========================================================================================

	TextsWrpp::TextsWrpp(survey::Texts* pObj, DocumentWrapper^ docWrpp)
		: SectionWrpp(pObj, docWrpp)
	{
		assert(pObj && docWrpp);
		pObj->AddNotificator(m_pNotifycator);

		replaceWrapps();
	}

	ObservableRangeCollection<TextWrpp^>^ TextsWrpp::Texts::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		return m_TextWrpps;
	}


	// ====================================================== DesignWrpp ======================================================


	DocumentObject^ DesignElementWrpp::DesignObject::get()
	{
		if (!m_pObject)
			throw gcnew System::Exception("Object was disposed");

		return dynamic_cast<DocumentObject^>(m_DocWrpp->m_ProjectWrpp->m_NativeToManaged[IntPtr(dynamic_cast<survey::DesignElement*>(m_pObject)->GetOwner())]);
	}


	DesignWrpp::DesignWrpp(survey::Design* pObj, DocumentWrapper^ docWrpp)
		: SectionWrpp(pObj, docWrpp)
	{
		assert(pObj && docWrpp);
		pObj->AddNotificator(m_pNotifycator);

		replaceWrapps();
	}


	ObservableRangeCollection<DesignElementWrpp^>^ DesignWrpp::Designs::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		return m_DesignElWrpps;
	}


	// ============================================ BlockShemeDesignWrpp =====================================================================================

	BlockShemeDesignWrpp::BlockShemeDesignWrpp(survey::BlockShemeDesign* pObj, DocumentWrapper^ docWrpp)
		: SectionWrpp(pObj, docWrpp)
	{
		assert(pObj && docWrpp);
		pObj->AddNotificator(m_pNotifycator);

		replaceWrapps();
	}


	ObservableRangeCollection<BSDesignElementWrpp^>^ BlockShemeDesignWrpp::Designs::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		return m_DesignElWrpps;
	}


	// ====================================== ProjectWrpp ======================================================================================================

	ObservableRangeCollection<XMLObjectWrpp^>^ ProjectWrpp::TreeObjects::get()
	{
		if (!m_pObject || !m_DocWrpp)
			throw gcnew System::Exception("Object was disposed");

		return m_DocObjects;
	}


	// ====================================== Image ============================================================================================================

	System::Drawing::Image^ ImageWrpp::ImageData::get()
	{
		std::vector<unsigned char> fData;
		size_t sizeData = 0;
		if (!survey::SurveyType::Cast<survey::Image>(m_pObject)->ImageData(fData) || !(sizeData = fData.size()))
			return nullptr;

		try
		{
			array<unsigned char>^ buff = gcnew array<unsigned char>(sizeData);
			pin_ptr<Byte> pin(&buff[0]);
			memcpy(pin, fData.data(), sizeData);

			System::IO::MemoryStream^ ms = gcnew System::IO::MemoryStream();
			ms->Write(buff, 0, sizeData);

			return System::Drawing::Image::FromStream(ms);
		}
		catch (...)
		{
			// to do log error
		}

		return nullptr;
	}


	String^ ImageWrpp::ImagePath::get()
	{
		return gcnew String(survey::SurveyType::Cast<survey::Image>(m_pObject)->GetImagePath().c_str());
	}


	BitmapImage^ ImageWrpp::PreviewImageData::get()
	{
		std::vector<unsigned char> fData;
		size_t sizeData = 0;
		if (!survey::SurveyType::Cast<survey::Image>(m_pObject)->PreviewImageData(fData) || !(sizeData = fData.size()))
			return nullptr;

		try
		{
			array<unsigned char>^ buff = gcnew array<unsigned char>(sizeData);
			pin_ptr<Byte> pin(&buff[0]);
			memcpy(pin, fData.data(), sizeData);

			System::IO::MemoryStream^ ms = gcnew System::IO::MemoryStream();
			ms->Write(buff, 0, sizeData);

			BitmapImage^ img = gcnew BitmapImage();
			img->BeginInit();
			img->StreamSource = ms;
			img->EndInit();

			return img;
		}
		catch (...)
		{
			// to do log error
		}

		return nullptr;
	}


	BitmapImage^ ImageWrpp::TryGetImageDataAsBitmap()
	{
		std::vector<unsigned char> fData;
		size_t sizeData = 0;
		if (!survey::SurveyType::Cast<survey::Image>(m_pObject)->ImageDataAsPNG(fData) || !(sizeData = fData.size()))
		{
			// to do log error
			return nullptr;
		}

		try
		{
			array<unsigned char>^ buff = gcnew array<unsigned char>(sizeData);
			pin_ptr<Byte> pin(&buff[0]);
			memcpy(pin, fData.data(), sizeData);

			System::IO::MemoryStream^ ms = gcnew System::IO::MemoryStream();
			ms->Write(buff, 0, sizeData);

			BitmapImage^ img = gcnew BitmapImage();
			img->BeginInit();
			img->StreamSource = ms;
			img->EndInit();

			return img;
		}
		catch (...)
		{
			// to do log error
		}

		return nullptr;
	}


	void ImageElement::IdImage::set(String^ str)
	{
		assert(m_pObject);

		if (String::IsNullOrEmpty(str))
		{
			dynamic_cast<survey::ImageElement*>(m_pObject)->SetIdImage("");
			return;
		}

		Image = m_DocWrpp->m_ImagesWrpp->ImageByID(str);
	}

	
	ImageWrpp^ ImageElement::Image::get()
	{
		return m_DocWrpp->m_ImagesWrpp->ImageByID(gcnew String(dynamic_cast<survey::ImageElement*>(m_pObject)->GetIdImage().c_str()));
	}

	void ImageElement::Image::set(ImageWrpp^ imgWrpp)
	{
		if (!imgWrpp)
		{
			dynamic_cast<survey::ImageElement*>(m_pObject)->SetIdImage("");
			return;
		}

		dynamic_cast<survey::ImageElement*>(m_pObject)->SetIdImage(survey::marshalling::MarshallingHelper::ToNativeString(imgWrpp->ID));
	}

	// ====================================== Document =========================================================================================================

	void DocumentWrapper::Save(Dictionary<survey::ID, BitmapImage^>^ previews)
	{
		if (!previews)
			throw gcnew NullReferenceException("previews");

		survey::PreviewMap previewsData;

		for each(KeyValuePair<survey::ID, BitmapImage^>^ pairPreviews in previews)
		{
			std::vector<unsigned char>& byteArray = previewsData[pairPreviews->Key];

			PngBitmapEncoder^ encoder = gcnew PngBitmapEncoder();
			encoder->Frames->Add(BitmapFrame::Create(pairPreviews->Value));

			MemoryStream^ ms = gcnew MemoryStream();
			encoder->Save(ms);

			array<Byte>^ arr = ms->ToArray();
			byteArray.resize(arr->Length, 0);
			pin_ptr<Byte> pin(&arr[0]);

			unsigned char* pFirst(pin);
			unsigned char* pLast(pin + arr->Length);

			std::copy(pFirst, pLast, byteArray.begin());
			delete ms;
		}

		if (!m_pDoc->Save(previewsData))
			throw gcnew Exception("Internal error");
	}


	bool DocumentWrapper::ToNextRevision()
	{
		assert(m_pDoc);

		try
		{
			return m_pDoc->ToNextRevision();
		}
		catch (const boost::exception& ex)
		{
			// to do log error
		}
		catch (const std::exception& ex)
		{
			// to do log error
		}
		catch (...)
		{
			// to do log error
		}

		throw gcnew Exception("Fatal error !!!");
	}


	bool DocumentWrapper::ToPreviosRevision()
	{
		assert(m_pDoc);

		try
		{
			return m_pDoc->ToPreviosRevision();
		}
		catch (const boost::exception& ex)
		{
			// to do log error
		}
		catch (const std::exception& ex)
		{
			// to do log error
		}
		catch (...)
		{
			// to do log error
		}

		throw gcnew Exception("Fatal error !!!");
	}


	List<BackupProjectProperties^>^ DocumentWrapper::GetAllBackupDescriptions()
	{
		assert(m_pDoc);

		try
		{
			List<BackupProjectProperties^>^ resArr = gcnew List<BackupProjectProperties^>();
			const std::list<survey::BackupProjectProperties>& backupProjectProperties = m_pDoc->GetAllBackupDescriptions();

			for (const auto& bacupProp : backupProjectProperties)
				resArr->Add(gcnew BackupProjectProperties(bacupProp));

			return resArr;
		}
		catch (const boost::exception& ex)
		{
			// to do log error
		}
		catch (const std::exception& ex)
		{
			// to do log error
		}
		catch (...)
		{
			// to do log error
		}

		throw gcnew Exception("Fatal error !!!");
	}


	bool DocumentWrapper::SetBackup(String^ id)
	{
		assert(m_pDoc);

		try
		{
			return m_pDoc->SetBackup(msclr::interop::marshal_as<std::string>(id));
		}
		catch (const boost::exception& ex)
		{
			// to do log error
		}
		catch (const std::exception& ex)
		{
			// to do log error
		}
		catch (...)
		{
			// to do log error
		}

		throw gcnew Exception("Fatal error !!!");
	}


	// ================================================== ProjectInfoWrapper =============================================================

	BitmapImage^ ProjectInfoWrapper::GetPreview(survey::ID id)
	{
		std::vector<unsigned char> dataPreview;
		if (!m_pProjInfo->GetPreview(id, dataPreview))
			return nullptr;

		const size_t sizeData = dataPreview.size();
		array<unsigned char>^ buff = gcnew array<unsigned char>(sizeData);
		pin_ptr<Byte> pin(&buff[0]);
		memcpy(pin, dataPreview.data(), sizeData);

		System::IO::MemoryStream^ ms = gcnew System::IO::MemoryStream();
		ms->Write(buff, 0, sizeData);

		BitmapImage^ img = gcnew BitmapImage();
		img->BeginInit();
		img->StreamSource = ms;
		img->EndInit();
		
		return img;
	}


	BitmapImage^ ProjectInfoWrapper::GetPreviewSmall(survey::ID id)
	{
		std::vector<unsigned char> dataPreview;
		if (!m_pProjInfo->GetSmallPreview(id, dataPreview))
			return nullptr;

		const size_t sizeData = dataPreview.size();
		array<unsigned char>^ buff = gcnew array<unsigned char>(sizeData);
		pin_ptr<Byte> pin(&buff[0]);
		memcpy(pin, dataPreview.data(), sizeData);

		System::IO::MemoryStream^ ms = gcnew System::IO::MemoryStream();
		ms->Write(buff, 0, sizeData);

		BitmapImage^ img = gcnew BitmapImage();
		img->BeginInit();
		img->StreamSource = ms;
		img->EndInit();

		return img;
	}
}