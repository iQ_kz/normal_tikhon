// survey_server.cpp: определяет точку входа для консольного приложения.
//


#include "stdafx.h"

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/bind.hpp>
#include <iostream>
#include "../common/byte_buffer.h"


#include <cstdlib>
#include <iostream>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>

#include <boost/asio/ssl.hpp>

#include <deque>
#include <vector>
#include <string>
#include <stdio.h>

typedef boost::asio::io_service io_svc;
typedef boost::shared_ptr <io_svc> io_svc_ptr;
using  namespace boost::asio;


using boost::asio::ip::tcp;
namespace fs = boost::filesystem;


typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;

//typedef boost::shared_ptr<tcp::socket> socket_ptr;

typedef boost::shared_ptr<ssl_socket> socket_ptr;



typedef std::vector <std::string> string_v;


string_v g_string_v;
const int max_length = 1024;
bool file2buffer(byte_buffer& out_data, const std::string&file_name)
{
	bool ret = false;

	std::cout << "read file " << file_name << std::endl;
	FILE* f = fopen(file_name.c_str(),"r");
	long len = 0;
	if (f)
	{
	 	fseek(f, 0, SEEK_END);
		len = ftell(f);
		std::cout << "len = " << len << std::endl;
		fseek(f, SEEK_SET, 0);
		out_data.reserve(len+sizeof(int));
		//out_data.set_size(len);
		size_t check_len = out_data.get_size();
		
		byte* file_buff = out_data.append_buff(len);
		fread(file_buff, len,1, f);
		fclose(f);
		ret = true;

	}
	return ret;
}

void session(socket_ptr sock)
{
	try
	{
		std::cout << "new connection:  " << sock->remote_endpoint().address().to_string() << std::endl;
		for (;;)
		{
	

			int command = 1;
			while (command)
			{
				for (string_v::iterator it = g_string_v.begin(), it_end = g_string_v.end(); it != it_end; ++it)
				{

					std::cout << "read client command" << std::endl;
					size_t reply_length = boost::asio::read(*sock, boost::asio::buffer(&command, sizeof(command)));
					if (reply_length > 0)
					{
						if (command == 0)
						{
							std::cout << "command '0'; end session " << std::endl;
							break;
						}
					}
					byte_buffer buff;
					
					if (!file2buffer(buff, *it))
					{
						const char* err = "read error";
						byte* msg =  buff.append_buff(strlen(err));
						memcpy(msg, err, strlen(err));
						std::cout << err << std::endl;
					}
					std::cout << buff.get_size() << "  bytes in buffer" << std::endl;
					std::cout << buff.get_full_size() << "  bytes in buffer with size" << std::endl;
					if (buff.get_size() == 0)
					{
						std::cout << "skip, because size == 0" << std::endl;
						continue;
					}
					std::cout << "send file. " << *it << ":  " << buff.get_size() << "  bytes"<< std::endl;
					
					for (size_t i = 0; i < 10&& i <buff.get_full_size(); ++i)
					{
						char c = (char)buff.get_full_buffer()[i];
						std::cout <<std::hex<< (int)c << ";";

					}
					std::cout << std::dec<< std::endl;
					size_t len = buff.get_size();
					size_t get_full_size_len = buff.get_full_size();

					size_t len2 = *(int*)buff.get_full_buffer();


					std::cout << "len =  " << len << "; get_full_size_len = " << get_full_size_len << "; len2 = " << len2<<std::endl;

					boost::asio::write(*sock, boost::asio::buffer((char*)&len, sizeof(len)));
					size_t write_len = boost::asio::write(*sock, boost::asio::buffer(buff.get_data(), buff.get_size()));
			
					std::cout << "file sent. " << write_len << "bytes" << std::endl;

				}
			}
			std::cout << "send 'end' command. "  << std::endl;
			int end_size = 0;
			boost::asio::write(*sock, boost::asio::buffer(&end_size, sizeof(end_size)));
		}
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception in thread: " << e.what() << "\n";
	}
}


boost::asio::ssl::context _context(boost::asio::ssl::context::sslv23);

void server(boost::shared_ptr<boost::asio::io_service> io_service, unsigned short port)
{
	tcp::acceptor a(*io_service, tcp::endpoint(tcp::v4(), port));
	for (;;)
	{
		socket_ptr sock(new ssl_socket(*io_service, _context));
		a.accept(*sock);

		boost::thread t(boost::bind(session, sock));
	}
}

void read_file_names(string_v& names, const std::string& self_name)
{

	fs::path p = fs::complete(fs::path(self_name));
	p = p.branch_path();
	fs::directory_iterator end_iter;
	for (fs::directory_iterator dir_itr(p);
		dir_itr != end_iter;
		++dir_itr)
	{
		std::string current_name = dir_itr->path().string();
		if (current_name != self_name)
		{
			names.push_back(current_name);
		}
	}
}

int main(int argc, char* argv[])
{



	try
	{
		std::string self_name = argv[0];

	

		read_file_names(g_string_v, self_name);
	
		int port = 801;
		
		if (argc != 2)
		{

		}
		else
		{
			port = atoi(argv[1]);
		}
		

		//boost::asio::io_service io_service;

		boost::shared_ptr<boost::asio::io_service> io_service_ptr( new boost::asio::io_service);
		using namespace std; // For atoi.
		std::cout << "start server " << std::endl;

		boost::thread ServNetThread(boost::bind(server, io_service_ptr, port));
		//server(io_service, port);
		
		std::cin.get();
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}
