// tcp_test.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>



#ifdef WIN32
#define _WIN32_WINNT 0x0501
#include <stdio.h>
#endif		

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <boost/asio.hpp>
#include <boost/timer.hpp>
#include <boost/chrono.hpp>
#include "../common/byte_buffer.h"


typedef boost::asio::io_service io_svc;
typedef boost::shared_ptr <io_svc> io_svc_ptr;
using boost::asio::ip::tcp;
//namespace tmr =  boost::timer;


enum { max_length = 1024 };

int main(int argc, char* argv[])
{
	try
	{
		std::cout << "start" << std::endl;
		std::cin.get();

		std::string host = "localhost";
		std::string port = "801";

		if (argc != 3)
		{
			std::cout << "host and port by default" << std::endl;
		
		}
		else
		{
			host = argv[1];
			port = argv[2];

		}

		boost::asio::io_service io_service;

		tcp::resolver resolver(io_service);
		tcp::resolver::query query(tcp::v4(), host, port);
		tcp::resolver::iterator iterator = resolver.resolve(query);
		tcp::endpoint ep = *iterator;
		std::cout << ep.address().to_string() << std::endl;

		tcp::socket s(io_service);
		boost::asio::connect(s, iterator);

		using namespace std; // For strlen.


		int32_t len = 0;

		size_t max_count = 100;
		size_t counter = 0;
	
		boost::timer timer;
		

		size_t TotalLen = 0;
		
		while (true)
		{

			int command = 1;
			if (counter++ > max_count)
			{
				command = 0;
			}

			boost::asio::write(s, boost::asio::buffer(&command, sizeof(command)));
			std::cout << "command " << command << "sent" << std::endl;


			std::cout << "read reply len.... " << std::endl;
			byte_buffer buff;

			//size_t reply_length = boost::asio::read(s,
			//	boost::asio::buffer((char*)&len, sizeof(len)));

			size_t reply_length = boost::asio::read(s,
				boost::asio::buffer((char*)buff.get_full_buffer(), 4));
			len = buff.get_size();

			TotalLen += reply_length;
		/*	for (size_t i = 0; i < 4&& i < buff.get_full_size(); ++i)
			{
				char c = (char)buff.get_full_buffer()[i];
				std::cout << std::hex <<(int) c << ";";

			}*/
			std::cout << std::dec << std::endl;

			if (len == 0)
			{
				std::cout << "len == 0" << std::endl;
				break;
			}
			buff.set_size(0);
			buff.append_buff(len);
			std::cout << "len == " << len << std::endl;
			if (reply_length == sizeof(len))
			{
				std::vector<char> read_buff(len);

				std::cout << "read reply....." << std::endl;
				//size_t reply_length = boost::asio::read(s,
				//	boost::asio::buffer(&read_buff.front(), len));



				size_t reply_length = boost::asio::read(s,
					boost::asio::buffer((char*)buff.get_full_buffer()+4, len));

				TotalLen += reply_length;
				std::cout << "end, " << reply_length<< " bytes" << std::endl;

		/*		for (size_t i = 4; i < 10&&i < buff.get_full_size(); ++i)
				{
					char c = (char)buff.get_full_buffer()[i];
					std::cout << std::hex <<(int) c <<" ;";

				}*/
				std::cout << std::dec << std::endl;

			}
			else
			{
				std::cout << "read len failed" ;
				break;
			}
			std::cout << "------------------------------------------------------------------" << std::endl;
			std::cout << "counter = " << counter << ";  TotalLen = " << TotalLen/1024<< "Kb"<< std::endl;
		
			std::cout << "------------------------------------------------------------------" << std::endl;
		}
	
		double   res_time = timer.elapsed();
		std::cout << "work time == " << res_time << " sec"<<std::endl;

	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	std::cin.get();
	return 0;
}


