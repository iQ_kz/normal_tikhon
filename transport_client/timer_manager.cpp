#include "stdafx.h"
#include "timer_manager.h"

namespace  survey
{
	namespace transport
	{




		void TimerManager::RegisterTimer(size_t ms, ID Id)
		{
			TimerPtr NewTimer(new boost::asio::deadline_timer(IO_Handler::m_IO));
			{
				Guard Lock(m_mutex);
				m_TimerContainer[Id] = NewTimer;
			}


			RunTimer(NewTimer, ms, Id);
		}

		void TimerManager::RunTimer(TimerPtr TimerObj, size_t ms, ID Id)
		{
			TimerObj->expires_from_now(boost::posix_time::millisec(ms));
			TimerObj->async_wait(boost::bind(&TimerManager::OnTimeout, this, ms, Id));
		}

		void TimerManager::OnTimeout(size_t ms, ID Id)
		{
			m_Handler(Id);
			Guard Lock(m_mutex);
			TimerContainer::iterator it = m_TimerContainer.find(Id);
			if (it != m_TimerContainer.end())
			{
				TimerPtr CurrentTimer = it->second;

				RunTimer(CurrentTimer, ms, Id);
			}
		}

		void TimerManager::SetHandler(HandlerType Handler)
		{
			m_Handler = Handler;
		}

		void TimerManager::Stop()
		{
			Guard Lock(m_mutex);
			m_TimerContainer.clear();
		}



	} //!transport

};	//!survey