#pragma once
#include <iostream>
#include <string>
#include "../client_side.h"


//#include "../transport_client_core.h"



#include "..\..\Include\test_utils.h"

#include <boost/thread.hpp>



#include "../../include/common_types.h"
#include "../../include/transport/transport_types.h"
#include "../../include/transport_client_interface.h"
//#include "../../survey_server/server_handler.h"
//#include "../../survey_server/server_side.h"

#include "openssl/md5.h"
static std::string CalculateMd5(const std::string& str)
{
	std::string res;
	res.resize(MD5_DIGEST_LENGTH, 0);

	MD5((unsigned char*)str.data(), str.length(), (unsigned char*)res.data());
	return res;
}



namespace survey
{
	namespace transport
	{

		class TestClientHandler : public ITransportClientRep
		{
		public:
			//TODO disconnect, error
			TestClientHandler() :m_AnswerCounter(0), m_IsConnected(false), m_ProjectId(0){}
			virtual void OnConnect()
			{
				m_IsConnected = true;
				std::cout << "TestClientHandler::OnConnect" << std::endl;
			}

			virtual void OnDisconnect()
			{
				m_IsConnected = false;
				std::cout << "TestClientHandler::OnDisconnect" << std::endl;
			}

			virtual void OnError(const std::string msg)
			{
				m_IsConnected = false;
				std::cout << "TestSurveyCore::TestClientHandler: " << msg << std::endl;
			}

			virtual void OnLogin(const Result& res)
			{
				std::cout << "TestClientHandler::OnLogin Result code = " << (int)res.RetCode << std::endl;
				m_LastResult = res;
				++m_AnswerCounter;
			}


			virtual void OnCreateProject(const Result& res, const ProjectId& Id)
			{
				std::cout << "TestClientHandler::OnCreateProject Result code = " << (int)res.RetCode << std::endl;
				m_LastResult = res;
				++m_AnswerCounter;
				m_ProjectId = Id;
			}

		
			void OnUploadFile(const Result& res, ID Id, const std::string& fileHash, const std::string& Name)
			{
				std::cout << "TestClientHandler::OnUploadFile Result code = " << (int)res.RetCode << std::endl;
				m_LastResult = res;
				++m_AnswerCounter;
				m_ProjectId = Id;
			} 

			virtual void OnGetFile(const Result& res, const std::string&  hash, const std::string& Name, const boost::filesystem::path&  FilePath)
			{
				std::cout << "TestClientHandler::OnGetFile Result code = " << (int)res.RetCode << std::endl;
				m_LastResult = res;
				++m_AnswerCounter;
			}

			virtual void OnCommit(const Version& versCommit, const CommitConfirm& commitConfirm) {}

			virtual void Commit(const CommitData& commit, Version& versCommit){}//TODO ?

			virtual void OnGetRevision(const Result& res, const FileInfoContainer& Info, const Version& CommitId) {}
			virtual void OnSendFile(const Result& res, const Version& CommitId) {}
			virtual void OnGetFile(const Result& res, const FileInfo& Info, const Version&  CommitId){}
			virtual void OnGetVersionList(const ProjectId& Id, const VersionContainer& VersionList){}
			virtual void OnTimeoutEvent(ID eventId) {}

			Result m_LastResult;
			size_t m_AnswerCounter;

			bool m_IsConnected;
			ProjectId m_ProjectId;
		};


		class TestSession
		{
		public:
			void Send(ByteBufferPtr BuffPtr)
			{
				BuffPtr->SetStart();
				m_Stub->OnRead(BuffPtr);
			}

			void SetStub(IStub* Stub)
			{
				m_Stub = StubPtr(Stub);
			}

			StubPtr m_Stub;
		};
		typedef boost::shared_ptr <TestSession>	TestConnectionPtr;

		class TestServHandler// : public ServerHandler
		{
		public:

			TestServHandler() :m_RequestCounter(0){}

			void Login(const std::string& login, const std::string&  password, ID RequestID)
			{
				m_Method = "Login";
				++m_RequestCounter;
				m_LastLogin = login;
				m_LastPassword = password;
				m_LastRequestID = RequestID;
				Result res;   // stub, TODO
				if (login != "tester")
				{
					res.RetCode = UnknownLogin;
				}
				else
				if (password != "111111")
				{
					res.RetCode = WrongPassword;
				}
				//m_proxy.OnLogin(res, RequestID);
				m_LastResult = res;
			}

			void Init(ConnectionPtr SPtr)
			{

			}

			std::string m_Method;
			std::string m_LastLogin;
			std::string m_LastPassword;
			size_t m_RequestCounter;
			Result m_LastResult;
			ID m_LastRequestID;
		private:


		};


		class TestSurveyCore : public ITransportClientRep
		{
		public:
			virtual void OnConnect()
			{
				std::cout << "TestSurveyCore::OnConnect" << std::endl;

			}

			virtual void OnDisconnect()
			{
				std::cout << "OnDisconnect" << std::endl;

			}

			virtual void OnError(const std::string msg)
			{

				std::cout << "TestSurveyCore::OnConnect: " << msg << std::endl;
			}

			virtual void OnLogin(const Result& res)
			{

			}

			virtual void OnCommit(const Result& res, const HashContainer&  info, const Version& CommitId)
			{

			}

			virtual void OnGetRevision(const Result& res, const FileInfoContainer& Info, const Version& CommitId)
			{

			}

			virtual void OnSendFile(const Result& res, const Version& CommitId)
			{

			}

			virtual void OnGetFile(const Result& res, const FileInfo& Info, const Version&  CommitId)
			{

			}

			virtual void OnGetVersionList(const ProjectId& Id, const VersionContainer& VersionList)
			{

			}

			virtual void OnTimeoutEvent(ID eventId)
			{

			}

		};


	}//!namespace transport

}//!namespace survey
