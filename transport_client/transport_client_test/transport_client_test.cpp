// transport_client_test.cpp: ���������� ����� ����� ��� ����������� ����������.
//

//#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include "stdafx.h"

//#include "../../survey_server/server_side.h"
#define BOOST_THREAD_USES_DATETIME

#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp> 

#include "boost/thread/thread.hpp"
#include "boost/thread/xtime.hpp"
#include "tests.h"
#include "net_tests.h"



#include "../../../include/logger.h"

namespace logging = boost::log;


namespace fs = boost::filesystem;

using namespace survey;

using namespace survey::transport;

int _tmain(int argc, _TCHAR* argv[])
{

	ProjectListSerializationTest();

	UserListSerializationTest();

	std::string selfName(argv[0]);

	fs::path p(selfName);
	p = p.branch_path();
	setlocale(LC_ALL, "Russian");
	Logger::Init("test_client");

	src::severity_logger< SeverityLevel > slg;
	BOOST_LOG_SEV(slg, critical) << "Start";

	DownloadNetTest(p);


//	TestClientCore();

//	TestTimer();


//	LoginSerializationTest();

//	LocalNetLoginTest();


	//UploadFileNetTest(p);

	
	std::cin.get();

	
	return 0;
}

