#pragma once
#include "SurveyCore.h"


namespace survey {


	template<class T>
	struct SURVEYCORE_API TreeElement
	{
		TreeElement(T* pElement)
			: m_pElement(pElement)
			, m_pParentTreeElement(NULL)
		{ }

		T*  m_pElement;
		TreeElement<T>*  m_pParentTreeElement;
		std::list<TreeElement<T>*>  m_Elements;
	};


	template<class T>
	class SURVEYCORE_API Tree
	{
	public:

		Tree() 
		{ }

		void Reload(const std::unordered_map<ID, T*>& elements)
		{
			// create tree elements
			std::unordered_map<ID, TreeElement<T>*> treeElements;

			for (auto elPair : elements)
				treeElements.emplace(elPair.first, new TreeElement<T>(elPair.second));

			// sort lists
			for (auto elPair : treeElements)
				;

			// build tree


		}

		void Add(T* pElement)
		{

		}

		void Remove(T* pElement)
		{

		}

		void TopMost(T* pElement)
		{

		}

		void HideMost(T* pElement)
		{

		}

		void Up(T* pElement)
		{

		}

		void Down(T* pElement)
		{

		}


	private:

		void setToList(std::list<TreeElement<T>*>& lst, TreeElement<T>* pElement)
		{
			assert(pElement);

			const ID& lowerID = pElement->m_pElement->LowerID();
			if (!lowerID)
			{
				lst.push_front(pElement);
				return;
			}

			std::list<TreeElement<T>*>::iterator itrtLst;

			for (itrtLst = lst.begin(); itrtLst != lst.end(); itrtLst++)
			{
				TreeElement<T>* pCurrentElem = *itrtLst;

				if (pCurrentElem->m_pElement->GetID() == lowerID)
					break;
			}

			lst.insert(++itrtLst, pElement);
		}


		void sort(std::list<TreeElement<T>*>& lst)
		{

		}


		TreeElement<T>* getElementByID();

		std::list<TreeElement<T>*>  m_TreeElements;
	};

}
