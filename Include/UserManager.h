#pragma once
#include "SurveyCore.h"
#include "SurveyType.h"
#include "SurveyObject.h"


namespace survey {


	enum PermissionToRead
	{
		PermRdLockAll = 0x000,
		PermRdBlockDiagramm = 0x001,
		PermRdBlockLogic = 0x002,
		PermRdBlockDesign = 0x004,
		PermRdBlockToDo = 0x008,
		PermRdUnlockAll = PermRdBlockDiagramm | PermRdBlockLogic | PermRdBlockDesign | PermRdBlockToDo,
	};


	enum PermissionToWrite
	{
		PermWrLockAll = 0x000,
		PermWrBlockDiagramm = 0x001,
		PermWrBlockLogic = 0x002,
		PermWrBlockDesign = 0x004,
		PermWrBlockToDo = 0x008,
		PermWrUnlockAll = PermWrBlockDiagramm | PermWrBlockLogic | PermWrBlockDesign | PermWrBlockToDo,
	};


	class SURVEYCORE_API UserInfo : public ISurveyObject
	{
	public:

		UserInfo()
			: m_Login("vasia")
			, m_Email("vasia@gmail.com")
			, m_FirstName("Vasia")
			, m_LastName("Pupkin")
			, m_Password("1111")
			, m_DefaultPermissionRead(PermissionToRead::PermRdUnlockAll)
			, m_DefaultPermissionWrite(PermissionToWrite::PermWrUnlockAll)
		{ }

		std::string    m_Login;
		std::string    m_UUID;
		std::string    m_Email;
		std::string    m_FirstName;
		std::string    m_LastName;
		std::string    m_Password;
		PermissionToRead  m_DefaultPermissionRead;
		PermissionToWrite m_DefaultPermissionWrite;
		// ...

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("UserInfo"); }
	};

	typedef std::unique_ptr<UserInfo> UserInfoPtr;



	class SURVEYCORE_API UserManager : public Singleton<UserManager>
	{
	public:

		UserManager()
			: m_pCurrentUser(NULL)
		{ }

		UserInfo* CurrentUser() const
		{
			return m_pCurrentUser;
		}

		void ExitCurrentUser();

		bool CreateUser();

		bool EraseUser();

		UserInfo* Login(const std::string& login, const std::string& password)
		{
			assert(!m_pCurrentUser);

			auto itrt = m_Users.find(login);
			if (itrt != m_Users.end())
				return m_pCurrentUser = itrt->second.get();

			for (itrt = m_Users.begin(); itrt != m_Users.end(); itrt++)
			if (itrt->second->m_Email == login)
				return m_pCurrentUser = itrt->second.get();

			return NULL;
		}

		UserInfo* CurrentUser()
		{
			return m_pCurrentUser;
		}

	private:

		UserManager(const UserManager&) { }
		UserManager& operator = (const UserManager&) { return *this; }


		UserInfo*  m_pCurrentUser;
		std::unordered_map<std::string, UserInfoPtr>  m_Users;
	};

}