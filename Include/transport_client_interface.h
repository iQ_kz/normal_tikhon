#pragma once;

#include <vector>
#include <string>

#include "../Include/common_types.h"


#ifdef TRANSPORT_CLIENT_EXPORTS
	#define TRANSPORT_API __declspec(dllexport)
#else
	#define TRANSPORT_API __declspec(dllimport)
#endif

//*# ����� �� ���� ������, ��� �� ������������ ������ ��������� ��������, ����� ���������.

namespace  survey
{
	namespace transport
	{


		struct Hash
		{
			enum{ HashLen = 24 };
			int hash[HashLen];
		};

		typedef std::vector <Hash> HashContainer;


		/*
		*# ���� ���������,  �������� ���������� ��������� � ������.
		*/
		enum  State
		{
			Success = 0,
			UnknownError = 1,
			UnknownLogin = 2, 
			WrongPassword = 3,
			StateCodeCount = 4,
			DBError,
			AlreadyExists,
			CommonServerError,
			ClientFilesyStemError,
			ClientCommonError,
		};

		class CodeInfo
		{
		public:
			static std::string Convert(State Code)
			{
				std::string ret = "Unexpected error code";
				static const char* Msg[] =
				{
					"Success"
					, "Unknown error"
					, "Unknown login"
					, "Wrong password"
				};

				if (Code >= Success && Code < StateCodeCount)
				{
					ret = Msg[Code];
				}
					return ret;
			}
		};

		/*
		*# ����� ������ ��, ��� �������������� ������
		*  TimeMarker - ����� �������� ������.
		*  Id - ��������� ���������� �������������� ������ ��� ���������� ��������.
		*/
		struct Version
		{
			time_t TimeMarker;
			std::string Id;
		};
		typedef std::vector <Version> VersionContainer;


		/*
		*# ������������� �����.
		* InstanceId ������������� ���������� - ��� �����������.
		*  Id  �������������  ����� ��� ����� ������, ������������� ���� � �������.
		*/
		struct FileId
		{
			Hash InstanceId;
			ID 	Id;
		};

		typedef std::vector <FileId> FileIdContainer;

		/*
		*# �������� �������.���� ��������� ��, ��� ��������� ����� � ��� ������������

		*/
		struct  ProjectInfo
		{
			ProjectId Id;
			std::string Name;
			std::string  Autor;
			time_t CreateTime;
			ProjectInfo() :Id(0){}

			ProjectInfo(const ProjectInfo& obj)
			{
				operator = (obj);
			}

			ProjectInfo& operator =(const ProjectInfo& obj)
			{
				Id = obj.Id;
				Name = obj.Name;
				Autor = obj.Autor;
				CreateTime = obj.CreateTime;
				return *this;
			}
			bool operator ==(const ProjectInfo& obj)const
			{
				return Id == obj.Id&&
					Name == obj.Name&&
					Autor == obj.Autor&&
					CreateTime == obj.CreateTime;
			}
			///
		};

		typedef std::vector <ProjectInfo> ProjectInfoV;



		typedef unsigned int UserState;
		typedef unsigned int Role;
		struct UserInfo
		{
			ID Id;
			std::string Name;
			std::string Login;
			std::string Password;
			UserState State;
			UserInfo() :Id(0), State(0){}

			UserInfo(const UserInfo& obj)
			{
				operator = (obj);
			}

			UserInfo& operator =(const UserInfo& obj)
			{
				Id = obj.Id;
				Name = obj.Name;
				Login = obj.Login;
				Password = obj.Password;
				State = obj.State;
				return  *this;
			}

			bool operator ==(const UserInfo& obj)const
			{
				return Id == obj.Id&&
					Name == obj.Name&&
					Login == obj.Login&&
					Password == obj.Password&&
					State == obj.State;
			}
		};


		typedef std::vector <UserInfo> UserInfoV;

		/*
		*# ���������� � �����, ����������� ��� ��� �������� �� ����.
		* Id ������������� ����
		* Body ���������� �����

		*/
		struct  FileInfo
		{
			FileId Id;
			std::string Body;
		};

		typedef std::vector <FileInfo> FileInfoContainer;

		/*
		*# ����������, ���������� ������� �������.
		* InTheCloud ������� ���������� "� ������", �.�. ��������� � ������� ���������

		*/
		struct  CommitInfo
		{
			CommitInfo() :InTheCloud(false){}
			bool InTheCloud;
			std::string Description;
		};


		struct ITransportClientRep;

		/************************************************************************/
		/*
		*# ����������� ����� �������� �������
		* Init  - �������������, �������� �������� ���������� ��������� ������.
		* Connect  - ����������� � �������
		* Login  - �����������
		* Commit  - �������� ������ ��� ���������� �� �������
		* GetRevision - ������ ProjectInfo ������ ������
		* SendFileList  - �������� ����������� ������ � ����� � ������������ ��������
		* GetFile		 - ������ ����������� �����
		* GetVersionList - ������ ������ ������.
		* RegisterTimeoutEvent 	 - �����������  �������
		* SetRequestTimeout ��������� ������� �������� ������ �� ������.

		*/
		struct  ITransportClient
		{
			virtual ~ITransportClient(){}
			//*# ��������� 0 ����� ������������  ����������� ����� RepObj;
			virtual void Init(ITransportClientRep* RepObj, EConnectionType ConnType = eNative) = 0;

			virtual bool IsConnected() = 0;

			virtual bool LoggedOn() = 0;

			virtual bool SetTempDirectory(const std::string& tempDirectory) = 0;


			/*#	 ����������� � �������
			*
			*/
			virtual void Connect(const char* host, long port) = 0;

			//////////////////////////////////////////////////////////////////////////


			enum MethodId
			{
				ePing = 0,
				ePong ,
				eLogin,
				eCreateProject,
				eUploadFile,
				eCommit ,
				eGetRevision ,
				eSendFileList ,
				eGetFile ,
				eGetVersionList ,
				eGetProjectList,
				eGetUserList,
			};
			/*#�����������
			*/
			virtual void Login(const char* login, const char* password) = 0;


			/*
			*# �������� ���������������� �������� �� ����������� �������.
			*  Id ������������� ������������� �������.
			*/
			virtual void OpenProject(const ProjectId& Id) = 0;

			/*
			*# ������ �������� �������, � ������ ������ ������ ��� �������������.
			*  
			*/
			virtual void CreateProject(const std::string& Name) = 0;


			virtual bool UploadFile(const std::string&  hash, const std::string& Name, const boost::filesystem::path&  FilePath, UploadProjID Id){ return false; }

			virtual bool ProjectQueueIsEmpty(GlobalID ProjId) { return false; }

			//////////////////////////////////////////////////////////////////////////

			/*
			*#  �������� ������ ��� ���������� �� �������.
			*  commit  - ��������� ��� �������� �� ������
			*/
			virtual void Commit(const CommitData& commit, Version& versCommit){}

			/*
			*#  ������ ProjectInfo ������ ������
			*  	info - ���������� � ��������� �������� ������ �������
			*   commitInfo - ����������� �������
			*  	CommitId -  ������������� ������� 
			*/
			virtual void GetRevision(const ProjectInfo& info, const CommitInfo& commitInfo, const Version& CommitId){}

			/*
			*#  �������� ����������� ����� �� ������
			*  Id - ������������� ������� 
			*  info  - ������ ����� � ������, ������� ����� ��������� �� ������� .
			*  	CommitId -  ������������� �������, � ������� ��������� �����
			*/
			virtual void SendFileList(const ProjectId& Id, const FileInfoContainer& info, const Version& CommitId) {}


			/*
			*#  ������ ����� 
			*  
			*  	
			*/
			virtual void GetFile(ID ProjId, const std::string& fileHash, const std::string& Name) {}



			virtual void GetProjectList(){}

			virtual void GetUserList(){}


			/*
			*#  ������ ������ ������ �������.
			*  id  - ������������� �������, ������ �������� ���� ��������.
			*  StartTime - ������������� ������. ����� ��������, ��� ������� �  StartTime
			*/
			virtual void GetVersionList(const ProjectId& Id, time_t StartTime) {}


			/*
			*# ���������
			*/
			/////////////////////// ///////////////////////////////////////////////////

			/*
			*#  milliseconds - ������ ��������� ������ � �������������. '0' �������� ���������� �������.
			*  eventId    - ������������� ��������������� �������.
			*/
			virtual void RegisterTimeoutEvent(unsigned int milliseconds, ID eventId) {}
											 

			/*
			*#  SetRequestTimeout ������������� ����� �������� ������ �� ������.
			*	milliseconds - 	 ����� �������� ������ �� ������.
			*  
			*/
			virtual void SetRequestTimeout(unsigned int milliseconds) {}
		};


		/*
		*#  ��������� ������ ������ ������
		*  RetCode  - ��� ��������. ��������� � ���������� ��������, � ������.
		*/
		struct  Result
		{
			Result() :RetCode(Success){}
			State RetCode;
		};

		/*# ����������� ����� �������� �������.
		*	��������� ��������� ��������� ������ ��� ���������� �������.

		*/
		struct  ITransportClientRep
		{
			enum MethodId
			{
				ePing = 0,
				ePong ,
				eOnCreateProject,
				eOnLogin ,
				eOnUploadFile,
				eOnCommit,
				eOnGetRevision,
				eOnSendFile ,
				eOnGetFile,
				eGetVersionList ,
				eOnGetProjectLList,
				eOnGetUserList,


			};

			virtual ~ITransportClientRep(){}

			//////////////////////////////////////////////////////////////////////////

			virtual void OnConnect() = 0;

			virtual void OnDisconnect() = 0;

			virtual void OnError(const std::string msg) = 0;

			//////////////////////////////////////////////////////////////////////////


			virtual void OnLogin(const Result& res) = 0;



			virtual void OnCreateProject(const Result& res, const ProjectId& Id) {}


			virtual void OnUploadFile(const Result& res, ID ProjId, const std::string& fileHash, const std::string& Name) = 0;

			
			virtual void OnGetFile(const Result& res, const std::string&  hash, const std::string& Name, const boost::filesystem::path&  FilePath){}

			virtual void OnGetProjectList(const Result& res, const ProjectInfoV& pr){}

			virtual void OnGetUserList(const Result& res, const UserInfoV& ui){}


			////*# ��� �� ������ �� ����� ������������ �����������, ������� �������� �� ������ ������.

			///*#
			//*  ����� �� ITransportClient::Commit,
			//*  versCommit - ������ �������
			//*  commitConfirm - ������ �������������
			//*/
			//virtual void OnCommit(const Version& versCommit, const CommitConfirm& commitConfirm){}

			///*
			//*#  ������ �� ������� �� ����
			//*  commit - ������ �������
			//*  versCommit - ������ ������� 
			//*  !!! ����� ����������, ��������� ����� ����� ������� ����� ��� ����������
			//*/
			//virtual void Commit(const CommitData& commit, Version& versCommit){}


			////# ����������� � ����������� �������� �����
			///*
			//*  res  -  �������� ��������, ���������� � ����� ��  GetRevision
			//*  Info  - ������ ������ �� ������� ������.
			//*  CommitId -	 ������������� �������, � ������� ������� �������� ������.
			//*/
			//virtual void OnGetRevision(const Result& res, const FileInfoContainer& Info, const Version& CommitId){}


			////# ����������� � ����������� �������� �����, ����� �� ITransportClient::SendFile
			///*
			//*  res  -  �������� ��������, ���������� � ����� ��  SendFileList
			//*  CommitId -	 ������������� �������, � ������� ������� ��������� �������� ������.
			//* 
			//*/
			//virtual void OnSendFile(const Result& res, const Version& CommitId) {}


			///*
			//*#  ����� �� ITransportClient::GetFile
			//*   res  -  ��������� ������� �����,
			//*	Info -	����������� ���������� � �����.
			//*  CommitId -	 ������������� �������, � ������� ������ ���������� ����.
			//*/
			//virtual void OnGetFile(const Result& res, const FileInfo& Info, const Version&  CommitId){}


			/*
			*#  ��������  ������ ������� �� ������� GetVersionList.
			*  id  - ������������� �������, ��� �������� �������� ������.
			*  VersionList - ������ ������.
			*/
			virtual void OnGetVersionList(const ProjectId& Id, const VersionContainer& VersionList){}


			/*
			*# ������, �� ��������� � ������� �� ������.
			*/

			/*
			*#  eventId    - ������������� �������.
			*/
			virtual void OnTimeoutEvent(ID eventId) = 0;

		};


		/*#�� ������ ����� ��������.
		*/
		extern "C" 
		{

			void TRANSPORT_API GetTransport(ITransportClient*& pt);

			void TRANSPORT_API FreeTransport(ITransportClient* pt);

		}

	} //!transport

};	//!survey
