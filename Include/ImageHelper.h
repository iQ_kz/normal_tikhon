#pragma once
#include "SurveyCore.h"


namespace survey {


	enum ImageType
	{
		IT_JPG = 3,
		IT_PNG = 4,
		IT_BMP = 1,
	};

	struct SURVEYCORE_API ImageHelper
	{
		static bool ResizeImage(const boost::filesystem::path& srcImgPath, const boost::filesystem::path& dstImgPath, ImageType imgTp, int width, int height);
		static bool ScaleImage(const boost::filesystem::path& srcImgPath, const boost::filesystem::path& dstImgPath, ImageType imgTp, float scale);
		static bool ConvertToPNG(const std::vector<unsigned char>& inData, std::vector<unsigned char>& outData);
	};

}