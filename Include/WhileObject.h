#pragma once
#include "SurveyObject.h"

#include "ShowObject.h";
#include "HideObject.h";
#include "ValueObject.h";
#include "EmptyObject.h";
#include "EnableObject.h"
#include "DisableObject.h"

namespace survey {
	class SURVEYCORE_API WhileObject : public IXMLObject
	{
	public:

		WhileObject(Document* pDoc, const std::string& condition, const float& value)
			: IXMLObject(pDoc)
			, m_Condition(condition)
			, m_Value(value)
		{}

		WhileObject(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(pTiElement, pDoc)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}


		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);

			// clear data
			m_Condition.clear();
			//m_Value.clear();

			loadElement(pTiElement);
		}

		// =====================================================================================================

		std::string GetCondition() const { return m_Condition; }
		double GetValue() const { return m_Value; }

		void SetCondition(const std::string& str)
		{
			m_Condition = str;
		}
		void SetValue(const double& val)
		{
			m_Value = val;
		}

		// ============================ XML ======================================================================

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = IXMLObject::SaveToXmlElement();
			pXmlelement->SetAttribute("condition", m_Condition);
			pXmlelement->SetAttribute("value", m_Value);

			return pXmlelement;
		}


		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();
		}

		virtual void AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject = NULL) override
		{
			if (pXmlObject->GetType() != ShowObject::Type())
				throw std::exception("Invalid type of the added object");

			IXMLObject::AddChild(pXmlObject, pAfterXmlObject);
		}

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("while"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("while"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			if (const char* pCnd = pTiElement->Attribute("condition"))
				m_Condition = pCnd;

			double value;
			if (pTiElement->QueryDoubleAttribute("value", &value) != TIXML_SUCCESS)
				m_Value = value;
		}

		void afterTreeUpdate()
		{
			// ...
		}

	protected:

		WhileObject(const WhileObject& obj) : IXMLObject(obj) { }
		WhileObject& operator = (const WhileObject& obj) { return *this; }

		// fields

		std::string m_Condition;
		double m_Value;
	};
}