#pragma once
#include "common_types.h"
#include "SurveyCore.h"
#include "NotifycatorSupport.h"
#include "Section.h"


namespace survey {

	// predefinition
	class SURVEYCORE_API Texts;


	class SURVEYCORE_API Text : public IXMLObject, public INotifycatorSupport
	{
	protected:

		Text(ID id, const std::string& text, Document* pDoc);

		Text(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc);

	public:

		void SetText(const std::string& text);

		ID GetID() const { return m_ID; }
		const std::string& GetText() const { return m_Text; }

		// xml support
		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override;

		virtual TiXmlElement* SaveToXmlElement() const override;

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("text"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("text"); }

	private:

		void loadElement(const TiXmlElement* pTiElement);

	protected:

		// fields
		ID  m_ID;
		std::string  m_Text; // utf 8

		// friends
		friend class Texts;
		friend class SurveyType;
	};


	class SURVEYCORE_API Texts : public Section
	{
	public:

		Texts(Document* pDoc);

		Texts(const TiXmlElement* pTiElement, Document* pDoc);

		Text* AddText(ID id, const std::string& txt);

		void RemoveText(Text* pText);

		void RemoveTextRange(const std::list<Text*>& txts);

		std::list<Text*>  GetTexts() const;

		Text* GetTextByID(ID id) const;

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override;

		virtual TiXmlElement* SaveToXmlElement() const override;

		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override;

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("texts"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("texts"); }

	private:

		void updateFromXmlElement(const TiXmlElement* pTiElement);

		void afterTreeUpdate();

	protected:

		Texts(const Texts& obj) : Section(obj) { }
		Texts& operator = (const Texts& obj) { return *this; }

		std::unordered_map<ID, Text*>  m_Texts;
	};

}