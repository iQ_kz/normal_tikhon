#pragma once
#include "SurveyObject.h"

namespace survey {
	class SURVEYCORE_API OnLeaveObject : public IXMLObject
	{
	public:

		// =====================================================================================================
		OnLeaveObject(Document* pDoc, const std::string& condition)
			: IXMLObject(pDoc)
			, m_Condition(condition)
		{ }

		OnLeaveObject(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(pTiElement, pDoc)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}


		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);

			// clear data
			m_Condition.clear();

			loadElement(pTiElement);
		}

		// =====================================================================================================

		std::string GetCondition() const { return m_Condition; }

		void SetCondition(const std::string& str)
		{
			m_Condition = str;
		}

		// ============================ XML ======================================================================

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = IXMLObject::SaveToXmlElement();
			pXmlelement->SetAttribute("if", m_Condition);
			return pXmlelement;
		}


		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();
		}

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("on_leave"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("on_leave"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			if (const char* pVl = pTiElement->Attribute("if"))
				m_Condition = pVl;
		}

		void afterTreeUpdate()
		{
			// ...
		}

	protected:

		OnLeaveObject(const OnLeaveObject& obj) : IXMLObject(obj) { }
		OnLeaveObject& operator = (const OnLeaveObject& obj) { return *this; }

		// fields

		std::string  m_Condition;
	};
}