#pragma once
#include <set>

#include "common_types.h"


namespace survey {


	class ID_Genearator
	{
	public:

		ID_Genearator(ID counter = 1)
			: m_Counter(counter)
		{
			assert(counter);
		}

		ID GetID()
		{
			if (m_DeletedItems.empty())
				return m_Counter++;

			auto it = m_DeletedItems.begin();
			ID res = *it;

			m_DeletedItems.erase(it);
			return res;
		}

		bool RemoveID(ID id)
		{
			if (!id)
				return false;

			if (id >= m_Counter)
				return false;

			auto res = m_DeletedItems.insert(id);
			return res.second;
		}

		void KeepID(ID id)
		{
			if (m_DeletedItems.erase(id))
				return;
			
			if (m_Counter > id)
				return;

			while (m_Counter < id)
				m_DeletedItems.emplace(m_Counter++);

			m_Counter++;
		}

		void Reset()
		{
			m_Counter = 1;
			m_DeletedItems.clear();
		}

		void Reset(const std::set<ID>& ids)
		{
			Reset();

			for (const ID& id : ids)
			{
				while (m_Counter < id)
					m_DeletedItems.emplace(m_Counter++);

				m_Counter++;
			}
		}

	private:

		ID            m_Counter;
		std::set<ID>  m_DeletedItems;
	};

}
