#pragma once
#include "DocumentObject.h"
#include "Page.h"


namespace survey {


	class SURVEYCORE_API Chapter : public DocumentObject
	{
	public:

		Chapter(Document* pDoc, const ColorDat& bsColor, const CoordDat& bsCoord, const std::string& name = "")
			: DocumentObject(pDoc, bsColor, bsCoord, name)
		{ }

		Chapter(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: DocumentObject(pTiElement, childs, pDoc)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}

		// =================== Work with pages ==========================

		std::list<Page*> Pages() const
		{
			return Childs<Page>();
		}

		// ===================  XML ======================================

		virtual void AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject = NULL) override
		{
			if (pXmlObject->GetType() != Page::Type())
				throw std::exception("Invalid type of the added object");

			DocumentObject::AddChild(pXmlObject, pAfterXmlObject);

			// notify
			if (Page* pPage = SurveyType::Cast<Page>(pXmlObject))
			{
				CollectionEventArg evArg("pages", CollectionEventType::CET_PushBack, std::list<IXMLObject*> { pPage });
				InvokeChange(dynamic_cast<ISurveyObject*>(this), &evArg);
			}
		}

		virtual void RemoveChild(IXMLObject* pXmlObject) override
		{
			DocumentObject::RemoveChild(pXmlObject);

			// notify
			if (Page* pPage = SurveyType::Cast<Page>(pXmlObject))
			{
				CollectionEventArg evArg("pages", CollectionEventType::CET_Erase, std::list<IXMLObject*> { pPage });
				InvokeChange(dynamic_cast<ISurveyObject*>(this), &evArg);
			}
		}


		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			DocumentObject::UpdateFromXmlElement(pTiElement);
			loadElement(pTiElement);
		}


		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = DocumentObject::SaveToXmlElement();
			// ....
			return pXmlelement;
		}


		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			DocumentObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();
		}


		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("chapter"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("chapter"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			// ....
		}

		void afterTreeUpdate()
		{
			// ...
		}

	protected:

		Chapter(const Chapter& obj) : DocumentObject(obj) { }
		Chapter& operator = (const Chapter& obj) { return *this; }

		// fields

	};


}