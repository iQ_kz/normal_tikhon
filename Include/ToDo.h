#pragma once
#include "common_types.h"
#include "SurveyCore.h"
#include "Section.h"



namespace survey {

	class SURVEYCORE_API ToDo : public Section
	{
	public:

		ToDo(Document* pDoc)
			: Section(pDoc)
		{ }

		ToDo(const TiXmlElement* pTiElement, Document* pDoc)
			: Section(pTiElement, pDoc)
		{ }

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("toDo"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("toDo"); }

	protected:

		ToDo(const ToDo& obj) : Section(obj) { }
		ToDo& operator = (const ToDo& obj) { return *this; }
	};

}