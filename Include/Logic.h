#pragma once
#include "common_types.h"
#include "SurveyCore.h"
#include "Section.h"
#include "DocumentObject.h"

#include "LogicElement.h"
/*
#include "ShowObject.h"
#include "HideObject.h"
#include "ValueObject.h"
#include "EmptyObject.h"
#include "EnableObject.h"
#include "DisableObject.h"
#include "WhileObject.h"
#include "WhileEnabledObject.h"
#include "WhileDisabledObject.h"
#include "WhileShownObject.h"
#include "WhileHiddenObject.h"
#include "OnChangeObject.h"
#include "OnClickObject.h"
#include "OnEnableObject.h"
#include "OnDisableObject.h"
#include "OnEnterObject.h"
#include "OnLeaveObject.h"
#include "OnShowObject.h"
#include "OnHideObject.h"
*/

namespace survey {

	class SURVEYCORE_API Logic : public Section, public virtual IRangedNotifycatorSupport
	{
	public:

		Logic(Document* pDoc)
			: Section(pDoc)
		{ }

		Logic(const TiXmlElement* pTiElement, Document* pDoc)
			: Section(pTiElement, pDoc)
		{
			updateFromXmlElement(pTiElement);
			afterTreeUpdate();
		}

		LogicElement* AddLogicElement(ID id, const TypesLogicElement& tle)
		{
			LogicElement* pDesignElement = new LogicElement(m_pDocument, id, tle);
			AddChild(pDesignElement);
			return pDesignElement;
		}

		// =================== Work with Logic elements ==========================

		std::list<LogicElement*> LogicElements() const
		{
			return Childs<LogicElement>();
		}

		// =========================== XML =====================================================================

		virtual void AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject = NULL) override
		{
			if (pXmlObject->GetType() != LogicElement::Type())
				throw std::exception("Invalid type of the added object");

			IXMLObject::AddChild(pXmlObject, pAfterXmlObject);
			/*
			DocumentObject* pAddObject = dynamic_cast<DocumentObject*>(pXmlObject);
			if (pAddObject)
			pAddObject->setRangedNotifycators(std::unordered_set<IRangedNotifycatorSupport*> { this });

			// notify
			if (LogicElement* pAddChapter = SurveyType::Cast<LogicElement>(pXmlObject))
			{
			CollectionEventArg evArg("logic_el", CollectionEventType::CET_PushBack, std::list<IXMLObject*> { pAddChapter });
			InvokeChange(dynamic_cast<ISurveyObject*>(this), &evArg);
			}

			notifyAddChild(pXmlObject);
			*/

		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			Section::UpdateFromXmlElement(pTiElement);
			updateFromXmlElement(pTiElement);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = Section::SaveToXmlElement();
			// ...
			return pXmlelement;
		}

		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();

			// notify
			CollectionEventArg evArg("logic_elements", CollectionEventType::CET_Reset);
			InvokeChange(this, &evArg);
		}

		// =========================== Types =====================================================================
		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("logic"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("logic"); }

	private:

		void updateFromXmlElement(const TiXmlElement* pTiElement)
		{

		}

		void afterTreeUpdate();

	protected:

		Logic(const Logic& obj) : Section(obj) { }
		Logic& operator = (const Logic& obj) { return *this; }

		// fields

		// friends
		friend class IDocumentObject;
		friend class DocumentObject;
	};
}