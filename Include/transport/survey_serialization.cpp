#include"stdafx.h"
#include "survey_serialization.h"



namespace survey
{
	namespace transport
	{
		size_t survey::transport::GetSize(ITransportClient::MethodId)
		{
			return 1;
		}

		size_t survey::transport::GetSize(ITransportClientRep::MethodId)
		{
			return 1;
		}

		void Write(ByteBuffer& Buff, const ITransportClient::MethodId v)
		{
			Buff.Append((Byte*)&v, GetSize(v));
		}

		void Write(ByteBuffer& Buff, const ITransportClientRep::MethodId v)
		{
			Buff.Append((Byte*)&v, GetSize(v));
		}

		void Write(ByteBuffer& Buff, const State v)
		{
			Buff.Append((Byte*)&v, GetSize(v));
		}

		void Write(ByteBuffer& Buff, const Result v)
		{
			Write(Buff, v.RetCode);
		}

		void Write(ByteBuffer& Buff, const  ResourceInfo v)
		{
			Write(Buff, v.Hash);
			Write(Buff, v.Name);

		}

	

		void Read(ByteBuffer& Buff, ResourceInfo& v)
		{
			Read(Buff, v.Hash);
			Read(Buff, v.Name);

		}



		void Write(ByteBuffer& Buff, const ResourceData v)
		{
			Write(Buff, v.Resource);
			Write(Buff, v.Id);
		}

		void Read(ByteBuffer& Buff, ITransportClient::MethodId& v)
		{
			Byte b;
			Buff.GetFront((Byte*)&b, GetSize(v));
			v = (ITransportClient::MethodId) b;
		}

		void Read(ByteBuffer& Buff, ITransportClientRep::MethodId& v)
		{
			Byte b;
			Buff.GetFront((Byte*)&b, GetSize(v));
			v = (ITransportClientRep::MethodId) b;
		}

		void Read(ByteBuffer& Buff, State& v)
		{
			Byte b;
			Buff.GetFront((Byte*)&b, GetSize(v));
			v = (State)b;
		}

		void Read(ByteBuffer& Buff, Result& v)
		{
			Read(Buff, v.RetCode);
		}

		void Read(ByteBuffer& Buff, ResourceData& v)
		{
			Read(Buff, v.Resource);
			Read(Buff, v.Id);
		}

		size_t GetSize(State)
		{
			return 1;
		}



		void Write(ByteBuffer& Buff, const ProjectInfo v)
		{
			Write(Buff, v.Autor);
			Write(Buff, v.CreateTime);
			Write(Buff, v.Id);
			Write(Buff, v.Name);
		}

		void Read(ByteBuffer& Buff, ProjectInfo& v)
		{
			Read(Buff, v.Autor);
			Read(Buff, v.CreateTime);
			Read(Buff, v.Id);
			Read(Buff, v.Name);

		}


		void Write(ByteBuffer& Buff, const UserInfo v)
		{
			Write(Buff, v.Id);
			Write(Buff, v.Login);
			Write(Buff, v.Name);
			Write(Buff, v.Password);
			Write(Buff, v.State);
		}

		void Read(ByteBuffer& Buff, UserInfo& v)
		{
			Read(Buff, v.Id);
			Read(Buff, v.Login);
			Read(Buff, v.Name);
			Read(Buff, v.Password);
			Read(Buff, v.State);

		}

	}//!namespace transport

}//!namespace survey
