#pragma once
#include "serialization.h"
#include "transport_types.h"
#include "../transport_client_interface.h"


namespace survey
{
	namespace transport
	{
		size_t GetSize(ITransportClient::MethodId);
		size_t GetSize(ITransportClientRep::MethodId);


		void Write(ByteBuffer& Buff, const  ITransportClient::MethodId v);

		void Read(ByteBuffer& Buff, ITransportClient::MethodId& v);


		void Write(ByteBuffer& Buff, const  ITransportClientRep::MethodId v);

		void Read(ByteBuffer& Buff, ITransportClientRep::MethodId& v);

		size_t GetSize(State);

		void Write(ByteBuffer& Buff, const  State v);

		void Read(ByteBuffer& Buff, State& v);


		void Write(ByteBuffer& Buff, const  Result v);

		void Read(ByteBuffer& Buff, Result& v);

		void Write(ByteBuffer& Buff, const  ResourceInfo v);

		void Read(ByteBuffer& Buff, ResourceInfo& v);


		void Write(ByteBuffer& Buff, const  ResourceData v);

		void Read(ByteBuffer& Buff, ResourceData& v);



		void Write(ByteBuffer& Buff, const ProjectInfo v);

		void Read(ByteBuffer& Buff, ProjectInfo& v);

		void Write(ByteBuffer& Buff, const UserInfo v);

		void Read(ByteBuffer& Buff, UserInfo& v);



		





	}//!namespace transport

}//!namespace survey
