#pragma once
#include "common_types.h"
#include "SurveyCore.h"
#include "Section.h"
#include "DocumentObject.h"
#include "Header.h"
#include "Question.h"
#include "Qa.h"
#include "Block.h"
#include "Page.h"
#include "Chapter.h"


namespace survey {


	class SURVEYCORE_API Project : public Section, public virtual IRangedNotifycatorSupport
	{
	public:

		Project(Document* pDoc)
			: Section(pDoc)
		{ }

		Project(const TiXmlElement* pTiElement, Document* pDoc)
			: Section(pTiElement, pDoc)
		{
			updateFromXmlElement(pTiElement);
			afterTreeUpdate();
		}

		// ================================ Work with chapters ==================================

		std::list<Chapter*> Chapters() const
		{
			return Childs<Chapter>();
		}

		// =================================== XML ===============================================

		virtual void AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject = NULL) override
		{
			if (pXmlObject->GetType() != Chapter::Type())
				throw std::exception("Invalid type of the added object");

			IXMLObject::AddChild(pXmlObject, pAfterXmlObject);

			DocumentObject* pAddObject = dynamic_cast<DocumentObject*>(pXmlObject);
			if (pAddObject)
				pAddObject->setRangedNotifycators(std::unordered_set<IRangedNotifycatorSupport*> { this });

			// notify
			if (Chapter* pAddChapter = SurveyType::Cast<Chapter>(pXmlObject))
			{
				CollectionEventArg evArg("chapter", CollectionEventType::CET_PushBack, std::list<IXMLObject*> { pAddChapter });
				InvokeChange(dynamic_cast<ISurveyObject*>(this), &evArg);
			}

			notifyAddChild(pXmlObject);
		}

		virtual void RemoveChild(IXMLObject* pXmlObject) override
		{
			IXMLObject::RemoveChild(pXmlObject);

			DocumentObject* pDocObject = dynamic_cast<DocumentObject*>(pXmlObject);
			if (pDocObject)
				pDocObject->removeRangedNotifycators(std::unordered_set<IRangedNotifycatorSupport*> { this });

			// notify
			if (Chapter* pRemoveChapter = SurveyType::Cast<Chapter>(pXmlObject))
			{
				CollectionEventArg evArg("chapter", CollectionEventType::CET_Erase, std::list<IXMLObject*> { pRemoveChapter });
				InvokeChange(dynamic_cast<ISurveyObject*>(this), &evArg);
			}

			notifyRemoveChild(pXmlObject);
		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			Section::UpdateFromXmlElement(pTiElement);
			updateFromXmlElement(pTiElement);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = Section::SaveToXmlElement();
			// ...
			return pXmlelement;
		}

		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();

			// notify
			CollectionEventArg evArg("elements", CollectionEventType::CET_Reset);
			InvokeChange(this, &evArg);
		}


		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("project"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("project"); }

	private:

		void updateFromXmlElement(const TiXmlElement* pTiElement)
		{
			//...
		}

		void afterTreeUpdate();

	protected:

		Project(const Project& obj) : Section(obj) { }
		Project& operator = (const Project& obj) { return *this; }

		// fields

		// friends
		friend class IDocumentObject;
		friend class DocumentObject;
	};


}