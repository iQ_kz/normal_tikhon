#pragma once
#include <vector>
#include <iostream>

#include "SurveyCore.h"
#include "FileStream.h"
#include "ImageHelper.h"


namespace survey {

	// predefinitions
	class Section;
	class Document;

	class ProjectInfo;
	class FileManager;



	struct SURVEYCORE_API DocumentSectionInfo
	{
		std::string    m_Hash;
		TiXmlDocument  m_Xml;
	};


	struct SURVEYCORE_API DocumentProperties
	{
		ID  m_ServerID;
		std::string  m_Name;
		std::string  m_Guid;
		std::string  m_CreatedDate;
		std::string  m_ModifyDate;
	};


	typedef std::unordered_map<std::string, DocumentSectionInfo>  SectionsMap;
	typedef std::unordered_map<ID, std::vector<unsigned char>>  PreviewMap;


	struct SURVEYCORE_API PreviewDescription
	{
		std::string  m_Hash;
		std::vector<unsigned char>  m_Data;
	};


	typedef std::unordered_map<ID, PreviewDescription>  HashesPreviewMap;


	inline void PreviewMapToHashesPreviewMap(const PreviewMap& previewMap, const std::unordered_map<ID, std::string> hashes, HashesPreviewMap& hashesPreviewMap)
	{
		for (auto pairPreviewMap : previewMap)
		{
			auto itrtHashes = hashes.find(pairPreviewMap.first);
			if (itrtHashes == hashes.end())
				continue;

			hashesPreviewMap.emplace(itrtHashes->first, PreviewDescription{ itrtHashes->second, std::move(pairPreviewMap.second)});
		}
	}


	inline void HashesPreviewMapToPreviewMap(const HashesPreviewMap& hashesPreviewMap, PreviewMap& previewMap)
	{
		for (auto pairHashesPreviewMap : hashesPreviewMap)
			previewMap.emplace(pairHashesPreviewMap.first, std::move(pairHashesPreviewMap.second.m_Data));
	}


	struct SURVEYCORE_API DocumentInfo
	{
		// Propertyes of document not associated with blocks
		DocumentProperties  m_DocumentProprties;

		// name section -> section info
		SectionsMap  m_Sections;

		// id preview ->  binary data
		PreviewMap  m_Previews;
	};


	struct SURVEYCORE_API TiXmlHelper
	{
		static bool LoadTiXml(const boost::filesystem::path& pathDoc, OUT TiXmlDocument& xmlDoc)
		{
# ifdef BOOST_WINDOWS_API

			FILE* xmlFile = _wfopen(pathDoc.c_str(), L"rb");
			if (!xmlFile)
			{
				// to do log error
				return false;
			}

			return xmlDoc.LoadFile(xmlFile);

# else

			return xmlDoc.LoadFile(pathDoc.c_str(), TIXML_ENCODING_UTF8);

# endif
		}

		static bool SaveTiXml(const boost::filesystem::path& pathDoc, const TiXmlDocument& xmlDoc)
		{
# ifdef BOOST_WINDOWS_API

			FILE* xmlFile = _wfopen(pathDoc.c_str(), L"w");
			if (!xmlFile)
			{
				// to do log error
				return false;
			}

			bool res = xmlDoc.SaveFile(xmlFile);

			fclose(xmlFile);
			return res;

# else 

			return xmlDoc.SaveFile(pathDoc.c_str());

# endif
		}

		static boost::posix_time::ptime GetTime(const TiXmlElement* pElem)
		{
			boost::posix_time::ptime pt;

			try
			{
				const char* pStr = pElem->GetText();
				if (!pStr)
				{
					// to do log error
					throw std::exception("Invalid XML!");
				}

 				std::locale lcl(std::locale::classic(), new boost::posix_time::time_input_facet("%Y-%m-%d %H:%M:%S"));
 				std::stringstream stream(pStr);
 				//stream.imbue(lcl);
				stream >> pt;
			}
			catch (const boost::exception& ex)
			{
				// to do log error
			}
			catch (const std::exception& ex)
			{
				// to do log error
			}
			catch (...)
			{
				// to do log error
			}

			return pt;
		}

		static std::string GetNowTime()
		{
			try
			{
				const boost::posix_time::ptime& timeNow = boost::posix_time::second_clock::universal_time();
				std::locale lcl(std::locale::classic(), new boost::posix_time::time_input_facet("%Y-%m-%d %H:%M:%S"));

				std::stringstream stream;
				stream.imbue(lcl);
				stream << timeNow;

				return stream.str();
			}
			catch (const boost::exception& ex)
			{
				// to do log error
			}
			catch (const std::exception& ex)
			{
				// to do log error
			}
			catch (...)
			{
				// to do log error
			}

			return "time error";
		}

	};


	// ====================================== Backup ==============================================================================================

	struct SURVEYCORE_API BackupProjectData
	{
		TiXmlDocument     m_projectDoc;
		SectionsMap       m_Sections;
		HashesPreviewMap  m_Previews;

		void Clear()
		{
			m_projectDoc.Clear();
			m_Sections.clear();
			m_Previews.clear();
		}
	};

	struct SURVEYCORE_API BackupProjectProperties
	{
		std::string  m_Hash;
		boost::posix_time::ptime  m_CreatedTime;
	};


	class SURVEYCORE_API BackupInfo
	{
	public:

		BackupInfo(ProjectInfo* pProjectInfo);

		~BackupInfo()
		{
			m_pProjectInfo = NULL;

			if (m_BackupStream.is_open())
				m_BackupStream.close();
		}

		bool Update(const BackupProjectData& backupProjectData);

		bool GetNextBackup(BackupProjectData& backupProjectData);

		bool GetPreviosBackup(BackupProjectData& docInfo);

		bool GetAllBackups(std::list<std::pair<std::string, BackupProjectData>>& docBackups);

		bool GetBackup(const std::string& backupHash, BackupProjectData& docBackup);

		bool GetAllBackupDescriptions(std::list<BackupProjectProperties>& descriptions);

		boost::filesystem::path GetBackupDirectory() const;

		const std::string s_BackupDirectory = "backups";
		const std::string s_BeginOfBackup = "<backup>";
		const std::string s_EndOfBackup = "</backup>";

		const char* s_DefBackup = "<backup>\
								  		<hash></hash>\
										<document></document>\
									</backup>";

		static bool BackupProjectDataToDocumentInfo(const BackupProjectData& backupProjectData, DocumentInfo& documentInfo);

	private:

		BackupInfo() { }
		BackupInfo(const BackupInfo& bcpInf) { }
		BackupInfo& operator = (const BackupInfo& bcpInf) { return *this; }

		bool createBackupFile();
		bool setProjectData(const BackupProjectData& backupProjectData);
		bool write(const BackupProjectData& docInfo);
		
		bool getCurrent(BackupProjectData& docInfo);
		bool getPrevios(BackupProjectData& docInfo);
		bool getNext(BackupProjectData& docInfo);

		bool moveToNext();
		bool moveToPrevios();
		
		bool buildBackupProjectData(const TiXmlElement* pDescription, BackupProjectData& docInfo);
		bool getNextBackupDescription(TiXmlElement& description);

		// fields
		ProjectInfo*  m_pProjectInfo;
		boost::filesystem::fstream  m_BackupStream;

		// friends
		friend class ProjectInfo;
	};

	
	class SURVEYCORE_API ProjectInfo
	{
	public:

		ProjectInfo(FileManager* pFileManager, const boost::filesystem::path& pathFile)
			: m_Project(NULL)
			, m_pFileManager(pFileManager)
			, m_ProjectBackup(this)
		{
			assert(pFileManager);

			if (!boost::filesystem::exists(pathFile))
			{
				// to do log error
				throw std::exception("Invalid path!");
			}

			// directory project
			// // directory sources
			// // directory images
			// // backup file
			// // buckup directory
			// // section files

			if (!TiXmlHelper::LoadTiXml(pathFile, m_ProjectDoc))
			{
				// to do log error
				throw std::exception("Invalid file!");
			}

			if (!(m_Project = m_ProjectDoc.FirstChildElement("document")))
			{
				// to do log error
				throw std::exception("Invalid Path XML!");
			}

			// backup
			if (!m_ProjectBackup.createBackupFile())
			{
				// to do log error
				throw std::exception("Fail of create of backup file");
			}
		}

		ProjectInfo(FileManager* pFileManager, const DocumentInfo& blocksInfo);

		// propertyes
		const char* GetID() const
		{
			assert(m_Project);
			return m_Project->Attribute("guid");
		}

		const char* GetCurrentBackupHash() const
		{
			assert(m_Project);
			return m_Project->Attribute("currentBackupHash");
		}

		const char* GetName() const
		{
			TiXmlNode* pNameElem = m_Project->IterateChildren("name", NULL);
			if (!pNameElem)
			{
				// to do log error
				throw std::exception("Invalid XML!");
			}

			TiXmlElement* pElem = pNameElem->ToElement();
			if (!pElem)
			{
				// to do log error
				throw std::exception("Invalid XML!");
			}

			return pElem->GetText();
		}

		ID GetServerID() const
		{
			TiXmlNode* pModify = m_Project->IterateChildren("server_id", NULL);
			if (!pModify)
			{
				// to do log error
				throw std::exception("Invalid XML!");
			}

			TiXmlElement* pElem = pModify->ToElement();
			if (!pElem)
			{
				// to do log error
				throw std::exception("Invalid XML!");
			}

			const char* pStr = pElem->GetText();
			if (!pStr)
			{
				// to do log error
				throw std::exception("Invalid XML!");
			}

			return boost::lexical_cast<ID>(pStr);
		}

		boost::posix_time::ptime GetCreatedDate() const
		{
			TiXmlNode* pCreated = m_Project->IterateChildren("created", NULL);
			if (!pCreated)
			{
				// to do log error
				throw std::exception("Invalid XML!");
			}

			TiXmlElement* pElem = pCreated->ToElement();
			if (!pElem)
			{
				// to do log error
				throw std::exception("Invalid XML!");
			}

			return TiXmlHelper::GetTime(pElem);
		}

		boost::posix_time::ptime GetModifyDate() const
		{
			TiXmlNode* pCreated = m_Project->IterateChildren("modify", NULL);
			if (!pCreated)
			{
				// to do log error
				throw std::exception("Invalid XML!");
			}

			TiXmlElement* pElem = pCreated->ToElement();
			if (!pElem)
			{
				// to do log error
				throw std::exception("Invalid XML!");
			}

			return TiXmlHelper::GetTime(pElem);
		}

		// previews
		bool GetPreview(ID idObject, std::vector<unsigned char>& previewData) const;

		bool GetSmallPreview(ID idObject, std::vector<unsigned char>& previewData) const;

		// 
		unsigned int Edited() const
		{
			return 0;
		}

		bool Status() const
		{
			return false;
		}

		// files
		boost::filesystem::path GetProjectDirectory() const;

		boost::filesystem::path GetPreviewDirectory() const;

		boost::filesystem::path GetProjectPath() const;

		const TiXmlDocument& ProjectDoc() const { return m_ProjectDoc; }

		BackupInfo&  GetBackupInfo() { return m_ProjectBackup; }

		// update from document
		bool SetUpdate(const DocumentInfo& updateInfo);

		// get document data
		bool GetDocInfo(DocumentInfo& updateInfo) const;

		// work with commits
		bool GetCommit(OUT CommitProjectData& commitData) const;
		
		bool SetCommit(const CommitProjectData& commitData, OUT DocumentInfo& projectData);
		
		void ConfirmCommit(const ConfirmInfo& confirmCommit);
		
		bool NeedCommit() const;

		// statics
		static const char* s_ProjectsExtension;

		const int s_SmallPreviewHeight = 128;
		const int s_SmallPreviewWidth = 128;

		const char* s_SourceProjectDirectory = "sources";

		const char* s_NamePreviewsSection = "previews";

		const char* s_NamePreviewNode = "preview";

		const char* s_DefProjectDoc = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\
										<document guid=\"\">\
											<name></name>\
											<server_id></server_id>\
											<created></created>\
											<modify></modify>\
											<server_id>0</server_id>\
											<project></project>\
											<logic></logic>\
											<design></design>\
											<block_sheme_design></block_sheme_design>\
											<toDo></toDo>\
											<roLs></roLs>\
											<images></images>\
											<texts></texts>\
										</document>";


	private:

		ProjectInfo(const ProjectInfo& projInfo) { }
		ProjectInfo& operator = (const ProjectInfo& projInfo) { return *this; }

		std::unordered_map<ID, std::string> savePreviews(const PreviewMap& previews);
		bool updateProperties(const DocumentInfo& updateInfo);

		// fields
		TiXmlDocument  m_ProjectDoc;
		BackupInfo     m_ProjectBackup;

		TiXmlElement*  m_Project;
		FileManager*   m_pFileManager;

		// friends
		friend class BackupInfo;
	};


	class SURVEYCORE_API FileManager
	{
		static std::unique_ptr<FileManager>  s_FileManager;

		FileManager(const boost::filesystem::path& workDirectory, const boost::filesystem::path& tempDir)
			: m_WorkDirectory(workDirectory)
			, m_TempDirectory(tempDir)
		{
			if (!setWorkDirectory(workDirectory))
			{
				// to do log error
				throw std::exception("Cannot create work directory !!!");
			}

			if (!setTempDirectory(tempDir))
			{
				// to do log error
				throw std::exception("Cannot create temp directory !!!");
			}
		}

	public:

		static FileManager* Instance(const boost::filesystem::path& workDirectory, const boost::filesystem::path& tempDir)
		{
			if (!s_FileManager)
				s_FileManager.reset(new FileManager(workDirectory, tempDir));

			return s_FileManager.get();
		}

		static FileManager* GetInstance()
		{
			if (!s_FileManager)
				throw std::exception("Invalid instance");

			return s_FileManager.get();
		}

		static FileManager* TryGetInstance()
		{
			return s_FileManager.get();
		}

		static void Clear()
		{
			s_FileManager.reset(NULL);
		}

		/*
		*  Save document to project info struct and calculate hash
		*  if true then need commit
		*/
		bool GetDocument(const std::string& id, DocumentInfo& saveDocStruct);
		bool SaveDocument(const DocumentInfo& saveDocStruct);

		const boost::filesystem::path& GetWorkDirectory() const 
		{
		#ifdef BOOST_WINDOWS_API

			/*return boost::filesystem::path(std::wstring(L"\\\\?\\") + m_WorkDirectory.c_str());*/
		
			return m_WorkDirectory;

		#else

			return m_WorkDirectory;
		#endif
		}		

		bool SetWorkDirectory(const boost::filesystem::path& workDir)
		{
			m_Projects.clear();
			m_WorkDirectory.clear();

			return setWorkDirectory(workDir);
		}

		// commits
		bool SetCommit(const CommitData& commit);
		bool GetCommit(OUT CommitData& commit);
		bool ConfirmCommit(const CommitConfirm& commitConfirm);

		std::unordered_map<std::string, std::unique_ptr<ProjectInfo>>& GetProjectInfos()
		{
			return m_Projects;
		}

	private:

		FileManager(const FileManager& fileManager) { }

		FileManager& operator = (const FileManager fileManager) { return *this; }

		bool setWorkDirectory(const boost::filesystem::path& workDirectory)
		{
			if (!boost::filesystem::exists(workDirectory))
			{
				// to do creation directory processor
				if (!boost::filesystem::create_directory(workDirectory))
				{
					// to do log error
					return false;
				}

				// ...
				return true;
			}

			//iterate by files
			boost::filesystem::directory_iterator end_itr;
			for (boost::filesystem::directory_iterator itrt(workDirectory); itrt != end_itr; itrt++)
			{
				if (!boost::filesystem::is_regular_file(itrt->status()))
					continue;

				const boost::filesystem::path& extension = itrt->path().extension();
				// const std::string extStr = boost::lexical_cast<std::string>(extension);

				if (extension != boost::filesystem::path(ProjectInfo::s_ProjectsExtension))
					continue;

				try 
				{
					std::unique_ptr<ProjectInfo> projInfo(new ProjectInfo(this, itrt->path()));
					m_Projects.emplace(projInfo->GetID(), std::move(projInfo));
				}
				catch (const std::exception& ex)
				{
					// to do log error
				}
				catch (...)
				{
					// to do log error
				}
			}

			return true;
		}

		bool setTempDirectory(const boost::filesystem::path& workDirectory)
		{
			return true;
		}

		// fields
		// guid -> ProjectInfo*
		std::unordered_map<std::string, std::unique_ptr<ProjectInfo>>  m_Projects;	
		boost::filesystem::path  m_WorkDirectory;
		boost::filesystem::path  m_TempDirectory;
	};

}
