#pragma once
#include "DocumentObject.h"
#include "Header.h"

namespace survey {


	class SURVEYCORE_API Question : public DocumentObject
	{
	public:

		Question(Document* pDoc, const ColorDat& bsColor, const CoordDat& bsCoord, const ColorDat& color, const CoordDat& coord, const std::string& name = "")
			: DocumentObject(pDoc, bsColor, bsCoord, color, coord, name)
		{ }

		Question(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: DocumentObject(pTiElement, childs, pDoc)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}

		// ===================  XML ======================================

		virtual void AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject = NULL) override
		{
			if (pXmlObject->GetType() != Header::Type())
				throw std::exception("Invalid type of the added object");

			DocumentObject::AddChild(pXmlObject, pAfterXmlObject);
		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			DocumentObject::UpdateFromXmlElement(pTiElement);
			loadElement(pTiElement);
		}


		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = DocumentObject::SaveToXmlElement();
			// ....
			return pXmlelement;
		}


		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			DocumentObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();
		}


		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("question"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("question"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			// ....
		}

		void afterTreeUpdate()
		{
			// ...
		}

	protected:

		Question(const Question& obj) : DocumentObject(obj) { }
		Question& operator = (const Question& obj) { return *this; }

		// fields

	};


}