#pragma once
#include "common_types.h"
#include "SurveyCore.h"
#include "Section.h"
#include "UserManager.h"


namespace survey {

	class SURVEYCORE_API RoLs : public Section
	{
	public:

		RoLs(Document* pDoc)
			: Section(pDoc)
		{ }

		RoLs(const TiXmlElement* pTiElement, Document* pDoc)
			: Section(pTiElement, pDoc)
		{ }

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("roLs"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("roLs"); }

	protected:

		RoLs(const RoLs& obj) : Section(obj) { }
		RoLs& operator = (const RoLs& obj) { return *this; }

		PermissionToWrite CurrentUserPermissionWrite() const;
		PermissionToRead CurrentUserPermissionRead() const;

	private:

		std::unordered_map<std::string, PermissionToWrite>  m_WriteRols;
		std::unordered_map<std::string, PermissionToRead>   m_ReadRols;
	};

}
