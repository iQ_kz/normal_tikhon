#pragma once
#include "SurveyCore.h"
#include "transport_client_interface.h"


namespace survey {


	class SURVEYCORE_API TransportClientRep : survey::transport::ITransportClientRep
	{
	public:

		TransportClientRep()
		{ }

		// ITransportClientRep
		virtual void OnConnect() override
		{ }

		virtual void OnDisconnect() override
		{ }

		virtual void OnError(const std::string msg) override
		{ }

		virtual void OnLogin(const survey::transport::Result& res) override
		{ }

		virtual void OnCommit(const survey::transport::Version& versCommit, const CommitConfirm& commitConfirm)
		{ }

		virtual void Commit(const CommitData& commit, survey::transport::Version& versCommit)
		{ }

		virtual void OnGetRevision(const survey::transport::Result& res, const survey::transport::FileInfoContainer& Info, const survey::transport::Version& CommitId)
		{ }

		virtual void OnUploadFile(const survey::transport::Result& res, ID ProjId, const std::string& fileHash, const std::string& Name)
		{ }

		virtual void OnGetFile(const survey::transport::Result& res, const survey::transport::FileInfo& Info, const survey::transport::Version&  CommitId)
		{ }

		virtual void OnTimeoutEvent(ID eventId)
		{ }

		virtual void OnGetVersionList(const ProjectId& Id, const survey::transport::VersionContainer& VersionList)
		{ }

	private:

		
	};


	class SURVEYCORE_API HostManager : public Singleton<HostManager>
	{
		HostManager()
			: m_pTransportClientRep(NULL)
		{
			m_pTransportClientRep = new TransportClientRep();
		}

	public:

		virtual ~HostManager()
		{
			delete m_pTransportClientRep;
			m_pTransportClientRep = NULL;
		}

	private:

		// transport interaction
		TransportClientRep*  m_pTransportClientRep;

		std::string   m_Host;
		int           m_Port;
		unsigned int  m_UpdateTime; // miliseconds
	};


}