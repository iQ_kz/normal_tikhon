#pragma once
#include "SurveyCore.h"
#include "openssl/md5.h"

#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.
#include <boost/algorithm/hex.hpp>



namespace survey {

	class Cryptography
	{
	public:

		static std::string CalculateMd5(const std::string& str)
		{
			std::string res;
			res.resize(MD5_DIGEST_LENGTH, 0);

			MD5((unsigned char*)str.data(), str.length(), (unsigned char*)res.data());
			return res;
		}

		template<class T>
		static std::string CalculateMd5(const std::vector<T>& arr)
		{
			std::string res;
			res.resize(MD5_DIGEST_LENGTH, 0);

			MD5((unsigned char*)arr.data(), arr.size() * sizeof(T), (unsigned char*)res.data());
			return res;
		}

		static std::string CalculateMd5(const TiXmlElement* pXMLDocument)
		{
			if (!pXMLDocument)
				return "";

			TiXmlPrinter printer;
			if (!pXMLDocument->Accept(&printer))
			{
				// to do log error
				return "";
			}

			return CalculateMd5(printer.CStr());
		}

		static std::string CalculateMd5_ByFile(const boost::filesystem::path& pathFile)
		{
			boost::filesystem::ifstream fileStream(pathFile, boost::filesystem::fstream::in | boost::filesystem::fstream::binary);
			if (!fileStream.is_open())
				return "";

			MD5_CTX mdContext;
			MD5_Init(&mdContext);

			fileStream.seekg(0, fileStream.end);
			size_t fileSize = fileStream.tellg();
			fileStream.seekg(0, fileStream.beg);

			if (!fileSize)
				return "";

			const size_t buffSize = 1024;
			char buff[buffSize] = "";
			size_t readSize = 0;

			do
			{
				readSize = buffSize < fileSize ? buffSize : fileSize;

				fileStream.read(buff, readSize);
				MD5_Update(&mdContext, buff, readSize);

				fileSize -= readSize;
			} 
			while (readSize == buffSize && fileSize);

			fileStream.close();

			std::string res;
			res.resize(MD5_DIGEST_LENGTH, 0);

			MD5_Final((unsigned char*)res.data(), &mdContext);
			return res;
		}

		static std::string UUID_Generate()
		{
			boost::uuids::uuid uuid = boost::uuids::random_generator()();
			return boost::lexical_cast<std::string>(uuid);
		}

		static std::string Bin2hex(const std::string& binStr)
		{
			std::string res;
			boost::algorithm::hex(binStr.begin(), binStr.end(), back_inserter(res));
			return res;
		}

		static std::string Hex2Bin(const std::string& hexStr)
		{
			std::string res;
			boost::algorithm::unhex(hexStr.begin(), hexStr.end(), back_inserter(res));
			return res;
		}

	};

}