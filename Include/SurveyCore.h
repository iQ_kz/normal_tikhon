#pragma once
#include <assert.h>
#include <memory>
#include <unordered_set>
#include <unordered_map>
#include <string>
#include <list>
#include <set>
#include <stack>
#include <queue>
#include <functional>
#include <algorithm>


#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>
#include <boost/date_time.hpp>
#include <boost/filesystem/fstream.hpp>


#define TIXML_USE_STL

#include "tinystr.h"
#include "tinyxml.h"

#include "common_types.h"


#pragma warning( disable : 4251)
#pragma warning( disable : 4275)
#pragma warning( disable : 4075)
#pragma warning( disable : 4099)
#pragma warning( disable : 4996)


#ifdef SURVEYCORE_EXPORTS
	#define SURVEYCORE_API __declspec(dllexport)
#else
	#define SURVEYCORE_API __declspec(dllimport)
#endif


#define DEF_TOL 1e-10


namespace survey {


	template<class BuildClass>
	class SURVEYCORE_API Singleton
	{
		static std::unique_ptr<BuildClass>  m_BuildObj;

	public:

		static BuildClass* Instance()
		{
			if (!m_BuildObj)
				m_BuildObj.reset(new BuildClass());

			return m_BuildObj.get();
		}

		static void Clear()
		{
			m_BuildObj.reset(NULL);
		}
	};


}
